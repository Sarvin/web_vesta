<?php
$this->breadcrumbs=array(
	'Cities'=>array('index'),
	$model->name,
);

<h1>View Cities #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'status',
		'sort',
		'create_time',
		'update_time',
	),
)); ?>
