<div class="wrap-cart">
	<div class="fix_width">
		<div class="title">
			<p class="caption"><?=$state==1 ? 'Корзина с покупками' : 'Отложеные покупки' ?></p>

			<?if ($state==1){?>
			<div class="items-count">
				<span>
					Ваши товары в корзине(<span data-positions="2"><?=Yii::app()->cart->getItemsCountWithState(1)?></span>)
				</span>
				<a href="/cart?state=2">в отложеных (<span data-positions="1"><?=$this->getFavoriteCount()?></span>)</a>
			</div>
			<?} else {?>
			<div class="items-count">
				<span>
					Ваши товары в отложеных(<span data-positions="2"><?=$this->getFavoriteCount()?></span>)
				</span>
				<a href="/cart">в корзине (<span data-positions="1"><?=Yii::app()->cart->getCount()?></span>)</a>
			</div>
			<?}?>

			<?if (count($models)){?>

			<div class="data" style="text-align:center">
				<div class="items-table">
					<table>
						<thead>
							<tr>
								<th>Название и фото товара</th>
								<th>Количество</th>
								<th>Размер</th>
								<th>Стоимость</th>
								<th>Сумма</th>
								<th></th>
							</tr>
							<tr>
							</tr>
						</thead>
						<tbody>
							
							<?
								$text = $state == 1  ? 'отложить' : 'в корзину';

								foreach ($models as $key => $data) {

									$this->renderPartial('_item',array('data'=>$data,'text'=>$text,'state'=>$state));
								}
							?>

						</tbody>
					</table>
				</div>

				<div class="total">
					<div class="result">
						<p>Итого 
							<span data-positions="<?=$state?>">
								<?=$state==1 ? Yii::app()->cart->getCount() : $this->getFavoriteCount()?>
							</span> товара на сумму 
							<span class="price" data-cost="<?=$state?>">
								<?=$state==1 ? Yii::app()->cart->getCost() : Items::getFavoriteCost()?>
								<span>
								</span>
							</span></p>
						<a href="/catalog">Продолжить покупку</a>
					</div>
					<div class="book">
						<div class="phone">
							<a href="#call" class="gray-btn modal">
								<span>
									Сделать заказ по телефону
								</span>
							</a>
							<p class="desc">
								* Воспользуйтесь этой услугой чтобы мы пе-<br>резвонили Вам сами!
							</p>
						</div>
						<div class="simple">
							<a href="/book<?=$state==1 ? '' : '?state=2'?>" class="green-btn">
								Оформить заказ
							</a>
							<p class="desc">
								* Оформление заявки займет у <br>Вас не боле 5ти минут.
							</p>
						</div>
					</div>
				</div>
			</div>
			<?}?>
			<div class="empty" style="display:<?=!count($models) ? 'block' : 'none'?>">
				<br><br>
				<?if ($state==1){?>
					Корзина пуста
					<?} else {?>
					Нет отложеных товаров
					<?}?>
				<br><br><br>
				<a href="/catalog">Перейти в каталог штор</a>
			</div>
		</div>
	</div>
</div>