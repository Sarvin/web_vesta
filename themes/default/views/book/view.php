<?php
$this->breadcrumbs=array(
	'Books'=>array('index'),
	$model->id,
);

<h1>View Book #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'phone',
		'email',
		'fio',
		'delivery',
		'address',
		'dt_sold',
		'status',
		'sort',
		'create_time',
		'update_time',
	),
)); ?>
