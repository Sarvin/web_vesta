<div class="wrap-book">
	<div class="fix_width">
		<p class="caption">
			Оформление заказа
		</p>
		<div class="law">
			<a href="/page/law-face"><span>Для юридических лиц</span></a>
		</div>
		<?php $form=$this->beginWidget('CActiveForm', array(
			'id'=>'book-form',
			'action'=>Yii::app()->request->requestUri,
			'enableClientValidation' => true,
            'clientOptions' => array(
                'validateOnType' => false,
                'validateOnSubmit' => true,
            ),
		)); ?>

			<p class="customer">Заказчик</p>
			<div class="fields">
				<input type="hidden" name="id" value="<?=$_GET['id']?>">
				<input type="hidden" name="state" value="<?=$_GET['state']?>">
				<div class="row">
					<?=$form->labelEx($model,'phone')?>
					<span></span>
					<?=$form->textField($model,'phone')?>
					<?=$form->error($model,'phone')?>
				</div>
				<div class="row">
					<?=$form->labelEx($model,'email')?>
					<span></span>
					<?=$form->textField($model,'email')?>
					<?=$form->error($model,'email')?>
				</div>
				<div class="row">
					<?=$form->labelEx($model,'fio')?>
					<span></span>
					<?=$form->textField($model,'fio')?>
					<?=$form->error($model,'fio')?>
				</div>
				<div class="row">
					<?=$form->labelEx($model,'id_city')?>
					<?=$form->dropDownList($model,'id_city',CHtml::listData(Cities::model()->findAll(),'id','name'),array(
							
						)
					)?>
					<?=$form->error($model,'id_city')?>
				</div>
				<div class="row">
					<?=$form->labelEx($model,'delivery')?>
					<?=$form->dropDownList($model,'delivery',Book::getDelivery())?>
					<?=$form->error($model,'delivery')?>
				</div>
			</div>
			<div class="courier" style="text-align:center;">
				<p class="info">
						Заказы в Ваш ород осуществляются через курьерскую службу доставки. Стоимость услуги составляет 300 руб.
					</p>
				<div class="fields">
					<div class="row">
						<?=$form->labelEx($model,'address')?>
						<span></span>
						<?=$form->textField($model,'address')?>
						<?=$form->error($model,'address')?>
					</div>
				</div>
			</div>
			<div class="pickup">
				<p class="customer">Вы так же можете забрать товар самостоятельно из наших магазинов</p>
				<div class="address">

					<?

						foreach ($shops as $key => $shop) {
							?>
								<input type="checkbox" id="shop_<?=$shop->id?>" name="Book[id_shop]" value="<?=$data->id?>">
								<label for="shop_<?=$shop->id?>">
									г.<?=$shop->city?><br> <?=$shop->shop_address?>
									<span>
										Пн-Сб с <?=$shop->time_from?> <br>до <?=$shop->time_to?>
									</span>
								</label>
							<?
						}
					?>
				</div>

			</div>	
			<div class="total">
				<?
					$discount=Yii::app()->request->cookies[md5('bbf_discount')] ? Config::getValue('discount.cost') : 0;
				?>
				<p>Итого к оплате с учетом скидок <?=$discount ? ' ('.$discount.' руб.)' : '';?> и доставки:</p>
				<span class="price">
					<?=($state==1 ? Yii::app()->cart->getItemsCostWithState($state) : Items::getFavoriteCost())-$discount?> руб.
				</span>
			</div>
			<p class="desc">
				самый удобный способпокупать одежду и обувь онлайн.
Доступно для устройств на iOS и Android. Способы оплаты.Вы можете оплатить покупки наличными при получении, либо выбрать другой способ оплаты.
			</p>
			<div class="links">
				<a href="#"><span>Что делать если шторы не подошли</span></a>
				<a href="/catalog"><span>Продолжить покупки</span></a>
			</div>
			<div class="actions">
				<a href="#call" class="gray-btn modal">сделать заказ по телефону</a>
				<input type="submit" class="green-btn" value="оформить заказ">
			</div>
		<?php $this->endWidget(); ?>
		<div class="cart-items">
				<div class="items">
					<p class="info">В заказе: <?=Yii::t('{n} товар|{n} товара|{n} товаров',Yii::app()->cart->getItemsCountWithState($state))?> товара на сумму <?=Yii::app()->cart->getItemsCostWithState($state)?> руб. без учета доставки</p>
					<div class="content">
						<div class="scrollbar" style="height: 400px;">
							<div class="track" style="height: 400px;">
								<div class="thumb" style="top: 0px; height: 228.571428571429px;">
									<div class="end"></div>
								</div>
							</div>
						</div>
						<div class="viewport">
							<div class="overview" style="top: 0px;">
								<div class="param">
									<ol>

										<?	
										if ($state == 1)
										{

											$models=Yii::app()->cart->getPositions();
											foreach ($models as $key => $data) {
												?>
												<li>
													<a href="<?=$data->getUrl()?>"><?=$data->name?></a><br>
													Количество: <?=$data->getQuantity()?> шт.<br>
													Стоимость: <?=$data->priceBySize*$data->getQuantity()?> руб.
												</li>
												<?
											}
										} else {

											$models=Items::getFavorite();

											foreach ($models as $key => $data) {
												?>
												<li>
													<a href="<?=$data->getUrl()?>"><?=$data->name?></a><br>
													Количество: <?= $data->favorite->count?> шт.<br>
													Стоимость: <?=$data->priceBySize * $data->favorite->count?> руб.
												</li>
												<?
											}
										}
										?>

									</ol>
								</div>
							</div>
						</div>
					</div>
					<div style="display:block">
						<div class="into-cart">
							<a href="/cart">
								<span>Вернуться в корзину</span>
							</a>
						</div>
					</div>
				</div>
			</div>
			<div class="clr"></div>
	</div>
</div>
<script type="text/javascript">
$(function(){
	$(".courier").show();
	$(".pickup").hide();
	$('#Book_delivery').on('change',function(){
		if ($(this).val()==2)
			{
				$(".courier").hide();
				$(".pickup").show();
				$('.courier input,.pickup input').removeAttr('checked').val('');
			} else {
				$(".courier").show();
				$(".pickup").hide();
				$('.courier  input,.pickup input').removeAttr('checked').val('');
			}
	})
})
		
</script>