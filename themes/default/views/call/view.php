<?php
$this->breadcrumbs=array(
	'Calls'=>array('index'),
	$model->id,
);

<h1>View Call #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'id_user',
		'phone',
		'status',
		'sort',
		'create_time',
		'update_time',
	),
)); ?>
