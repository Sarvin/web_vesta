<?php
/* @var $this CallController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Calls',
);

$this->menu=array(
	array('label'=>'Create Call', 'url'=>array('create')),
	array('label'=>'Manage Call', 'url'=>array('admin')),
);
?>

<h1>Calls</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
