<?php
$this->breadcrumbs=array(
	'Favorites'=>array('index'),
	$model->id,
);

<h1>View Favorite #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'items',
		'notify_dt',
		'email',
		'status',
		'sort',
		'create_time',
		'update_time',
	),
)); ?>
