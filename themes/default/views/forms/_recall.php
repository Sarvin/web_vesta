<div class="call-us" id="call">
	<input type="checkbox" id="close">
	<label for="close"></label>
	<p class="caption">Мы вам перезвоним!</p>
	<img src="/media/phone-wait.png" alt="">
	<p class="desc">
		Укажите свой контактный телефон , и мы перзвоним Вам в ближайшие несколько минут:
	</p>
	<?php $form=$this->beginWidget('CActiveForm', array(
				'id'=>'question-form',
				'action'=>'/manage/recall',
				'enableClientValidation' => true,
	            'clientOptions' => array(
	                'validateOnType' => false,
	                'validateOnSubmit' => true,
	                'afterValidate'=>'js:function(form,data,hasError){
	                	if (hasError)
	                		return;
	                	$.ajax({
	                		url:"",
	                		data:form.serialize(),
	                		success:function(data){
	                			if (data.success)
	                				window.location="/page/thanks",
	                		},
	                	})
	                }'
	            ),
			)); ?>
		<label for="phone" class="flag">+7</label><input type="text" name="Call[phone]" id="phone">
		<input type="submit" value="жду звонка!" class="green-btn">
	<?$this->endWidget()?>
</div>