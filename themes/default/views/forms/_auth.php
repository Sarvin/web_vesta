<div class="authorization" id="authorization">
	<?php $form=$this->beginWidget('CActiveForm', array(
			'id'=>'auth-form',
			'action'=>Yii::app()->request->requestUri,
			'enableClientValidation' => true,
            'clientOptions' => array(
                'validateOnType' => false,
                'validateOnSubmit' => true,
                'afterValidate' => "js: function(form, data, hasError) {
                    if ( hasError ) return;
                    form = $('#auth-form');
                    var action = '/account/auth';

                    $.ajax({
                        url: action,
                        type: 'POST',
                        dataType: 'json',
                        data: form.serialize(),
                        success: function(data) {
                            if ( data.success ) {
                            	window.location=form.attr('action');
                            }else {
                            	$('.auth-error').show();
                            }

                            return false;
                        },
                        error:function(data){
                        }
                    });
            		return;
                }"
            ),
		)); ?>
		<label class="close"></label>
		<p class="caption">Авторизация</p>
		<div class="auth-error" style="display:none">
			Не верно заданы почта или пароль!
		</div>
		<div class="row">
			<?=$form->labelEx($model,'email')?>
			<span></span>
			<?=$form->textField($model,'email')?>
			<?=$form->error($model,'email')?>
		</div>
		<div class="row">
			<?=$form->labelEx($model,'password')?>
			<span></span>
			<?=$form->passwordField($model,'password')?>
			<?=$form->error($model,'password')?>
		</div>
		<div class="row">
			<input type="submit" class="green-btn">
		</div>
	<?php $this->endWidget(); ?>
</div>
