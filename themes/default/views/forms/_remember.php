<div id="remember">
	<div class="favorites">
		<label class="close" for="favorites"></label>
		<p class="caption">
			Не забудьте товары в избранном
		</p>
		<p class="desc">
			Скорость решения задачи пользователя и общее впечатление от сервиса во многом определяются тем, насколько успешным будет взаимодействие с формами. Вход с паролем, заполнение адреса доставки, платежных данных или анкеты — все это формы, которые требуют особого внимания.
		</p>
		<?php $form=$this->beginWidget('CActiveForm', array(
				'id'=>'remember-form',
				'action'=>'/manage/question',
				'enableClientValidation' => true,
	            'clientOptions' => array(
	                'validateOnType' => false,
	                'validateOnSubmit' => true,
	                'afterValidate' => "js: function(form, data, hasError) {
	                    if ( hasError ) return;
	                    $.ajax({
	                    	type:'POST',
                			url:'/manage/remember',
                			data:form.serialize(),
                			success:function(data){
                				if (data)
                				{
                					$.fancybox.close();
                				}
                					
                			}
                		})
	            		return;
	                }"
	            ),
			)); ?>
			<?$model=new Favorite;?>
			<?=$form->emailField($model,'email')?>
			<?=$form->error($model,'email')?>
			<input type="submit" for="favorites" class="green-btn" value="Напомнить!">
		<?php $this->endWidget(); ?>
	</div>
</div>
