<div id="question" style="display:none;">
		<h1 class="caption">
		   Зайдайте нам интересующий Вас вопрос!
		</h1>
	<?php $form=$this->beginWidget('CActiveForm', array(
				'id'=>'question-form',
				'action'=>'/manage/question',
				'enableClientValidation' => true,
	            'clientOptions' => array(
	                'validateOnType' => false,
	                'validateOnSubmit' => true,
	            ),
			)); ?>
		<div class="fields reg">
			<div class="row">
				<?=$form->labelEx($model,'email')?>
				<span></span>
				<?=$form->textField($model,'email')?>
				<?=$form->error($model,'email')?>
			</div>
			<div class="row">
				<?=$form->labelEx($model,'comment')?>
				<span></span>
				<?=$form->textarea($model,'comment',array('rows'=>4,'width'=>'90%'))?>
				<?=$form->error($model,'phone')?>
			</div>
			<div class="row center">
				<input type="submit" class="green-btn center" value="Зарегистрироваться">
			</div>
		</div>
	<?php $this->endWidget(); ?>
	</div>
</div>