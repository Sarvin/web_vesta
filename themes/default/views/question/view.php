<?php
$this->breadcrumbs=array(
	'Questions'=>array('index'),
	$model->id,
);

<h1>View Question #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'id_user',
		'comment',
		'status',
		'sort',
		'create_time',
		'update_time',
	),
)); ?>
