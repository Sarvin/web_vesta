<?php
$this->breadcrumbs=array(
	'Requests'=>array('index'),
	$model->id,
);

<h1>View Request #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'id_user',
		'state',
		'post_request_id',
		'post_id',
		'status',
		'sort',
		'create_time',
		'update_time',
	),
)); ?>
