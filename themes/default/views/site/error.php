<header class="menu_404 fix_width">
	<ul>
		<li>
			<a href="#">О компании</a>
		</li>
		<li>
			<a href="#">Публичная оферта</a>
		</li>
		<li>
			<a href="#">Как совершить покупку?</a>
		</li>
		<li>
			<a href="#">Доставка</a>
		</li>
		<li>
			<a href="#">Возврат</a>
		</li>
		<li>
			<div class="social-small">
				<a class="fb" href="<?=Config::getValue('social.fb')?>"></a>
				<a class="vk" href="<?=Config::getValue('social.vk')?>"></a>
				<a class="tw" href="<?=Config::getValue('social.tw')?>"></a>
			</div>
		</li>
	</ul>
	<div class="social">
	</div>
</header>
<div class="wrap-404">
	<div class="fix_width">
		<p class="caption">Ошибка <strong><?=$code?></strong></p>
		<?if ($code==404){?>
		<p class="desc">Такое бывает, не расстраивайтесь! Предлагаем Вам<br> перейти на следующие страницы:</p>
		<?} else {?>
		<p class="desc"><?=$message?></p>
		<?}?>
		<div class="pages">
			<a href="/">На главную</a>
			<a href="/catalog">В каталог</a>
			<?if (Yii::app()->user->id && Yii::app()->user->isAdmin==false){?>
				<a href="/account">В личный кабинет</a>
			<?}?>
		</div>
		<img src="<?=$this->getAssetsUrl();?>/img/thinknes.png">
	</div>
</div>