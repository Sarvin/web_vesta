<?if ($showDiscount){?>
<script type="text/javascript">
$(function(){
	$.fancybox.open([
		{
			href:'#discount',
			padding:0,
			closeBtn:false,
		}
	])
})
</script>
<div id="discount" class="call-us" style="discouny:none;">
	<input type="checkbox" id="close">
	<label for="close"></label>
	<p class="caption">Скида!</p>
	<img src="/media/discount.png" alt="">
	<p class="desc" style="text-align:center">
		Вам доступна скинда при покупке от <?=Config::getValue('discount.sill');?> на сумму <?=Config::getValue('discount.cost')?><br>
		<a href="/catalog" style="margin-top:20px;display:inline-block">Перейти в каталог</a>
	</p>
</div>
<?}?>

<div class="wrap-price-list">
	<div class="fix_width">
		<div class="links">
			<ul>
				<?
					foreach ($catalogs as $key => $data){
						?>
							<li>
								<a href="/catalog?Filter[catalogs][]=<?=$data->id?>">
									<img src="<?=$data->getImageUrl('main')?>" alt="">
									<p><?=$data->name?></p>
								</a>
							</li>
						<?
					}
				?>
			</ul>
		</div>
		<div class="slider">
			<?

				foreach ($slider as $key => $data) {
					?>
					<div class="item">
						<a href="<?=$data->getUrl()?>">
							<img  width="500" height="330" src="<?=$data->firstImage ? $data->firstImage->getUrl('slider') : '/media/slider.png'?>" alt="" title="<?=$data->name?>"/>
						</a>
					</div>
					<?

				}

			?>
		</div>	
		<div class="clr"></div>
		<div class="sales">
			<div class="item">
				<p class="caption">
					Распродажа готовых штор
				</p>
				<p class="desc">
					Плотные ткани из коллекции Anabel представлены для вас, чтобы найти и удовлетворить свой...
				</p>
				<a href="#" class="more">
					<span>Смотреть</span>
				</a>
			</div>
			<div class="item">
				<p class="caption">
					Распродажа готовых штор
				</p>
				<p class="desc">
					Плотные ткани из коллекции Anabel представлены для вас, чтобы найти и удовлетворить свой...
				</p>
				<a href="#" class="more">
					<span>Смотреть</span>
				</a>
			</div>
			<div class="item">
				<p class="caption">
					Распродажа готовых штор
				</p>
				<p class="desc">
					Плотные ткани из коллекции Anabel представлены для вас, чтобы найти и удовлетворить свой...
				</p>
				<a href="#" class="more">
					<span>Смотреть</span>
				</a>
			</div>
		</div>
	</div>
</div>
<?if ($best){?>
<div class="wrap-deals">
	<div class="fix_width">
		<p class="caption">Лучшие предложения</p>
		<?

			$this->renderPartial('//items/_items',array('models'=>$best));
			
		?>
	</div>
</div>
<?}?>
<div class="wrap-about">
	<p class="caption"><strong>vesta decor</strong> это:</p>
	<div class="items">
		<div class="fix_width">
			<div class="item">
				<div class="info">
					<div class="not-hover">
						<img src="/media/man.png">
						<p class="title">
							Доставка по всей России
						</p>
					</div>
					<div class="info-hover">
						Аппартаменты находятся на острове Паг, населенный пункт Шимуны, недалеко от...
города Паг. Здание построено в 2012 г...
Парковочное место на две машины, подсобное помещение в подвале. 
					</div>
				</div>
			</div>
			<div class="item">
				<div class="info">
					<div class="not-hover">
						<img src="/media/warranty.png">
						<p class="title">
							Доставка по всей России
						</p>
					</div>
					<div class="info-hover">
						Аппартаменты находятся на острове Паг, населенный пункт Шимуны, недалеко от...
города Паг. Здание построено в 2012 г...
Парковочное место на две машины, подсобное помещение в подвале. 
					</div>
				</div>
			</div>
			<div class="item">
				<div class="info">
					<div class="not-hover">
						<img src="/media/present-main.png">
						<p class="title">
							Доставка по всей России
						</p>
					</div>
					<div class="info-hover">
						Аппартаменты находятся на острове Паг, населенный пункт Шимуны, недалеко от...
города Паг. Здание построено в 2012 г...
Парковочное место на две машины, подсобное помещение в подвале. 
					</div>
				</div>
			</div>
		</div>
	</div>
</div>