<?if (!Yii::app()->user->isGuest){?>
<div class="user-info">
	<input type="checkbox" id="name">
	<div class="cover"></div>
	<div class="content">
		<div class="favorite">
			<a href="/cart?state=2">
				<span>Мое избранное</span><span class="slesh">|</span>
			</a>
		</div>
		<div class="auth_as">
			<span class="as"><strong>Вы вошли</strong> как</span>	
			<label for="name" class="name"><span><?=Yii::app()->user->name?></span></label>
			<div class="triangle"></div>
			<ul class="dropDown">
				<li>
					<span>Привет, <?=Yii::app()->user->name?>!!</span>
				</li>
				<li>
					<a href="/account?tab=2">Мои заказы</a>
				</li>
				<li>
					<a href="/account">Мои Данные</a>
				</li>
			</ul>
			<?
				
			?>
			<a href="/account/logout?url=<?=Yii::app()->request->requestUri?>" class="logout"><span>Выйти</span></a>
		</div>
	</div>
</div>
<?} else {?>
<div class="auth_reg">
	<a href="#authorization" class="auth modal"><span>Вход</span></a>
	<span class="slash"></span>
	<a href="/account/registration" class="reg"><span>Регистрация</span></a>
</div>
<?}?>