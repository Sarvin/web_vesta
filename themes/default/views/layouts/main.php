<?php

	$cs = Yii::app()->clientScript;
	
	$assetsUrl=$this->getAssetsUrl();
	$cs->registerCssFile($assetsUrl.'/css/style.css');
	$cs->registerCssFile($assetsUrl.'/css/reset.css');
	$cs->registerCssFile($assetsUrl.'/css/main.css');
	$cs->registerCssFile($assetsUrl.'/css/jquery.autocomplete.css');
	$cs->registerCssFile($assetsUrl.'/css/fancybox/jquery.fancybox.css');
	$cs->registerCssFile($assetsUrl.'/js/fancybox/helpers/jquery.fancybox-thumbs.css');
	$cs->registerCssFile($assetsUrl.'/css/owl.carousel.css');
	$cs->registerCssFile($assetsUrl.'/css/tool-tip.css');
	$cs->registerCssFile($assetsUrl.'/css/jquery.selectBox.css');
	$cs->registerCssFile($assetsUrl.'/css/jquery.ui/overcast/jquery-ui-1.10.3.custom.min.css');
	//$cs->registerCssFile($this->getAssetsUrl().'/css/fancybox/jquery.fancybox-buttons.css');
	
	$cs->registerCoreScript('jquery');
	$cs->registerCoreScript('jquery.ui');
	$cs->registerScriptFile($assetsUrl.'/js/jquery.autocomplete.js', CClientScript::POS_END);
	$cs->registerScriptFile($assetsUrl.'/js/tool-tip.js', CClientScript::POS_END);
	$cs->registerScriptFile($assetsUrl.'/js/jquery.selectBox.js', CClientScript::POS_END);
	$cs->registerScriptFile($assetsUrl.'/js/lib/jquery.fancybox.js', CClientScript::POS_END);
	$cs->registerScriptFile($assetsUrl.'/js/fancybox/helpers/jquery.fancybox-thumbs.js', CClientScript::POS_END);
	$cs->registerScriptFile($assetsUrl.'/js/owl.carousel.js', CClientScript::POS_END);
	$cs->registerScriptFile($assetsUrl.'/js/jquery.tinyscrollbar.js', CClientScript::POS_END);
	$cs->registerScriptFile($assetsUrl.'/js/script.js', CClientScript::POS_END);
	//$cs->registerScriptFile('http://api-maps.yandex.ru/2.0.27/?load=package.standard&lang=ru-RU', CClientScript::POS_HEAD);
	
?><!DOCTYPE html>
<html lang="ru">
	<head>
		<meta charset="utf-8" />
		<title><?php echo $this->title; ?></title>
		<!--[if IE]>
	    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	    <![endif]-->
	</head>
	<body>
		<?

			$this->renderPartial('//forms/_auth',array('model'=>new AuthForm));

		?>
		<?
			$this->renderPartial('//forms/_question',array('model'=>new Question));

		?>
		<?
			$this->renderPartial('//layouts/_cities');

		?>
		<?
			$this->renderPartial('//forms/_recall');

		?>
		<?
			$this->renderPartial('//forms/_remember');

		?>
		<header>
			<div class="wrap-loc">
				<div class="fix_width">
					<div class="location">
						<span class="first">Ваше местонахождение:</span>
						<label for=""><span>Тюмень</span></label>
					</div>
					<div class="hot-line">
						<span class="caption">Телефон горячей линии Vesta Decor:</span>
						<a href="#call" class="modal"><span class="phone">8 800 200 00</span></a>
					</div>

				</div>
			</div>
			<div class="clr"></div>
			<div class="wrap-info">
				<div class="fix_width">
					<div class="logo">
						<a href="/" class="logo">
							<img src="<?=$assetsUrl?>/img/head-logo.png" />
						</a>
					</div>
					<div class="shop-info">
						<p>Интернет-магазин готовых штор <strong>Vesta Decor</strong></p>
						<p class="delivery">С доставкой по России</p>
					</div>
					<?
						$this->renderPartial('//layouts/_regAuth');

					?>
					<div class="clr"></div>
				</div>
			</div>
			<div class="clr"></div>
			<div class="wrap-search">
				<div class="fix_width">
					<div class="criteria">
						<input type="checkbox" id="all">
						<label for="all"><span>Все шторы<span></label>
						<!-- <a href="#" class="choice"><span>Все шторы</span></a> -->
						<div class="popup">
							<div class="arrow-top"></div>
							<?
								$cat=Catalog::getMenu();
								$filter=new Filter;
								if ($cat){
							?>
								<div class="item">
									<p >Шторы по значению</p>
									<ul>
										<?
											
											foreach ($cat as $key => $data) {
												?>
													<li><a href="/catalog?Filter[catalogs][]=<?=$data->id?>"><?=$data->name?></a></li>
												<?

											}
										?>
									</ul>
								</div>
							<?}?>
							<?
								if ($data=$filter->findColors()){
							?>
							<div class="item">
								<p >Шторы по цвету</p>
								<ul class="colors">
									<?
										foreach ($data as $key => $cl) {

											?>
												<li><a href="/catalog?Filter[colors][]=<?=$cl->key?>"><span style="background:#<?=$cl->key?>"></span><span><?=$cl->value?></span></a></li>
											<?
										}
									?>
								</ul>
							</div>
							<?}?>
							<?if ($data=$filter->findStyles()){?>
								<div class="item">
									<p >По стилю</p>
									<ul>
										<?
											foreach ($data as $key => $cl) {

												?>
													<li><a href="/catalog?Filter[images][]=<?=$cl->key?>"><span></span><span><?=$cl->value?></span></a></li>
												<?
											}
										?>
									</ul>
								</div>
							<?}?>
							<?if ($data=$filter->findImages()){?>
								<div class="item">
									<p >Рисунок на ткани</p>
									<ul>
										<?
											foreach ($data as $key => $cl) {

												?>
													<li><a href="/catalog?Filter[images][]=<?=$cl->key?>"><span></span><span><?=$cl->value?></span></a></li>
												<?
											}
										?>
									</ul>
								</div>
							<?}?>
						</div>
						<label for="all" class="cover">
						</label>
					</div>
					<form class="search-query" action="/search">
						<input type="text" id="search" placeholder="Че кого че ищем ?" value="<?=$_GET['Filter']['term']?>"/>
						<input type="submit" value=""/>
						<div class="example">
							<a href="#">Например: <span>шторы для гостинной</span></a>
						</div>
					</form>
					<div class="cart">
						<a href="/cart">Корзина<span class="a-ico"></span><span class="count"><?=Yii::app()->cart->getCount()?></span></a>
						<?
							$this->renderPartial('//items/_cartItems');
						?>
					</div>
					<div class="cover"></div>
				</div>
			</div>
				<?

					$this->widget('zii.widgets.CBreadcrumbs', array(
					    'links'=>$this->breadcrumbs,
					    'separator'=>' / ',
					    'htmlOptions'=>
					    array('class'=>'breadcrumbs fix_width')
					));
				?>
		</header>

		<div id="layout">
			<?=$content;?>
		</div>
		<div class="wrap-menu">
			<div class="fix_width">
				<ul>
					<?
						if ($this->menu['footer'])
							foreach ($this->menu['footer']->item_list as $key => $item) {
								?>
								<li>
									<a href="<?=$item->getUrl()?>"><?=$item->title?></a>
								</li>
								<?
							}
					?>
					<!-- <li>
						<a href="">О компании</a>
					</li>
					<li>
						<a href="">Публичная оферта</a>
					</li>
					<li>
						<a href="">Как совершить покупку?</a>
					</li>
					<li>
						<a href="">Доставка</a>
					</li>
					<li>
						<a href="">Возврат</a>
					</li> -->
				</ul>
			</div>
		</div>
		<div class="page-footer">
		<footer>
			<div class="fix_width">
				<div class="top">
					<div class="hot-line">
						<p>
							Телефон горячей линии Vesta Decor: 
						</p>
						<p class="phone">
							<a style="color:#fff;text-decoration:none" href="tel:8 800 200 00 00">8 800 200 00 00</a>
						</p>
						<p>
							* Для регионов звонок бесплатный
						</p>
					</div>
					<div class="question-info">
						<p>
							Мы всегда открыты для вопросов и предложений клиентов!
						</p>
						<a href="#question" class="green-btn modal">Задать вопрос</a>
					</div>
					<div class="pay-ways">
						<p>
							Способы оплаты
						</p>
						<p class="desc">
							Вы можете оплатить покупки наличными 
							при получении, либо выбрать другой 
							способ оплаты.
						</p>
						<div class="pay-links">
							<a href="#">
								<img src="<?=$assetsUrl?>/img/pay1.png" alt=""/>
							</a>
							<a href="#">
								<img src="<?=$assetsUrl?>/img/pay2.png" alt=""/>
							</a>
							<a href="#">
								<img src="<?=$assetsUrl?>/img/pay2.png" alt=""/>
							</a>
							<a href="#">
								<img src="<?=$assetsUrl?>/img/pay4.png" alt=""/>
							</a>
							<a href="#">
								<img src="<?=$assetsUrl?>/img/pay5.png" alt=""/>
							</a>
						</div>
					</div>
					<div class="clr"></div>
				</div>
				
				<div class="bot">
					<div class="company">
						* Vesta Deco 2000 - <?=date('Y')?>
					</div>
					<div class="developer">
						Разработка сайта: <a href="amobile-studio.ru"><img src="<?=$assetsUrl?>/img/developer.png"></a>
					</div>
				</div>
			</div>
		</footer>
	</div>
	</body>
</html>