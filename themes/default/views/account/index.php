<div class="wrap-lk">
	<div class="fix_width">
		<p class="caption">
			Личный кабинет
		</p>
		<div class="bloks">
			<div class="pages">
				<a href="/account?tab=2"><span>Мои заказы</span></a>
				<a href="/account"><span>Мои данные</span></a>
			</div>
			<?if ($tab==1){?>
			<div class="info">
				<?php $form=$this->beginWidget('CActiveForm', array(
					'id'=>'client-form',
					'action'=>'/account',
					'enableClientValidation' => true,
		            'clientOptions' => array(
		                'validateOnType' => false,
		                'validateOnSubmit' => true,
		            ),
				)); ?>
					<div class="left">
						<div class="row">
							<?=$form->labelEx($model,'phone')?>
							<?=$form->textField($model,'phone')?>
							<?=$form->error($model,'phone')?>
						</div>
						<div class="row">
							<?=$form->labelEx($model,'email')?>
							<?=$form->textField($model,'email')?>
							<?=$form->error($model,'email')?>
						</div>
						<div class="row">
							<?=$form->labelEx($model,'password')?>
							<?=$form->passwordField($model,'password')?>
							<?=$form->error($model,'password')?>
						</div>
						<div class="row gender">
							<label>
								Пол
							</label>
							<input type="radio" <?=$model->gender==1 ? "checked" : ""?> id="male" name="Client[gender]" value="1">
							<label for="male">
								Мужской
							</label>
							<input type="radio" <?=$model->gender==2 ? "checked" : ""?> id="female" name="Client[gender]" value="2">
							<label for="female">
								Женский
							</label>
						</div>
					</div>
					<div class="right">
						<div class="row">
							<?=$form->labelEx($model,'fio')?>
							<?=$form->textField($model,'fio')?>
							<?=$form->error($model,'fio')?>
						</div>
						<div class="row">
							<?=$form->labelEx($model,'id_city')?>
							<?=$form->dropDownList($model,'id_city',CHtml::listData(Cities::model()->findAll(),'id','name'));?>
							<?=$form->error($model,'id_city')?>
						</div>
						<div class="row changepwd">
							<a href="/account/changepwd">Сменить пароль</a>
						</div>
					</div>
					<div class="submit">
						<input type="submit" value="Сохранить" class="green-btn">
					</div>
				<?php $this->endWidget(); ?>
			</div>
			<?} else {?>
			<div class="info">
				<div class="orders">
							<?
							$cs=Yii::app()->clientScript;
							$cs->scriptMap = array(
							    'pager.css' => false,
							    'styles.css'=>false,
							);
							$this->widget('zii.widgets.grid.CGridView', array(
							    'dataProvider'=>$dataProvider,
							    'template'=>'{items}{pager}',
							    'columns'=>array(
							        array(
							        	'header'=>'Н/пп',
							        	'type'=>'raw',
							        	'value'=>'$row+1'
							        ),         // display the 'title' attribute
							        array(
							        	'header'=>'Дата заказа',
							        	'type'=>'raw',
							        	'value'=>'CHtml::link(date("d.m-Y",strtotime($data->create_time)),"/account/bookView/".$data->id)'
							        ),
							        array(
							        	'header'=>'Способ приобретения',
							        	'type'=>'raw',
							        	'value'=>'Book::getDelivery($data->delivery)',
							        ),
							        array(
							        	'header'=>'Статус заказа',
							        	'type'=>'raw',
							        	'value'=>'Book::getStatusAliases($data->status)',
							        ),
							    ),
							    'pager'=>array(
								    	'header'=>false,
								    	'nextPageLabel'=>'Следующая',
								    	'lastPageLabel'=>'Последняя',
								    	'firstPageLabel'=>'Первая',
								    	'prevPageLabel'=>'Первая'
								    )
							));
							?>
				</div>
			</div>
			<?}?>
		</div>
	</div>
</div>
