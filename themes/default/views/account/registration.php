<div class="wrap-book reg">
	<div class="fix_width">
		<h1 class="caption">
		   Регистрация
		</h1>
	<?php $form=$this->beginWidget('CActiveForm', array(
				'id'=>'registration-form',
				'action'=>'/account/registration',
				'enableClientValidation' => true,
	            'clientOptions' => array(
	                'validateOnType' => false,
	                'validateOnSubmit' => true,
	            ),
				'htmlOptions' => array('enctype'=>'multipart/form-data'),
			)); ?>
		<div class="fields reg">
			<div class="row">
				<?=$form->labelEx($model,'fio')?>
				<span></span>
				<?=$form->textField($model,'fio')?>
				<?=$form->error($model,'fio')?>
			</div>
			<div class="row">
				<?=$form->labelEx($model,'phone')?>
				<span></span>
				<?=$form->textField($model,'phone')?>
				<?=$form->error($model,'phone')?>
			</div>
			<div class="row">
				<?=$form->labelEx($model,'email')?>
				<?=$form->textField($model,'email')?>
				<?=$form->error($model,'email')?>
			</div>
			<div class="row">
				<?=$form->labelEx($model,'id_city')?>
				<?=$form->dropDownList($model,'id_city',CHtml::listData(Cities::model()->published()->findAll(),'id','name'))?>
				<?=$form->error($model,'id_city')?>
			</div>
			<div class="row gender">
				<label>
					Пол
				</label>
				<input type="radio" id="male" name="gender">
				<label for="male">
					Мужской
				</label>
				<input type="radio" id="female" name="gender">
				<label for="female">
					Женский
				</label>
			</div>
			<div class="row">
				<?=$form->labelEx($model,'password')?>
				<?=$form->passwordField($model,'password')?>
				<?=$form->error($model,'password')?>
			</div>
			<div class="row">
				<?=$form->labelEx($model,'verifyPassword')?>
				<?=$form->passwordField($model,'verifyPassword')?>
				<?=$form->error($model,'verifyPassword')?>
			</div>
			<div class="row center">
				<label></label>
				<input style="width:226px;" type="submit" class="green-btn center" value="Зарегистрироваться">
			</div>
		</div>
	<?php $this->endWidget(); ?>
	</div>
</div>