<div class="wrap-lk">
	<div class="fix_width">
		<h1 class="caption">Заказ от <?=date('d.m.y',strtotime($model->create_time))?></h1>
		<div class="orders">
			<table style="width:100%;">
				<thead>
					<tr>
						<th>
							Фото
						</th>
						<th>Наименование</th>
						<th>Количество</th>
						<th>Цена</th>
						<th>Стоимость</th>
					</tr>
				</thead>
				<tbody>
				<?
					foreach ($model->items as $key => $data) {
						?>
							<tr>
								<td>
									<a href="/items/<?=$data->item->alias?>"><img src="<?=$data->item->firstImage->getUrl('small')?>"></a>
								</td>
								<td><?=$data->item->name?></td>
								<td><?=$data->count?></td>
								<td><?=$data->item->price?></td>
								<td><?=$data->count*$data->item->price?></td>
							</tr>
						<?
					}
				?>
				</tbody>
			</table>
		</div>
		<br>
		<br>
	</div>
</div>