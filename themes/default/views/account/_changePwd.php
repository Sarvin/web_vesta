<div class="wrap-book reg">
	<div class="fix_width">
		<h1 class="caption">
		   Регистрация
		</h1>
	<?php $form=$this->beginWidget('CActiveForm', array(
				'id'=>'registration-form',
				'action'=>'/account/changePwd',
				'enableClientValidation' => true,
	            'clientOptions' => array(
	                'validateOnType' => false,
	                'validateOnSubmit' => true,
	            ),
				'htmlOptions' => array('enctype'=>'multipart/form-data'),
			)); ?>
		<div class="fields reg">
			<div class="row">
				<?=$form->labelEx($model,'oldPassword')?>
				<?=$form->passwordField($model,'oldPassword')?>
				<?=$form->error($model,'oldPassword')?>
			</div>
			<div class="row">
				<?=$form->labelEx($model,'newPassword')?>
				<?=$form->passwordField($model,'newPassword')?>
				<?=$form->error($model,'newPassword')?>
			</div>
			<div class="row">
				<?=$form->labelEx($model,'repeatNewPassword')?>
				<?=$form->passwordField($model,'repeatNewPassword')?>
				<?=$form->error($model,'repeatNewPassword')?>
			</div>
			<div class="row center">
				<input type="submit" class="green-btn center" value="Зарегистрироваться">
			</div>
		</div>
	<?php $this->endWidget(); ?>
	</div>
</div>