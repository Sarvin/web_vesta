<div class="wrap-thanks">
	<div class="fix_width">
		<p class="caption">
			<strong>Спасибо</strong> за Ваш заказ!
		</p>
		<p class="desc">
			Самый удобный способ покупать одежду и обувь онлайн. Доступно для устройств на iOS и Android. Способы оплаты.Вы можете оплатить покупки наличными при получении, либо выбрать другой способ оплаты.
		</p>
		<p class="title">
			Это купон со скидкойдля твоих друзей на покупку в нашем магазине
		</p>
		<div class="discount">
			Код скидки: <span class="code"><?=$model->hash_key?></div>
			<p class="share">
				Поделись им!
			</p>
			<div class="social">
				<?
					$img=$this->getAssetsUrl().'/img/head-logo.png';
					$url=$this->createAbsoluteUrl('/hash/'.$model->hash_key);
				?>
				<a class="fb" onclick="Share.facebook('<?=$url?>','Скидка <?=Config::getValue('discount.cost');?> рублей при покупке от <?=Config::getValue('discount.sill');?> руб.','<?=$img?>')" href="javascript: void(0)"></a> 
				<a class="vk" onclick="Share.vkontakte('<?=$url?>','Скидка <?=Config::getValue('discount.cost');?> рублей при покупке от <?=Config::getValue('discount.sill');?> руб.','<?=$img?>','');" href="javascript: void(0)"></a> 
				<a  class="tw" onclick="Share.twitter('<?=$url?>', 'Скидка <?=Config::getValue('discount.cost');?> рублей при покупке от <?=Config::getValue('discount.sill');?> руб.');" href="javascript: void(0);"></a>
			</div>
		</div>
	</div>
</div>