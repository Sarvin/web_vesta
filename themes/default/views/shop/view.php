<?php
$this->breadcrumbs=array(
	'Shops'=>array('index'),
	$model->name,
);

<h1>View Shop #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'time_table',
		'address',
		'status',
		'sort',
	),
)); ?>
