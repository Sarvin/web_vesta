<?
	$this->widget('zii.widgets.CListView', array(
	    'dataProvider'=>$dataProvider,
	    'itemView'=>'//items/_item',   // refers to the partial view named '_post'
	    'itemsCssClass'=>'catalog-items',
	    'template'=>'{pager}{items}{pager}',
	    'viewData'=>array('additionalItems'=>$dataProvider->getTotalItemCount()%4,'lastIndex'=>$dataProvider->getTotalItemCount()),
	));
?>
