<div class="wrap-catalog">
				<div class="fix_width">
					<div class="blocks">
						<div class="filter">
							<p class="caption">Полезный фильтр по шторам</p>
							<form action="#">
								<p class="title">
									Форма
								</p>
								<input type="term" value="<?=$filter->term?>" name="Filter[term]">
								<div class="size">
									<input type="checkbox" id="1">
									<label for="1">Длинные</label>
									<input type="checkbox" id="2">
									<label for="2">Длинные</label>
									<input type="checkbox" id="3">
									<label for="3">Длинные</label>
									<input type="checkbox" id="4">
									<label for="4">Длинные</label>
								</div>
								<div class="long">
									<p class="title">Шторы</p>
									<input type="text" id="long" value="0">
									<div class="slide ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all"><span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" style="left: 0%;"></span></div>
									<ul>
										<li style="color:#fff">0,5</li>
										<li>1</li>
										<li>1,5</li>
										<li>2</li>
										<li>2,5</li>
									</ul>
								</div>
								<div class="colors">
									<p class="title">Цвета</p>
									<input type="checkbox" id="cl1">
									<label for="cl1" style="background:#d58ecc"><span></span></label>
									<input type="checkbox" id="cl2">
									<label style="background:#6cc31f" for="cl2"><span></span></label>
									<input type="checkbox" id="cl3">
									<label style="background:#dfefec" for="cl3"><span></span></label>
									<input type="checkbox" id="cl4">
									<label style="background:#fff" for="cl4"><span></span></label>
									<input type="checkbox" id="cl5">
									<label style="background:#93afc1" for="cl5"><span></span></label>
									<input type="checkbox" id="cl6">
									<label style="background:#e0d924" for="cl6"><span></span></label>
								</div>
								<div class="places">
									<p class="title">Цвет</p>
									<input type="checkbox" id="1">
									<label for="1">Для гостинной</label>
									<input type="checkbox" id="2">
									<label for="2">Для спальни</label>
									<input type="checkbox" id="3">
									<label for="3">Для ванной</label>
									<input type="checkbox" id="4">
									<label for="4">Для детской</label>
									<input type="checkbox" id="1">
									<label for="1">Для кухни</label>
									<input type="checkbox" id="2">
								</div>
								<div class="actions">
									<input type="submit" id="sub" value="Применить фильтр" class="green-btn">
									<input type="reset" id="sub" value="Cбросить фильтр">
								</div>
							</form>
						</div>
						<div class="content">
							<div class="info">
								<p class="title">Лучшие шторы для детской</p>
								<p class="desc">Незаменимым атрибутом любой квартиры или дома, несомненно, являются шторы. Они не 	только защищают помещения от яркого дневного света и от посторонних взглядов, но и украшают окна, придавая им особый шарм. Мы предлагаем купить шторы для кухни.
								</p>
								<div class="fast-filter">
									<a href="#">
										До 1 500 руб.
									</a>
									<a href="#">
										До 2 500 руб.
									</a>
									<a href="#">
										До 3 500 руб.
									</a>
									<a href="#">
										До 5 500 руб.
									</a>
								</div>
							</div>
							<div class="pager">
								<ul>
									<li>
										<a href="#">Первая</a>
									</li>
									<li>
										<a href="#">Предыдущая</a>
									</li>
									<li class="active">
										<a href="#">1</a>
									</li>
									<li>
										<a href="#">1</a>
									</li>
									<li>
										<a href="#">1</a>
									</li>
									<li>
										<a href="#">1</a>
									</li>
									<li>
										<a href="#">1</a>
									</li>
									<li>
										<a href="#">1</a>
									</li>
									<li>
										<a href="#">Следующая</a>
									</li>
									<li>
										<a href="#">Последняя</a>
									</li>
									
								</ul>
							</div>
							<div class="catalog-items">
						<div class="item">
							<div class="content-hover">
										<a href="#">
											<img src="img/deal-item.png">
											<span>Шторы Sunlight Pro</span>
										</a>
										<span class="price">
											25 000
										</span>
										<a href="" class="green-btn">
											В корзину
										</a>
										<div class="sizes force-choose">
											<div class="width">
												<p>Ширина:</p>
												<input type="checkbox" id="width1">
												<label for="width1">190</label>
												<input type="checkbox" id="width2">
												<label for="width2">250</label>
											</div>
											<div class="count">
												<label class="caption">Количество:</label> <input type="text" value="3"> шт.
											</div>
										</div>
										<div class="desc">
											* Точная стоимость будет указана после выбора размера
										</div>
										<form class="sizes">
											<p class="caption"><strong>Доступные размеры</strong></p>
											<div class="heights">
												<p>Высота:</p>
												<input type="checkbox" id="height1" disabled="">
												<label for="height1">190</label>
												<input type="checkbox" id="height2">
												<label for="height2">250</label>
												<input type="checkbox" id="height3">
												<label for="height3">280</label>
											</div>
											<div class="width">
												<p>Ширина:</p>
												<input type="checkbox" id="width1">
												<label for="width1">190</label>
												<input type="checkbox" id="width2">
												<label for="width2">250</label>
											</div>
											<div class="count">
												<label class="caption">Количество:</label> <input type="text" value="3"> шт.
											</div>
											<div class="cart">
												<a href="#" class="one-click">Подробнее</a>
											</div>
										</form>
									</div>
						</div>
						<div class="item">
							<div class="content-hover">
										<a href="#">
											<img src="img/deal-item.png">
											<span>Шторы Sunlight Pro</span>
										</a>
										<span class="price">
											25 000
										</span>
										<a href="" class="green-btn">
											В корзину
										</a>
										<div class="sizes force-choose">
											<div class="width">
												<p>Ширина:</p>
												<input type="checkbox" id="width1">
												<label for="width1">190</label>
												<input type="checkbox" id="width2">
												<label for="width2">250</label>
											</div>
											<div class="count">
												<label class="caption">Количество:</label> <input type="text" value="3"> шт.
											</div>
										</div>
										<div class="desc">
											* Точная стоимость будет указана после выбора размера
										</div>
										<form class="sizes">
											<p class="caption"><strong>Доступные размеры</strong></p>
											<div class="heights">
												<p>Высота:</p>
												<input type="checkbox" id="height1" disabled="">
												<label for="height1">190</label>
												<input type="checkbox" id="height2">
												<label for="height2">250</label>
												<input type="checkbox" id="height3">
												<label for="height3">280</label>
											</div>
											<div class="width">
												<p>Ширина:</p>
												<input type="checkbox" id="width1">
												<label for="width1">190</label>
												<input type="checkbox" id="width2">
												<label for="width2">250</label>
											</div>
											<div class="count">
												<label class="caption">Количество:</label> <input type="text" value="3"> шт.
											</div>
											<div class="cart">
												<a href="#" class="one-click">Подробнее</a>
											</div>
										</form>
									</div>
						</div>
						<div class="item">
							<div class="content-hover">
										<a href="#">
											<img src="img/deal-item.png">
											<span>Шторы Sunlight Pro</span>
										</a>
										<span class="price">
											25 000
										</span>
										<a href="" class="green-btn">
											В корзину
										</a>
										<div class="sizes force-choose">
											<div class="width">
												<p>Ширина:</p>
												<input type="checkbox" id="width1">
												<label for="width1">190</label>
												<input type="checkbox" id="width2">
												<label for="width2">250</label>
											</div>
											<div class="count">
												<label class="caption">Количество:</label> <input type="text" value="3"> шт.
											</div>
										</div>
										<div class="desc">
											* Точная стоимость будет указана после выбора размера
										</div>
										<form class="sizes">
											<p class="caption"><strong>Доступные размеры</strong></p>
											<div class="heights">
												<p>Высота:</p>
												<input type="checkbox" id="height1" disabled="">
												<label for="height1">190</label>
												<input type="checkbox" id="height2">
												<label for="height2">250</label>
												<input type="checkbox" id="height3">
												<label for="height3">280</label>
											</div>
											<div class="width">
												<p>Ширина:</p>
												<input type="checkbox" id="width1">
												<label for="width1">190</label>
												<input type="checkbox" id="width2">
												<label for="width2">250</label>
											</div>
											<div class="count">
												<label class="caption">Количество:</label> <input type="text" value="3"> шт.
											</div>
											<div class="cart">
												<a href="#" class="one-click">Подробнее</a>
											</div>
										</form>
									</div>
						</div>
						<div class="item">
							<div class="content-hover">
										<a href="#">
											<img src="img/deal-item.png">
											<span>Шторы Sunlight Pro</span>
										</a>
										<span class="price">
											25 000
										</span>
										<a href="" class="green-btn">
											В корзину
										</a>
										<div class="sizes force-choose">
											<div class="width">
												<p>Ширина:</p>
												<input type="checkbox" id="width1">
												<label for="width1">190</label>
												<input type="checkbox" id="width2">
												<label for="width2">250</label>
											</div>
											<div class="count">
												<label class="caption">Количество:</label> <input type="text" value="3"> шт.
											</div>
										</div>
										<div class="desc">
											* Точная стоимость будет указана после выбора размера
										</div>
										<form class="sizes">
											<p class="caption"><strong>Доступные размеры</strong></p>
											<div class="heights">
												<p>Высота:</p>
												<input type="checkbox" id="height1" disabled="">
												<label for="height1">190</label>
												<input type="checkbox" id="height2">
												<label for="height2">250</label>
												<input type="checkbox" id="height3">
												<label for="height3">280</label>
											</div>
											<div class="width">
												<p>Ширина:</p>
												<input type="checkbox" id="width1">
												<label for="width1">190</label>
												<input type="checkbox" id="width2">
												<label for="width2">250</label>
											</div>
											<div class="count">
												<label class="caption">Количество:</label> <input type="text" value="3"> шт.
											</div>
											<div class="cart">
												<a href="#" class="one-click">Подробнее</a>
											</div>
										</form>
									</div>
						</div>
						<div class="item">
							<div class="content-hover">
										<a href="#">
											<img src="img/deal-item.png">
											<span>Шторы Sunlight Pro</span>
										</a>
										<span class="price">
											25 000
										</span>
										<a href="" class="green-btn">
											В корзину
										</a>
										<div class="sizes force-choose">
											<div class="width">
												<p>Ширина:</p>
												<input type="checkbox" id="width1">
												<label for="width1">190</label>
												<input type="checkbox" id="width2">
												<label for="width2">250</label>
											</div>
											<div class="count">
												<label class="caption">Количество:</label> <input type="text" value="3"> шт.
											</div>
										</div>
										<div class="desc">
											* Точная стоимость будет указана после выбора размера
										</div>
										<form class="sizes">
											<p class="caption"><strong>Доступные размеры</strong></p>
											<div class="heights">
												<p>Высота:</p>
												<input type="checkbox" id="height1" disabled="">
												<label for="height1">190</label>
												<input type="checkbox" id="height2">
												<label for="height2">250</label>
												<input type="checkbox" id="height3">
												<label for="height3">280</label>
											</div>
											<div class="width">
												<p>Ширина:</p>
												<input type="checkbox" id="width1">
												<label for="width1">190</label>
												<input type="checkbox" id="width2">
												<label for="width2">250</label>
											</div>
											<div class="count">
												<label class="caption">Количество:</label> <input type="text" value="3"> шт.
											</div>
											<div class="cart">
												<a href="#" class="one-click">Подробнее</a>
											</div>
										</form>
									</div>
						</div>
						<div class="item">
							<div class="content-hover">
										<a href="#">
											<img src="img/deal-item.png">
											<span>Шторы Sunlight Pro</span>
										</a>
										<span class="price">
											25 000
										</span>
										<a href="" class="green-btn">
											В корзину
										</a>
										<div class="sizes force-choose">
											<div class="width">
												<p>Ширина:</p>
												<input type="checkbox" id="width1">
												<label for="width1">190</label>
												<input type="checkbox" id="width2">
												<label for="width2">250</label>
											</div>
											<div class="count">
												<label class="caption">Количество:</label> <input type="text" value="3"> шт.
											</div>
										</div>
										<div class="desc">
											* Точная стоимость будет указана после выбора размера
										</div>
										<form class="sizes">
											<p class="caption"><strong>Доступные размеры</strong></p>
											<div class="heights">
												<p>Высота:</p>
												<input type="checkbox" id="height1" disabled="">
												<label for="height1">190</label>
												<input type="checkbox" id="height2">
												<label for="height2">250</label>
												<input type="checkbox" id="height3">
												<label for="height3">280</label>
											</div>
											<div class="width">
												<p>Ширина:</p>
												<input type="checkbox" id="width1">
												<label for="width1">190</label>
												<input type="checkbox" id="width2">
												<label for="width2">250</label>
											</div>
											<div class="count">
												<label class="caption">Количество:</label> <input type="text" value="3"> шт.
											</div>
											<div class="cart">
												<a href="#" class="one-click">Подробнее</a>
											</div>
										</form>
									</div>
						</div>
						<div class="item">
							<div class="content-hover">
										<a href="#">
											<img src="img/deal-item.png">
											<span>Шторы Sunlight Pro</span>
										</a>
										<span class="price">
											25 000
										</span>
										<a href="" class="green-btn">
											В корзину
										</a>
										<div class="sizes force-choose">
											<div class="width">
												<p>Ширина:</p>
												<input type="checkbox" id="width1">
												<label for="width1">190</label>
												<input type="checkbox" id="width2">
												<label for="width2">250</label>
											</div>
											<div class="count">
												<label class="caption">Количество:</label> <input type="text" value="3"> шт.
											</div>
										</div>
										<div class="desc">
											* Точная стоимость будет указана после выбора размера
										</div>
										<form class="sizes">
											<p class="caption"><strong>Доступные размеры</strong></p>
											<div class="heights">
												<p>Высота:</p>
												<input type="checkbox" id="height1" disabled="">
												<label for="height1">190</label>
												<input type="checkbox" id="height2">
												<label for="height2">250</label>
												<input type="checkbox" id="height3">
												<label for="height3">280</label>
											</div>
											<div class="width">
												<p>Ширина:</p>
												<input type="checkbox" id="width1">
												<label for="width1">190</label>
												<input type="checkbox" id="width2">
												<label for="width2">250</label>
											</div>
											<div class="count">
												<label class="caption">Количество:</label> <input type="text" value="3"> шт.
											</div>
											<div class="cart">
												<a href="#" class="one-click">Подробнее</a>
											</div>
										</form>
									</div>
						</div>
						<div class="item">
							<div class="content-hover">
										<a href="#">
											<img src="img/deal-item.png">
											<span>Шторы Sunlight Pro</span>
										</a>
										<span class="price">
											25 000
										</span>
										<a href="" class="green-btn">
											В корзину
										</a>
										<div class="sizes force-choose">
											<div class="width">
												<p>Ширина:</p>
												<input type="checkbox" id="width1">
												<label for="width1">190</label>
												<input type="checkbox" id="width2">
												<label for="width2">250</label>
											</div>
											<div class="count">
												<label class="caption">Количество:</label> <input type="text" value="3"> шт.
											</div>
										</div>
										<div class="desc">
											* Точная стоимость будет указана после выбора размера
										</div>
										<form class="sizes">
											<p class="caption"><strong>Доступные размеры</strong></p>
											<div class="heights">
												<p>Высота:</p>
												<input type="checkbox" id="height1" disabled="">
												<label for="height1">190</label>
												<input type="checkbox" id="height2">
												<label for="height2">250</label>
												<input type="checkbox" id="height3">
												<label for="height3">280</label>
											</div>
											<div class="width">
												<p>Ширина:</p>
												<input type="checkbox" id="width1">
												<label for="width1">190</label>
												<input type="checkbox" id="width2">
												<label for="width2">250</label>
											</div>
											<div class="count">
												<label class="caption">Количество:</label> <input type="text" value="3"> шт.
											</div>
											<div class="cart">
												<a href="#" class="one-click">Подробнее</a>
											</div>
										</form>
									</div>
						</div>
						<div class="item">
							<div class="content-hover">
										<a href="#">
											<img src="img/deal-item.png">
											<span>Шторы Sunlight Pro</span>
										</a>
										<span class="price">
											25 000
										</span>
										<a href="" class="green-btn">
											В корзину
										</a>
										<div class="sizes force-choose">
											<div class="width">
												<p>Ширина:</p>
												<input type="checkbox" id="width1">
												<label for="width1">190</label>
												<input type="checkbox" id="width2">
												<label for="width2">250</label>
											</div>
											<div class="count">
												<label class="caption">Количество:</label> <input type="text" value="3"> шт.
											</div>
										</div>
										<div class="desc">
											* Точная стоимость будет указана после выбора размера
										</div>
										<form class="sizes">
											<p class="caption"><strong>Доступные размеры</strong></p>
											<div class="heights">
												<p>Высота:</p>
												<input type="checkbox" id="height1" disabled="">
												<label for="height1">190</label>
												<input type="checkbox" id="height2">
												<label for="height2">250</label>
												<input type="checkbox" id="height3">
												<label for="height3">280</label>
											</div>
											<div class="width">
												<p>Ширина:</p>
												<input type="checkbox" id="width1">
												<label for="width1">190</label>
												<input type="checkbox" id="width2">
												<label for="width2">250</label>
											</div>
											<div class="count">
												<label class="caption">Количество:</label> <input type="text" value="3"> шт.
											</div>
											<div class="cart">
												<a href="#" class="one-click">Подробнее</a>
											</div>
										</form>
									</div>
						</div>
						<div class="item">
							<div class="content-hover">
										<a href="#">
											<img src="img/deal-item.png">
											<span>Шторы Sunlight Pro</span>
										</a>
										<span class="price">
											25 000
										</span>
										<a href="" class="green-btn">
											В корзину
										</a>
										<div class="sizes force-choose">
											<div class="width">
												<p>Ширина:</p>
												<input type="checkbox" id="width1">
												<label for="width1">190</label>
												<input type="checkbox" id="width2">
												<label for="width2">250</label>
											</div>
											<div class="count">
												<label class="caption">Количество:</label> <input type="text" value="3"> шт.
											</div>
										</div>
										<div class="desc">
											* Точная стоимость будет указана после выбора размера
										</div>
										<form class="sizes">
											<p class="caption"><strong>Доступные размеры</strong></p>
											<div class="heights">
												<p>Высота:</p>
												<input type="checkbox" id="height1" disabled="">
												<label for="height1">190</label>
												<input type="checkbox" id="height2">
												<label for="height2">250</label>
												<input type="checkbox" id="height3">
												<label for="height3">280</label>
											</div>
											<div class="width">
												<p>Ширина:</p>
												<input type="checkbox" id="width1">
												<label for="width1">190</label>
												<input type="checkbox" id="width2">
												<label for="width2">250</label>
											</div>
											<div class="count">
												<label class="caption">Количество:</label> <input type="text" value="3"> шт.
											</div>
											<div class="cart">
												<a href="#" class="one-click">Подробнее</a>
											</div>
										</form>
									</div>
						</div>
						<div class="item">
							<div class="content-hover">
										<a href="#">
											<img src="img/deal-item.png">
											<span>Шторы Sunlight Pro</span>
										</a>
										<span class="price">
											25 000
										</span>
										<a href="" class="green-btn">
											В корзину
										</a>
										<div class="sizes force-choose">
											<div class="width">
												<p>Ширина:</p>
												<input type="checkbox" id="width1">
												<label for="width1">190</label>
												<input type="checkbox" id="width2">
												<label for="width2">250</label>
											</div>
											<div class="heights">
												<p>Высота:</p>
												<input type="checkbox" id="height1" disabled="">
												<label for="height1">190</label>
												<input type="checkbox" id="height2">
												<label for="height2">250</label>
												<input type="checkbox" id="height3">
												<label for="height3">280</label>
											</div>
											<div class="count">
												<label class="caption">Количество:</label> <input type="text" value="3"> шт.
											</div>
										</div>
										<div class="desc">
											* Точная стоимость будет указана после выбора размера
										</div>
										<form class="sizes">
											<p class="caption"><strong>Доступные размеры</strong></p>
											<div class="heights">
												<p>Высота:</p>
												<input type="checkbox" id="height1" disabled="">
												<label for="height1">190</label>
												<input type="checkbox" id="height2">
												<label for="height2">250</label>
												<input type="checkbox" id="height3">
												<label for="height3">280</label>
											</div>
											<div class="width">
												<p>Ширина:</p>
												<input type="checkbox" id="width1">
												<label for="width1">190</label>
												<input type="checkbox" id="width2">
												<label for="width2">250</label>
											</div>
											<div class="count">
												<label class="caption">Количество:</label> <input type="text" value="3"> шт.
											</div>
											<div class="cart">
												<a href="#" class="one-click">Подробнее</a>
											</div>
										</form>
									</div>
						</div>
						<div class="item">
							
						</div>
					</div>
							<div class="pager">
								<ul>
									<li>
										<a href="#">Первая</a>
									</li>
									<li>
										<a href="#">Предыдущая</a>
									</li>
									<li class="active">
										<a href="#">1</a>
									</li>
									<li>
										<a href="#">1</a>
									</li>
									<li>
										<a href="#">1</a>
									</li>
									<li>
										<a href="#">1</a>
									</li>
									<li>
										<a href="#">1</a>
									</li>
									<li>
										<a href="#">1</a>
									</li>
									<li>
										<a href="#">Следующая</a>
									</li>
									<li>
										<a href="#">Последняя</a>
									</li>
									
								</ul>
							</div>
						</div>
					</div>
				</div>
				</div>