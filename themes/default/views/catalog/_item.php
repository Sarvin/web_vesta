<div class="item">
	<div class="content-hover">
		<a href="<?=$data->getUrl()?>">
			<img src="<?=$data->firstImage->getUrl('item')?>">
			<span><?=$data->name?></span>
		</a>
		<?

			$position=Yii::app()->cart->itemAt($data->getId());
			$flag=!$position;
			$flag=!$flag ? $position->state : $position->state!=1;
			if ($flag)
			{
			
		?>
		<a href="#" data-tooltip="Добавить товар в избранное" class="favorite" data-favorite="<?=$data->id?>">
			<img src="/media/favorite-heart.png">
		</a>
		<?}?>
		<span class="price">
			<?=$data->price?>
		</span>
		<a href="#" data-del="<?=!empty($model->position)?>" data-articul="<?=$data->id?>" class="green-btn incart">
			<?=$position ? 'Выложить' : 'В корзину'?>
		</a>
		
		<div class="sizes">
			<div class="width">
				<p>Ширина:</p>
				<?
					$this->renderPartial('//items/attrs/_width',array('data'=>$data,'prefix'=>'w','nested'=>'1'));
				?>
			</div>
			<div class="heights">
				<p>Высота:</p>
				<?
					$this->renderPartial('//items/attrs/_height',array('data'=>$data,'prefix'=>'h','nested'=>'2'));
				?>
			</div>
			<div class="count">
				<label class="caption">Количество:</label> <input type="text" value="3"> шт.
			</div>
		</div>
		<div class="desc">
			* Точная стоимость будет указана после выбора размера
		</div>
		<form class="sizes">
			<p class="caption"><strong>Доступные размеры</strong></p>
			<div class="heights">
				<p>Высота:</p>
				<?
					$this->renderPartial('//items/attrs/_height',array('data'=>$data));
				?>
			</div>
			<div class="width">
				<p>Ширина:</p>
				<?
					$this->renderPartial('//items/attrs/_width',array('data'=>$data));
				?>
			</div>
			<div class="count">
				<?

				?>
				<label class="caption">Количество:</label> <input type="text" data-count="<?=$data->id?>" value="<?=$data->position ? $data->position->getQuantity() : 0?>"> шт.
			</div>
			<div class="cart">
				<a href="/items/<?=$data->alias?>" class="one-click">Подробнее</a>
			</div>
		</form>
	</div>
	<div id="after-add_<?=$data->id?>" style="display:none">
		<?=$this->renderPartial('//items/_popup',array('data'=>$data))?>
	</div>
</div>