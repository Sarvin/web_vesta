<div class="wrap-catalog">
	<div class="fix_width">
		<div class="blocks">
			<div class="filter">
				<p class="caption">Полезный фильтр по шторам</p>
				<form action="/catalog">
					<p class="title">
						Форма
					</p>
					<input id="hidden_filter"  type="hidden" name="Filter[term]" value="<?=$filter->term?>">
					<?
					if ($model->prices)
						foreach ($model->prices as $key => $value) {
								?>
									<input type="radio" hidden id="price_<?=$key?>" name="Filter[price][<?=$key?>]" value="<?=$value?>">
								<?
							}
					?>
					<?
					if ($filter->images)
						foreach ($filter->findImages() as $key => $value) {
							if (in_array($value->key, $filter->images,true)) {
								?>
									<input type="hidden" id="form_<?=$key?>" name="Filter[images][<?=$value->id?>]" id="form_<?=$key?>" value="<?=$value->key?>">
								<?}
							}
					?>
					<div class="size">
						<?
						
							foreach ($filter->findForm() as $key => $value) {
								?>
									<input type="checkbox" id="form_<?=$key?>" name="Filter[form][<?=$value->id?>]" id="form_<?=$key?>" value="<?=$value->key?>">
									<label for="form_<?=$key?>"><?=$value->value?></label>
								<?
							}

						?>
					</div>
					<div class="long">
						<p class="title">Шторы</p>
						<input type="hidden" id="long" name="Filter[size]" value="0">
						<div class="slide"><span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" style="left: 0%;"></span></div>
						<ul>
							<?
							foreach ($filter->findSize() as $key => $attr) {
								?>
									<li data-articul="<?=$attr->key?>" ><?=$attr->value?></li>
								<?
							}?>
						</ul>
					</div>
					<div class="colors">
						<p class="title">Цвета</p>
						<?foreach ($filter->findColors() as $key => $attr) {
							?>
								<input type="checkbox" <?=$filter->isActive($attr->key,'colors') ? 'checked' : ''?> name="Filter[colors][<?=$attr->id?>]" id="cl_<?=$key?>" value="<?=$attr->key?>">
								<label for="cl_<?=$key?>" style="background:#<?=$attr->key?>">
									<span></span>
								</label>
							<?
						}?>
					</div>
					<div class="places">
						<p class="title">Назначение</p>
						<?foreach ($filter->findCatalogs() as $key => $data) {
							?>
								<input type="checkbox" <?=$filter->isActive($data->id,'catalogs') ? 'checked' : ''?> id="cat_<?=$key?>" name="Filter[catalogs][]" value="<?=$data->id?>">
								<label for="cat_<?=$key?>"><?=$data->name?></label>
							<?
						}?>
					</div>
					<div class="actions">
						<input type="submit" id="sub" value="Применить фильтр" class="green-btn">
						<a href="/catalog" type="reset" id="sub">Cбросить фильтр</a>
					</div>
				</form>
			</div>
			
			<div class="content">
				<?if ($model->price_range){?>
				<div class="info">
					<p class="title"><?=$model->name?></p>
					<p class="desc">
						<?=$model->wswg_body?>
					</p>
					<div class="fast-filter">
						<?
						if ($model->prices)
							foreach ($model->prices as $key => $value) {
								?>
								<label data-price="<?=$value?>" for="price_<?=$key?>">
									До <?=$value?> руб.
								</label>
								<?
							}
						?>
					</div>
				</div>
				<?}?>
				<div class="catalog-content">
				<?
					$this->renderPartial('_items',array('dataProvider'=>$dataProvider));
				?>
				</div>
			</div>
			
		</div>
	</div>
</div>