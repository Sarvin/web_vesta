<div class="popup">
	<p class="caption">Ваш товар добавлен в корзину!</p>
	<div class="data">
		<img height="242" src="<?=$data->firstImage ? $data->firstImage->getUrl('item') : '/media/default.png'?>">
		<div class="info-view">
			<ul>
				<li>
					<span>Название:</span><?=$data->name?>
				</li>
				<li>
					<span>тип:</span><?=$data->catalogView?>
				</li>
				<li>
					<span>Размер:</span><span data-size><?=$data->size?></span>
				</li>
				<li>
					<span>Количество:</span><span data-count="<?=$data->id?>"><?=$data->position ? $data->position->getQuantity() : 0?></span>
				</li>
				<li>
					<span>Цена за 1м:</span><?=$data->getPrice()?>
				</li>
			</ul>
			<hr>
			К оплате <span class="price">4500</span><br>
			<div class="actions">
				<a href="/cart" class="green-btn">Перейти к оплате</a>
				<a href="#" class="go-on">Продолжить покупки</a>
			</div>
		</div>
		
	</div>
</div>