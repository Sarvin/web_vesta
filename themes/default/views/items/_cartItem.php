<div class="param">
	<div class="preview">
		<img src="<?=$data->firstimage ? $data->firstImage->getUrl('item') : '/media/default.png'?>" width="128" height="126" alt="">
	</div>
	<ul>
		<li>
			<span>Модель:</span>
			<span><?=$data->name?></span>
		</li>
		<li>
			<span>Тип:</span>
			<span>Ролет</span>
		</li>
		<li>
			<span>Размер:</span>
			<span> на 90</span>
		</li>
		<li>
			<span>Колличество:</span>
			<span data-count="<?=$data->id?>"><?=$data->position ? $data->position->getQuantity() : 1?> шт.</span>
		</li>
		<li>
			<span>Цена за 1м:</span>
			<span><?=$data->price?> руб.</span>
		</li>
	</ul>
	<div class="remove">
		<label for="remove_1" data-param="1" data-articul="<?=$data->id?>" class="del"></label>
	</div>
</div>