<div class="item-view fix_width">
	<div class="item-info">
		<div class="preview">
			<?
				if ($model->gallery)
				{
					foreach ($model->gallery->galleryPhotos as $key => $data) {
						?>
							<a rel="1" style="display:<?=$key==0 ? 'inline-block' : 'none' ?>" href="<?=$data->getUrl('medium')?>" class="fancy">
								<img width="460" height="460" src="<?=$data->getUrl('view')?>">
							</a>
						<?
					}
				}
			?>
			<a href="#" class="like" data-articul="<?=$model->id?>" data-favorite="<?=$model->id?>" style="display:<?=$model->position->state==2 ? 'none' : 'block'?>">
				<span></span>
				<span></span>
			</a>
			<div class="like-item">
				<span>Понравился товар? Поделитесь им с друзьями!</span>
				<div class="social-small">
				<?
					if ($model->gallery->galleryPhotos)
					{
						$img=$this->createAbsoluteUrl('/').$model->firstImage->getUrl('main');
						$url=$this->createAbsoluteUrl('/').$model->getUrl();
					} else {
						$img=$this->createAbsoluteUrl('/').'/media/default_main.png';
						$url=$this->createAbsoluteUrl('/').'/media/default.png';
					}

				?>
				<a class="fb" onclick="Share.facebook('<?=$url?>','<?=$model->name?>','<?=$img?>')" href="javascript: void(0)"></a>
				<a class="vk" onclick="Share.vkontakte('<?=$url?>','<?=$model->name?>','<?=$img?>')" href="javascript: void(0)"></a>
				<a class="tw" onclick="Share.twitter('<?=$url?>','<?=$model->name?>')" href="javascript: void(0)"></a>
			</div>
			</div>
		</div>
		<div class="params">
			<p class="caption">
				<?=$model->name?>
			</p>
			<div class="price">
				<span class="old"><?=number_format($model->price, 0, ' ', ' ');?></span>
				<span class="current"><?=number_format($model->old_price, 0, ' ', ' ')?></span>
			</div>
			<div class="articul">
				<p><strong>Артиукул товара:</strong><?=$model->id?></p>
				<div class="find-out">
					<div class="delivery">
						<span></span>
						<a href="#" class="message">
							Узнать о доставке
						</a>
					</div>
					<div class="hz-che">
						<span></span>
						<a href="#" class="message">
							Узнать о доставке
						</a>
					</div>
					<div class="scissors">
						<span></span>
						<a href="#" class="message">
							Узнать о доставке
						</a>
					</div>
				</div>
			</div>
			<form class="sizes">
				<div class="hint">
					Выберите размер
				</div>
				<p class="caption"><strong>Доступные размеры</strong></p>
				<div class="heights">
					<p>Высота:</p>
					<?
						$this->renderPartial('//items/attrs/_height',array('data'=>$model));
					?>
				</div>
				<div class="width">
					<p>Ширина:</p>
					<?
						$this->renderPartial('//items/attrs/_width',array('data'=>$model));
					?>
				</div>
				<div class="count">
					<label class="caption">Количество:</label> <input data-count="<?=$model->id?>" type="text" data-count name="Book[count]" value="<?=$model->position ? $model->position->getQuantity() : 0?>"> шт.
				</div>
				<div class="cart" >
					<a href="#" data-articul="<?=$model->id?>" class="green-btn incart"><?='В корзину'?></a>
					<a href="/book?id=<?=$model->id?>&state=3" class="one-click">Купить в один клик</a>
				</div>
			</form>
			<p class="remeber">
			* Помните, положив товар в корзину - это Вас ничему не обязы-<br>
вает. Вы всегда можете отказаться от покупки.
			</p>
		</div>
	</div>
	<div class="dop-info">
		<div class="desc">
			<div class="item">
				<?if ($model->wswg_body){?>
					<p class="caption">
						Описание
					</p>
					<div class="content">
						<?=$model->wswg_body?>
					</div>
				<?}?>
			</div>
			<div class="item">
				<?if ($model->garanty){?>
					<p class="caption">
						Гарантия
					</p>
					<div class="content">
						<?=$model->garanty?>
					</div>
				<?}?>
			</div>
			<div class="item">
				<p class="caption">
					Относится
				</p>
				<div class="tags">
					<?if ($model->eavAttrStyle){?>
					<span>Стиль:</span>
					<?
						foreach ($model->eavAttrStyle as $key => $attr) {
							?>
								<a href="/catalog?Filter[style][]=<?=$attr->value?>"><?=$attr->valueView?></a>
							<?
						}
					}
					?>
					<?if ($model->eavAttrImages){?>
					<span>Рисунок:</span>
					<?
						foreach ($model->eavAttrImages as $key => $attr) {
							?>
								<a href="/catalog?Filter[images][]=<?=$attr->value?>"><?=$attr->valueView?></a>
							<?
						}
					}
					?>
				</div>
				
			</div>
		</div>
		<div class="recomendation">
			<div class="item">
				<?if ($recomendations){?>
				<p class="caption">
					Рекомендации по уходу
				</p>
				<?
					foreach ($recomendations as $key => $value) {
						?>
							<img src="<?=$value->getImageUrl('default')?>" alt="<?=$value->name?>" data-tooltip="<?=htmlspecialchars($model->wswg_body)?>">
						<?
					}
				?>
				<?}?>
			</div>
			<div class="item">
				<p class="caption">
					Характеристики
				</p>
				<ul>
					<?
						foreach ($model->eavSetModels as $key => $eavSet) {
							foreach ($eavSet->eavAttributes as $key => $eavAttr) {
								if ($eavAttr->valueView)
								{
								?>
									<li>
										<span><?=$eavAttr->label?></span>
										<span><?=$eavAttr->valueView?></span>
									</li>
								<?
								}
							}
						}
					?>
					<?
						foreach ($model->eavAttrModels as $key => $eavAttr) {
							if ($eavAttr->valueView){
								?>
									<li>
										<span><?=$eavAttr->label?></span>
										<span><?=$eavAttr->valueView?></span>
									</li>
								<?
							}
						}
						foreach ($model->attachedAttrs as $key => $eavAttr) {
							if ($eavAttr->valueView){
								?>
									<li>
										<span><?=$eavAttr->label?></span>
										<span><?=$eavAttr->valueView?></span>
									</li>
								<?
							}
						}
					?>
				</ul>
			</div>
			<div class="item">
				
			</div>
		</div>
	</div>
</div>
<div id="after-add_<?=$model->id?>" style="display:none">
<?
	$this->renderPartial('_popup',array('data'=>$model));
?>
</div>
