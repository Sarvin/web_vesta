<?
	$positions=Yii::app()->cart->getPositions();
	$content='';
	$cost=0;
	if ($positions)
		foreach ($positions as $key => $value) {
			$content.=$this->renderPartial('//items/_cartItem',array('data'=>$value),true);
			$cost+=$value->priceBySize;
		}
?>
<div class="items-wrap">
	<a href="/cart" class="go-to-cart">Корзина<span class="a-ico"></span><span class="count">1</span></a>
	<div class="params">
		<div class="content">
			<div class="scrollbar" style="height: 460px;">
				<div class="track" style="height: 460px;">
					<div class="thumb">
						<div class="end"></div>
					</div>
				</div>
			</div>
			<div class="viewport">
				<div class="overview">
					<?=$content?>
					<div class="empty" style="display:<?=$positions ? 'none' : 'block'?>;width: 500px;height: 100%;text-align: center;line-height: 460px;">
						Корзина пуста
					</div>
				</div>
			</div>
		</div>
		<div class="cost" style="display:<?=!count($positions) ? 'none' : 'block'?>">
			<div class="into-cart">
				<span>К оплате <strong data-cost="1"><?=$cost?></strong></span>
				<a href="/cart" class="green-btn">
					Перейти в корзину
				</a>
			</div>
		</div>
	</div>
</div>
<div class="cover">
</div>