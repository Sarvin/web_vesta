$(function(){
	
	function updateView(){
		$.ajax({
			url:'/catalog/getContent',
			dataType:'json',
			data:$('.filter form').serialize(),
			success:function(data){
				if (data.success)
				{
					$('.catalog-content').empty().append(data.success);
				}
			}
		});
	}
	$('.fast-filter label').on('click',function(){
		if ($(this).hasClass('active'))
		{
			$(this).removeClass('active')
			$('#'+$(this).attr('for')).prop('checked',false);
			updateView();
			return false;
		}
		else 
		{
			$('.fast-filter label').removeClass('active');
			$(this).addClass('active')
		}
	})
	$('.filter input').on('change',function(){
		updateView();
	})
	
	var long_li=$('.wrap-catalog .filter .long li');
	$('.wrap-catalog .filter .slide').slider({
		min:0.5,
		max:2.5,
		step:0.5,
		change:function(event,ui){
			long_li.removeAttr('style');
			var li=long_li.eq(ui.value/0.5-1).css('color','#fff');
			$('#long').prop('value',li.data('articul'));
			updateView();
		},
	});
})