$(function(){
	var win=$(window)
	var cart_items=$('.cart-items');

	$('.slider').owlCarousel({
		singleItem:true,
		pagination:true,
		items:1,
	})

	if ($('.cart .params .content').length || $('.cart-items .content').length)
	{
		$('.cart .params .content').tinyscrollbar();
		$('.cart-items .content').tinyscrollbar();
	}

	$('.modal').fancybox({
		closeBtn:false,
		fitToView:true,
		autoSize:true,
		padding:0
	})
	$('.fancy').fancybox({
		closeBtn:false,
		fitToView:true,
		autoSize:true,
		openEffect  : 'none',
		closeEffect : 'none',
		helpers	: {
			title	: {
				type: 'outside'
			},
			thumbs	: {
				width	: 50,
				height	: 50
			}
		}
	})
	if ($('select').length)
		$('select').selectBox();
	
	$('.go-on,.close,[for="close"]').on('click',function(){
		console.log(123)
		$.fancybox.close();
	})
	$('.example a span').click(function(){
		$('#search').val($(this).text());
	})
	if (cart_items.length)
	{
		var scrollTop=cart_items.offset().top;
		var scrollBottom=$('.wrap-menu').offset().top-cart_items.height()-50;
		win.scroll(function(){
			if (win.scrollTop()>=scrollTop && win.scrollTop()<=scrollBottom)
				cart_items.css({position:'fixed',top:10});
			else {
				if (scrollBottom<win.scrollTop() && cart_items.css('position')=='fixed')
				{
					cart_items.css({position:'absolute',top:scrollBottom-275});
				}
				else 
				if (cart_items.css('position')=='fixed')
					cart_items.removeAttr('style');
			}
		})
	}
	$('.wrap-search .criteria').on('change',' #all',function(){
		var params={
			visibility:$(this).is(':checked') ? 'visible' : 'hidden',
			opacity:$(this).is(':checked') ? 1 : 0
		}
	})
	
	$('#search').autocomplete({
		serviceUrl:'/catalog/autoComplite',
		onSelect:function(e){
			$('#search').data('url',e.data);
		},
		onSearchComplete:function(query,suggestions){
			if (suggestions.length==0)
				$('#search').removeData('url');
		}
	});

	$('.search-query').on('submit',function(){
		if ($('#search').data('url'))
			$('.search-query').attr('action',$('#search').data('url'));
		else 
			{
				if ($('#search').val())
					window.location='/catalog?Filter[term]='+$('#search').val();
				else 
					window.location='/catalog';
				return false;
			}
	})

	$('body').on('click','.incart',function(){
			var sizes={};
			if(sizes=sizeIsChoosen.apply(this,[]))
				request.apply(this,['/items/addToCart',{id:$(this).data('articul'),width:sizes.width,height:sizes.height},add]);
			else 
				$(this).next().not('.one-click').addClass('force-choose');
			return false;
		return false;
	});

	$('body').on('change','.force-choose input',function(){
		var param=$(this).closest('div').hasClass('heights') ? 'height' : 'width';
		var elemSelector=param+'_'+$(this).data('articul')+'_'+$(this).val();
		$('[data-nested="'+elemSelector+'"]').prop('checked',$(this).is(':checked'));
	})

	$('body').on('click','[data-state]',function(){
		var $this=$(this);
		request.apply(this,['/items/state',{state:$this.data('state'),id:$this.data('articul'),remove:$this.data('remove')},stateUpdate]);
		return false;
	});

	$('body').on('change','[data-count]',function(e){
		e.preventDefault();
		var $this=$(this);
		request.apply(this,['/items/count',{count:$this.val(),id:$this.data('count'),state:$this.data('param')}]);
		return false;
	});

	$('body').on('click','[data-favorite]',function(){
		var $this=$(this);
		request.apply(this,['/items/state',{state:2,id:$this.data('favorite')},favorite]);
		return false;
	})

	$('body').on('click','.del',function(){
		var $this=$(this);
		request.apply(this,['/items/removeFromCart',{id:$this.data('articul'),state:$this.data('param')},remove]);
	});
	
	$('body').on('change','[data-size]',function(){
		var $this=$(this);
		request.apply(this,['/items/size',{size:$(this).val(),id:$this.data('size'),state:$this.data('param')}]);
	})
})

function updateData(id,data,state)
{	
	var state = state!=undefined ? '[data-param="'+state+'"]' : "";
	$('[data-stateval="2"]').text(data.state_2);
	$('[data-stateval="1"]').text(data.state_1);
	$('[data-positions="2"]').text(data.state_2);
	$('[data-positions="1"]').text(data.state_1);
	$('[data-cost="2"]').text(data.cost_2);
	$('[data-cost="1"]').text(data.cost_1);
	$('[data-price="'+id+'"]'+state).text(data.price);
	$('[data-count="'+id+'"]'+state).text(data.itemCount).val(data.itemCount);
	$('[data-summ]').text(data.cost);
	$('[data-itemcost="'+id+'"]'+state).text(data.itemcost);
	$('.wrap-search .cart .count').text(data.state_1);
	if (data.view!=undefined)
		$('.cart .items-wrap .overview').append(data.view);
}

function sizeIsChoosen(){

	var context=$(this).closest('.content-hover').length ? $(this).closest('.content-hover') : $(this).closest('.item-info');
	var selectedWidth=$('.sizes:last .width input[type="radio"]:checked',context).val();
	var selectedHeight=$('.sizes:last .heights input[type="radio"]:checked',context).val();

	if (!selectedHeight || !selectedWidth){
		
		var selectedWidth=$('.sizes:first .width input[type="radio"]:checked',context).val();
		var selectedHeight=$('.sizes:first .heights input[type="radio"]:checked',context).val();

		if (!selectedHeight || !selectedWidth)
		{
			if ($('.hint').length)
			{
				$('.hint').addClass('active').delay(4000);
				setTimeout(function(){
					$('.hint').removeClass('active');
				},5000)
			}
			return false;
		}
		else 
			return {height:selectedHeight,width:selectedWidth};
	}
	return {height:selectedHeight,width:selectedWidth};
}

function add(data){

		var $this=$(this);
		var context=$this.closest('.content-hover,.item-info');
		
		var h=$('.sizes .heights input:checked',context).attr('id');
		var w=$('.sizes .width input:checked',context).attr('id');
		
		var size=$('[for="'+h+'"]').text()+"x"+$('[for="'+w+'"]').text();

		$("[data-size]",'#after-add_'+$this.data('articul')).text(size);

		$('.cart .items-wrap .empty').hide();
		$('.cart .items-wrap .cost').show();

		$.fancybox.open([{
			href:'#after-add_'+$this.data('articul'),
			fitToView:true,
			padding:0,
		}]);

		$('.force-choose',context).removeClass('force-choose');
		$('input:checked',context).prop('checked',false);
		
	}

	function remove(data){
		var $this=$(this);
		console.log(data);
		var elem=$('tr',$this.closest('tbody')).length==1 ? '.data' : 'tr';
		$('[data-articul="'+$this.data('articul')+'"][data-param="'+$this.data('param')+'"]').closest('.param,'+elem).slideUp(300,function(){
			$(this).remove();
			if (data.state_1==0)
			{
				$('.cart .items-wrap .empty').show();
				$('.cart .items-wrap .cost').hide();
			}
			
		});
		
		var context=$this.closest('.content-hover');
		$('.force-choose',context).removeClass('force-choose').find('input').prop('checked',false);
	}

	function favorite(data){
		var $this=$(this);
		if (!data.count)
		{
			$('.cart .items-wrap .empty').show();
			$('.cart .items-wrap .cost').hide();
		}
		var elem=$('tr',$this.closest('tbody')).length==1 ? '.data' : 'tr';
		$('[data-articul="'+$this.data('articul')+'"]').closest('.param,'+elem).slideUp(300,function(){
			$(this).remove();
			if ($('.param').length==0)
				$('.empty').show();
		});
		if (data.showPopUp==1){
			$.fancybox.open([{
				href:'#remember',
				fitToView:true,
				closeBtn:false,
				padding:0,
			}])
		}
		$(this).fadeOut(300);
	}

	function stateUpdate(data){
		var $this=$(this);
		var elem=$('tr',$this.closest('tbody')).length==1 ? '.data' : 'tr';
		$('[data-articul="'+$this.data('articul')+'"]').closest('.param,'+elem).slideUp(300,function(){
			$(this).remove();
			if (!$('.param').length)
				$('.empty').show();
		});
		if (data.showPopUp==1){
			$.fancybox.open([{
				href:'#remember',
				fitToView:true,
				padding:0,
			}])
		}
	}

	function request(url,params,callback){
		var $this=$(this);
		$.ajax({
			url:url,
			dataType:'json',
			data:params,
			success:function(data){
				if (data.success)
				{
					if (callback)
						callback.apply($this,[data]);
					updateData($this.data('articul'),data,params.state);
				}
			},
			error:function(){
				alert("Ошбика! Товар не был добавлен в корзину, возможно он был удален администратором, или зарезервирован другим пользователем.");
			}
		})
	}
Share = {
vkontakte: function(purl, ptitle, pimg, text) {
    url  = 'http://vkontakte.ru/share.php?';
    url += 'url='          + encodeURIComponent(purl);
    url += '&title='       + encodeURIComponent(ptitle);
    url += '&description=' + encodeURIComponent(text);
    url += '&image='       + encodeURIComponent(pimg);
    //url += '&noparse=true';
    Share.popup(url);
},
odnoklassniki: function(purl, text) {
    url  = 'http://www.odnoklassniki.ru/dk?st.cmd=addShare&st.s=1';
    url += '&st.comments=' + encodeURIComponent(text);
    url += '&st._surl='    + encodeURIComponent(purl);
    Share.popup(url);
},
facebook: function(purl, ptitle, pimg, text) {
    url  = 'http://www.facebook.com/sharer.php?s=100';
    url += '&p[title]='     + encodeURIComponent(ptitle);
    url += '&p[summary]='   + encodeURIComponent(text);
    url += '&p[url]='       + encodeURIComponent(purl);
    url += '&p[images][0]=' + encodeURIComponent(pimg);
    Share.popup(url);
},
twitter: function(purl, ptitle) {
    url  = 'http://twitter.com/share?';
    url += 'text='      + encodeURIComponent(ptitle);
    url += '&url='      + encodeURIComponent(purl);
    url += '&counturl=' + encodeURIComponent(purl);
    Share.popup(url);
},
mailru: function(purl, ptitle, pimg, text) {
    url  = 'http://connect.mail.ru/share?';
    url += 'url='          + encodeURIComponent(purl);
    url += '&title='       + encodeURIComponent(ptitle);
    url += '&description=' + encodeURIComponent(text);
    url += '&imageurl='    + encodeURIComponent(pimg);
    Share.popup(url)
},

popup: function(url) {
    window.open(url,'','toolbar=0,status=0,width=626,height=436');
}
};