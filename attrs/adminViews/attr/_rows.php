	<?php echo $form->dropDownListControlGroup($model,'id_attr_type_field',
					CHtml::listData(AttrFieldType::model()->findAll(),'id','name'),
					array(
							'class'=>'span8',
							'ajax'=>array(
								'url'=>'/admin/attr/checkOnMultiple',
								'data'=>'js:{id:this.value}',
								'dataType'=>'json',
								'success'=>'function(data){
									if (data.success)
									{
										$(".add-range-value").slideDown(300);
									} else 
										$(".add-range-value").slideUp(300);
								}',
							)
						)
				); 
	?>

	<?php echo $form->textFieldControlGroup($model,'label',array('class'=>'span8','maxlength'=>255)); ?>

	<?php echo $form->textFieldControlGroup($model,'alias',array('class'=>'span8','disabled'=>true)); ?>
	<?php echo CHtml::dropDownList('key_type',$model->range->key_type, array('Нумерация','Палитра'),array('class'=>'span8'));?>
	<div class="add-range-value" style="display:<?=$model->isRanged ? 'block' : 'none'?>">
		<hr>
		<p>Значения мульти выборки</p>
		<select id="key_type" name="Attr[key_type]">
			<option value="0">Число</option>
			<option value="1">Палитра</option>
		</select>
		<div class="range-values">
			<?
				$count=1;
				if ($model->rangeValues)
					foreach ($model->rangeValues as $key => $range) {
						$count=$key+1;
						?>
							<div class="value">
						<?
							echo CHtml::textField('AttrRange[key][]',$range->key,array('class'=>'span5 key'));
							echo CHtml::textField('AttrRange[value][]',$range->value,array('class'=>'span5'));
						?>
							</div>
						<?
					}
			?>
			<input name="AttrRange[key][]" type="text" class="span5 key" placeholder="Ключ">
			<input name="AttrRange[value][]" type="text" class="span5" placeholder="Значение">
		</div>
		<input  type="button" data-count="<?=$count?>" value="Добавить значение" class="btn add-val">
	</div>
	<script>
		$(function(){
			$('#key_type').on('change',function(){

				if ($(this).val()==1){
					$('.key').colpick({
						colorScheme:"dark",
						onChange:function(hsb,hex,rgb,el,bySetColor) {
						$(el).css("border-color","#"+hex);
						if(!bySetColor) 
						{
							$(el).val(hex);
							$(el).closest(".color-area").find("div").css("background", "#" + hex);
						}
					}}).keyup(function(){
						$(this).colpickSetColor(this.value);
					});
				}
			})
			
			$('.add-val').on('click',function(){
				$('.range-values').append('<input name="AttrRange[key][]" type="text" class="span5 key"  placeholder="Ключ"><input name="AttrRange[value][]" type="text" class="span5"  placeholder="Значение">');
				$('.key:last').colpick({
						colorScheme:"dark",
						onChange:function(hsb,hex,rgb,el,bySetColor) {
						$(el).css("border-color","#"+hex);
						if(!bySetColor) 
						{
							$(el).val(hex);
							$(el).closest(".color-area").find("div").css("background", "#" + hex);
						}
					}}).keyup(function(){
						$(this).colpickSetColor(this.value);
					});
			})
			
		})
	</script>