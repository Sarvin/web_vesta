<?php

class AttachedSetsWidget extends CWidget {
    public $model;
    public $controllerRoute;
    public $modelName;
    public $regScript=true;
    protected $assets;

    public function init(){
        if ($this->regScript)
        {
            $this->assets = Yii::app()->getAssetManager()->publish(dirname(__FILE__) . '/assets', false, -1, true);
            $cs=Yii::app()->clientScript;
            $cs->registerCssFile($this->assets.'/css/attributes.css');
            $cs->registerScriptFile($this->assets.'/js/initFunctions.js',CClientScript::POS_END);
        }
    }
    public function regAddFieldScript($id,$view,$class){
        
        Yii::app()->clientScript->registerScript('append_attached_set_'.$id  ,'
                $("#btn_set'.$id.'").on("click",function(){
                     $(\'.multy-group-'.$id.'\').append(\''.preg_replace('%[\r\n|\n|\r]%', "", $view).'\');
                    init'.$class.'();
                });
            ',CClientScript::POS_END);
    }

    public function run(){
        
        foreach ($this->model->attachedSets as $key => $group) {
            
            echo "<div class=\"eav-attached-sets\">";
            echo "<hr>";
            echo "<p><strong>Прицепленныая группа атрибтов</strong></p>";
            foreach ($group->attrs as $key => $data) {
                $name=$this->modelName.'[AttachedSets]['.$group->id.']['.$data->id.']';
                $id=$this->modelName.'_attachedSets_'.$group->id.'_'.$data->id;
                $btn_id=$key.$eav_group_id;
                $fieldView=$data->field_type->id_widget!=null && $data->field_type->id_widget!=0 ? $data->widget : 'string';

                if ($data->field_type->value_range==1)
                {
                    echo "<br>";
                    echo "<p class=\"caption\">$data->label</p>";
                    $data->label="";
                    $range=CHtml::listData($data->rangeValues,'key','value');
                    $this->regAddFieldScript($data->id, Yii::app()->controller
                                                ->renderPartial('appext.ownattributes.views.attrTypeView/'.$fieldView,
                                                array('model'=>$data,'range'=>$range,'name'=>$name.'[]'),true),$fieldView);

                    echo "<div class=\"multy-group-".$data->id."\">";
                    foreach ($data->multyVals as $key => $multy) {
                        $multy->label="";
                        $this->render('attrTypeView/'.$fieldView,array('model'=>$multy,'range'=>$range,'name'=>$name.'['.$multy->id.']','id'=>$data->id));
                    }

                    $this->render('attrTypeView/'.$fieldView,array('model'=>$data,'range'=>$range,'name'=>$name.'[]','id'=>$id));
                    echo "</div>";
                    
                    echo TbHtml::button('Добавить',array('id'=>'btn_set'.$data->id)).'<hr>';
                } else {
                    
                    $this->render('attrTypeView/'.$fieldView,array('model'=>$data,'name'=>$name,'id'=>$id,'range'=>($data->isRanged ? CHtml::listData($data->rangeValues,'key','value') : array())));
                }
            }
            echo "</div>";
            Yii::app()->clientScript->registerScript('init','initAllWidgets()');
        }
    }
}