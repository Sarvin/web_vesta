<?php

class SetsWidget extends CWidget {
    public $groups;
    public $controllerRoute;
    public $modelName;
    public $regScript=true;
    protected $assets;

    public function init(){
        if ($this->regScript)
        {
            $this->assets = Yii::app()->getAssetManager()->publish(dirname(__FILE__) . '/assets', false, -1, true);
            $cs=Yii::app()->clientScript;
            $cs->registerCssFile($this->assets.'/css/attributes.css');
            $cs->registerScriptFile($this->assets.'/js/initFunctions.js',CClientScript::POS_END);
        }
    }
    public function regAddFieldScript($id,$view,$class){
        
        Yii::app()->clientScript->registerScript('append_'.$id  ,'
            console.log('.$id.');
                $("#btn'.$id.'").on("click",function(){
                    
                     $(\'.multy-group-'.$id.'\').append(\''.preg_replace('%[\r\n|\n|\r]%', "", $view).'\');
                    init'.$class.'();
                });
            ',CClientScript::POS_END);
    }
    public function run(){

        foreach ($this->groups as $key => $group) {
            $id=$group->parent ? $group->parent_model->ident->id_model : $group->ident->id_model;
            $eav_group_id=$id;
            
            echo "<div class=\"eav-group-".$id."\">";
            echo "<hr>";
            echo "<p><strong>".$group->group->name."</strong></p>";
            
            foreach ($group->eavAttributes as $key => $data) {
                $name=$this->modelName.'[eavSetValues]['.$group->id.']['.$data->id.']';
                $id=$this->modelName.'_eavSetValues_'.$group->id.'_'.$data->id;
                $btn_id=$key.$eav_group_id;
                $fieldView=$data->field_type->id_widget!=null && $data->field_type->id_widget!=0 ? $data->widget : 'string';

                if ($data->field_type->value_range==1)
                {
                    echo "<br>";
                    echo "<p class=\"caption\">$data->label</p>";
                    $data->label="";
                    $range=CHtml::listData($data->rangeValues,'key','value');
                    $this->regAddFieldScript($btn_id, Yii::app()->controller
                                                ->renderPartial('appext.ownattributes.views.attrTypeView/'.$fieldView,
                                                array('model'=>$data,'range'=>$range,'name'=>$name.'[]'),true),$fieldView);

                    echo "<div class=\"multy-group-".$key.$eav_group_id."\">";

                    foreach ($data->multyVals as $key => $multy) {
                        $multy->label="";
                        $this->render('attrTypeView/'.$fieldView,array('model'=>$multy,'range'=>$range,'name'=>$name.'['.$multy->id.']','id'=>$id));
                    }

                    $this->render('attrTypeView/'.$fieldView,array('model'=>$data,'range'=>$range,'name'=>$name.'[]','id'=>$id));
                    echo "</div>";
                    
                    echo TbHtml::button('Добавить',array('id'=>'btn'.$btn_id)).'<hr>';
                } else {
                    $this->render('attrTypeView/'.$fieldView,array('model'=>$data,'name'=>$name,'id'=>$id,'range'=>($data->isRanged ? CHtml::listData($data->rangeValues,'key','value') : array())));
                }
            }
            echo "</div>";
            
            
        }
        Yii::app()->clientScript->registerScript('init','initAllWidgets()');
    }
}