<div class="color-area">
	<?=CHtml::label($model->label,'',array('class'=>'color'));?>
	<?=CHtml::textField($name,$model->value,array('class'=>'color span10','id'=>$id));?>
	<div style="background:#<?=$model->value?>">
	</div>
</div>
<?
	Yii::app()->clientScript->registerScript('color',
		'$("#'.$id.'").colpick({
			colorScheme:"dark",
			onChange:function(hsb,hex,rgb,el,bySetColor) {
			$(el).css("border-color","#"+hex);
			if(!bySetColor) 
			{
				$(el).val(hex);
				$(el).closest(".color-area").find("div").css("background", "#" + hex);
			}
		}}).keyup(function(){
			$(this).colpickSetColor(this.value);
		});
		'
	);
?>