
<div class="control-group">
<?
echo CHtml::label($model->label,'',array('required'=>$model->required));
$this->widget('zii.widgets.jui.CJuiDatePicker',array(
    'name'=>$name,
    // additional javascript options for the date picker plugin
    'value'=>$model->value,
    'options'=>array(
        'showAnim'=>'fold',
    ),
    'htmlOptions'=>array(
        'class'=>'span12 date',
        'id'=>false
    ),
));
?>
</div>