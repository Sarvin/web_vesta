<div class="EavAttributes">
	<?
		
		$this->widget('appext.ownattributes.SetsWidget', array(
			'groups' => $model->eavSetModels,
			'modelName'=>$modelName,
			'regScript'=>$regAssets,
		));
	?>
</div>
<div class="EavAttributes">
	<?
		$this->widget('appext.ownattributes.AttributesWidget', array(
			'attributes' => $model->eavAttrModels,
			'modelName'=>$modelName,
			'regScript'=>$regAssets,
		));
	?>
</div>