<?php

/**
* This is the model class for table "{{attr_multy_val}}".
*
* The followings are the available columns in table '{{attr_multy_val}}':
    * @property integer $id
    * @property integer $id_attr_val
    * @property string $value
*/
class AttrMultyVal extends EActiveRecord
{
    public $label;
    public $required=false;
    public function tableName()
    {
        return '{{attr_multy_val}}';
    }


    public function rules()
    {
        return array(
            array('id_attr_val', 'numerical', 'integerOnly'=>true),
            array('value,label', 'length', 'max'=>255),
            array('required','boolean'),
            // The following rule is used by search().
            array('id, id_attr_val, value', 'safe', 'on'=>'search'),
        );
    }


    public function relations()
    {
        return array(
        );
    }


    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'id_attr_val' => 'Комментарий',
            'value' => 'Value',
        );
    }



    public function search()
    {
        $criteria=new CDbCriteria;
		$criteria->compare('id',$this->id);
		$criteria->compare('id_attr_val',$this->id_attr_val);
		$criteria->compare('value',$this->value,true);
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }


}
