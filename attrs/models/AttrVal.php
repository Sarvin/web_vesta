<?php

/**
* This is the model class for table "{{attr_val}}".
*
* The followings are the available columns in table '{{attr_val}}':
    * @property integer $id
    * @property integer $id_entity
    * @property integer $id_model
    * @property integer $post_id
*/
class AttrVal extends EActiveRecord
{
    
    public function tableName()
    {
        return '{{attr_val}}';
    }

    public function rules()
    {
        return array(
            array('post_id,id_group', 'numerical', 'integerOnly'=>true),
            array('value','length','max'=>255),
            array('post_type,multy','boolean'),
            // The following rule is used by search().
            array('id, id_entity, id_model, post_id', 'safe', 'on'=>'search'),
        );
    }

    public function relations()
    {
        return array(
            'multyVal'=>array(self::HAS_MANY,'AttrMultyVal','id_attr_val'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'post_type' => 'Атрибут или группа атрибутов',// 0 - не относится к группе, 1 - относится к группе
            'post_id' => 'Атрибут',
            'id_group' => 'группа',
            'multy'=>'Мульти значение',
            'value' => 'Значение',
        );
    }

    public function search()
    {
        $criteria=new CDbCriteria;
		$criteria->compare('id',$this->id);
		$criteria->compare('post_type',$this->post_type);
		$criteria->compare('post_id',$this->post_id);
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
    public function beforeDelete(){
        parent::beforeDelete();
        if ($this->multyVal)
        {
            foreach ($this->multyVal as $key => $value) {
                $value->delete();
            }
        }
    }
}
