<?php

/**
* This is the model class for table "{{attr_model}}".
*
* The followings are the available columns in table '{{attr_model}}':
    * @property integer $id
    * @property integer $id_group_attr_set
    * @property integer $id_model_ident
*/
class AttrModel extends EActiveRecord
{

    
    public $label;
    public $id_attr;
    public $isRanged;

    private $_range;
    private $_value;
    private $_multyVals=false;
    private $_id_attr_val=false;
    private $_rangeVal=false;
    private $_valueView=false;

    public function getRangeVal(){
        if ($this->_rangeVal===false)
            $this->_rangeVal=$this->rangeValues[$this->value];
        return $this->_rangeVal;
    }

    public function getId_attr_val(){

        if ($this->_id_attr_val===false)
        {
            $result=Yii::app()->db->createCommand()
                ->select('id')
                ->from('{{attr_val}}')
                ->where('post_id=:attr and post_type=false',array(':attr'=>$this->id,':attr'=>$this->id))
                ->queryRow();

            $this->_id_attr_val=$result['id'];
        }
        return $this->_id_attr_val;
    }

    public function setId_attr_val($data){
        $this->_id_attr_val=$data;
    }

    public function getValue()
    {
        if (!$this->_value)
        {
            $result=Yii::app()->db->createCommand()
            ->select('value')
            ->from('{{attr_val}} val')
            ->where('val.post_id=:id and post_type!=true',array(':id'=>$this->id))
            ->queryRow();    
            $this->_value=$result['value'];
        }
        return $this->_value;
    }
    public function getValueView(){
        
        if ($this->isRanged){
            $this->_valueView=$this->rangeValues[$this->value]->value;
        } else 
        {
            if ($this->isMulty)
                $this->_valueView=Chtml::listData($this->multyVals,'key','value');
            else 
                $this->_valueView=$this->value;
        }
        
        return $this->_valueView;
    }
    public function setValue($data){
        $this->_value=$data;
    }
    public function adterFind(){
        $this->attributes=$this->attr->attributes;
    }
    public function tableName()
    {
        return '{{attr_model}}';
    }

    public function setMultyVals($data){
        $this->_multyVals=$data;
    }

    public function getMultyVals(){
        if ($this->_multyVals===false){
            $criteria=new CDbCriteria;
            $criteria->addCondition('id_attr_val=:val');
            $criteria->params[':val']=$this->id_attr_val;
            $criteria->select="*,'".$this->label."' as label,false as required";
            $this->_multyVals=AttrMultyVal::model()->findAll($criteria);
        }
        return $this->_multyVals ? $this->_multyVals :array();
    }
    public function getIsMulty(){
        return $this->attr->isMulty;
    }
    public function rules()
    {
        return array(
            array('id_attr, id_model_ident, id_attr_val, parent, id_attr', 'numerical', 'integerOnly'=>true),
            array('required,isRanged,attach','boolean'),
            array('label','length','max'=>255),
            array('range,id_attr, multyVals, rangeVal, alias, value','safe'),
            // The following rule is used by search().
            array('id, id_attr, id_model_ident', 'safe', 'on'=>'search'),
        );
    }

    public function getField_type(){
        return $this->attr->field_type;
    }

    public function getRangeValues(){
        return $this->attr->rangeValues;
    }

    public function isRanged(){
        return $this->attr->isRanged;
    }

    public function relations()
    {
        return array(
            'attr'=>array(self::BELONGS_TO,'Attr','id_attr'),
            'parent_model'=>array(self::BELONGS_TO,'AttrModel','parent'),
            'ident'=>array(self::BELONGS_TO,'ModelIdent','id_model_ident'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'id_group_attr_set' => 'Атрибут',
            'required'=>'Обязательный / не обязательный',
            'id_model_ident' => 'Уникальный ключ модели',
        );
    }

    public function getRange(){
        if (!$this->_range)
        {
            $this->_range=$this->attr->range;
        }
        return $this->_range;
    }

    public function setRange($data){
        $this->_range=$data;
    }

    public function search()
    {
        $criteria=new CDbCriteria;
		$criteria->compare('id',$this->id);
		$criteria->compare('id_group_attr_set',$this->id_group_attr_set);
		$criteria->compare('id_model_ident',$this->id_model_ident);
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    public function beforeSave(){
        $this->alias=$this->attr->widget;
        return parent::beforeSave();
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

}
