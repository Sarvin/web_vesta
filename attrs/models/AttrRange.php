<?php

/**
* This is the model class for table "{{attr_range}}".
*
* The followings are the available columns in table '{{attr_range}}':
    * @property integer $id
    * @property integer $id_attr
    * @property string $values
*/
class AttrRange extends EActiveRecord
{
    public function tableName()
    {
        return '{{attr_range}}';
    }

    public function rules()
    {
        return array(
            array('id_attr', 'numerical', 'integerOnly'=>true),
            array('values', 'safe'),
            // The following rule is used by search().
            array('id, id_attr, values', 'safe', 'on'=>'search'),
        );
    }

    public function relations()
    {
        
        return array(
            'rangeValues'=>array(self::HAS_MANY,'RangeValue','id_range','order'=>'id'),
        );

    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'id_attr' => 'Комментарий',
            'values' => 'Коментарий',
        );
    }

    public function search()
    {
        $criteria=new CDbCriteria;
		$criteria->compare('id',$this->id);
		$criteria->compare('id_attr',$this->id_attr);
		$criteria->compare('values',$this->values,true);
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }


}
