(function($){
	$.fn.customSelect=function(options){
		var options=$.extend({
			color:null,
		},options)
		return this.each(function(){
			var base=$(this);
			$(this).simpleselect();
			if (options.color){
				var conteiner=$(this).closest('.control-group');
				$.each($('.option',conteiner),function(key,val){
					$(val).append('<div class="option-cl" style="background:#'+$('option',base).eq($(val).index()).val()+'">')	
				})
			}
		})
	}
})($)
function initdate(el){
	if (el!=undefined)
		$(el).datepicker();
	else 
		$('.date').datepicker();
}

function initstring(){
	
}

function initcolor_picker(el){
	if (el!=undefined)
		$(el).initColorPicker();
	else 
		$(".color").colpick({
			colorScheme:"dark",
			onChange:function(hsb,hex,rgb,el,bySetColor) {
			$(el).css("border-color","#"+hex);
			if(!bySetColor) 
			{
				$(el).val(hex);
				$(el).closest(".color-area").find("div").css("background", "#" + hex);
			}
		}}).keyup(function(){
			$(this).colpickSetColor(this.value);
		});
}

function initphone(el){
	// if (el!=undefined)
	// 	$(el).mask("+7-999-999-99-99",{placehoder:'*'});
	// else 
	// {
	// 	$('.phone').mask("+7-999-999-99-99",{placehoder:'*'});
	// }
}
function initselect(el){
	// if (el!=undefined)
	// 	$(el).customSelect();
	// else 
	// 	$('.eav select').customSelect({color:true});
}
function initAllWidgets(){
	initphone();
	initcolor_picker();
	initdate();
	initselect();
}