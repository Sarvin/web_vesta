﻿<html>
<head>
		<meta charset="utf-8" />
		<title></title>
		<!--[if IE]>
	    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	    <![endif]-->
	    <link rel="stylesheet" type="text/css" href="css/style.css">
	    <link rel="stylesheet" type="text/css" href="css/reset.css">
	    <link rel="stylesheet" type="text/css" href="css/jquery.fancybox.css">
	    <link rel="stylesheet" type="text/css" href="css/main.css">
	    <link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
	    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	    <script src="js/fancybox/jquery.fancybox.js"></script>
	    <script src="js/script.js"></script>
	    <script src="js/owl.carousel.js"></script>
	</head>
<body>
	<header class="menu_404 fix_width">
		<ul>
			<li>
				<a href="#">О компании</a>
			</li>
			<li>
				<a href="#">Публичная оферта</a>
			</li>
			<li>
				<a href="#">Как совершить покупку?</a>
			</li>
			<li>
				<a href="#">Доставка</a>
			</li>
			<li>
				<a href="#">Возврат</a>
			</li>
		</ul>
		<div class="social">
		</div>
	</header>
	<div class="wrap-404">
		<div class="fix_width">
			<p class="caption">Ошибка <strong>404</strong></p>
			<p class="desc">Такое бывает, не расстраивайтесь! Предлагаем Вам<br> перейти на следующие страницы:</p>
			<div class="pages">
				<a href="#">На главную</a>
				<a href="#">В каталог</a>
				<a href="#">В личный кабинет</a>
			</div>
			<img src="img/thinknes.png">
		</div>
	</div>
</body>
</html>