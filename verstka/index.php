﻿<!DOCTYPE html>
<html lang="ru">
	<head>
		<meta charset="utf-8" />
		<title></title>
		<!--[if IE]>
	    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	    <![endif]-->
	    <link rel="stylesheet" type="text/css" href="css/style.css">
	    <link rel="stylesheet" type="text/css" href="css/reset.css">
	    <link rel="stylesheet" type="text/css" href="css/jquery.fancybox.css">
	    <link rel="stylesheet" type="text/css" href="css/main.css">
	    <link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
	    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	    <script src="js/fancybox/jquery.fancybox.js"></script>
	    <script src="js/script.js"></script>
	    <script src="js/owl.carousel.js"></script>
	</head>
<body>
	<div class="shadow"></div>
	<div class="page-wrapper">
		<div class="set-user-town">
			<input type="checkbox" id="no">
			<div class="welcome-wrap">
				<input type="checkbox" id="welcome" checked>
				<div class="cover">
					<div class="welcome">
						<form action="#">
							<input type="checkbox" id="close">
							<label for="welcome"></label>
							<p class="caption">Добро пожаловать на сайт</p>
							<p class="desc">
								Мы определили ваш город как<br><strong>Москва</strong>. Все верно?
							</p>
							<div class="actions">
								<label for="welcome">Да</label>
								<label for="no">Нет</label>
							</div>
						</form>
					</div>
				</div>
			</div>
			<input type="checkbox" id="cities" class="dropDown">
			<div class="cover">
				<div class="choose-town">
					<form action="#">
						<label class="close" for="cities"></label>
						<p class="caption">Пожалуйста укажите свой город</p>

						<div class="search">
							<input type="text" value="" placeholder="Начните вводить название города">
						</div>
						<div class="get-location">
							<a href="#">Определить местоположение</a>
						</div>
						<div class="cities">
							<ul>
								<li><input type="radio" name="city" id="city_1"><label for="city_1">Москва</label></li>
								<li><input type="radio" name="city"><label for="city">Москва</label></li>
								<li><input type="radio" name="city"><label for="city">Москва</label></li>
								<li><input type="radio" name="city"><label for="city">Москва</label></li>
								<li><input type="radio" name="city"><label for="city">Москва</label></li>
								<li><input type="radio" name="city"><label for="city">Москва</label></li>
								<li><input type="radio" name="city"><label for="city">Москва</label></li>
								<li><input type="radio" name="city"><label for="city">Москва</label></li>
								<li><input type="radio" name="city"><label for="city">Москва</label></li>
								<li><input type="radio" name="city"><label for="city">Москва</label></li>
								<li><input type="radio" name="city"><label for="city">Москва</label></li>
								<li><input type="radio" name="city"><label for="city">Москва</label></li>
								<li><input type="radio" name="city"><label for="city">Москва</label></li>
								<li><input type="radio" name="city"><label for="city">Москва</label></li>
								<li><input type="radio" name="city"><label for="city">Москва</label></li>
								<li><input type="radio" name="city"><label for="city">Москва</label></li>
								<li><input type="radio" name="city"><label for="city">Москва</label></li>
								<li><input type="radio" name="city"><label for="city">Москва</label></li>
								<li><input type="radio" name="city"><label for="city">Москва</label></li>
								<li><input type="radio" name="city"><label for="city">Москва</label></li>
								<li><input type="radio" name="city"><label for="city">Москва</label></li>
								<li><input type="radio" name="city"><label for="city">Москва</label></li>
								<li><input type="radio" name="city"><label for="city">Москва</label></li>
								<li><input type="radio" name="city"><label for="city">Москва</label></li>
								<li><input type="radio" name="city"><label for="city">Москва</label></li>
								<li><input type="radio" name="city"><label for="city">Москва</label></li>
								<li><input type="radio" name="city"><label for="city">Москва</label></li>
							</ul>
						</div>
						<div class="actions">
							<label for="cities">
								ок
							</label>
						</div>
					</form>
				</div>
			</div>
		</div>
		<header>
			<div class="wrap-loc">
				<div class="fix_width">
					<div class="location">
						<span class="first">Ваше местонахождение:</span>
						
						<label for="cities"><span>Тюмень</span></label>
					</div>
					<div class="hot-line">
						<span class="caption">Телефон горячей линии Vesta Decor:</span>
						<span class="phone">8 800 200 00</span>
					</div>
				</div>
			</div>
			<div class="clr"></div>
			<div class="wrap-info">
				<div class="fix_width">
					<div class="logo">
						<a href="#" class="logo">
							<img src="img/head-logo.png" />
						</a>
					</div>
					<div class="shop-info">
						<p>Интернет-магазин готовых штор <strong>Vesta Decor</strong></p>
						<p class="delivery">С доставкой по России</p>
					</div>
					<div class="auth_reg">
						<a href="#" class="auth"><span>Вход</span></a>
						<span class="slash"></span>
						<a href="#" class="reg"><span>Регистрация</span></a>
					</div>
					<div class="clr"></div>
				</div>
			</div>
			<div class="clr"></div>
			<div class="wrap-search">
				<div class="fix_width">
					<div class="criteria">
						<input type="checkbox" id="all">
						<label for="all"><span>Все шторы<span></label>
						<!-- <a href="#" class="choice"><span>Все шторы</span></a> -->
						<div class="popup">
							<div class="arrow-top"></div>
							<div class="item">
								<p >Шторы по значению</p>
								<ul>
									<li><a href="#">для кухни</a></li>
									<li><a href="#">для детской</a></li>
									<li><a href="#">для спальни</a></li>
									<li><a href="#">для гостинной</a></li>
								</ul>
							</div>
							<div class="item">
								<p >Шторы по стилю</p>
								<ul>
									<li><a href="#">для кухни</a></li>
									<li><a href="#">для детской</a></li>
									<li><a href="#">для спальни</a></li>
									<li><a href="#">для гостинной</a></li>
								</ul>
							</div>
							<div class="item">
								<p >Шторы по цвету</p>
								<ul class="colors">
									<li><a href="#"><span></span><span>Желтые</span></a></li>
									<li><a href="#"><span></span><span>Зеленые</span></a></li>
									<li><a href="#"><span></span><span>Красные</span></a></li>
									<li><a href="#"><span></span><span>Розовые</span></a></li>
									<li><a href="#"><span></span><span>Белые</span></a></li>
									<li><a href="#"><span></span><span>Черный</span></a></li>
								</ul>
							</div>
							<div class="item">
								<p >Рисунок на ткани</p>
								<ul>
									<li><a href="#">Облака</a></li>
									<li><a href="#">Ангелы</a></li>
									<li><a href="#">3D</a></li>
									<li><a href="#">Круги</a></li>
								</ul>
							</div>
						</div>
						<div class="cover">
						</div>
					</div>
					<form class="search-query" action="/search">
						<input type="hidden" name="serachType" value="all">
						<input type="text" name="term"  id="search" placeholder="Че кого че ищем ?"/>
						<input type="submit" name="send" value=""/>
						<div class="example">
							<a href="#">Например: <span>шторы для гостинной</span></a>
						</div>
					</form>
					<div class="cart">
						<a href="/cart">Корзина<span class="a-ico"></span><span class="count">15</span></a>
					</div>
				</div>
			</div>
		</header>
		<div class="clr"></div>
		<div class="page-buffer">
			<div class="wrap-price-list">
				<div class="fix_width">
					<div class="links">
						<ul>
							<li>
								<a href="#">
									<img src="img/first.png"/>
									<p>Для спальни</p>
								</a>
							</li>
							<li>
								<a href="#">
									<img src="img/second.png"/>
									<p>Для кухни</p>
								</a>
							</li>
							<li>
								<a href="#">
									<img src="img/third.png"/>
									<p>Для гостинной</p>
								</a>
							</li>
							<li>
								<a href="#">
									<img src="img/forth.png"/>
									<p>Для детской</p>
								</a>
							</li>
						</ul>
					</div>
					<div class="slider">
						<div class="item">
							<a href="#">
								<img src="img/slide1.png" alt="" title=""/>
							</a>
						</div>
						<div class="item">
							<a href="#">
								<img src="img/slide1.png" alt="" title=""/>
							</a>
						</div>
						<div class="item">
							<a href="#">
								<img src="img/slide1.png" alt="" title=""/>
							</a>
						</div>
						<div class="item">
							<a href="#">
								<img src="img/slide1.png" alt="" title=""/>
							</a>
						</div>
						<div class="item">
							<a href="#">
								<img src="img/slide1.png" alt="" title=""/>
							</a>
						</div>
					</div>	
					<div class="clr"></div>
					<div class="sales">
						<div class="item">
							<p class="caption">
								Распродажа готовых штор
							</p>
							<p class="desc">
								Плотные ткани из коллекции Anabel представлены для вас, чтобы найти и удовлетворить свой...
							</p>
							<a href="#" class="more">
								<span>Смотреть</span>
							</a>
						</div>
						<div class="item">
							<p class="caption">
								Распродажа готовых штор
							</p>
							<p class="desc">
								Плотные ткани из коллекции Anabel представлены для вас, чтобы найти и удовлетворить свой...
							</p>
							<a href="#" class="more">
								<span>Смотреть</span>
							</a>
						</div>
						<div class="item">
							<p class="caption">
								Распродажа готовых штор
							</p>
							<p class="desc">
								Плотные ткани из коллекции Anabel представлены для вас, чтобы найти и удовлетворить свой...
							</p>
							<a href="#" class="more">
								<span>Смотреть</span>
							</a>
						</div>
					</div>
				</div>
			</div>
			<div class="clr"></div>
			<div class="wrap-deals">
				<div class="fix_width">
					<p class="caption">Лучшие предложения</p>
					<div class="deals-items">
						<div class="item">
							<a href="#">
								<img src="img/deal-item.png" />
								<span>Шторы Sunlight Pro</span>
							</a>
							<span class="price">
								25 000
							</span>
							<a href="" class="green-btn">
								В корзину
							</a>
						</div>
						<div class="item">
							<a href="#">
								<img src="img/deal-item.png" />
								<span>Шторы Sunlight Pro</span>
							</a>
							<span class="price">
								25 000
							</span>
							<a href="" class="green-btn">
								В корзину
							</a>
						</div>
						<div class="item">
							<a href="#">
								<img src="img/deal-item.png" />
								<span>Шторы Sunlight Pro</span>
							</a>
							<span class="price">
								25 000
							</span>
							<a href="" class="green-btn">
								В корзину
							</a>
						</div>
						<div class="item">
							<a href="#">
								<img src="img/deal-item.png" />
								<span>Шторы Sunlight Pro</span>
							</a>
							<span class="price">
								25 000
							</span>
							<a href="" class="green-btn">
								В корзину
							</a>
						</div>
						<div class="item">
							<a href="#">
								<img src="img/deal-item.png" />
								<span>Шторы Sunlight Pro</span>
							</a>
							<span class="price">
								25 000
							</span>
							<a href="" class="green-btn">
								В корзину
							</a>
						</div>
						<div class="item">
							<a href="#">
								<img src="img/deal-item.png" />
								<span>Шторы Sunlight Pro</span>
							</a>
							<span class="price">
								25 000
							</span>
							<a href="" class="green-btn">
								В корзину
							</a>
						</div>
						<div class="item">
							<a href="#">
								<img src="img/deal-item.png" />
								<span>Шторы Sunlight Pro</span>
							</a>
							<span class="price">
								25 000
							</span>
							<a href="" class="green-btn">
								В корзину
							</a>
						</div>
						<div class="item">
							<a href="#">
								<img src="img/deal-item.png" />
								<span>Шторы Sunlight Pro</span>
							</a>
							<span class="price">
								25 000
							</span>
							<a href="" class="green-btn">
								В корзину
							</a>
						</div>
					</div>
				</div>
			</div>
			<div class="clr"></div>
			<div class="wrap-about">
				<p class="caption"><strong>vesta decor</strong> это:</p>
				<div class="items">
					<div class="fix_width">
						<div class="item">
							<div class="info">
								<div class="not-hover">
									<img src="img/man.png">
									<p class="title">
										Доставка по всей России
									</p>
								</div>
								<div class="info-hover">
									Аппартаменты находятся на острове Паг, населенный пункт Шимуны, недалеко от...
 города Паг. Здание построено в 2012 г...
 Парковочное место на две машины, подсобное помещение в подвале. 
								</div>
							</div>
						</div>
						<div class="item">
							<div class="info">
								<div class="not-hover">
									<img src="img/warranty.png">
									<p class="title">
										Доставка по всей России
									</p>
								</div>
								<div class="info-hover">
									Аппартаменты находятся на острове Паг, населенный пункт Шимуны, недалеко от...
 города Паг. Здание построено в 2012 г...
 Парковочное место на две машины, подсобное помещение в подвале. 
								</div>
							</div>
						</div>
						<div class="item">
							<div class="info">
								<div class="not-hover">
									<img src="img/present-mail.png">
									<p class="title">
										Доставка по всей России
									</p>
								</div>
								<div class="info-hover">
									Аппартаменты находятся на острове Паг, населенный пункт Шимуны, недалеко от...
 города Паг. Здание построено в 2012 г...
 Парковочное место на две машины, подсобное помещение в подвале. 
								</div>	
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="clr"></div>
			<div class="wrap-menu">
				<div class="fix_width">
					<ul>
						<li>
							<a href="">О компании</a>
						</li>
						<li>
							<a href="">Публичная оферта</a>
						</li>
						<li>
							<a href="">Как совершить покупку?</a>
						</li>
						<li>
							<a href="">Доставка</a>
						</li>
						<li>
							<a href="">Возврат</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="clr"></div>
	</div>
	<div class="page-footer">
		<footer>
			<div class="fix_width">
				<div class="top">
					<div class="hot-line">
						<p>
							Телефон горячей линии Vesta Decor: 
						</p>
						<p class="phone">
							8 800 200 00 00
						</p>
						<p>
							* Для регионов звонок бесплатный
						</p>
					</div>
					<div class="question-info">
						<p>
							Мы всегда открыты для вопросов и предложений клиентов!
						</p>
						<a href="#" class="green-btn">Задать вопрос</a>
					</div>
					<div class="pay-ways">
						<p>
							Способы оплаты
						</p>
						<p class="desc">
							Вы можете оплатить покупки наличными 
							при получении, либо выбрать другой 
							способ оплаты.
						</p>
						<div class="pay-links">
							<a href="#">
								<img src="img/pay1.png" alt=""/>
							</a>
							<a href="#">
								<img src="img/pay2.png" alt=""/>
							</a>
							<a href="#">
								<img src="img/pay2.png" alt=""/>
							</a>
							<a href="#">
								<img src="img/pay4.png" alt=""/>
							</a>
							<a href="#">
								<img src="img/pay5.png" alt=""/>
							</a>
						</div>
					</div>
					<div class="clr"></div>
				</div>
				
				<div class="bot">
					<div class="company">
						* Vesta Deco 2000 - <?=date('Y')?>
					</div>
					<div class="developer">
						Разработка сайта: <a href="amobile-studio.ru"><img src="img/developer.png"></a>
					</div>
				</div>
			</div>
		</footer>
	</div>
</body>
</html>
