$(function(){
	$('.slider').owlCarousel({
		singleItem:true,
		pagination:true,
		items:1,
	})

	if ($('.cart .params .content').length || $('.cart-items .content').length)
	{
		$('.cart .params .content').tinyscrollbar();
		$('.cart-items .content').tinyscrollbar();
	}
	$('.modal').fancybox({
		closeBtn:false,
		fitToView:true,
		autoSize:true,
	})
	var win=$(window)
	var cart_items=$('.cart-items');
	if ($('.wrap-catalog .filter').length){
		var long_li=$('.wrap-catalog .filter .long li');

			long_li.on('click',function(){
				$('.wrap-catalog .filter .slide').slider('option','value',$(this).text());		
				$('#long').val($(this).text());
				$('.wrap-catalog .filter .slide').slider('instance');
			})

		$('.wrap-catalog .filter .slide').slider({
			min:0.5,
			max:2.5,
			step:0.5,
			stop:function(event,ui){
				long_li.removeAttr('style');
				long_li.eq(ui.value/0.5-1).css('color','#fff');
				$('#long').val(ui.value);
			}
		});
	}
	if (cart_items.length)
	{
		var scrollTop=cart_items.offset().top;
		var scrollBottom=$('.wrap-menu').offset().top-cart_items.height()-50;
		win.scroll(function(){
			if (win.scrollTop()>=scrollTop && win.scrollTop()<=scrollBottom)
				cart_items.css({position:'fixed',top:10});
			else {
				if (scrollBottom<win.scrollTop() && cart_items.css('position')=='fixed')
				{
					cart_items.css({position:'absolute',top:scrollBottom-275});
				}
				else 
				if (cart_items.css('position')=='fixed')
					cart_items.removeAttr('style');
			}
		})
	}
	$('.wrap-search .criteria').on('change',' #all',function(){
		var params={
			visibility:$(this).is(':checked') ? 'visible' : 'hidden',
			opacity:$(this).is(':checked') ? 1 : 0
		}
		//$('.shadow').css(params);
	})

})



