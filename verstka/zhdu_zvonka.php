﻿<!DOCTYPE html>
<html lang="ru">
	<head>
		<meta charset="utf-8" />
		<title></title>
		<!--[if IE]>
	    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	    <![endif]-->
	    <link rel="stylesheet" type="text/css" href="css/style.css">
	    <link rel="stylesheet" type="text/css" href="css/reset.css">
	    <link rel="stylesheet" type="text/css" href="css/jquery.fancybox.css">
	    <link rel="stylesheet" type="text/css" href="css/main.css">
	    <link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
	    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	    <script src="js/fancybox/jquery.fancybox.js"></script>
	    <script src="js/selectivizr.js.js"></script>
	    <script src="js/jquery.tinyscrollbar.js"></script>
	    <script src="js/script.js"></script>
	    <script src="js/owl.carousel.js"></script>
	</head>
<body>
	<div class="shadow"></div>
	<div class="page-wrapper">
		<header>
			<div class="wrap-loc">
				<div class="fix_width">
					<div class="location">
						<span class="first">Ваше местонахождение:</span>
						<a href="#" class="dropDown"><span>Тюмень</span></a>
					</div>
					<div class="hot-line">
						<span class="caption">Телефон горячей линии Vesta Decor:</span>
						<span class="phone">8 800 200 00</span>
					</div>

				</div>
			</div>
			<div class="clr"></div>
			<div class="wrap-info">
				<div class="fix_width">
					<div class="logo">
						<a href="#" class="logo">
							<img src="img/head-logo.png" />
						</a>
					</div>
					<div class="shop-info">
						<p>Интернет-магазин готовых штор <strong>Vesta Decor</strong></p>
						<p class="delivery">С доставкой по России</p>
					</div>
					<div class="auth_reg">
						<a href="#" class="auth"><span>Вход</span></a>
						<span class="slash"></span>
						<a href="#" class="reg"><span>Регистрация</span></a>
					</div>
					<div class="clr"></div>
				</div>
			</div>
			<div class="clr"></div>
			<div class="wrap-search">
				<div class="fix_width">
					<div class="criteria">
						<input type="checkbox" id="all">
						<label for="all"><span>Все шторы<span></label>
						<!-- <a href="#" class="choice"><span>Все шторы</span></a> -->
						<div class="popup">
							<div class="arrow-top"></div>
							<div class="item">
								<p >Шторы по значению</p>
								<ul>
									<li><a href="#">для кухни</a></li>
									<li><a href="#">для детской</a></li>
									<li><a href="#">для спальни</a></li>
									<li><a href="#">для гостинной</a></li>
								</ul>
							</div>
							<div class="item">
								<p >Шторы по стилю</p>
								<ul>
									<li><a href="#">для кухни</a></li>
									<li><a href="#">для детской</a></li>
									<li><a href="#">для спальни</a></li>
									<li><a href="#">для гостинной</a></li>
								</ul>
							</div>
							<div class="item">
								<p >Шторы по цвету</p>
								<ul class="colors">
									<li><a href="#"><span></span><span>Желтые</span></a></li>
									<li><a href="#"><span></span><span>Зеленые</span></a></li>
									<li><a href="#"><span></span><span>Красные</span></a></li>
									<li><a href="#"><span></span><span>Розовые</span></a></li>
									<li><a href="#"><span></span><span>Белые</span></a></li>
									<li><a href="#"><span></span><span>Черный</span></a></li>
								</ul>
							</div>
							<div class="item">
								<p >Рисунок на ткани</p>
								<ul>
									<li><a href="#">Облака</a></li>
									<li><a href="#">Ангелы</a></li>
									<li><a href="#">3D</a></li>
									<li><a href="#">Круги</a></li>
								</ul>
							</div>
						</div>
						<div class="cover">

						</div>
					</div>
					<form class="search-query" action="/search">
						<input type="hidden" name="serachType" value="all">
						<input type="text" name="term"  id="search" placeholder="Че кого че ищем ?"/>
						<input type="submit" name="send" value=""/>
						<div class="example">
							<a href="#">Например: <span>шторы для гостинной</span></a>
						</div>
					</form>
					<div class="cart">
						<a href="/cart">Корзина<span class="a-ico"></span><span class="count">15</span></a>
						<div class="items-wrap">
							<div class="params">
								<div class="content">
									<div class="scrollbar">
										<div class="track">
											<div class="thumb">
												<div class="end"></div>
											</div>
										</div>
									</div>
									<div class="viewport">
										<div class="overview">
											<div class="param">
												<div class="preview">
													<img src="img/cart-item.png" alt="">
												</div>
												<ul>
													<li>
														<span>Модель:</span>
														<span>Elit white shtora, Grande</span>
													</li>
													<li>
														<span>Тип:</span>
														<span>Ролет</span>
													</li>
													<li>
														<span>Размер:</span>
														<span>180 на 90</span>
													</li>
													<li>
														<span>Колличество:</span>
														<span>2 шт.</span>
													</li>
													<li>
														<span>Цена за 1м:</span>
														<span>1500 руб.</span>
													</li>
												</ul>
												<div class="remove">
													<label for="remove_1"></label>
													<input type="checkbox" id="remove_1">
												</div>
											</div>
											<div class="param">
												<div class="preview">
													<img src="img/cart-item.png" alt="">
												</div>
												<ul>
													<li>
														<span>Модель:</span>
														<span>Elit white shtora, Grande</span>
													</li>
													<li>
														<span>Тип:</span>
														<span>Ролет</span>
													</li>
													<li>
														<span>Размер:</span>
														<span>180 на 90</span>
													</li>
													<li>
														<span>Колличество:</span>
														<span>2 шт.</span>
													</li>
													<li>
														<span>Цена за 1м:</span>
														<span>1500 руб.</span>
													</li>
												</ul>
												<div class="remove">
													<label for="remove_1"></label>
													<input type="checkbox" id="remove_1">
												</div>
											</div>
											<div class="param">
												<div class="preview">
													<img src="img/cart-item.png" alt="">
												</div>
												<ul>
													<li>
														<span>Модель:</span>
														<span>Elit white shtora, Grande</span>
													</li>
													<li>
														<span>Тип:</span>
														<span>Ролет</span>
													</li>
													<li>
														<span>Размер:</span>
														<span>180 на 90</span>
													</li>
													<li>
														<span>Колличество:</span>
														<span>2 шт.</span>
													</li>
													<li>
														<span>Цена за 1м:</span>
														<span>1500 руб.</span>
													</li>
												</ul>
												<div class="remove">
													<label for="remove_1"></label>
													<input type="checkbox" id="remove_1">
												</div>
											</div>
											<div class="param">
												<div class="preview">
													<img src="img/cart-item.png" alt="">
												</div>
												<ul>
													<li>
														<span>Модель:</span>
														<span>Elit white shtora, Grande</span>
													</li>
													<li>
														<span>Тип:</span>
														<span>Ролет</span>
													</li>
													<li>
														<span>Размер:</span>
														<span>180 на 90</span>
													</li>
													<li>
														<span>Колличество:</span>
														<span>2 шт.</span>
													</li>
													<li>
														<span>Цена за 1м:</span>
														<span>1500 руб.</span>
													</li>
												</ul>
												<div class="remove">
													<label for="remove_2"></label>
													<input type="checkbox" id="remove_2">
												</div>
											</div>
											<div class="param">
												<div class="preview">
													<img src="img/cart-item.png" alt="">
												</div>
												<ul>
													<li>
														<span>Модель:</span>
														<span>Elit white shtora, Grande</span>
													</li>
													<li>
														<span>Тип:</span>
														<span>Ролет</span>
													</li>
													<li>
														<span>Размер:</span>
														<span>180 на 90</span>
													</li>
													<li>
														<span>Колличество:</span>
														<span>2 шт.</span>
													</li>
													<li>
														<span>Цена за 1м:</span>
														<span>1500 руб.</span>
													</li>
												</ul>
												<div class="remove">
													<label for="remove_3"></label>
													<input type="checkbox" id="remove_3">
												</div>
											</div>
										</div>
									</div>
								</div>
								<div style="display:block">
									<div class="into-cart">
										<span>К оплате <strong>4500</strong></span>
										<a href="#" class="green-btn">
											Перейти в корзину
										</a>
									</div>
								</div>
							</div>
							
						</div>
					</div>
					<div class="cover">
					</div>
				</div>
				
			</div>
			<div class="breadcrumbs fix_width">
				<a href="#">Главная</a>
				<span>›</span>
				<a href="#">Каталог</a>
				<span>›</span>
				<a href="#">Шторы для кухни</a>
				<span>›</span>
				<span>Штора White Decor Grande Nova</span>
			</div>
		</header>
		<div class="clr"></div>
		<div class="call-us">
			<input type="checkbox" id="close">
			<label for="close"></label>
			<p class="caption">Мы вам перезвоним!</p>
			<img src="img/phone-wait.png" alt="">
			<p class="desc">
				Укажите свой контактный телефон , и мы перзвоним Вам в ближайшие несколько минут:
			</p>
			<form action="#">
				<label for="phone" class="flag">+7</label><input type="text" id="phone">
				<input type="submit" value="жду звонка!" class="green-btn">
			</form>
		</div>
		<div class="page-buffer">
			<div class="fix_width">
				<div class="item-view">
					<div class="item-info">
						<div class="preview">
							<a href="#gallery" class="modal">
								<img src="img/item-view.png">
							</a>
							<a href="#" class="like">
								<span></span>
								<span></span>
							</a>
							<div class="like-item">
								<span>Понравился товар? Поделитесь им с друзьями!</span>
								<a href="#" class="f"></a>
								<a href="#" class="t"></a>
							</div>
						</div>
						<div class="params">
							<p class="caption">
								Штора White Deco Grande Nova Super Elite Tralala в две
	строки название модели
							</p>
							<div class="price">
								<span class="old">5000</span>
								<span class="current">3000</span>
							</div>
							<div class="articul">
								<p><strong>Артиукул товара:</strong>123125</p>
								<div class="find-out">
									<div class="delivery">
										<span></span>
										<a href="#" class="message">
											Узнать о доставке
										</a>
									</div>
									<div class="hz-che">
										<span></span>
										<a href="#" class="message">
											Узнать о доставке
										</a>
									</div>
									<div class="scissors">
										<span></span>
										<a href="#" class="message">
											Узнать о доставке
										</a>
									</div>
								</div>
							</div>
							<form class="sizes">
								<p class="caption"><strong>Доступные размеры</strong></p>
								<div class="heights">
									<p>Высота:</p>
									<input type="checkbox" id="height1" disabled>
									<label for="height1">190</label>
									<input type="checkbox" id="height2">
									<label for="height2" >250</label>
									<input type="checkbox" id="height3">
									<label for="height3" >280</label>
								</div>
								<div class="width">
									<p>Ширина:</p>
									<input type="checkbox" id="width1">
									<label for="width1">190</label>
									<input type="checkbox" id="width2">
									<label for="width2" >250</label>
								</div>
								<div class="own-size">
									<p>Свой размер:</p>
									<input type="text" id="own1">
									<input type="text" id="own2">
								</div>
								<div class="count">
									<label class="caption">Количество:</label> <input type="text" value="3"> шт.
								</div>
								<div class="cart">
									<a href="#" class="green-btn">В корзину</a>
									<a href="#" class="one-click">Купить в один клик</a>
								</div>
							</form>
							<p class="remeber">
							* Помните, положив товар в корзину - это Вас ничему не обязы-<br>
вает. Вы всегда можете отказаться от покупки.
							</p>
						</div>
					</div>
					<div class="dop-info">
						<div class="desc">
							<div class="item">
								<p class="caption">
									Описание
								</p>
								<p class="content">
									Стильная мужская сумка от Fossil. Модель выполнена из гладкой 
кожи коричневого цвета. Детали: клапан на кнопке; текстильная
подкладка; карман на молнии; накладной карман с держателями для
ручек; снаружи карман на молнии; несъемный регулируемый 
наплечный ремень.
								</p>
							</div>
							<div class="item">
								<p class="caption">
									Гарантия
								</p>
								<p class="content">
									Стильная мужская сумка от Fossil. Модель выполнена из гладкой 
кожи коричневого цвета. Детали: клапан на кнопке; текстильная
подкладка; карман на молнии; накладной карман с держателями для
ручек; снаружи карман на молнии; несъемный регулируемый 
наплечный ремень.
								</p>
							</div>
							<div class="item">
								<p class="caption">
									Относится
								</p>
								<div class="tags">
								<span>Стиль:</span><a href="#">Восточный,</a> <a href="#">Ретро</a>
									<span>Рисунок:</span><a href="#">3D,</a> <a href="#">Облака</a>
								</div>
								
							</div>
						</div>
						<div class="recomendation">
							<div class="item">
								<p class="caption">
									Рекомендации по уходу
								</p>
								<img src="img/30.png">
								<img src="img/utyug.png">
								<img src="img/treugolnik.png">
								<img src="img/strani-kvadrat.png">
							</div>
							<div class="item">
								<p class="caption">
									Характеристики
								</p>
								<ul>
									<li>
										<span>Тип</span>
										<span>Классические</span>
									</li>
									<li>
										<span>Тип</span>
										<span>Классические</span>
									</li>
									<li>
										<span>Тип</span>
										<span>Классические</span>
									</li>
									<li>
										<span>Тип</span>
										<span>Классические</span>
									</li>
									<li>
										<span>Тип</span>
										<span>Классические</span>
									</li>
									<li>
										<span>Тип</span>
										<span>Классические</span>
									</li>
									<li>
										<span>Тип</span>
										<span>Классические</span>
									</li>
									<li>
										<span>Тип</span>
										<span>Классические</span>
									</li>
									<li>
										<span>Тип</span>
										<span>Классические</span>
									</li>
									<li>
										<span>Тип</span>
										<span>Классические</span>
									</li>
								</ul>
							</div>
							<div class="item">
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="clr"></div>
			<div class="wrap-menu">
				<div class="fix_width">
					<ul>
						<li>
							<a href="">О компании</a>
						</li>
						<li>
							<a href="">Публичная оферта</a>
						</li>
						<li>
							<a href="">Как совершить покупку?</a>
						</li>
						<li>
							<a href="">Доставка</a>
						</li>
						<li>
							<a href="">Возврат</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="clr"></div>
	</div>
	<div class="page-footer">
		<footer>
			<div class="fix_width">
				<div class="top">
					<div class="hot-line">
						<p>
							Телефон горячей линии Vesta Decor: 
						</p>
						<p class="phone">
							8 800 200 00 00
						</p>
						<p>
							* Для регионов звонок бесплатный
						</p>
					</div>
					<div class="question-info">
						<p>
							Мы всегда открыты для вопросов и предложений клиентов!
						</p>
						<a href="#" class="green-btn">Задать вопрос</a>
					</div>
					<div class="pay-ways">
						<p>
							Способы оплаты
						</p>
						<p class="desc">
							Вы можете оплатить покупки наличными 
							при получении, либо выбрать другой 
							способ оплаты.
						</p>
						<div class="pay-links">
							<a href="#">
								<img src="img/pay1.png" alt=""/>
							</a>
							<a href="#">
								<img src="img/pay2.png" alt=""/>
							</a>
							<a href="#">
								<img src="img/pay2.png" alt=""/>
							</a>
							<a href="#">
								<img src="img/pay4.png" alt=""/>
							</a>
							<a href="#">
								<img src="img/pay5.png" alt=""/>
							</a>
						</div>
					</div>
					<div class="clr"></div>
				</div>
				
				<div class="bot">
					<div class="company">
						* Vesta Deco 2000 - <?=date('Y')?>
					</div>
					<div class="developer">
						Разработка сайта: <a href="amobile-studio.ru"><img src="img/developer.png"></a>
					</div>
				</div>
			</div>
		</footer>
	</div>
</body>
</html>
