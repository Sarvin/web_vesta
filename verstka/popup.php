<html lang="ru">
	<head>
		<meta charset="utf-8" />
		<title></title>
		<!--[if IE]>
	    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	    <![endif]-->
	    <link rel="stylesheet" type="text/css" href="css/style.css">
	    <link rel="stylesheet" type="text/css" href="css/reset.css">
	    <link rel="stylesheet" type="text/css" href="css/jquery.fancybox.css">
	    <link rel="stylesheet" type="text/css" href="css/main.css">
	    <link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
	    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	    <script src="js/jquery-ui.js"></script>
	    <script src="js/fancybox/jquery.fancybox.js"></script>
	    <script src="js/script.js"></script>
	    <script src="js/owl.carousel.js"></script>
	</head>
<body>
	<div class="popup">
		<p class="caption">Ваш товар добавлен в корзину!</p>
		<div class="data">
			<img src="img/popup.png" alt="">
			<div class="info">
				<ul>
					<li>
						<span>Модель:</span>123
					</li>
					<li>
						<span>Модель:</span>123
					</li>
					<li>
						<span>Модель:</span>123
					</li>
					<li>
						<span>Модель:</span>123
					</li>
					<li>
						<span>Модель:</span>123
					</li>
				</ul>
				<hr>
				К оплате <span class="price">4500</span><br>
				<div class="actions">
					<a href="/cart" class="green-btn">Перейти к оплате</a>
					<a href="#" class="go-on">Продолжить покупки</a>
				</div>
			</div>
			
		</div>
	</div>
	<div class="page-wrapper">
		<header>
			<div class="wrap-loc">
				<div class="fix_width">
					<div class="location">
						<span class="first">Ваше местонахождение:</span>
						<a href="#" class="dropDown"><span>Тюмень</span></a>
					</div>
					<div class="hot-line">
						<span class="caption">Телефон горячей линии Vesta Decor:</span>
						<span class="phone">8 800 200 00</span>
					</div>

				</div>
			</div>
			<div class="clr"></div>
			<div class="wrap-info">
				<div class="fix_width">
					<div class="logo">
						<a href="#" class="logo">
							<img src="img/head-logo.png" />
						</a>
					</div>
					<div class="shop-info">
						<p>Интернет-магазин готовых штор <strong>Vesta Decor</strong></p>
						<p class="delivery">С доставкой по России</p>
					</div>
					<div class="user-info">
						<input type="checkbox" id="name">
						<div class="cover"></div>
						<div class="content">
							<div class="favorite">
								<a href="#">
									<span>Мое избранное</span><span class="slesh">|</span>
								</a>
							</div>
							<div class="auth_as">
								<span class="as"><strong>Вы вошли</strong> как</span>	
								<label for="name" class="name"><span>Леонид</span></label>
								<div class="triangle"></div>
								<ul class="dropDown">
									<li>
										<span>Дарова, Мазепа!!</span>
									</li>
									<li>
										<a href="#">Мои заказы</a>
									</li>
									<li>
										<a href="#">Мои Данные</a>
									</li>
								</ul>
								<a href="#" class="logout"><span>Выйти</span></a>
							</div>
						</div>
					</div>
					<!-- <div class="auth_reg">
						<a href="#" class="auth"><span>Вход</span></a>
						<span class="slash"></span>
						<a href="#" class="reg"><span>Регистрация</span></a>
					</div> -->
					<div class="clr"></div>
				</div>
			</div>
			<div class="clr"></div>
			<div class="wrap-search">
				<div class="fix_width">
					<div class="criteria">
						<input type="checkbox" id="all">
						<label for="all"><span>Все шторы<span></label>
						<!-- <a href="#" class="choice"><span>Все шторы</span></a> -->
						<div class="popup">
							<div class="arrow-top"></div>
							<div class="item">
								<p >Шторы по значению</p>
								<ul>
									<li><a href="#">для кухни</a></li>
									<li><a href="#">для детской</a></li>
									<li><a href="#">для спальни</a></li>
									<li><a href="#">для гостинной</a></li>
								</ul>
							</div>
							<div class="item">
								<p >Шторы по стилю</p>
								<ul>
									<li><a href="#">для кухни</a></li>
									<li><a href="#">для детской</a></li>
									<li><a href="#">для спальни</a></li>
									<li><a href="#">для гостинной</a></li>
								</ul>
							</div>
							<div class="item">
								<p >Шторы по цвету</p>
								<ul class="colors">
									<li><a href="#"><span></span><span>Желтые</span></a></li>
									<li><a href="#"><span></span><span>Зеленые</span></a></li>
									<li><a href="#"><span></span><span>Красные</span></a></li>
									<li><a href="#"><span></span><span>Розовые</span></a></li>
									<li><a href="#"><span></span><span>Белые</span></a></li>
									<li><a href="#"><span></span><span>Черный</span></a></li>
								</ul>
							</div>
							<div class="item">
								<p >Рисунок на ткани</p>
								<ul>
									<li><a href="#">Облака</a></li>
									<li><a href="#">Ангелы</a></li>
									<li><a href="#">3D</a></li>
									<li><a href="#">Круги</a></li>
								</ul>
							</div>
						</div>
					</div>
					<form class="search-query" action="/search">
						<input type="hidden" name="serachType" value="all">
						<input type="text" name="term"  id="search" placeholder="Че кого че ищем ?"/>
						<input type="submit" name="send" value=""/>
						<div class="example">
							<a href="#">Например: <span>шторы для гостинной</span></a>
						</div>
					</form>
					<div class="cart">
						<a href="/cart">Корзина<span class="a-ico"></span><span class="count">15</span></a>
					</div>
				</div>
			</div>
			<div class="breadcrumbs fix_width">
				<a href="#">Главная</a>
				<span>›</span>
				<span>Корзина</span>
			</div>
		</header>
		<div class="clr"></div>
		<div class="page-buffer">
			<div class="wrap-catalog">
				<div class="fix_width">
					<div class="blocks">
						<div class="filter">
							<p class="caption">Полезный фильтр по шторам</p>
							<form action="#">
								<p class="title">
									Форма
								</p>
								<div class="size">
									<input type="checkbox" id="1">
									<label for="1">Длинные</label>
									<input type="checkbox" id="2">
									<label for="2">Длинные</label>
									<input type="checkbox" id="3">
									<label for="3">Длинные</label>
									<input type="checkbox" id="4">
									<label for="4">Длинные</label>
								</div>
								<div class="long">
									<p class="title">Шторы</p>
									<input type="text" id="long" value="0">
									<div class="slide"></div>
									<ul>
										<li style="color:#fff">0,5</li>
										<li>1</li>
										<li>1,5</li>
										<li>2</li>
										<li>2,5</li>
									</ul>
								</div>
								<div class="colors">
									<p class="title">Цвета</p>
									<input type="checkbox" id="cl1">
									<label for="cl1" style="background:#d58ecc"><span></span></label>
									<input type="checkbox" id="cl2">
									<label style="background:#6cc31f" for="cl2"><span></span></label>
									<input type="checkbox" id="cl3">
									<label style="background:#dfefec" for="cl3"><span></span></label>
									<input type="checkbox" id="cl4">
									<label style="background:#fff" for="cl4"><span></span></label>
									<input type="checkbox" id="cl5">
									<label style="background:#93afc1" for="cl5"><span></span></label>
									<input type="checkbox" id="cl6">
									<label style="background:#e0d924" for="cl6"><span></span></label>
								</div>
								<div class="places">
									<p class="title">Цвет</p>
									<input type="checkbox" id="1">
									<label for="1">Для гостинной</label>
									<input type="checkbox" id="2">
									<label for="2">Для спальни</label>
									<input type="checkbox" id="3">
									<label for="3">Для ванной</label>
									<input type="checkbox" id="4">
									<label for="4">Для детской</label>
									<input type="checkbox" id="1">
									<label for="1">Для кухни</label>
									<input type="checkbox" id="2">
								</div>
								<div class="actions">
									<input type="submit" id="sub" value="Применить фильтр" class="green-btn">
									<input type="reset" id="sub" value="Cбросить фильтр" >
								</div>
							</form>
						</div>
						<div class="content">
							<div class="info">
								<p class="title">Лучшие шторы для детской</p>
								<p class="desc">Незаменимым атрибутом любой квартиры или дома, несомненно, являются шторы. Они не 	только защищают помещения от яркого дневного света и от посторонних взглядов, но и украшают окна, придавая им особый шарм. Мы предлагаем купить шторы для кухни.
								</p>
								<div class="fast-filter">
									<a href="#">
										До 1 500 руб.
									</a>
									<a href="#">
										До 2 500 руб.
									</a>
									<a href="#">
										До 3 500 руб.
									</a>
									<a href="#">
										До 5 500 руб.
									</a>
								</div>
							</div>
							<div class="pager">
								<ul>
									<li>
										<a href="#">Первая</a>
									</li>
									<li>
										<a href="#">Предыдущая</a>
									</li>
									<li class="active">
										<a href="#">1</a>
									</li>
									<li>
										<a href="#">1</a>
									</li>
									<li>
										<a href="#">1</a>
									</li>
									<li>
										<a href="#">1</a>
									</li>
									<li>
										<a href="#">1</a>
									</li>
									<li>
										<a href="#">1</a>
									</li>
									<li>
										<a href="#">Следующая</a>
									</li>
									<li>
										<a href="#">Последняя</a>
									</li>
									
								</ul>
							</div>
							<div class="catalog-items">
						<div class="item">
							<div class="content-hover">
										<a href="#">
											<img src="img/deal-item.png">
											<span>Шторы Sunlight Pro</span>
										</a>
										<span class="price">
											25 000
										</span>
										<a href="" class="green-btn">
											В корзину
										</a>
										<div class="sizes force-choose">
											<div class="width">
												<p>Ширина:</p>
												<input type="checkbox" id="width1">
												<label for="width1">190</label>
												<input type="checkbox" id="width2">
												<label for="width2">250</label>
											</div>
											<div class="own-size">
												<p>Свой размер:</p>
												<input type="text" id="own1">
												<input type="text" id="own2">
											</div>
											<div class="count">
												<label class="caption">Количество:</label> <input type="text" value="3"> шт.
											</div>
										</div>
										<div class="desc">
											* Точная стоимость будет указана после выбора размера
										</div>
										<form class="sizes">
											<p class="caption"><strong>Доступные размеры</strong></p>
											<div class="heights">
												<p>Высота:</p>
												<input type="checkbox" id="height1" disabled="">
												<label for="height1">190</label>
												<input type="checkbox" id="height2">
												<label for="height2">250</label>
												<input type="checkbox" id="height3">
												<label for="height3">280</label>
											</div>
											<div class="width">
												<p>Ширина:</p>
												<input type="checkbox" id="width1">
												<label for="width1">190</label>
												<input type="checkbox" id="width2">
												<label for="width2">250</label>
											</div>
											<div class="own-size">
												<p>Свой размер:</p>
												<input type="text" id="own1">
												<input type="text" id="own2">
											</div>
											<div class="count">
												<label class="caption">Количество:</label> <input type="text" value="3"> шт.
											</div>
											<div class="cart">
												<a href="#" class="one-click">Подробнее</a>
											</div>
										</form>
									</div>
						</div>
						<div class="item">
							<div class="content-hover">
										<a href="#">
											<img src="img/deal-item.png">
											<span>Шторы Sunlight Pro</span>
										</a>
										<span class="price">
											25 000
										</span>
										<a href="" class="green-btn">
											В корзину
										</a>
										<div class="sizes force-choose">
											<div class="width">
												<p>Ширина:</p>
												<input type="checkbox" id="width1">
												<label for="width1">190</label>
												<input type="checkbox" id="width2">
												<label for="width2">250</label>
											</div>
											<div class="own-size">
												<p>Свой размер:</p>
												<input type="text" id="own1">
												<input type="text" id="own2">
											</div>
											<div class="count">
												<label class="caption">Количество:</label> <input type="text" value="3"> шт.
											</div>
										</div>
										<div class="desc">
											* Точная стоимость будет указана после выбора размера
										</div>
										<form class="sizes">
											<p class="caption"><strong>Доступные размеры</strong></p>
											<div class="heights">
												<p>Высота:</p>
												<input type="checkbox" id="height1" disabled="">
												<label for="height1">190</label>
												<input type="checkbox" id="height2">
												<label for="height2">250</label>
												<input type="checkbox" id="height3">
												<label for="height3">280</label>
											</div>
											<div class="width">
												<p>Ширина:</p>
												<input type="checkbox" id="width1">
												<label for="width1">190</label>
												<input type="checkbox" id="width2">
												<label for="width2">250</label>
											</div>
											<div class="own-size">
												<p>Свой размер:</p>
												<input type="text" id="own1">
												<input type="text" id="own2">
											</div>
											<div class="count">
												<label class="caption">Количество:</label> <input type="text" value="3"> шт.
											</div>
											<div class="cart">
												<a href="#" class="one-click">Подробнее</a>
											</div>
										</form>
									</div>
						</div>
						<div class="item">
							<div class="content-hover">
										<a href="#">
											<img src="img/deal-item.png">
											<span>Шторы Sunlight Pro</span>
										</a>
										<span class="price">
											25 000
										</span>
										<a href="" class="green-btn">
											В корзину
										</a>
										<div class="sizes force-choose">
											<div class="width">
												<p>Ширина:</p>
												<input type="checkbox" id="width1">
												<label for="width1">190</label>
												<input type="checkbox" id="width2">
												<label for="width2">250</label>
											</div>
											<div class="own-size">
												<p>Свой размер:</p>
												<input type="text" id="own1">
												<input type="text" id="own2">
											</div>
											<div class="count">
												<label class="caption">Количество:</label> <input type="text" value="3"> шт.
											</div>
										</div>
										<div class="desc">
											* Точная стоимость будет указана после выбора размера
										</div>
										<form class="sizes">
											<p class="caption"><strong>Доступные размеры</strong></p>
											<div class="heights">
												<p>Высота:</p>
												<input type="checkbox" id="height1" disabled="">
												<label for="height1">190</label>
												<input type="checkbox" id="height2">
												<label for="height2">250</label>
												<input type="checkbox" id="height3">
												<label for="height3">280</label>
											</div>
											<div class="width">
												<p>Ширина:</p>
												<input type="checkbox" id="width1">
												<label for="width1">190</label>
												<input type="checkbox" id="width2">
												<label for="width2">250</label>
											</div>
											<div class="own-size">
												<p>Свой размер:</p>
												<input type="text" id="own1">
												<input type="text" id="own2">
											</div>
											<div class="count">
												<label class="caption">Количество:</label> <input type="text" value="3"> шт.
											</div>
											<div class="cart">
												<a href="#" class="one-click">Подробнее</a>
											</div>
										</form>
									</div>
						</div>
						<div class="item">
							<div class="content-hover">
										<a href="#">
											<img src="img/deal-item.png">
											<span>Шторы Sunlight Pro</span>
										</a>
										<span class="price">
											25 000
										</span>
										<a href="" class="green-btn">
											В корзину
										</a>
										<div class="sizes force-choose">
											<div class="width">
												<p>Ширина:</p>
												<input type="checkbox" id="width1">
												<label for="width1">190</label>
												<input type="checkbox" id="width2">
												<label for="width2">250</label>
											</div>
											<div class="own-size">
												<p>Свой размер:</p>
												<input type="text" id="own1">
												<input type="text" id="own2">
											</div>
											<div class="count">
												<label class="caption">Количество:</label> <input type="text" value="3"> шт.
											</div>
										</div>
										<div class="desc">
											* Точная стоимость будет указана после выбора размера
										</div>
										<form class="sizes">
											<p class="caption"><strong>Доступные размеры</strong></p>
											<div class="heights">
												<p>Высота:</p>
												<input type="checkbox" id="height1" disabled="">
												<label for="height1">190</label>
												<input type="checkbox" id="height2">
												<label for="height2">250</label>
												<input type="checkbox" id="height3">
												<label for="height3">280</label>
											</div>
											<div class="width">
												<p>Ширина:</p>
												<input type="checkbox" id="width1">
												<label for="width1">190</label>
												<input type="checkbox" id="width2">
												<label for="width2">250</label>
											</div>
											<div class="own-size">
												<p>Свой размер:</p>
												<input type="text" id="own1">
												<input type="text" id="own2">
											</div>
											<div class="count">
												<label class="caption">Количество:</label> <input type="text" value="3"> шт.
											</div>
											<div class="cart">
												<a href="#" class="one-click">Подробнее</a>
											</div>
										</form>
									</div>
						</div>
						<div class="item">
							<div class="content-hover">
										<a href="#">
											<img src="img/deal-item.png">
											<span>Шторы Sunlight Pro</span>
										</a>
										<span class="price">
											25 000
										</span>
										<a href="" class="green-btn">
											В корзину
										</a>
										<div class="sizes force-choose">
											<div class="width">
												<p>Ширина:</p>
												<input type="checkbox" id="width1">
												<label for="width1">190</label>
												<input type="checkbox" id="width2">
												<label for="width2">250</label>
											</div>
											<div class="own-size">
												<p>Свой размер:</p>
												<input type="text" id="own1">
												<input type="text" id="own2">
											</div>
											<div class="count">
												<label class="caption">Количество:</label> <input type="text" value="3"> шт.
											</div>
										</div>
										<div class="desc">
											* Точная стоимость будет указана после выбора размера
										</div>
										<form class="sizes">
											<p class="caption"><strong>Доступные размеры</strong></p>
											<div class="heights">
												<p>Высота:</p>
												<input type="checkbox" id="height1" disabled="">
												<label for="height1">190</label>
												<input type="checkbox" id="height2">
												<label for="height2">250</label>
												<input type="checkbox" id="height3">
												<label for="height3">280</label>
											</div>
											<div class="width">
												<p>Ширина:</p>
												<input type="checkbox" id="width1">
												<label for="width1">190</label>
												<input type="checkbox" id="width2">
												<label for="width2">250</label>
											</div>
											<div class="own-size">
												<p>Свой размер:</p>
												<input type="text" id="own1">
												<input type="text" id="own2">
											</div>
											<div class="count">
												<label class="caption">Количество:</label> <input type="text" value="3"> шт.
											</div>
											<div class="cart">
												<a href="#" class="one-click">Подробнее</a>
											</div>
										</form>
									</div>
						</div>
						<div class="item">
							<div class="content-hover">
										<a href="#">
											<img src="img/deal-item.png">
											<span>Шторы Sunlight Pro</span>
										</a>
										<span class="price">
											25 000
										</span>
										<a href="" class="green-btn">
											В корзину
										</a>
										<div class="sizes force-choose">
											<div class="width">
												<p>Ширина:</p>
												<input type="checkbox" id="width1">
												<label for="width1">190</label>
												<input type="checkbox" id="width2">
												<label for="width2">250</label>
											</div>
											<div class="own-size">
												<p>Свой размер:</p>
												<input type="text" id="own1">
												<input type="text" id="own2">
											</div>
											<div class="count">
												<label class="caption">Количество:</label> <input type="text" value="3"> шт.
											</div>
										</div>
										<div class="desc">
											* Точная стоимость будет указана после выбора размера
										</div>
										<form class="sizes">
											<p class="caption"><strong>Доступные размеры</strong></p>
											<div class="heights">
												<p>Высота:</p>
												<input type="checkbox" id="height1" disabled="">
												<label for="height1">190</label>
												<input type="checkbox" id="height2">
												<label for="height2">250</label>
												<input type="checkbox" id="height3">
												<label for="height3">280</label>
											</div>
											<div class="width">
												<p>Ширина:</p>
												<input type="checkbox" id="width1">
												<label for="width1">190</label>
												<input type="checkbox" id="width2">
												<label for="width2">250</label>
											</div>
											<div class="own-size">
												<p>Свой размер:</p>
												<input type="text" id="own1">
												<input type="text" id="own2">
											</div>
											<div class="count">
												<label class="caption">Количество:</label> <input type="text" value="3"> шт.
											</div>
											<div class="cart">
												<a href="#" class="one-click">Подробнее</a>
											</div>
										</form>
									</div>
						</div>
						<div class="item">
							<div class="content-hover">
										<a href="#">
											<img src="img/deal-item.png">
											<span>Шторы Sunlight Pro</span>
										</a>
										<span class="price">
											25 000
										</span>
										<a href="" class="green-btn">
											В корзину
										</a>
										<div class="sizes force-choose">
											<div class="width">
												<p>Ширина:</p>
												<input type="checkbox" id="width1">
												<label for="width1">190</label>
												<input type="checkbox" id="width2">
												<label for="width2">250</label>
											</div>
											<div class="own-size">
												<p>Свой размер:</p>
												<input type="text" id="own1">
												<input type="text" id="own2">
											</div>
											<div class="count">
												<label class="caption">Количество:</label> <input type="text" value="3"> шт.
											</div>
										</div>
										<div class="desc">
											* Точная стоимость будет указана после выбора размера
										</div>
										<form class="sizes">
											<p class="caption"><strong>Доступные размеры</strong></p>
											<div class="heights">
												<p>Высота:</p>
												<input type="checkbox" id="height1" disabled="">
												<label for="height1">190</label>
												<input type="checkbox" id="height2">
												<label for="height2">250</label>
												<input type="checkbox" id="height3">
												<label for="height3">280</label>
											</div>
											<div class="width">
												<p>Ширина:</p>
												<input type="checkbox" id="width1">
												<label for="width1">190</label>
												<input type="checkbox" id="width2">
												<label for="width2">250</label>
											</div>
											<div class="own-size">
												<p>Свой размер:</p>
												<input type="text" id="own1">
												<input type="text" id="own2">
											</div>
											<div class="count">
												<label class="caption">Количество:</label> <input type="text" value="3"> шт.
											</div>
											<div class="cart">
												<a href="#" class="one-click">Подробнее</a>
											</div>
										</form>
									</div>
						</div>
						<div class="item">
							<div class="content-hover">
										<a href="#">
											<img src="img/deal-item.png">
											<span>Шторы Sunlight Pro</span>
										</a>
										<span class="price">
											25 000
										</span>
										<a href="" class="green-btn">
											В корзину
										</a>
										<div class="sizes force-choose">
											<div class="width">
												<p>Ширина:</p>
												<input type="checkbox" id="width1">
												<label for="width1">190</label>
												<input type="checkbox" id="width2">
												<label for="width2">250</label>
											</div>
											<div class="own-size">
												<p>Свой размер:</p>
												<input type="text" id="own1">
												<input type="text" id="own2">
											</div>
											<div class="count">
												<label class="caption">Количество:</label> <input type="text" value="3"> шт.
											</div>
										</div>
										<div class="desc">
											* Точная стоимость будет указана после выбора размера
										</div>
										<form class="sizes">
											<p class="caption"><strong>Доступные размеры</strong></p>
											<div class="heights">
												<p>Высота:</p>
												<input type="checkbox" id="height1" disabled="">
												<label for="height1">190</label>
												<input type="checkbox" id="height2">
												<label for="height2">250</label>
												<input type="checkbox" id="height3">
												<label for="height3">280</label>
											</div>
											<div class="width">
												<p>Ширина:</p>
												<input type="checkbox" id="width1">
												<label for="width1">190</label>
												<input type="checkbox" id="width2">
												<label for="width2">250</label>
											</div>
											<div class="own-size">
												<p>Свой размер:</p>
												<input type="text" id="own1">
												<input type="text" id="own2">
											</div>
											<div class="count">
												<label class="caption">Количество:</label> <input type="text" value="3"> шт.
											</div>
											<div class="cart">
												<a href="#" class="one-click">Подробнее</a>
											</div>
										</form>
									</div>
						</div>
						<div class="item">
							<div class="content-hover">
										<a href="#">
											<img src="img/deal-item.png">
											<span>Шторы Sunlight Pro</span>
										</a>
										<span class="price">
											25 000
										</span>
										<a href="" class="green-btn">
											В корзину
										</a>
										<div class="sizes force-choose">
											<div class="width">
												<p>Ширина:</p>
												<input type="checkbox" id="width1">
												<label for="width1">190</label>
												<input type="checkbox" id="width2">
												<label for="width2">250</label>
											</div>
											<div class="own-size">
												<p>Свой размер:</p>
												<input type="text" id="own1">
												<input type="text" id="own2">
											</div>
											<div class="count">
												<label class="caption">Количество:</label> <input type="text" value="3"> шт.
											</div>
										</div>
										<div class="desc">
											* Точная стоимость будет указана после выбора размера
										</div>
										<form class="sizes">
											<p class="caption"><strong>Доступные размеры</strong></p>
											<div class="heights">
												<p>Высота:</p>
												<input type="checkbox" id="height1" disabled="">
												<label for="height1">190</label>
												<input type="checkbox" id="height2">
												<label for="height2">250</label>
												<input type="checkbox" id="height3">
												<label for="height3">280</label>
											</div>
											<div class="width">
												<p>Ширина:</p>
												<input type="checkbox" id="width1">
												<label for="width1">190</label>
												<input type="checkbox" id="width2">
												<label for="width2">250</label>
											</div>
											<div class="own-size">
												<p>Свой размер:</p>
												<input type="text" id="own1">
												<input type="text" id="own2">
											</div>
											<div class="count">
												<label class="caption">Количество:</label> <input type="text" value="3"> шт.
											</div>
											<div class="cart">
												<a href="#" class="one-click">Подробнее</a>
											</div>
										</form>
									</div>
						</div>
						<div class="item">
							<div class="content-hover">
										<a href="#">
											<img src="img/deal-item.png">
											<span>Шторы Sunlight Pro</span>
										</a>
										<span class="price">
											25 000
										</span>
										<a href="" class="green-btn">
											В корзину
										</a>
										<div class="sizes force-choose">
											<div class="width">
												<p>Ширина:</p>
												<input type="checkbox" id="width1">
												<label for="width1">190</label>
												<input type="checkbox" id="width2">
												<label for="width2">250</label>
											</div>
											<div class="own-size">
												<p>Свой размер:</p>
												<input type="text" id="own1">
												<input type="text" id="own2">
											</div>
											<div class="count">
												<label class="caption">Количество:</label> <input type="text" value="3"> шт.
											</div>
										</div>
										<div class="desc">
											* Точная стоимость будет указана после выбора размера
										</div>
										<form class="sizes">
											<p class="caption"><strong>Доступные размеры</strong></p>
											<div class="heights">
												<p>Высота:</p>
												<input type="checkbox" id="height1" disabled="">
												<label for="height1">190</label>
												<input type="checkbox" id="height2">
												<label for="height2">250</label>
												<input type="checkbox" id="height3">
												<label for="height3">280</label>
											</div>
											<div class="width">
												<p>Ширина:</p>
												<input type="checkbox" id="width1">
												<label for="width1">190</label>
												<input type="checkbox" id="width2">
												<label for="width2">250</label>
											</div>
											<div class="own-size">
												<p>Свой размер:</p>
												<input type="text" id="own1">
												<input type="text" id="own2">
											</div>
											<div class="count">
												<label class="caption">Количество:</label> <input type="text" value="3"> шт.
											</div>
											<div class="cart">
												<a href="#" class="one-click">Подробнее</a>
											</div>
										</form>
									</div>
						</div>
						<div class="item">
							<div class="content-hover">
										<a href="#">
											<img src="img/deal-item.png">
											<span>Шторы Sunlight Pro</span>
										</a>
										<span class="price">
											25 000
										</span>
										<a href="" class="green-btn">
											В корзину
										</a>
										<div class="sizes force-choose">
											<div class="width">
												<p>Ширина:</p>
												<input type="checkbox" id="width1">
												<label for="width1">190</label>
												<input type="checkbox" id="width2">
												<label for="width2">250</label>
											</div>
											<div class="heights">
												<p>Высота:</p>
												<input type="checkbox" id="height1" disabled="">
												<label for="height1">190</label>
												<input type="checkbox" id="height2">
												<label for="height2">250</label>
												<input type="checkbox" id="height3">
												<label for="height3">280</label>
											</div>
											<div class="own-size">
												<p>Свой размер:</p>
												<input type="text" id="own1">
												<input type="text" id="own2">
											</div>
											<div class="count">
												<label class="caption">Количество:</label> <input type="text" value="3"> шт.
											</div>
										</div>
										<div class="desc">
											* Точная стоимость будет указана после выбора размера
										</div>
										<form class="sizes">
											<p class="caption"><strong>Доступные размеры</strong></p>
											<div class="heights">
												<p>Высота:</p>
												<input type="checkbox" id="height1" disabled="">
												<label for="height1">190</label>
												<input type="checkbox" id="height2">
												<label for="height2">250</label>
												<input type="checkbox" id="height3">
												<label for="height3">280</label>
											</div>
											<div class="width">
												<p>Ширина:</p>
												<input type="checkbox" id="width1">
												<label for="width1">190</label>
												<input type="checkbox" id="width2">
												<label for="width2">250</label>
											</div>
											<div class="own-size">
												<p>Свой размер:</p>
												<input type="text" id="own1">
												<input type="text" id="own2">
											</div>
											<div class="count">
												<label class="caption">Количество:</label> <input type="text" value="3"> шт.
											</div>
											<div class="cart">
												<a href="#" class="one-click">Подробнее</a>
											</div>
										</form>
									</div>
						</div>
						<div class="item">
							
						</div>
					</div>
							<div class="pager">
								<ul>
									<li>
										<a href="#">Первая</a>
									</li>
									<li>
										<a href="#">Предыдущая</a>
									</li>
									<li class="active">
										<a href="#">1</a>
									</li>
									<li>
										<a href="#">1</a>
									</li>
									<li>
										<a href="#">1</a>
									</li>
									<li>
										<a href="#">1</a>
									</li>
									<li>
										<a href="#">1</a>
									</li>
									<li>
										<a href="#">1</a>
									</li>
									<li>
										<a href="#">Следующая</a>
									</li>
									<li>
										<a href="#">Последняя</a>
									</li>
									
								</ul>
							</div>
						</div>
					</div>
				</div>
				</div>
			</div>
		</div>
		<div class="clr"></div>
			<div class="wrap-menu">
				<div class="fix_width">
					<ul>
						<li>
							<a href="">О компании</a>
						</li>
						<li>
							<a href="">Публичная оферта</a>
						</li>
						<li>
							<a href="">Как совершить покупку?</a>
						</li>
						<li>
							<a href="">Доставка</a>
						</li>
						<li>
							<a href="">Возврат</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="clr"></div>
	</div>
	<div class="page-footer">
		<footer>
			<div class="fix_width">
				<div class="top">
					<div class="hot-line">
						<p>
							Телефон горячей линии Vesta Decor: 
						</p>
						<p class="phone">
							8 800 200 00 00
						</p>
						<p>
							* Для регионов звонок бесплатный
						</p>
					</div>
					<div class="question-info">
						<p>
							Мы всегда открыты для вопросов и предложений клиентов!
						</p>
						<a href="#" class="green-btn">Задать вопрос</a>
					</div>
					<div class="pay-ways">
						<p>
							Способы оплаты
						</p>
						<p class="desc">
							Вы можете оплатить покупки наличными 
							при получении, либо выбрать другой 
							способ оплаты.
						</p>
						<div class="pay-links">
							<a href="#">
								<img src="img/pay1.png" alt=""/>
							</a>
							<a href="#">
								<img src="img/pay2.png" alt=""/>
							</a>
							<a href="#">
								<img src="img/pay2.png" alt=""/>
							</a>
							<a href="#">
								<img src="img/pay4.png" alt=""/>
							</a>
							<a href="#">
								<img src="img/pay5.png" alt=""/>
							</a>
						</div>
					</div>
					<div class="clr"></div>
				</div>
				
				<div class="bot">
					<div class="company">
						* Vesta Deco 2000 - <?=date('Y')?>
					</div>
					<div class="developer">
						Разработка сайта: <a href="amobile-studio.ru"><img src="img/developer.png"></a>
					</div>
				</div>
			</div>
		</footer>
	</div>
</body>
</html>
