<html lang="ru">
	<head>
		<meta charset="utf-8" />
		<title></title>
		<!--[if IE]>
	    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	    <![endif]-->
	    <link rel="stylesheet" type="text/css" href="css/style.css">
	    <link rel="stylesheet" type="text/css" href="css/reset.css">
	    <link rel="stylesheet" type="text/css" href="css/jquery.fancybox.css">
	    <link rel="stylesheet" type="text/css" href="css/main.css">
	    <link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
	    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	    <script src="js/fancybox/jquery.fancybox.js"></script>
	    <script src="js/script.js"></script>
	    <script src="js/owl.carousel.js"></script>
	</head>
<body>
	<div class="shadow"></div>
	<div class="page-wrapper">
		<header>
			<div class="wrap-loc">
				<div class="fix_width">
					<div class="location">
						<span class="first">Ваше местонахождение:</span>
						<a href="#" class="dropDown"><span>Тюмень</span></a>
					</div>
					<div class="hot-line">
						<span class="caption">Телефон горячей линии Vesta Decor:</span>
						<span class="phone">8 800 200 00</span>
					</div>

				</div>
			</div>
			<div class="clr"></div>
			<div class="wrap-info">
				<div class="fix_width">
					<div class="logo">
						<a href="#" class="logo">
							<img src="../img/head-logo.png" />
						</a>
					</div>
					<div class="shop-info">
						<p>Интернет-магазин готовых штор <strong>Vesta Decor</strong></p>
						<p class="delivery">С доставкой по России</p>
					</div>
					<div class="auth_reg">
						<a href="#" class="auth"><span>Вход</span></a>
						<span class="slash"></span>
						<a href="#" class="reg"><span>Регистрация</span></a>
					</div>
					<div class="clr"></div>
				</div>
			</div>
			<div class="clr"></div>
			<div class="wrap-search">
				<div class="fix_width">
					<div class="criteria">
						<input type="checkbox" id="all">
						<label for="all"><span>Все шторы<span></label>
						<!-- <a href="#" class="choice"><span>Все шторы</span></a> -->
						<div class="popup">
							<div class="arrow-top"></div>
							<div class="item">
								<p >Шторы по значению</p>
								<ul>
									<li><a href="#">для кухни</a></li>
									<li><a href="#">для детской</a></li>
									<li><a href="#">для спальни</a></li>
									<li><a href="#">для гостинной</a></li>
								</ul>
							</div>
							<div class="item">
								<p >Шторы по стилю</p>
								<ul>
									<li><a href="#">для кухни</a></li>
									<li><a href="#">для детской</a></li>
									<li><a href="#">для спальни</a></li>
									<li><a href="#">для гостинной</a></li>
								</ul>
							</div>
							<div class="item">
								<p >Шторы по цвету</p>
								<ul class="colors">
									<li><a href="#"><span></span><span>Желтые</span></a></li>
									<li><a href="#"><span></span><span>Зеленые</span></a></li>
									<li><a href="#"><span></span><span>Красные</span></a></li>
									<li><a href="#"><span></span><span>Розовые</span></a></li>
									<li><a href="#"><span></span><span>Белые</span></a></li>
									<li><a href="#"><span></span><span>Черный</span></a></li>
								</ul>
							</div>
							<div class="item">
								<p >Рисунок на ткани</p>
								<ul>
									<li><a href="#">Облака</a></li>
									<li><a href="#">Ангелы</a></li>
									<li><a href="#">3D</a></li>
									<li><a href="#">Круги</a></li>
								</ul>
							</div>
						</div>
					</div>
					<form class="search-query" action="/search">
						<input type="hidden" name="serachType" value="all">
						<input type="text" name="term"  id="search" placeholder="Че кого че ищем ?"/>
						<input type="submit" name="send" value=""/>
						<div class="example">
							<a href="#">Например: <span>шторы для гостинной</span></a>
						</div>
					</form>
					<div class="cart">
						<a href="/cart">Корзина<span class="a-ico"></span><span class="count">15</span></a>
					</div>
				</div>
			</div>
			<div class="breadcrumbs fix_width">
				<a href="#">Главная</a>
				<span>›</span>
				<span>Корзина</span>
			</div>
		</header>
		<div class="clr"></div>
		<div class="page-buffer">
			<div class="wrap-cart">
				<div class="fix_width">
					<div class="title">
						<p class="caption">Корзина с покупками</p>
						<div class="items-count">
							<span>
								Ваши товары в корзине
							</span>
							<a href="#">в отлоденых (5)</a>
						</div>
						<div class="items-table">
							<table>
								<thead>
									<tr>
										<th>Название и фото товара</div></th>
										<th>Количество</th>
										<th>Размер</th>
										<th>Стоимость</th>
										<th>Сумма</th>
										<th></th>
									</tr>
									<tr>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>
											<img src="img/table-item.png">
											<a class="name" href="#">
												Elit white shtora, Grande
											</a>
										</td>
										<td>
											<input type="text" value="3"> шт.
										</td>
										<td>
											<select>
												<option>
													192х215х90	
												</option>
											</select>
											шт.
										</td>
										<td>
											5 000 руб.
										</td>
										<td> 
											25 000 руб.
										</td>
										<td>
											<a class="green-btn" href="">Отложить</a>
											<a class="gray-btn" href="">Выложить</a>
										</td>
									</tr>
									<tr>
										<td>

											<img src="img/table-item.png">
											<a class="name" href="#">
												Elit white shtora, Grande
											</a>
										</td>
										<td>
											<input type="text" value="3"> шт.
										</td>
										<td>
											<select>
												<option>
													192х215х90	
												</option>
											</select>
											шт.
										</td>
										<td>
											5 000 руб.
										</td>
										<td> 
											25 000 руб.
										</td>
										<td>
											<a class="green-btn" href="">Отложить</a>
											<a class="gray-btn" href="">Выложить</a>
										</td>
									</tr>	
									<tr>
										<td>

											<img src="img/table-item.png">
											<a class="name" href="#">
												Elit white shtora, Grande
											</a>
										</td>
										<td>
											<input type="text" value="3"> шт.
										</td>
										<td>
											<select>
												<option>
													192х215х90	
												</option>
											</select>
											шт.
										</td>
										<td>
											5 000 руб.
										</td>
										<td> 
											25 000 руб.
										</td>
										<td>
											<a class="green-btn" href="">Отложить</a>
											<a class="gray-btn" href="">Выложить</a>
										</td>
									</tr>	
								</tbody>
							</table>
						</div>
						<div class="total">
							<div class="result">
								<p>Итого <span>3</span> товара на сумму <span class="price">150 000<span></p>
								<a href="#">Продолжить покупку</a>
							</div>
							<div class="book">
								<div class="phone">
									<a href="#" class="gray-btn">
										<span>
											Сделать заказ по телефону
										</span>
									</a>
									<p class="desc">
										* Воспользуйтесь этой услугой чтобы мы пе-<br>резвонили Вам сами!
									</p>
								</div>
								<div class="simple">
									<a href="#" class="green-btn">
										Оформить заказ
									</a>
									<p class="desc">
										* Оформление заявки займет у <br>Вас не боле 5ти минут.
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="clr"></div>
			<div class="wrap-menu">
				<div class="fix_width">
					<ul>
						<li>
							<a href="">О компании</a>
						</li>
						<li>
							<a href="">Публичная оферта</a>
						</li>
						<li>
							<a href="">Как совершить покупку?</a>
						</li>
						<li>
							<a href="">Доставка</a>
						</li>
						<li>
							<a href="">Возврат</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="clr"></div>
	</div>
	<div class="page-footer">
		<footer>
			<div class="fix_width">
				<div class="top">
					<div class="hot-line">
						<p>
							Телефон горячей линии Vesta Decor: 
						</p>
						<p class="phone">
							8 800 200 00 00
						</p>
						<p>
							* Для регионов звонок бесплатный
						</p>
					</div>
					<div class="question-info">
						<p>
							Мы всегда открыты для вопросов и предложений клиентов!
						</p>
						<a href="#" class="green-btn">Задать вопрос</a>
					</div>
					<div class="pay-ways">
						<p>
							Способы оплаты
						</p>
						<p class="desc">
							Вы можете оплатить покупки наличными 
							при получении, либо выбрать другой 
							способ оплаты.
						</p>
						<div class="pay-links">
							<a href="#">
								<img src="img/pay1.png" alt=""/>
							</a>
							<a href="#">
								<img src="img/pay2.png" alt=""/>
							</a>
							<a href="#">
								<img src="img/pay2.png" alt=""/>
							</a>
							<a href="#">
								<img src="img/pay4.png" alt=""/>
							</a>
							<a href="#">
								<img src="img/pay5.png" alt=""/>
							</a>
						</div>
					</div>
					<div class="clr"></div>
				</div>
				
				<div class="bot">
					<div class="company">
						* Vesta Deco 2000 - <?=date('Y')?>
					</div>
					<div class="developer">
						Разработка сайта: <a href="amobile-studio.ru"><img src="img/developer.png"></a>
					</div>
				</div>
			</div>
		</footer>
	</div>
</body>
</html>
