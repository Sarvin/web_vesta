<?php
/**
 * Миграция m141201_074901_add_post_type_field_to_model_group_set
 *
 * @property string $prefix
 */
 
class m141201_074901_add_post_type_field_to_model_group_set extends CDbMigration
{
    // таблицы к удалению, можно использовать '{{table}}'
	public function Up(){
        $this->addColumn('{{model_group_set}}','post_type','tinyint');
    }

    public function Down(){
        $this->dropColumn('{{model_group_set}}','post_type');
    }
}