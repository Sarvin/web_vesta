<?php
/**
 * Миграция m150216_065430_add_discount_to_book
 *
 * @property string $prefix
 */
 
class m150216_065430_add_discount_to_book extends CDbMigration
{
    // таблицы к удалению, можно использовать '{{table}}'
	public function Up(){
        $this->addColumn('{{book}}','discount','int');
    }

    public function Down(){
        $this->dropColumn('{{book}}','discount');

    }
}