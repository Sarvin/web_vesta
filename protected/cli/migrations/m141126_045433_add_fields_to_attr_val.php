<?php
/**
 * Миграция m141126_045433_add_fields_to_attr_val
 *
 * @property string $prefix
 */
 
class m141126_045433_add_fields_to_attr_val extends CDbMigration
{
    // таблицы к удалению, можно использовать '{{table}}'
	public function Up(){
        $this->addColumn('{{attr_val}}','post_id_type','int');
        $this->addColumn('{{attr_val}}','post_id','int');
    }

    public function Down(){
        $this->dropColumn('{{attr_val}}','post_id_type');
        $this->dropColumn('{{attr_val}}','post_id');   
    }
}