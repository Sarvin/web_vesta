<?php
/**
 * Миграция m150203_073501_add_fields
 *
 * @property string $prefix
 */
 
class m150203_073501_add_fields extends CDbMigration
{
    // таблицы к удалению, можно использовать '{{table}}'
	public function Up(){
        $this->addColumn('{{catalog}}','price_range','int');
    }

    public function Down(){
        $this->dropColumn('{{catalog}}','price_range');
    }
}