<?php
/**
 * Миграция m141201_073203_add_value_field_to_attr
 *
 * @property string $prefix
 */
 
class m141201_073203_add_value_field_to_attr extends CDbMigration
{
    // таблицы к удалению, можно использовать '{{table}}'
	// таблицы к удалению, можно использовать '{{table}}'
    public function Up(){
        $this->addColumn('{{attr_val}}','value','string');
    }

    public function Down(){
        $this->dropColumn('{{attr_val}}','value');
    }
}