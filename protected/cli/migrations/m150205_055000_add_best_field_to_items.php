<?php
/**
 * Миграция m150205_055000_add_best_field_to_items
 *
 * @property string $prefix
 */
 
class m150205_055000_add_best_field_to_items extends CDbMigration
{
    // таблицы к удалению, можно использовать '{{table}}'
	public function Up(){
        $this->addColumn('{{items}}','best','boolean');
    }

    public function Down(){
        $this->dropColumn('{{items}}','best');
    }
}