<?php
/**
 * Миграция m150205_060137_add_field_display_to_catalog
 *
 * @property string $prefix
 */
 
class m150205_060137_add_field_display_to_catalog extends CDbMigration
{
    // таблицы к удалению, можно использовать '{{table}}'
	public function Up(){
        $this->addColumn('{{catalog}}','display','boolean');
    }

    public function Down(){
        $this->dropColumn('{{catalog}}','display');
    }
}