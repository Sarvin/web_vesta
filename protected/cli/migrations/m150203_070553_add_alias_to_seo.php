<?php
/**
 * Миграция m150203_070553_add_alias_to_seo
 *
 * @property string $prefix
 */
 
class m150203_070553_add_alias_to_seo extends CDbMigration
{
    // таблицы к удалению, можно использовать '{{table}}'
	public function Up(){
        $this->addColumn('{{seo}}','alias','int');
    }

    public function Down(){
        $this->dropColumn('{{seo}}','alias');
    }

}