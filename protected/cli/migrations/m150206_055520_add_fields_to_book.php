<?php
/**
 * Миграция m150206_055520_add_fields_to_book
 *
 * @property string $prefix
 */
 
class m150206_055520_add_fields_to_book extends CDbMigration
{
    // таблицы к удалению, можно использовать '{{table}}'
	public function Up(){
        $this->addColumn('{{book}}','height','string');
        $this->addColumn('{{book}}','width','string');
        $this->addColumn('{{book}}','cost','int');
    }

    public function Down(){
        $this->dropColumn('{{book}}','height');
        $this->dropColumn('{{book}}','width');
        $this->dropColumn('{{book}}','cost');

    }
}