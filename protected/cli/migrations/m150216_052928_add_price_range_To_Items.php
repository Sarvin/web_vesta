<?php
/**
 * Миграция m150216_052928_add_price_range_To_Items
 *
 * @property string $prefix
 */
 
class m150216_052928_add_price_range_To_Items extends CDbMigration
{
    // таблицы к удалению, можно использовать '{{table}}'
	public function Up(){
        $this->addColumn('{{items}}','priceRange','text');
    }

    public function Down(){
        $this->dropColumn('{{items}}','priceRange');

    }
}