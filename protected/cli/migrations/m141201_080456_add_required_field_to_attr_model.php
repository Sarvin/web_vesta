<?php
/**
 * Миграция m141201_080456_add_required_field_to_attr_model
 *
 * @property string $prefix
 */
 
class m141201_080456_add_required_field_to_attr_model extends CDbMigration
{
    // таблицы к удалению, можно использовать '{{table}}'
	public function Up(){
        $this->addColumn('{{attr_model}}','required','boolean');
    }

    public function Down(){
        $this->dropColumn('{{attr_model}}','required');
    }
}