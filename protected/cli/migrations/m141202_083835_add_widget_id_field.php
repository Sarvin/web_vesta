<?php
/**
 * Миграция m141202_083835_add_widget_id_field
 *
 * @property string $prefix
 */
 
class m141202_083835_add_widget_id_field extends CDbMigration
{
    // таблицы к удалению, можно использовать '{{table}}'
	public function Up(){
        $this->addColumn('{{attr_field_type}}','id_widget','boolean');
    }

    public function Down(){
        $this->dropColumn('{{attr_field_type}}','id_widget');
    }

}