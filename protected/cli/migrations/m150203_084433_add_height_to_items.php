<?php
/**
 * Миграция m150203_084433_add_height_to_items
 *
 * @property string $prefix
 */
 
class m150203_084433_add_height_to_items extends CDbMigration
{
    // таблицы к удалению, можно использовать '{{table}}'
	public function Up(){
        $this->addColumn('{{items}}','height','int');
    }

    public function Down(){
        $this->dropColumn('{{items}}','height');
    }
}