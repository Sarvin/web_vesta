<?php
/**
 * Миграция m150302_121741_add_size_and_count_to_favoriteItem
 *
 * @property string $prefix
 */
 
class m150302_121741_add_size_and_count_to_favoriteItem extends CDbMigration
{
    // таблицы к удалению, можно использовать '{{table}}'
	public function Up(){
        $this->addColumn('{{favorite_item}}','size','string');
        $this->addColumn('{{favorite_item}}','count','string');
    }

    public function Down(){
        $this->dropColumn('{{favorite_item}}','size');
        $this->dropColumn('{{favorite_item}}','count');
    }
}