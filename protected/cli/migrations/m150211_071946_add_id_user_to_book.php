<?php
/**
 * Миграция m150211_071946_add_id_user_to_book
 *
 * @property string $prefix
 */
 
class m150211_071946_add_id_user_to_book extends CDbMigration
{
    // таблицы к удалению, можно использовать '{{table}}'
	public function Up(){
        $this->addColumn('{{book}}','id_client','string');
    }

    public function Down(){
        $this->dropColumn('{{book}}','id_client');

    }
}