<?php
/**
 * Миграция m150205_103629_add_recomendation_to_items
 *
 * @property string $prefix
 */
 
class m150205_103629_add_recomendation_to_items extends CDbMigration
{
    // таблицы к удалению, можно использовать '{{table}}'
	public function Up(){
        $this->addColumn('{{items}}','recomendations','text');
    }

    public function Down(){
        $this->dropColumn('{{items}}','recomendations');
    }
}