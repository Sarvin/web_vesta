<?php
/**
 * Миграция m150302_102306_add_config_to_favorite_item
 *
 * @property string $prefix
 */
 
class m150302_102306_add_config_to_favorite_item extends CDbMigration
{
    // таблицы к удалению, можно использовать '{{table}}'
	public function Up(){
        $this->addColumn('{{favorite_item}}','params','text');
    }

    public function Down(){
        $this->dropColumn('{{favorite_item}}','params');
    }
}