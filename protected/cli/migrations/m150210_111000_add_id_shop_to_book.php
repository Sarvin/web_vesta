<?php
/**
 * Миграция m150210_111000_add_id_shop_to_book
 *
 * @property string $prefix
 */
 
class m150210_111000_add_id_shop_to_book extends CDbMigration
{
    // таблицы к удалению, можно использовать '{{table}}'
	public function Up(){
        $this->addColumn('{{book}}','id_shop','int');
    }

    public function Down(){
        $this->dropColumn('{{book}}','id_shop');

    }
}