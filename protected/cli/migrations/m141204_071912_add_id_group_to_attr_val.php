<?php
/**
 * Миграция m141204_071912_add_id_group_to_attr_val
 *
 * @property string $prefix
 */
 
class m141204_071912_add_id_group_to_attr_val extends CDbMigration
{
    // таблицы к удалению, можно использовать '{{table}}'
	public function Up(){
        $this->addColumn('{{attr_val}}','id_group','int');
    }

    public function Down(){
        $this->dropColumn('{{attr_val}}','id_group');
    }
}