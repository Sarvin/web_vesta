<?php
/**
 * Миграция m141125_044558_add_attrFieldType_value_range_type
 *
 * @property string $prefix
 */
 
class m141125_044558_add_attrFieldType_value_range_type extends CDbMigration
{
    // таблицы к удалению, можно использовать '{{table}}'
	private $dropped = array('{{add_attrFieldType_value_range_type}}');
 
    public function Up(){
        $this->addColumn('{{attr_field_type}}','value_range','int');
    }
    public function Down(){
        $this->dropColumn('{{attr_field_type}}','value_range');
    }
}