<?php
/**
 * Миграция m150416_072645_add_key_type_to_attr
 *
 * @property string $prefix
 */
 
class m150416_072645_add_key_type_to_attr extends CDbMigration
{
	public function Up(){
        $this->addColumn('{{attr}}','key_type','tinyint');
    }

    public function Down(){
        $this->dropColumn('{{attr}}','key_type');
    }
}
    // таблицы к удалению, можно использовать '{{table}}'