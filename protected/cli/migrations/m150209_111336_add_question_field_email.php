<?php
/**
 * Миграция m150209_111336_add_question_field_email
 *
 * @property string $prefix
 */
 
class m150209_111336_add_question_field_email extends CDbMigration
{
    // таблицы к удалению, можно использовать '{{table}}'
	public function Up(){
        $this->addColumn('{{question}}','email','string');
    }

    public function Down(){
        $this->dropColumn('{{question}}','email');

    }
}