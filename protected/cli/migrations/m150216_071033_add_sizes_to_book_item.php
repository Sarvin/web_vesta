<?php
/**
 * Миграция m150216_071033_add_sizes_to_book_item
 *
 * @property string $prefix
 */
 
class m150216_071033_add_sizes_to_book_item extends CDbMigration
{
    // таблицы к удалению, можно использовать '{{table}}'
	public function Up(){
        $this->addColumn('{{book_item}}','width','int');
        $this->addColumn('{{book_item}}','height','int');
    }

    public function Down(){
        $this->dropColumn('{{book_item}}','width');
        $this->dropColumn('{{book_item}}','height');

    }
}