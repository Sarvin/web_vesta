<?php
/**
 * Миграция m150218_071333_create_add_alias_to_pages
 *
 * @property string $prefix
 */
 
class m150218_071333_create_add_alias_to_pages extends CDbMigration
{
    // таблицы к удалению, можно использовать '{{table}}'
	public function Up(){
        $this->addColumn('{{pages}}','alias','string');
        $this->addColumn('{{pages}}','seo_id','int');
    }

    public function Down(){
        $this->dropColumn('{{pages}}','alias');
        $this->dropColumn('{{pages}}','seo_id');
    }
}