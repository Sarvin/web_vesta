<?php

class AttrController extends AdminController
{

	public function beforeAction($action){
		parent::beforeAction($action);
		
		if ($action->id=="create" || $action->id=="update"){
			$cs=Yii::app()->clientScript;
			$cs->registerScriptFile($this->getAssetsUrl().'/js/colpick.js');
			$cs->registerCssFile($this->getAssetsUrl().'/css/colpick.css');
		}

		return true;
	}
	public function actionCheckOnMultiple($id){
		$check=Yii::app()->db->createCommand()
			->select('id_widget as type')
			->from('{{attr_field_type}}')
			->where('id=:id',array(':id'=>$id))
			->queryRow();
		$response['success']=$check['type']==4;
		echo CJSON::encode($response);
	}

	public function actionCreate(){
		
		$model=new Attr;
		if (isset($_POST['Attr']))
		{
			$model->attributes=$_POST['Attr'];
			$valid=$model->validate();
			if ($_POST['AttrRange'])
			{
				$values=array();
				foreach ($_POST['AttrRange']['value'] as $key => $value) {
					if ($value)
					{
						$key=$_POST['AttrRange']['key'][$key] ? $_POST['AttrRange']['key'][$key] : $key;
						$values[$key]=$value;
					}
						
				}
				$model->rangeValues=$values;
			}
			if ($valid)
			{
				$model->save();
				$this->redirect(array('list'));
			}
		}
		$this->render('create',array('model'=>$model));
	}

	public function actionUpdate($id){
		$cs=Yii::app()->clientScript;
		$cs->registerScriptFile($this->getAssetsUrl().'/js/colpick.js');
		$cs->registerCssFile($this->getAssetsUrl().'/css/colpick.css');
		$model=Attr::model()->findByPk($id);
		if (isset($_POST['Attr']))
		{
			if ($_POST['AttrRange'])
			{
				$values=array();
				foreach ($_POST['AttrRange']['value'] as $key => $value) {
					if ($value)
					{
						$key=$_POST['AttrRange']['key'][$key] ? $_POST['AttrRange']['key'][$key] : $key;
						$values[$key]=$value;
					}
				}
				$model->rangeValues=$values;
			}
		
			$model->attributes=$_POST['Attr'];
			$valid=$model->validate();
			if ($valid)
			{
				$model->save();
				$this->redirect(array('list'));
			}
		}
		$this->render('update',array('model'=>$model));
	}

	public function actionGetEavAttrViews($id,$entity,$modelName){
		
		$entity=Entity::model()->findByPk($entity);
		$model=$entity->getInstance()->findByPk($id);
		$response=array();
		if ($model)
		{
			$response['success']=$this->renderPartial('appext.eavAttrs.views._entityAttrs',array('model'=>$model,'modelName'=>$modelName,'renderAssets'=>false,'renderContainer'=>false),true,true);
		} else 
			$response['error']='Атрибуты не найдены!';
		echo CJSON::encode($response);
		Yii::app()->end();
	}

}
