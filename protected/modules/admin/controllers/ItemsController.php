<?php

class ItemsController extends AdminController
{
	public function actionSettings(){
		
		if ($_POST['Items'])
		{
			Yii::app()->db->createCommand()
				->update('{{items}}',array('best'=>false));
			$criteria=new CDbCriteria;
			$criteria->addInCondition('id',$_POST['Items']['populars']);
			Yii::app()->db->createCommand()
				->update('{{items}}',array('best'=>true),$criteria->condition,$criteria->params);
			$this->redirect(array('list'));
		}

		$this->render('_settings',array(
				'model'=>new Items,
				'data'=>CHtml::listData(Items::model()->findAll(),'id','name')
			)
		);
	}
}
