<?php

class CatalogController extends AdminController
{
	public function actionDisplay(){
		if ($_POST['Catalog']['displayed'])
		{
			$criteria=new CDbCriteria;
			$criteria->addInCondition('id',$_POST['Catalog']['displayed']);
			Yii::app()->db->createCommand()
				->update('{{catalog}}',array('display'=>true),$criteria->condition,$criteria->params);
			
			$criteria->condition=str_replace("IN", "NOT IN", $criteria->condition);
			Yii::app()->db->createCommand()
				->update('{{catalog}}',array('display'=>false),$criteria->condition,$criteria->params);
			$this->redirect(array('list'));
		}
		$this->render('_display',array(
				'model'=>new Catalog,
				'data'=>CHtml::listData(Catalog::model()->findAll(),'id','name')
			)
		);
	}
}
