<?php

class CallsController extends AdminController
{
	public function actionUpdate($id){

		$model = Calls::model()->findByPk($id);
		$model->status=1;
		$model->save();
	  	if(isset($_POST['Calls']))
	  	{
	   		$model->attributes=$_POST['Calls'];
	   		if($model->save())
	   		{
			    $this->redirect(array('/admin/calls/list/')); 
	   		}
	  	}
	 	$this->render('update',array('model'=>$model));

	}
}
