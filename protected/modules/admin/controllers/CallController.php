<?php

class CallController extends AdminController
{
	public function actionUpdate($id){

		$model = Call::model()->findByPk($id);
		$model->status=1;
		$model->save();
	  	if(isset($_POST['Call']))
	  	{
	   		$model->attributes=$_POST['Call'];
	   		if($model->save())
	   		{
			    $this->redirect(array('/admin/call/list/')); 
	   		}
	  	}
	 	$this->render('update',array('model'=>$model));

	}
}
