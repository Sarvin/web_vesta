<?php

class AdminModule extends EWebModule
{

	public $defaultController = 'user';


	public function init()
	{
        $this->setImport(array(
            'admin.models.*',
            'admin.components.*',
            'admin.helpers.*',
            'appext.eavactiverecord.EavActiveRecord.*',
            'appext.eavactiverecord.datatypes.*',
            'appext.eavactiverecord.helpers.*',
            'appext.EPhpThumb.EPhpThumb',
            'appext._select2.Select2',
        ));

        $this->setComponents(array(
            'errorHandler' => array(
                'errorAction' => 'admin/user/error'),
            'user' => array(
                'class' => 'CWebUser',
                'loginUrl' => Yii::app()->createUrl('admin/user/login'),
                'returnUrl' => Yii::app()->createUrl('admin/start/index'),
            ),
        ));
	}

	public function beforeControllerAction($controller, $action)
	{
        if (Yii::app()->user->isClient)
            throw new CHttpException("У вас не достаточно прав для этого дейст", 401);
        
		if(parent::beforeControllerAction($controller, $action))
		{
            $this->registerBootstrap();
            $this->registerCoreScripts();
            return true;
        }
            
        return false;
	}

    protected function registerCoreScripts()
    {
        parent::registerCoreScripts();
        Yii::app()->clientScript->registerCssFile($this->getAssetsUrl() . '/css/admin.css');
		Yii::app()->clientScript->registerCssFile($this->getAssetsUrl() . '/css/jquery-ui-bootstrap/custom-theme/jquery-ui-1.9.2.custom.css');
        Yii::app()->clientScript->registerCssFile($this->getAssetsUrl() . '/css/jquery-ui-bootstrap/custom-theme/jquery.ui.1.9.2.ie.css');
        Yii::app()->clientScript->registerCoreScript('jquery.ui');
        Yii::app()->clientScript->registerScriptFile('https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js', CClientScript::POS_END);
        
		Yii::app()->clientScript->registerScriptFile($this->getAssetsUrl() . '/js/knockout.js', CClientScript::POS_END);
        Yii::app()->clientScript->registerScriptFile($this->getAssetsUrl() . '/js/magic.js', CClientScript::POS_END);
	}


}
