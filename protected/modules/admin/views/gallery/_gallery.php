<?php if ($model->galleryBehaviorGallery->getGallery() === null) {
	echo '<p class="help-block">Прежде чем загружать изображения, нужно сохранить текущее состояние</p>';
} else {
	$this->widget('appext.imagesgallery.GalleryManager', array(
		'gallery' => $model->galleryBehaviorGallery->getGallery('adaptiveResize'),
		'controllerRoute' => '/admin/gallery',
	));
} ?>