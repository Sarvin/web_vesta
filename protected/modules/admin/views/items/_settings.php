
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'catalog-form',
	'enableAjaxValidation'=>false,
		'htmlOptions' => array('enctype'=>'multipart/form-data'),
)); ?>
<?
	$this->breadcrumbs=array(
		"Каталог"=>array('list'),
		'Настройки раздела'
	);

	$this->menu=array(
		array('label'=>'Список','url'=>array('list')),
	);
?>

<h1>Настройки раздела - Каталог</h1>
	<input type="hidden" name="Items[fake]">
	<?=CHtml::label('Выберите популярные объекты','');?>
	<?
		$this->widget('ext._select2.ESelect2',array(
		  'model'=>$model,
		  'attribute'=>'populars',
		  'data'=>$data,
		  'htmlOptions'=>array(
		    'multiple'=>'multiple',
		    'id'=>'section'
		  ),
		  'options'=>array(
		  	'width'=>'66%',
		    'placeholder'=>'Выбрать',
		    'allowClear'=>false,
		  ),
		));
	?>

	<div class="form-actions">
		<?php echo TbHtml::submitButton('Сохранить', array('color' => TbHtml::BUTTON_COLOR_PRIMARY)); ?>
        <?php echo TbHtml::linkButton('Отмена', array('url'=>'/admin/items/list')); ?>
	</div>

<?php $this->endWidget(); ?>