<?php
$this->menu=array(
	array('label'=>'Добавить','url'=>array('create')),
	array('label'=>'Настройки раздела','url'=>array('settings')),
);
?>

<h1>Управление <?php echo $model->translition(2); ?></h1>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'items-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'type'=>TbHtml::GRID_TYPE_HOVER,
    'afterAjaxUpdate'=>"function() {sortGrid('items')}",
    'rowHtmlOptionsExpression'=>'array(
        "id"=>"items[]_".$data->id,
        "class"=>"status_".(isset($data->status) ? $data->status : ""),
    )',
	'columns'=>array(
		'name',
		'price',
		'old_price',
		array(
			'name'=>'height',
			'type'=>'raw',
			'value'=>' $data->heightTextView'
		),
		array(
			'name'=>'width',
			'type'=>'raw',
			'value'=>' $data->widthTextView'
		),
		array(
			'name'=>'status',
			'type'=>'raw',
			'value'=>'Items::getStatusAliases($data->status)',
			'filter'=>Items::getStatusAliases()
		),
		
		array(
			'name'=>'create_time',
			'type'=>'raw',
			'value'=>'$data->create_time ? SiteHelper::russianDate($data->create_time).\' в \'.date(\'H:i\', strtotime($data->create_time)) : ""'
		),
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
'template'=>'{update}{delete}'
		),
	),
)); ?>

<?php if($model->hasAttribute('sort')) Yii::app()->clientScript->registerScript('sortGrid', 'sortGrid("items");', CClientScript::POS_END) ;?>