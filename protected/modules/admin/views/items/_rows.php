	<?php echo $form->textFieldControlGroup($model,'name',array('class'=>'span8 name','maxlength'=>255)); ?>

	<div class="control-group">
		<?php
			echo $form->labelEx($model,'width');
			$this->widget('ext._select2.ESelect2',array(
			  'model'=>$model,
			  //'name'=>'Items[marks]',
			  'attribute'=>'widthVal',
			  'data'=>items::getWidthSize(),
			  'htmlOptions'=>array(
			    'multiple'=>'multiple',
			  ),
			  'options'=>array(
			    'placeholder'=>'Выбрать',
			    'allowClear'=>false,
			    'maximumSelectionSize'=>10,
			    'width'=>'65.81196581196582%'
			  ),
			));
		?>
	</div>
	<br>
	<div class="control-group">
		<?php
			echo $form->labelEx($model,'height');
			$this->widget('ext._select2.ESelect2',array(
			  'model'=>$model,
			  //'name'=>'Items[marks]',
			  'attribute'=>'heightVal',
			  'data'=>items::getHeightSize(),
			  'htmlOptions'=>array(
			    'multiple'=>'multiple',
			  ),
			  'options'=>array(
			    'placeholder'=>'Выбрать',
			    'allowClear'=>false,
			    'maximumSelectionSize'=>10,
			    'width'=>'65.81196581196582%'
			  ),
			));
		?>
	</div>
	<style>
		td input{
			width: 70px;
		}
	</style>
	<div class="control-group">
		<?
			if ($model->id)
			{
		?>
		<p>
			Цены по размерам:
		</p>
		<table>
		<?
		if ($model->width && $model->height)
			foreach ($model->width as $key_w => $w) {
			?><tr><?
				foreach ($model->height as $key_h => $h) {
					?>
						<td>
							<label for="<?=$key_w?>_<?=$key_h?>"><?=Items::getWidthSize($w)?>x<?=Items::getHeightSize($h)?></label>
							<input type="text" id="<?=$key_w?>_<?=$key_h?>" placeholder="<?=Items::getWidthSize($w)?>x<?=Items::getHeightSize($h)?>" name="Items[priceRange][<?=$w?>][<?=$h?>]" value="<?=$model->priceRange[$w][$h]?>">
						</td>
					<?
				}
			}
			?></tr>
		</table>
		<?}?>
	</div>
	<div class="control-group">
		<?php
			// $this->widget('ext._select2.ESelect2',array(
			//   'model'=>$model,
			//   //'name'=>'Items[marks]',
			//   'attribute'=>'categories',
			//   'data'=>CHtml::listData(Catalog::model()->findAll(),'id','name'),
			//   'htmlOptions'=>array(
			//     'multiple'=>'multiple',
			//   ),
			//   'options'=>array(
			//     'placeholder'=>'Выбрать',
			//     'allowClear'=>false,
			//     'maximumSelectionSize'=>10,
			//     'width'=>'65.81196581196582%'
			//   ),
			// ));
		?>
		<?
			$this->widget('appext.eavAttrs.dependModelWidget',array(
					'model'=>$model,
					'data'=>CHtml::listData(Catalog::model()->findAll(),'id','name'),
					'attribute'=>'categories',
				)
			);
		?>

	</div>
	<br>
	<div class="control-group">
		<?php
			echo $form->labelEx($model,'recomendations');
			$this->widget('ext._select2.ESelect2',array(
			  'model'=>$model,
			  //'name'=>'Items[marks]',
			  'attribute'=>'recomendationsVal',
			  'data'=>CHtml::listData(Recomendation::model()->findAll(),'id','name'),
			  'htmlOptions'=>array(
			    'multiple'=>'multiple',
			  ),
			  'options'=>array(
			    'placeholder'=>'Выбрать',
			    'allowClear'=>false,
			    'maximumSelectionSize'=>10,
			    'width'=>'65.81196581196582%'
			  ),
			));
		?>
	</div>
	<br>
	<div class="control-group">
			<?=$form->labelEx($model,'price')?>
			<?php echo $form->textField($model,'price',array('class'=>'span8')); ?>
			<?=$form->error($model,'price')?>
	</div>
	<div class="control-group">
			<?=$form->labelEx($model,'old_price')?>
			<?php echo $form->textField($model,'old_price',array('class'=>'span8')); ?>
			<?=$form->error($model,'price')?>
	</div>
	<div class="clr" style="clear:both"></div>
	<?php echo $form->dropDownListControlGroup($model, 'status', Items::getStatusAliases(), array('class'=>'span8', 'displaySize'=>1)); ?>
	<?php echo $form->textAreaControlGroup($model,'garanty',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>
	<div class='control-group'>
		<?php echo CHtml::activeLabelEx($model, 'wswg_body'); ?>
		<?php $this->widget('appext.ckeditor.CKEditorWidget', array('model' => $model, 'attribute' => 'wswg_body', 'config' => array('width' => '100%')
		)); ?>
		<?php echo $form->error($model, 
	'wswg_body'); ?>
	</div>

