<?php echo $form->textFieldControlGroup($model,'name',array('class'=>'span8','maxlength'=>255)); ?>
<?
	echo $form->dropDownList($model,'id_entity',CHtml::listData(Entity::model()->findAll(),'id','name'),array('class'=>'span8', 'empty'=>'Выберите раздел'));
?>
<div class="control-group">
	<?php
	//var_dump($model->metaData);die();
		echo CHtml::label('Элементы карусели','');
		$this->widget('ext._select2.ESelect2',array(
		  'model'=>$model,

		  'attribute'=>'items',
		  'data'=>CHtml::listData($model->getEntityData(),'id','name'),
		  'htmlOptions'=>array(
		    'multiple'=>'multiple',
		    'class'=>'span8',
		    'id'=>'section'
		  ),
		  'options'=>array(
		    'placeholder'=>'Выбрать',
		    'allowClear'=>false,
		  ),
		));
	?>
</div>
<br>
<br>
	<?php echo $form->dropDownListControlGroup($model, 'status', Carusel::getStatusAliases(), array('class'=>'span8', 'displaySize'=>1)); ?>
<script>
	$(function(){
		$('#Carusel_id_entity').on('change',function(){
			var $this=$(this);
			$.ajax({
				url:"/admin/carusel/getModels",
				data:{id:$(this).val()},
				type:'get',
				success:function(data){
					$('#section').select2('destroy');
					$('#section').empty().append(data);
					$('#section').select2();
				},
				error:function(data){
					alert(data.responseText);
				}
			})
		});
	})
</script>
<style>
	.control-group{
		clear: both;
	}
	.select2-container{
		margin-left: 0!important;
		clear: both;
	}
</style>