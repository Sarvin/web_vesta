<fieldset>
	<?
		$modelName=get_class($model);
		if ($model->hasAttribute('alias'))
		echo TbHtml::textFieldControlGroup($modelName.'[alias]',$model->alias,array('class'=>'span8 alias','maxlength'=>255,'placeholder'=>'alias'));
	?>
	<?php echo TbHtml::textFieldControlGroup($modelName.'[seo][meta_title]',$seo->meta_title,array('class'=>'span8','maxlength'=>255,'placeholder'=>'meta_title')); ?>

	<?php echo TbHtml::textFieldControlGroup($modelName.'[seo][meta_keys]',$seo->meta_keys,array('class'=>'span8','maxlength'=>255,'placeholder'=>'meta_keys')); ?>

	<?php echo TbHtml::textAreaControlGroup($modelName.'[seo][meta_desc]',$seo->meta_desc,array('rows'=>6, 'cols'=>50, 'class'=>'span8','placeholder'=>'meta_desc')); ?>

	<?php echo TbHtml::textAreaControlGroup($modelName.'[seo][meta_html]',$seo->meta_html,array('rows'=>6, 'cols'=>50, 'class'=>'span8','placeholder'=>'meta_html')); ?>
</fieldset>