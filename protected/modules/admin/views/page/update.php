<?php
$this->breadcrumbs=array(
    "Структура сайта"=>array('/admin/page/list', 'opened' => $model->id),
    'Редактирование',
);

$this->menu=array(
    array('label'=>'Список страниц','url'=>array('/admin/page/list', 'opened' => $model->id)),
    array('label'=>'Добавить новую','url'=>array('create', 'node_id'=>$model->id)),
);

?>

<h2>Редактирование страница - "<?php echo $model->title; ?>"</h2>
<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>