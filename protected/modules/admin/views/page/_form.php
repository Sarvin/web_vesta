<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'page-form',
	'enableAjaxValidation'=>false,
		'htmlOptions' => array('enctype'=>'multipart/form-data'),
)); ?>

	<?php echo $form->errorSummary($model); ?>

	<?php $this->widget('bootstrap.widgets.TbTabs', array(
		'tabs' => array(
			array(
				'label' => 'Параметры страницы',
				'content' => $this->renderPartial('_rows', array(
					'form'=>$form,
					'model'=>$model,
					'parent'=>$parent
				), true),
				'active' => true
			),
			array('label' => 'SEO', 'content' => $this->getSeoForm($model), 'active' => false)
		)
	)); ?>

	<div class="form-actions">
		<?php echo TbHtml::submitButton('Сохранить', array('color' => TbHtml::BUTTON_COLOR_PRIMARY)); ?>
        <?php echo TbHtml::linkButton('Отмена', array('url'=>array('/admin/structure/list', 'opened'=>$_GET['id']))); ?>
	</div>

<?php $this->endWidget(); ?>
