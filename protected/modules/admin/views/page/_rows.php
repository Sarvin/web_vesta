<div class="control-group">
	<?php echo $form->textFieldControlGroup($model,'title',array('rows'=>6, 'cols'=>50, 'class'=>'span12 name')); ?>
</div>

<?php echo $form->dropDownListControlGroup($model, 'status', Page::getStatusAliases(), array('class'=>'span8', 'displaySize'=>1)); ?>

<div class='control-group'>
	<?php echo CHtml::activeLabelEx($model, 'wswg_body'); ?>
	<?php $this->widget('appext.ckeditor.CKEditorWidget', array(
		'model' => $model,
		'attribute' => 'wswg_body',
		'config' => array(
			'width' => '99%'
		),
	)); ?>
	<?php echo $form->error($model, 'wswg_body'); ?>
</div>

