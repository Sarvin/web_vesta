<?php
$this->breadcrumbs=array(
    "Список страниц"=>array('/admin/page/list', 'opened' => $_GET['id']),
    'Создание',
);


$this->menu=array(
    array('label'=>'Структура сайта','url'=>array('/admin/page/list', 'opened' => $_GET['id'])),
	array('label'=>'← Раздел', 'url'=>array('/admin/page/update', 'id'=>$_GET['id'])),
);

?>

<h2>Новая страница</h2>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>