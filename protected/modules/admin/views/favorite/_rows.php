	<?php echo $form->textAreaControlGroup($model,'items',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textFieldControlGroup($model,'notify_dt',array('class'=>'span8','maxlength'=>255)); ?>

	<?php echo $form->textFieldControlGroup($model,'email',array('class'=>'span8','maxlength'=>255)); ?>

	<?php echo $form->dropDownListControlGroup($model, 'status', Favorite::getStatusAliases(), array('class'=>'span8', 'displaySize'=>1)); ?>
