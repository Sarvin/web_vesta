<!DOCTYPE html>
<html lang="en">
	<head>
	  <meta charset="utf-8">
	  <title><?php echo CHtml::encode(Yii::app()->config->get('app.name')).' | Admin';?></title>
	  <meta name="viewport" content="width=device-width, initial-scale=1.0">
	</head>
	<body>

        <?php
            $menuItems = array(
                array('label'=>'Страницы', 'url'=>array('/admin/page')),
                array('label'=>'Каталоги', 'url'=>array('/admin/catalog')),
                array('label'=>'Товары', 'url'=>array('/admin/items')),
                array('label'=>'Магазины', 'url'=>array('/admin/shop')),
                array('label'=>'Запросы('.(Question::model()->count+Call::model()->count+Book::model()->count).')', 'items'=>array(
                        array('label'=>'Напомнить('.Book::model()->count.')', 'url'=>array('/admin/favorite')),
                        array('label'=>'Задать вопрос('.Question::model()->count.')', 'url'=>array('/admin/question')),
                        array('label'=>'Заказ звонков('.Call::model()->count.')', 'url'=>array('/admin/call')),
                        array('label'=>'Заказы('.Book::model()->count.')', 'url'=>array('/admin/book')),
                    )
                ),
                array('label'=>'Дополнительно', 'items'=>array(
                        array('label'=>'Города','url'=>array('/admin/cities')),
                        array('label'=>'Слайдеры','url'=>array('/admin/carusel')),
                        array('label'=>'Настройки','url'=>array('/admin/config')),
                        array('label'=>'Меню','url'=>array('/admin/menu')),
                        array('label'=>'Рекомендации по уходу','url'=>array('/admin/recomendation')),
                    )
                ),
                array('label'=>'Атрибуты', 'items'=>array(
                    array('label'=>'Арибуты','url'=>array('/admin/attr')),
                    array('label'=>'Группы атрибутов','url'=>array('/admin/attrGroup')),
                    array('label'=>'Типы значений атрибутов','url'=>array('/admin/attrFieldType')),
                    array('label'=>'Типы полей атрибутов','url'=>array('/admin/attrValType')),
                    )
                ),
            );
        ?>
        <?php
            $userlogin = Yii::app()->user->name ? Yii::app()->user->name : Yii::app()->user->email;
            $this->widget('bootstrap.widgets.TbNavbar', array(
                'color'=>'inverse', // null or 'inverse'
                'brandLabel'=> CHtml::encode(Yii::app()->name),
                'brandUrl'=>'/',
                'collapse'=>true, // requires bootstrap-responsive.css
                'items'=>array(
                    array(
                        'class'=>'bootstrap.widgets.TbNav',
                        'items'=>$menuItems,
                    ),
                    array(
                        'class'=>'bootstrap.widgets.TbNav',
                        'htmlOptions'=>array('class'=>'pull-right'),
                        'items'=>array(
                            array('label'=>'Выйти ('.$userlogin.')', 'url'=>'/user/logout'),
                        ),
                    ),
                ),
            ));
        ?>

        <?php echo $content;?>

	</body>
</html>
