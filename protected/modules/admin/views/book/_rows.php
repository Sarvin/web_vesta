	<?php echo $form->textFieldControlGroup($model,'phone',array('class'=>'span8','maxlength'=>255)); ?>

	<?php echo $form->textFieldControlGroup($model,'email',array('class'=>'span8','maxlength'=>255)); ?>

	<?php echo $form->textFieldControlGroup($model,'fio',array('class'=>'span8','maxlength'=>255)); ?>
	<div class="control-group">
		<hr>
		<p>Способ получения</p>
		<?php echo $form->dropDownListControlGroup($model,'delivery',Book::getDelivery(),array( 'empty'=>'Не выбрано', 'class'=>'span8')); ?>
		<?php echo $form->dropDownListControlGroup($model,'id_shop',CHtml::listData(Shop::model()->findAll(),'id','name'),array( 'empty'=>'Не выбрано', 'class'=>'span8')); ?>

		<?php echo $form->textFieldControlGroup($model,'address',array('class'=>'span8','maxlength'=>255)); ?>
		<hr>
	</div>
	<div class='control-group'>
		<?php echo CHtml::activeLabelEx($model, 'dt_sold'); ?>
		<?php $this->widget('yiiwheels.widgets.datetimepicker.WhDateTimePicker', array(
			'model' => $model,
			'attribute' => 'dt_sold',
			'pluginOptions' => array(
				'format' => 'dd-MM-yyyy',
				'language' => 'ru',
                'pickSeconds' => false,
                'pickTime' => false
			)
		)); ?>
		<?php echo $form->error($model, 'dt_sold'); ?>
	</div>

	<?php echo $form->dropDownListControlGroup($model, 'status', Book::getStatusAliases(), array('class'=>'span8', 'displaySize'=>1)); ?>

	<table class="items" style="text-align:center;width:80%;">
		<thead>
			<tr>
				<td>Фото</td>
				<td>Название</td>
				<td>Размер</td>
				<td>Количество</td>
				<td>Цена</td>
				<td>Общая стоимость</td>
			</tr>
		</thead>
		<tbody>
	<?
	if ($model->items)
	{
		$total=array(
			'summ'=>0,
			'price'=>0,
			'count'=>0,
		);
		foreach ($model->items as $key => $data) {
				$total['summ']+=$data->summ;
				$total['price']+=$data->item->priceBySize;
				$total['count']+=$data->count;
				?>
				<tr>
					<td><?=TbHtml::imageCircle($data->item->firstImage->getUrl("small"))?></td>
					<td><?=$data->item->name?></td>
					<td><?=$data->size?></td>
					<td><?=$data->count?> шт.</td>
					<td><?=$data->item->priceBySize?> руб.</td>
					<td><?=$data->summ?> руб.</td>
				</tr>
				<?
			}
			?>
				<tr>
					<td>Итого:</td>
					<td colspan="2"><?=$model->discount ? 'Скидка:'.$model->discount.' руб.' : ''?></td>
					<td><?=$total['count']?></td>
					<td><?=$total['price']?></td>
					<td><?=$total['summ']-$model->discount?> руб.</td>
				</tr>
			<?
		}
	?>
		</tbody>
	</table>
<style>
	.items td{
		border: dashed 1px gray;
		padding: 10px 0;
	}
</style>