	<?php echo $form->textFieldControlGroup($model,'name',array('class'=>'span8','maxlength'=>255)); ?>
	<?php echo $form->textFieldControlGroup($model,'alias',array('class'=>'span8','maxlength'=>255)); ?>
	<?php echo $form->dropDownListControlGroup($model,'id_entity',CHtml::listData(Entity::model()->findAll(),'id','name'),array('class'=>'span8 entity')); ?>

	<div class="control-group">
	<?php
	//var_dump($model->item_list);die();
		echo CHtml::label('Раздел','');
		$this->widget('ext._select2.ESelect2',array(
		  'model'=>$model,

		  'attribute'=>'items',
		  'data'=>!empty($model->entity) ? $model->entity->getInstance()->menu() : array(),
		  'htmlOptions'=>array(
		    'multiple'=>'multiple',
		    'class'=>'span8',
		    'id'=>'section'
		  ),
		  'options'=>array(
		    'placeholder'=>'Выбрать',
		    'allowClear'=>false,
		  ),
		));
	?>
</div>
<br>
<br>

	<?//php echo $form->dropDownListControlGroup($model, 'status', Menu::getStatusAliases(), array('class'=>'span8', 'displaySize'=>1)); ?>
<script>
	$(function(){
		$('.entity').on('change',function(){
			var $this=$(this);
			$.ajax({
				url:"/admin/carusel/getModels",
				data:{id:$(this).val()},
				type:'get',
				success:function(data){
					$('#section').select2('destroy');
					$('#section').empty().append(data);
					$('#section').select2();
				},
				error:function(data){
					alert(data.responseText);
				}
			})
		});
	})
</script>