	<?php echo $form->textFieldControlGroup($model,'name',array('class'=>'span8 name','maxlength'=>255)); ?>
	<div class='control-group'>
		<?php echo CHtml::activeLabelEx($model, 'img_preview'); ?>
		<?php echo $form->fileField($model,'img_preview', array('class'=>'span3')); ?>
		<div class='img_preview'>
			<?php if ( !empty($model->img_preview) ) echo TbHtml::imageRounded( $model->imgBehaviorPreview->getImageUrl('small') ) ; ?>
			<span class='deletePhoto btn btn-danger btn-mini' data-modelname='Catalog' data-attributename='Preview' <?php if(empty($model->img_preview)) echo "style='display:none;'"; ?>><i class='icon-remove icon-white'></i></span>
		</div>
		<?php echo $form->error($model, 'img_preview'); ?>
	</div>
	<div class="control-group">
		<?php
			echo $form->labelEx($model,'labels');
			$this->widget('ext._select2.ESelect2',array(
			  'model'=>$model,
			  //'name'=>'Items[marks]',
			  'attribute'=>'price_range',
			  'data'=>array(),
			  'htmlOptions'=>array(
			    'multiple'=>'multiple',
			    'class'=>'span8',
			    'id'=>'fake'
			  ),
			  'options'=>array(
			    'placeholder'=>'Выбрать',
			    'allowClear'=>false,
			    'maximumSelectionSize'=>10,
			  ),
			));
		?>
	</div>
	<br>
	<br>
	<div class="control-group">
	<?
		echo $form->labelEx($model,'price_range',array('class'=>'span8'));
	?>
	<br>
	<?php

		echo $form->hiddenField($model,'price_range',array('id'=>'prices'));
	?>
	<?

	?>
	<br>
	<script>
		$(function(){
			$('#fake').closest('.control-group').remove();
			$("#prices").select2({
			width:'100%',
		    createSearchChoice:function(term, data) { 
		    	if ($.isNumeric(term))
			    	if ($(data).filter(function() { return this.text.localeCompare(term)===0; }).length===0)
			    		{
			   				return {id:term, text:term};
			    		}
		    	},
			    multiple: true,
			    tags: <?=$model->encodePrice()?>
			});
		})
		
		</script>
	</div>
	<br>
	<br>
	<div class='control-group'>
		<?php echo CHtml::activeLabelEx($model, 'wswg_body'); ?>
		<?php $this->widget('appext.ckeditor.CKEditorWidget', array('model' => $model, 'attribute' => 'wswg_body', 'config' => array('width' => '100%')
		)); ?>
		<?php echo $form->error($model, 'wswg_body'); ?>
	</div>

	

	<?php echo $form->dropDownListControlGroup($model, 'status', Catalog::getStatusAliases(), array('class'=>'span8', 'displaySize'=>1)); ?>
