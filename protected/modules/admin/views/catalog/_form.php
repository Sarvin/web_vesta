<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'catalog-form',
	'enableAjaxValidation'=>false,
		'htmlOptions' => array('enctype'=>'multipart/form-data'),
)); ?>

	<?php echo $form->errorSummary($model); ?>

	<?php $tabs = array(); ?>
	<?php $tabs[] = array('label' => 'Основные данные', 'content' => $this->renderPartial('_rows', array('form'=>$form, 'model' => $model), true), 'active' => true); ?>

	<?php $tabs[] = array('label' => 'Характеристики', 'content' => $this->renderPartial('appext.eavAttrs.views._attachAttributes', array('form'=>$form, 'model' => $model,'form'=>$form), true), 'active' => false); ?>

	<?php $tabs[] = array('label' => 'Группы характеристик', 'content' => $this->renderPartial('appext.eavAttrs.views._attachAttrSets', array('form'=>$form, 'model' => $model,'form'=>$form), true), 'active' => false); ?>
	<?php $tabs[] = array('label' => 'SEO', 'content' => $this->getSeoForm($model), 'active' => false);
	 ?>
	<?php $this->widget('bootstrap.widgets.TbTabs', array( 'tabs' => $tabs)); ?>

	<div class="form-actions">
		<?php echo TbHtml::submitButton('Сохранить', array('color' => TbHtml::BUTTON_COLOR_PRIMARY)); ?>
        <?php echo TbHtml::linkButton('Отмена', array('url'=>'/admin/catalog/list')); ?>
	</div>

<?php $this->endWidget(); ?>
