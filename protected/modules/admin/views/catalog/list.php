<?php
$this->menu=array(
	array('label'=>'Добавить','url'=>array('create')),
	array('label'=>'Настройки раздела','url'=>array('display')),
);
?>

<h1>Управление <?php echo $model->translition(2); ?></h1>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'catalog-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'type'=>TbHtml::GRID_TYPE_HOVER,
    'afterAjaxUpdate'=>"function() {sortGrid('catalog')}",
    'rowHtmlOptionsExpression'=>'array(
        "id"=>"items[]_".$data->id,
        "class"=>"status_".(isset($data->status) ? $data->status : ""),
    )',
	'columns'=>array(
		array(
			'header'=>'Фото',
			'type'=>'raw',
			'value'=>'TbHtml::imageCircle($data->imgBehaviorPreview->getImageUrl("icon"))'
		),
		'name',
		array(
			'name'=>'status',
			'type'=>'raw',
			'value'=>'Catalog::getStatusAliases($data->status)',
			'filter'=>Catalog::getStatusAliases()
		),
		
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
'template'=>'{update}{delete}'
		),
	),
)); ?>

<?php if($model->hasAttribute('sort')) Yii::app()->clientScript->registerScript('sortGrid', 'sortGrid("catalog");', CClientScript::POS_END) ;?>