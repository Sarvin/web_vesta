	<div class="alert alert-success">
		<strong>
			Внимание!
		</strong>
		После удаления группы атрибутов, все связанные значения с категориями - будут удалены.
	</div>
	<?php echo $form->textFieldControlGroup($model,'name',array('class'=>'span8')); ?>
	<?php echo $form->textFieldControlGroup($model,'alias',array('class'=>'span8','disabled'=>true)); ?>
	<hr>
	<div class="control-group">
		<p>Выберите характеристики</p>
		<p>Чекбокс - определяет обязательная харак-ка или нет</p>
		<div class="required">
			<?
				foreach ($model->eavAttrs['required'] as $key => $value) {
						
					if ($value)
						echo '<div id="req_'.$value .'"></div>';
				}

			?>
		</div>
	<?php
	$this->widget('ext._select2.ESelect2',array(
		  'attribute'=>'eavAttrs[value]',
		  'data'=>AttrGroup::getAttrList(),
		  'model'=>$model,
		  'htmlOptions'=>array(
		    'multiple'=>'multiple',
		    'class'=>'span8'
		  ),
		  'options'=>array(
		    'placeholder'=>'Выбрать',
		    'allowClear'=>false,
		    'maximumSelectionSize'=>8,
		    'formatSelection'=>'js:function format(state,conteiner,markUp) {
			    conteiner.parent().append($("<input "+($("#req_"+state.id).length==1 ? \'checked\' : "") +" type=\'checkbox\' name=\'AttrGroup[eavAttrs][required]["+state.id+"]\'>"));
			    return  state.text;
			}',
		  ),
		));
	?>
	</div>
<style>
.select2-search-choice input:checked+label:after{
	content: 'Обязательное';
	display: block;
}
.select2-container{
	margin: 0!important;
	padding: 0;
}
</style>