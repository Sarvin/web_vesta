<?php
$this->menu=array(
	array('label'=>'Добавить','url'=>array('create')),
);
?>

<h1>Управление <?php echo $model->translition(2); ?></h1>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'shop-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'type'=>TbHtml::GRID_TYPE_HOVER,
    'afterAjaxUpdate'=>"function() {sortGrid('shop')}",
    'rowHtmlOptionsExpression'=>'array(
        "id"=>"items[]_".$data->id,
        "class"=>"status_".(isset($data->status) ? $data->status : ""),
    )',
	'columns'=>array(
		'name',
		array(
			'header'=>'Расписание',
			'type'=>'raw',
			'value'=>'$data->getAddressView()'
		),
		array(
			'name'=>'status',
			'type'=>'raw',
			'value'=>'Shop::getStatusAliases($data->status)',
			'filter'=>Shop::getStatusAliases()
		),
		
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
'template'=>'{update}{delete}'
		),
	),
)); ?>

<?php if($model->hasAttribute('sort')) Yii::app()->clientScript->registerScript('sortGrid', 'sortGrid("shop");', CClientScript::POS_END) ;?>