<?php
Yii::import('application.modules.auth.components.AuthWebUser');

class WebUser extends AuthWebUser
{

    /**
     * @var boolean whether to enable cookie-based login. Defaults to false.
     */
    public $allowAutoLogin=true;
    public $table='user';
    public $isClient;
    private $_id_favorite;

    public function getId_favorite(){
        if (Yii::app()->session[$this->_id_favorite]===false)
        {
            $f=new Favorite;
            $f->save();
            Yii::app()->session[$this->_id_favorite]=$f->id;
        }
        return Yii::app()->session[$this->_id_favorite];
    }

    public function setId_favorite($data)
    {
        Yii::app()->session[$this->_id_favorite]=$data;
    }
    /**
     * @var string|array the URL for login. If using array, the first element should be
     * the route to the login action, and the rest name-value pairs are GET parameters
     * to construct the login URL (e.g. array('/site/login')). If this property is null,
     * a 403 HTTP exception will be raised instead.
     * @see CController::createUrl
     */
    public $loginUrl=array('/user/login');

    public function getRole()
    {
        return $this->getState('__role');
    }
    public function init(){
        parent::init();
        $this->_id_favorite=md5('id_favorite');
        Yii::app()->session[$this->_id_favorite]=
            Yii::app()->session[$this->_id_favorite] ? Yii::app()->session[$this->_id_favorite] : false ;
        return true;
    }
    public function getId()
    {
        return $this->getState('__id') ? $this->getState('__id') : 0;
    }

//    protected function beforeLogin($id, $states, $fromCookie)
//    {
//        parent::beforeLogin($id, $states, $fromCookie);
//
//        $model = new UserLoginStats();
//        $model->attributes = array(
//            'user_id' => $id,
//            'ip' => ip2long(Yii::app()->request->getUserHostAddress())
//        );
//        $model->save();
//
//        return true;
//    }


    protected function afterLogin($fromCookie)
	{
        parent::afterLogin($fromCookie);
        if ($this->table=='client')
            $this->updateClientSession();
        else
        $this->updateSession();
	}

    public function updateClientSession() {
        $user = Yii::app()->getModule('user')->client($this->id);
        $this->name = $user->fio;
        $userAttributes = array(
                            'email'=>$user->email,
                            'fio'=>$user->fio,
                            'phone'=>$user->phone,
                            'gender'=>$user->gender,
                            'isClient'=>true,
                            'id_favorite'=>null,
                           );
        foreach ($userAttributes as $attrName=>$attrValue) {
            $this->setState($attrName,$attrValue);
        }
    }

    public function updateSession() {
        $user = Yii::app()->getModule('user')->user($this->id);
        $this->name = $user->username;
        $userAttributes = CMap::mergeArray(array(
                                                'email'=>$user->email,
                                                'username'=>$user->username,
                                                'create_at'=>$user->create_at,
                                                'lastvisit_at'=>$user->lastvisit_at,
                                                'isClient'=>false,
                                           ),$user->profile->getAttributes());
        foreach ($userAttributes as $attrName=>$attrValue) {
            $this->setState($attrName,$attrValue);
        }
    }

    public function model($id=0) {
        return Yii::app()->getModule('user')->user($id);
    }

    public function user($id=0) {
        return $this->model($id);
    }

    public function getUserByName($username) {
        return Yii::app()->getModule('user')->getUserByName($username);
    }

    public function getAdmins() {
        return Yii::app()->getModule('user')->getAdmins();
    }

    public function isAdmin() {
        return Yii::app()->getModule('user')->isAdmin();
    }

}