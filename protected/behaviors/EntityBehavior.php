<?php

class EntityBehavior extends CActiveRecordBehavior
{

    public function afterSave($event) {
        parent::afterSave($event);
        $owner = $this->getOwner();

        if ($this->owner->id_model_ident){
            $ident=new ModelIndet;
            $ident->id_entity=$this->owner->entity_id;
            $ident->id_model=$this->owner->id;
            $ident->save();
        }
    }
}