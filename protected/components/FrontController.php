<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class FrontController extends Controller
{
    public $layout='//layouts/simple';
    public $menu=array();
    public $breadcrumbs=array();
    public $regMeta=true;
    public $name;
    private $_model=false;

    public function getModel(){
        return $this->_model;
    }

    public function beforeAction($action){
        parent::beforeAction($action);
        $this->breadcrumbs=array($this->name=>'/'.Yii::app()->controller->id);

        return true;
    }

    public function getFavoriteCount($id=false){
        
        if ($id)
            return FavoriteItem::model()->count('id_favorite=:f and id_item=:item',array(':f'=>Yii::app()->user->id_favorite,':item'=>$id));
        return FavoriteItem::model()->count('id_favorite=:f',array(':f'=>Yii::app()->user->id_favorite));
    }

    public function setModel($data){

        if ($data===null || $data->status!=1)
            throw new CHttpException("Ошибка 404. Страница не найдена!", 404);
            
        if (!is_object($data))
            throw new Exception("Свойство get_class($this).model может хранить только object", 500);

        $this->_model=$data;

        if ($this->_model && $this->regMeta){
            
            if (!$this->_model->metaData->hasRelation('seo'))
                throw new Exception("Не определено отношение seo у модели {get_class($this->_model)}", 500);
            $seo = Seo::model()->findByPk($this->_model->seo_id);
            
            if ( !empty($seo->meta_title) )
                $this->title = $seo->meta_title;
            else
                $this->title = ($this->_model->{$this->_model->titleAttr}).' | '.Yii::app()->config->get('app.name');
            
            if ($seo->meta_desc)
                Yii::app()->clientScript->registerMetaTag($seo->meta_desc  , 'description', null, array('id'=>'meta_description'), 'meta_description');

            Yii::app()->clientScript->registerMetaTag($seo->meta_keys, 'keywords', null, array('id'=>'keywords'), 'meta_keywords');
        }
    }

    public function init() {
        parent::init();
        $this->title = Yii::app()->name;
    }

    //Check home page
    public function is_home(){
        return $this->route == 'site/index';
    }

    public function beforeRender($view)
    {
        $this->menu['footer']=Menu::model()->find('alias=\'footer\'');
        
        return parent::beforeRender($view);
    }
}