<?php

class BookController extends FrontController
{
	public $layout='//layouts/main';
	public $name="Оформление заказа";
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	public function accessRules()
	{
		$count=Yii::app()->cart->getCount() || Items::getFavoriteCount();
		if (!$count)
			return array(
				array('denny',  // allow all users to perform 'index' and 'view' actions
					'actions'=>array('index','auth'),
					'users'=>array('@'),
					'message'=>'В корзине должен быть как минимум 1 товар!',
					'deniedCallback'=>array($this,'error')
				),
			);
		else
			return array(
			);
	}
	
	public function error($e){
		Yii::app()->clientScript->registerCssFile($this->getAssetsUrl().'/css/main.css');
		$this->layout='//layout/simple';
		$this->render('//site/error',array('message'=>$e->message ? $e->message : 'Ошибка доступа!','code'=>'403','html'=>'<div class="empty"><br><br><a href="/catalog">Перейти в каталог</a></div>'));
	}

	
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel('Book', $id),
		));
	}

	public function actionGetCities($id){
		$city=Ctities::model()->findByPk($id);
		$this->breadcrumbs=array('Корзина'=>'/cart','оформление заказа');
		$responce['success']=$this->renderPartial('//book/shops',array('shops'=>$city->shops));
		echo CJSON::encode($responce);
		Yii::app()->end();
	}

	public function actionIndex($state=1)
	{
		$this->breadcrumbs=array('Корзина'=>'/cart','оформление заказа');
		$model=new Book;
		$state=$_GET['state'] ? $_GET['state'] : 1;
		if ($_GET['state'] && $_GET['id'])
		{
			if ($item=Items::model()->findByPk($_GET['id']))
			{
				$item->state=$_GET['state'];
				if (Yii::app()->cart->itemAt($item->getId()))
					Yii::app()->cart->update($item,1);
				else 
					Yii::app()->cart->put($item);
			} else 
				throw new CHttpException("Страница не найдена", 404);
			
		}

		if (Yii::app()->user->id)
		{
			$client=Client::model()->findByPk(Yii::app()->user->id);
			$model->attributes=$client->attributes;
		}
		
		if ($_POST['Book'])
		{
			$model->attributes=$_POST['Book'];
			$model->id_client=Yii::app()->user->id;
			if ($model->validate())
			{
				$model->discount=Yii::app()->request->cookies[md5('bbf_discount')] ? (int)Config::getValue('discount.cost') : 0;
				unset(Yii::app()->request->cookies[md5('bbf_discount')]);
				$model->status=0;
				$model->save();
				$positions=$state==1 ? Yii::app()->cart->getPositions() : Items::getFavorite();

				if ($positions)
				{
					foreach ($positions as $key => $data) {
						$item=new BookItem;
						$item->attributes=array(
							'count'=>$state==1 ? $data->getQuantity() : $data->favorite->count,
							'id_book'=>$model->id,
							'id_item'=>$data->id,
							'width'=>$data->choosenWidth,
							'height'=>$data->choosenHeight,
							'summ'=>$state==1 ? $data->getQuantity()*$data->priceBySize : $data->favorite->count * $data->priceBySize
						);

						$item->save();
					}
					$discount=new Discount;
					$discount->save();
					if ($state==1)
						Yii::app()->cart->clear();
					else {
						FavoriteItem::model()->deleteAll('id_favorite=:f',array(':f'=>Yii::app()->user->id_favorite));
					}
					$this->redirect('/page/discount/'.$discount->hash_key);
				} else {
					throw new CHttpException("В корзина не может быть пустой!", 400);
					
				}
			}
		}
		
		$shops=Shop::model()->findAll();
		$this->render('index',array(
			'model'=>$model,
			'shops'=>$shops,
			'state'=>$_GET['state'] ? $_GET['state'] : 1,
		));
	}
}
