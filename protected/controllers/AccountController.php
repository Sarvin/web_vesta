<?php

class AccountController extends FrontController
{
	public $layout='//layouts/main';

	
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	
	public function accessRules()
	{
		return array(
			array('denny',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('registration','auth'),
				'users'=>array('@'),
				'message'=>'Ошибка доступа',
				'deniedCallback'=>array($this,'error')
			),
			array('deny',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','logout','changePwd','bookView'),
				'users'=>array('?'),
				'message'=>'Дотупно только для авторезированных пользователей',
				'deniedCallback'=>array($this,'error')
			),
		);
	}

	public function actionBookView($id){

		$model=Book::model()->findByPk($id);
		$this->breadcrumbs=array('Личный кобинет'=>'/account','Мои заказы'=>'/account?tab=2','заказ  от '.date('d.m.Y',strtotime($model->create_time)));
		$this->render('_bookView',array('model'=>$model));
	}

	public function error($e){
		$this->layout="//layouts/simple";
		$this->render('//site/error',array('message'=>$e->message ? $e->message : 'Ошибка доступа!','code'=>'403'));
	}
	
	public function actionRegistration()
	{
		$this->breadcrumbs=array('Регистрация');
		$model=new RegistrationModel;
		if ($_POST['RegistrationModel'])
		{
			$model->attributes=$_POST['RegistrationModel'];
			
			if ($model->validate())
			{
				$model->save();
				$this->redirect('/');
			}
		}
		$this->render('registration',array('model'=>$model));
	}
	
	public function actionIndex($tab=1)
	{	
		$this->breadcrumbs=array('Личный кобинет');
		if ($tab==2)
			$this->breadcrumbs=array('Личный кобинет'=>'/account','Мои заказы');
		$id=Yii::app()->user->id;
		$model=Client::model()->findByPk($id);
		if ($tab==2)
		{
			$dataProvider=new CActiveDataProvider("Book",array(
					'criteria'=>array(
				        'condition'=>'id_client=:client',
				        'order'=>'create_time DESC',
				        'params'=>array(':client'=>Yii::app()->user->id)
				    ),
				    'pagination'=>array(
				    	'pageSize'=>3
				    ),
				)
			);
		}
		if ($_POST['Client']){
			unset($_POST['Client']['password']);
			$model->attributes=$_POST['Client'];
			if($model->validate())
				$model->save();
		}
		$this->render('index',array('model'=>$model,'tab'=>$tab,'dataProvider'=>$dataProvider));
	}

	public function actionLogout($url)
	{
		Yii::app()->user->logout();
		$this->redirect($_GET['url']);
	}

	public function actionChangePwd(){
		$model=new ChangePwd;
		$model->attributes=Client::model()->findByPK(Yii::app()->user->id)->attributes;
		if ($_POST['ChangePwd'])
		{
			$model->attributes=$_POST['ChangePwd'];
			if ($model->validate())
			{
				$model->savePass();
				Yii::app()->user->logout();
				$this->redirect('/page/password_changed');
			}
		}
		$this->render('_changePwd',array('model'=>$model));
	}

	public function actionAuth()
	{
		// Аутентифицируем пользователя по имени и паролю
		$responce=array();
		if ($_POST['AuthForm']){
			$auth=new AuthForm;
			$auth->attributes=$_POST['AuthForm'];
			if($auth->authenticate())
			{
				$responce['success']=true;
			} else {
				$responce['error']=true;
			}
		}
		echo CJSON::encode($responce);
		Yii::app()->end();
	}
}
