<?php

class ManageController extends FrontController
{
	public $layout='//layouts/main';
	
	public function actionRemember(){
		
		$responce=array();

		if (isset($_POST['Favorite']))
		{
			$model=Yii::app()->user->id_favorite ? Favorite::model()->findByPk(Yii::app()->user->id_favorite) : new Favorite;
			$model->attributes=$_POST['Favorite'];

			if ($responce['success']=$model->validate())
			{
				$model->status=0;
				$model->save();
				
				$responce['success']=Yii::app()->cart->getItemsCountWithState(2)>0;

				
				Yii::app()->user->id_favorite=$model->id;
				$items=Yii::app()->cart->getItemsWithState(2);

				foreach ($items as $key => $data) {
					if (!FavoriteItem::model()->count('id_favorite=:f and id_item=:item',array(':f'=>$model->id,':item'=>$data->id)))
					{
						$fItem=new FavoriteItem;
						$fItem->attributes=array('id_favorite'=>$mdoel->id,'id_item'=>$data->id);
						$fItem->save();
					}
				}

			}
		}
		echo CJSON::encode($responce);
		Yii::app()->end();
	}

	public function actionRecall(){
		$responce=array();
		if (isset($_POST['Call']))
		{
			$model=new Call;
			$model->attributes=$_POST['Call'];
			if ($responce['success']=$model->validate())
			{
				$model->status=0;
				$model->save();
				$this->redirect('/page/thanks');
			}
		}
		echo CJSON::encode($responce);
		Yii::app()->end();
	}

	public function actionQuestion(){
		$responce=array();
		if (isset($_POST['Question']))
		{
			$model=new Question;
			$model->attributes=$_POST['Question'];
			if ($responce['success']=$model->validate())
			{
				$model->status=0;
				$model->save();
				$this->redirect('/page/thanks');
			}
		}
		echo CJSON::encode($responce);
		Yii::app()->end();
	}

}
