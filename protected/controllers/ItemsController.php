<?php

class ItemsController extends FrontController
{
	public $layout='//layouts/main';
		
	public function actionAddToCart($id,$height,$width){
		if ($model=Items::model()->findByPk($id))
		{
			$model->choosenHeight=$height;
			$model->choosenWidth=$width;
			$model->state=1;

			Yii::app()->cart->put($model);
			$responce=$this->getResponceData($model);
			echo CJSON::encode($responce);
		}
	}
	
	public function actionSize($size,$id,$state){
		$response=array();
		if ($model=Items::model()->findByPk($id))
		{
			$model->state=$state;
			if ($state==1)
			{
				$model->size=$size;
				$count=$model->position->getQuantity();
				Yii::app()->cart->remove($model->getId());
				Yii::app()->cart->put($model,$count);
			} else {
				$model->favorite->size=$size;
				$model->favorite->save();
			}

			$responce=$this->getResponceData($model,$state);
		}
		echo CJSON::encode($responce);
		Yii::app()->end();
	}

	public function actionCount($count,$id,$state=1){

		$responce=array();
		if ($model=Items::model()->findByPk($id))
		{
			if ($state==1)
			{
				$model->size=$model->position->size;
				Yii::app()->cart->update($model,$count);
				
			}else {
				$model->favorite->count=$count;
				$model->favorite->save();
			}
			$responce=$this->getResponceData($model,$state);
		}
		echo CJSON::encode($responce);

	}

	public function getResponceData($model,$state=1){
		$model->state=$state;
		$responce['success']=!empty($model);
		$position=Yii::app()->cart->itemAt($model->getId());
		
		if ($responce['success']){
			$responce['itemcost']=$state==1 & $model->position ? $model->position->getQuantity() * $model->priceBySize : $model->favorite->count*$model->priceBySize;
			
			//$responce['itemcost']=$model->position->getQuantity();
			//var_dump($model->size,$model->position->size);die();
			$responce['price']=$model->priceBySize;
			
			$count=1;
			if ($state==1)
				$responce['itemCount']=$model->position ? $model->position->getQuantity() : 0;
			else
				$responce['itemCount']=$model->favorite ? $model->favorite->count : 0;

			$responce['cost_1']=Yii::app()->cart->getCost();
			$responce['cost_2']=Items::getFavoriteCost();

			$responce['state_1']=Yii::app()->cart->getCount();
			$responce['state_2']=$this->getFavoriteCount();
			
			if (Yii::app()->cart->itemAt($model->getId()) && $responce['itemCount']==1)
				$responce['view']=$this->renderPartial('_cartItem',array('data'=>$model),true);

		}
		return $responce;

	}

	public function actionRemoveFromCart($id,$state=1){
		if ($model=Items::model()->findByPk($id))
		{
			if ($state==1)
				Yii::app()->cart->remove($model->getId());
			else 
				FavoriteItem::model()->deleteAll('id_favorite=:f and id_item=:item',array(':f'=>Yii::app()->user->id_favorite,'item'=>$id));
			$responce=$this->getResponceData($model);
		}
		echo CJSON::encode($responce);
	}

	public function actionState($state,$id,$remove=false){
		$responce=array();
		$model=Items::model()->findByPk($id);

		if (Yii::app()->user->id_favorite){
			if ($state==1)
			{
				
				if ($model->favorite)
					$model->size=$model->favorite->size;
				$count=$model->favorite->count ? $model->favorite->count : 1;

				Yii::app()->cart->put($model,$count);

				if ($remove && $model->favorite)
				{
					$prevCount=$this->getFavoriteCount();
					$model->favorite->delete();
				}
			}
			else 
			{
				$prevCount=$this->getFavoriteCount();
				$fItem=$model->favorite ? $model->favorite : new FavoriteItem;
				$fItem->attributes=array(
					'id_favorite'=>Yii::app()->user->id_favorite,
					'id_item'=>$id,
					'count'=>$model->position ? $model->position->getQuantity() : 1,
					'size'=>$model->position ? $model->position->size : Items::DEFAULT_SIZE,
				);

				$fItem->save();

				if ($remove)
					Yii::app()->cart->remove($model->getId());
			}
		}
		$responce=$this->getResponceData($model,$state);
		$responce['showPopUp']=$this->getFavoriteCount() && $prevCount==0;
		echo CJSON::encode($responce);
		Yii::app()->end();
	}

	public function actionView($alias)
	{
		$model=Items::model()->find('alias=:alias',array(':alias'=>$alias));
		$this->model=$model;
		$this->breadcrumbs=array('Каталог'=>'/catalog',$model->name);
		$criteria=new CDbCriteria;
		if ($model->recomendations)
		{
			$criteria->addInCondition('id',$model->recomendations);
			$recomendations=Recomendation::model()->findAll($criteria);
		}
		$this->render('view',array('model'=>$model,'recomendations'=>$recomendations));
	}

	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Items');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}
}
