<?php

class SiteController extends FrontController
{
	public $layout = '//layouts/main';
	/**
	 * Declares class-based actions.
	 */
	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex($hash='')
	{

		$this->breadcrumbs=array();
		$discount=Discount::model()->find('hash_key=:alias',array(':alias'=>$hash));
		if (!$discount && $hash)
			throw new CHttpException("Страница не найдена", 404);
		if ($discount)
		{
			if ($discount->status==1)
				throw new CHttpException("Страница не найдена", 404);

			$datetime1 = date_create('now');
			$datetime2 = date_create($discount->dt_end);
			$interval = date_diff($datetime1, $datetime2);
			$diff=$discount->status ? 0 : (int)$interval->format('%R%a') ;
			if ($diff>0)
				Yii::app()->request->cookies[md5('bbf_discount')] = new CHttpCookie(md5('bbf_discount'), $diff ? true : false);
			else 
			{
				$discount->delete();
				throw new CHttpException("К сожалению время дайствия скидки вышло!", 403);
			}
			
			$discount->status=1;
			$discount->save();
		}
		$best=Items::model()->findAll('best=true');
		$slider=Carusel::model()->findByPk(1);
		$catalogs=Catalog::model()->findAll('display=true');
        $this->title = Yii::app()->config->get('app.name');
        
        if (!count($best)%4==0)
			for ($i=0; $i <count($best)%4 ; $i++) { 
				$best[]=new Items;
			}


		$this->render('index',
			array(
				'best'=>$best,
				'slider'=>$slider->item_list ? $slider->item_list : array(),
				'catalogs'=>$catalogs ? $catalogs : array(),
				'showDiscount'=>$diff ? true : false,
			)
		);
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		$this->layout='//layouts/simple';
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}
}