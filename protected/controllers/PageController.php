<?php

class PageController extends FrontController
{
	public $layout='//layouts/main';
	
	public function actionView($alias)
	{
		$model=Page::model()->find('alias=:alias',array(':alias'=>$alias));
		$this->model=$model;
		$this->breadcrumbs=array($model->title);
		$this->render('view',array(
			'model'=>$model,
		));
	}

	public function actionDiscount($hash){
		
		$discount=Discount::model()->find('hash_key=:hash',array(':hash'=>$hash));
		
		if (!$discount)
			throw new CHttpException("К сожалению время действия скидки вышло!", 403);

		$this->breadcrumbs=array('Скидка для друзей');
		$datetime1 = new DateTime('now');
		$datetime2 = new DateTime($discount->dt_end);
		$interval = $datetime1->diff($datetime2);

		// if ($interval->format('%R%a')<=0)
		// 	throw new CHttpException("К сожалению время действия скидки вышло!", 403);

		$this->render('_discount',array('model'=>$discount));
	}

	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Page');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}
}
