<?php

class CartController extends FrontController
{
	public $layout='//layouts/main';
	public $name="Оформление заказа";
	
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel('Book', $id),
		));
	}
	
	public function actionIndex($state=1)
	{	
		$this->breadcrumbs=array('Корзина');
		if ($state==2)
			$this->breadcrumbs=array('Корзина'=>'/cart','избранное');
		
		$models=$state==2 ? Items::getFavorite() : Yii::app()->cart->getPositions();
		$this->render('index',array(
			'models'=>$models,
			'state'=>$state,
		));
	}
}
