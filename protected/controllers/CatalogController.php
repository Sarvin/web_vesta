<?php

class CatalogController extends FrontController
{
	public $layout='//layouts/main';

	public function actionGetContent(){
		$response=array();
		if ($_GET['Filter'])
		{
			$filter=new Filter;
			$filter->attributes=$_GET['Filter'];
			$filter->getCriteria();
			$filter->criteria->addCondition('t.status=1');

			$dataProvider=new CActiveDataProvider('Items', array(
			    'criteria'=>$filter->criteria,
			    'countCriteria'=>array(
			        'condition'=>'status=1',
			    ),
			    'pagination'=>array(
			        'pageSize'=>20,
			    ),
			));
			$response['success']=$this->renderPartial('_items',array('dataProvider'=>$dataProvider),true);
		}
		echo CJSON::encode($response);
		Yii::app()->end();
	}

	public function actionAutoComplite($search=''){
		$search=new Search;
		$query=$_GET['query'];
		if ($query){
			$search->term=$query;
			$response['suggestions']=$search->getAutoCompliteData();
			echo CJSON::encode($response);
		}
	}

	public function actionSearch($search){
		$search=new Search;
		$search->term=$search;

		$search->getCriteria();

		$dataProvider=new CActiveDataProvider('Items', array(
		    'criteria'=>$search->criteria,
		    'countCriteria'=>array(
		        'condition'=>'status=1',
		    ),
		    'pagination'=>array(
		        'pageSize'=>20,
		    ),
		));

		$filter=new Filter;

		$this->render('index',array('filter'=>$filter,'dataProvider'=>$dataProvider));
	}

	public function actionView($alias='')
	{
		
	}
	
	public function actionIndex()
	{

		Yii::app()->clientScript->registerScriptFile($this->getAssetsUrl().'/js/filter.js');
		$filter=new Filter;
		if ($_GET['Filter'])
		{
			$filter->attributes=$_GET['Filter'];
			$filter->getCriteria();
			if (count($filter->catalogs)==1)
				$model=Catalog::model()->findByPk($filter->catalogs);
		}	
		
		$this->breadcrumbs=array('Каталог'=>'/catalog','фильтр');
		$dataProvider=new CActiveDataProvider('Items', array(
			    'criteria'=>$filter->criteria,
			    'countCriteria'=>array(
			        'condition'=>'status=1',
			    ),
			    'pagination'=>array(
			        'pageSize'=>20,
			    ),
			)
		);
		$this->render('index',array(
			'model'=>$model,
			'filter'=>$filter,
			'dataProvider'=>$dataProvider,
			)
		);
	}
}
