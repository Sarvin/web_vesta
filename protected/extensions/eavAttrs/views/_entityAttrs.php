<?
Yii::app()->clientScript->registerScriptFile($this->getAssetsUrl().'/js/colpick.js');
?>

<?=$renderContainer ? '<div class="eav">' : ''?>
	<div class="EavAttributes">
		<?
			$this->widget('appext.eavAttrs.SetsWidget', array(
				'groups' => $model->eavSetModels,
				'modelName'=>$modelName,
				'regScript'=>$renderAssets,
			));
		?>
		<?
			$this->widget('appext.eavAttrs.AttachedAttrWidget', array(
				'model' => $model,
				'modelName'=>get_class($model),
				'regScript'=>$renderAssets
			));
		?>
		<?
			$this->widget('appext.eavAttrs.AttachedSetsWidget', array(
				'model' => $model,
				'modelName'=>get_class($model),
				'regScript'=>$renderAssets
			));
		?>
		<?
			$this->widget('appext.eavAttrs.AttributesWidget', array(
				'attributes' => $model->eavAttrModels,
				'modelName'=>$modelName,
				'regScript'=>$renderAssets,
			));
		?>
	</div>
<?=$renderContainer ? '</div>' : ''?>