<?php
	$this->widget('ext.select2.ESelect2',array(
	  'attribute'=>'eavSets',
	  'data'=>AttrGroup::getList(),
	  'model'=>$model,
	  'htmlOptions'=>array(
	    'multiple'=>'multiple',
	    'class'=>'span12',
	    'id'=>'eavSets',
	  ),
	  'options'=>array(
	    'placeholder'=>'Выбрать',
	    'allowClear'=>false,
	    'maximumSelectionSize'=>10,
	  ),
	));
?>
<script type="text/javascript">
$(function(){
	$('#eavSets').on('select2-removing',function(e){
		if (confirm('Удалить? При удалении все значения атрибутов будут стерты!')) {
			$('.eav-group-'+e.val).remove();
			return true;
		} else {
			return false;
		}
	})
})
</script>
