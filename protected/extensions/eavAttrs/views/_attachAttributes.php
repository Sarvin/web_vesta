<?php
	
	$this->widget('ext.select2.ESelect2',array(
	  'attribute'=>'eavAttributes',
	  'data'=>Attr::getList(),
	  'model'=>$model,
	  'htmlOptions'=>array(
	    'multiple'=>'multiple',
	    'class'=>'span12',
	    'id'=>'eavAttributes',
	  ),
	  'options'=>array(
	    'placeholder'=>'Выбрать',
	    'allowClear'=>false,
	    'maximumSelectionSize'=>10,
	  ),
	));
?>
<script type="text/javascript">
$(function(){
	$('#eavAttributes').on('select2-removing',function(e){
		if (confirm('Удалить? При удалении все значения атрибутов будут стерты!')) {
			$('.eav-attr-'+e.val).remove();
			return true;
		} else {
			return false;
		}
	})
})
</script>

