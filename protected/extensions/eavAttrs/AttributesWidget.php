<?php

class AttributesWidget extends CWidget {

    public $attributes;
    public $controllerRoute;
    public $modelName;
    public $assets;
    public $parent;
    public $regScript=false;

    public function init(){
        if ($this->regScript)
        {
            $this->assets = Yii::app()->getAssetManager()->publish(dirname(__FILE__) . '/assets', false, -1, true);
            $cs=Yii::app()->clientScript;
            $cs->registerCssFile($this->assets.'/css/colpick.css');
            $cs->registerScriptFile($this->assets.'/js/colpick.js',CClientScript::POS_END);
            $cs->registerCssFile($this->assets.'/css/attributes.css');
            $cs->registerScriptFile($this->assets.'/js/initFunctions.js',CClientScript::POS_END);
        }
        
    }

    public function regAddFieldScript($id,$view,$class){
        
        Yii::app()->clientScript->registerScript('append_'.$id  ,'
                $("#btn'.$id.'").on("click",function(){
                    
                     $(\'.multy-common-'.$id.'\').append(\''.preg_replace('%[\r\n|\n|\r]%', "", $view).'\');
                    init'.$class.'();
                });
            ',CClientScript::POS_END);
    }

    public function run(){
            
        if ($this->attributes){
            $data=$this->attributes[0];
            $id=$data->parent ? $data->parent_model->ident->id_model : $data->ident->id_model;
            echo "<div class=\"eav-group-".$id."\">";
            echo "<p><strong>Общие характеристики</strong></p>";

            foreach ($this->attributes as $key => $data) {
                $name=$this->modelName.'[eavAttributeValues]['.$data->id.']';
                $id=$this->modelName.'_eavAttributeValues_'.$data->id;
                
                $fieldView=$data->attr->field_type->id_widget!=null && $data->attr->field_type->id_widget!=0 ? $data->attr->widget : 'string';
                $btn_id=$key;
                $fieldView=$data->attr->widget ? $data->attr->widget : 'string';
                if ($data->rangeValues)
                {
                    echo "<br>";
                    echo "<p class=\"caption\">$data->label</p>";
                    $data->label="";
                    $range=CHtml::listData($data->rangeValues,'key','value');
                    $this->regAddFieldScript($btn_id, Yii::app()->controller
                                                ->renderPartial('appext.eavAttrs.views.attrTypeView/'.$fieldView,
                                                array('model'=>$data,'range'=>$range,'name'=>$name.'[]'),true),$fieldView);

                    echo "<div class=\"multy-common-".$btn_id."\">";
                    foreach ($data->multyVals as $key => $multy) {
                        $multy->label="";
                        $this->render('attrTypeView/'.$fieldView,array('model'=>$multy,'range'=>$range,'name'=>$name.'['.$multy->id.']','id'=>$id));
                    }

                    $this->render('attrTypeView/'.$fieldView,array('model'=>$data,'range'=>$range,'name'=>$name.'[]','id'=>$id));
                    echo "</div>";
                    
                    echo TbHtml::button('Добавить',array('id'=>'btn'.$btn_id));
                } else {
                    $this->render('attrTypeView/'.$fieldView,array('model'=>$data,'name'=>$name,'id'=>$id,'range'=>($data->isRanged ? CHtml::listData($data->rangeValues,'key','value') : array())));
                }
                Yii::app()->clientScript->registerScript('init','initAllWidgets()');
            }
            echo "</div>";
        }
    }
}