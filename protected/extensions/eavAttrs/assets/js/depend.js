$(function(){	
	$('#eavAttr').on('change',function(e){
		e.preventDefault();
		if (e.removed!=undefined){
			if (confirm('Удалить? При удалении все значения атрибутов будут стерты!')) {
				$('.eav-group-'+e.removed.id).closest('.EavAttributes').remove();
				return true;
			} else {
				return false;
			}
		}
	})

	$('#eavAttr').on('change',function(e){
		$this = $(this);
		if (typeof e.added!="undefined")
		{
			$.ajax({
				url:"/admin/attr/getEavAttrViews",
				data:{id:e.added.id,entity:1,modelName:$this.data('model')},
				dataType:"JSON",
				type:'get',
				success:function(data)
				{
					$('.eav').append(data.success);
				},
				error:function(data){
					
				}
			})
		}
	})
})