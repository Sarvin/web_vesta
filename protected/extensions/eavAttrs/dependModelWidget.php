<?php

class dependModelWidget extends CWidget {
    private $_model;
    
    public $modelName;
    public $regScript=true;
    public $attribute;
    public $data=array();
    public $htmlOptions=array();

    protected $assets;
    
    public function getModel(){
        return $this->_model;
    }

    public function setModel($data){
        $this->_model=$data;
        $this->modelName=get_class($data);
    }

    public function init(){
        if ($this->regScript)
        {
            $this->assets = Yii::app()->getAssetManager()->publish(dirname(__FILE__) . '/assets', false, -1, true);
            $cs=Yii::app()->clientScript;
            $cs->registerCssFile($this->assets.'/css/colpick.css');
            $cs->registerScriptFile($this->assets.'/js/colpick.js',CClientScript::POS_END);
            $cs->registerScriptFile($this->assets.'/js/initFunctions.js',CClientScript::POS_END);
            $cs->registerScriptFile($this->assets.'/js/depend.js',CClientScript::POS_END);
        }
    }

    public function run(){
        echo '<br><div class="control-group">';
                echo CHtml::activeLabelEx($this->model,$this->attribute);
                $this->widget('ext._select2.ESelect2',array(
                  'model'=>$this->model,
                  //'name'=>'Items[marks]',
                  'attribute'=>$this->attribute,
                  'data'=>$this->data,
                  'htmlOptions'=>array(
                    'multiple'=>'multiple',
                    'id'=>'eavAttr',
                    'data-model'=>$this->modelName,
                  ),
                  'options'=>array(
                    'placeholder'=>'Выбрать',
                    'allowClear'=>false,
                    'width'=>'66%',
                  ),
                ));

        echo '</div>';
    }
}