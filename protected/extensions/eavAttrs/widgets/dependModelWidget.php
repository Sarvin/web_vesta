<?php

class SetsWidget extends CWidget {
    private $_model;
    
    public $controllerRoute;
    public $modelName;
    public $regScript=true;
    public $attribute;
    public $data=array();
    public $htmlOptions=array();

    protected $assets;
    
    public function getModel(){
        return $this->_model;
    }

    public function setModel($data){
        $this->_model=$data;
        $this->modelName=get_class($model);
    }

    public function init(){
        if ($this->regScript)
        {
            $this->assets = Yii::app()->getAssetManager()->publish(dirname(__FILE__) . '/assets', false, -1, true);
            $cs=Yii::app()->clientScript->registerScriptFile($this->assets.'/js/depend.js',CClientScript::POS_END);
        }
    }

    public function run(){
        echo '<div class="control-group">';
                echo $form->labelEx($model,$this->attribute);
                $this->widget('ext.select2.ESelect2',array(
                  'model'=>$this->model,
                  //'name'=>'Items[marks]',
                  'attribute'=>$this->attribute,
                  'data'=>$data,
                  'htmlOptions'=>array(
                    'multiple'=>'multiple',
                    'class'=>'span8',
                    'id'=>'eavAttr',
                    'data-model'=>$this->modelName,
                  ),
                  'options'=>array(
                    'placeholder'=>'Выбрать',
                    'allowClear'=>false,
                  ),
                ));
        echo '</div><br><br>';
    }
}