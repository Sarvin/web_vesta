<?php

class AttachedAttrWidget extends CWidget {

    public $model;
    public $modelName;
    public $assets;
    public $parent;
    public $regScript=false;

    public function init(){
        if ($this->regScript)
        {
            $this->assets = Yii::app()->getAssetManager()->publish(dirname(__FILE__) . '/assets', false, -1, true);
            $cs=Yii::app()->clientScript;
            $cs->registerCssFile($this->assets.'/css/attributes.css');
            $cs->registerCssFile($this->assets.'/css/jquery.simpleselect.css');
            $cs->registerScriptFile($this->assets.'/js/initFunctions.js',CClientScript::POS_END);
            $cs->registerScriptFile($this->assets.'/js/jquery.simpleselect.js',CClientScript::POS_END);
        }
    }

    public function regAddFieldScript($id,$view,$class){
        
        Yii::app()->clientScript->registerScript('append_attached_attr_'.$id  ,'
                $("#btn_attr'.$id.'").on("click",function(){
                     $(\'.multy-attached-attr-'.$id.'\').append(\''.preg_replace('%[\r\n|\n|\r]%', "", $view).'\');
                    init'.$class.'();
                });
            ',CClientScript::POS_END);
    }

    public function run(){
        if ($this->model->attachedAttrs){
            $id=$data->parent ? $data->parent_model->ident->id_model : $data->ident->id_model;
            echo "<div class=\"eav-attached-attrs\">";
            echo "<hr>";
            echo "<p><strong>Прицепленные атрибты</strong></p>";

            foreach ($this->model->attachedAttrs as $key => $data) {
                $name=$this->modelName.'[AttachedAttr]['.$data->id.']';
                $id=$this->modelName.'_attachedAttr_'.$data->id;
                
                $fieldView=$data->field_type->id_widget!=null && $data->field_type->id_widget!=0 ? $data->widget : 'string';
                $btn_id=$data->id;
                $fieldView=$data->widget ? $data->widget : 'string';
                if ($data->field_type->value_range==1)
                {
                    echo "<br>";
                    echo "<p class=\"caption\">$data->label</p>";
                    $data->label="";
                    $range=CHtml::listData($data->rangeValues,'key','value');
                    $this->regAddFieldScript($btn_id, Yii::app()->controller
                                                ->renderPartial('appext.eavAttrs.views.attrTypeView/'.$fieldView,
                                                array('model'=>$data,'range'=>$range,'name'=>$name.'[]'),true),$fieldView);

                    echo "<div class=\"multy-attached-attr-".$btn_id."\">";
                    foreach ($data->multyVals as $key => $multy) {

                        $multy->label="";
                        $this->render('attrTypeView/'.$fieldView,array('model'=>$multy,'range'=>$range,'name'=>$name.'['.$multy->id.']','id'=>$id));
                    }
                    $data->value="";
                    $this->render('attrTypeView/'.$fieldView,array('model'=>$data,'range'=>$range,'name'=>$name.'[]','id'=>$id));
                    echo "</div>";
                    
                    echo TbHtml::button('Добавить',array('id'=>'btn_attr'.$btn_id)).'<hr>';
                } else {
                    $this->render('attrTypeView/'.$fieldView,array('model'=>$data,'name'=>$name,'id'=>$id,'range'=>($data->isRanged ? CHtml::listData($data->rangeValues,'key','value') : array())));
                }
                Yii::app()->clientScript->registerScript('init','initAllWidgets()');
            }
            echo "</div>";
        }
    }
}