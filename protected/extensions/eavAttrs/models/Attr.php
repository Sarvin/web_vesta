<?php

/**
* This is the model class for table "{{attr}}".
*
* The followings are the available columns in table '{{attr}}':
    * @property integer $id
    * @property integer $id_attr_type_field
    * @property string $label
    * @property string $alias
*/
class Attr extends EActiveRecord
{

    private $_isRanged;
    private $_rangeValues=false;
    private $_valType=false;
    private $_widget=false;
    private $_value=false;
    private $_id_attr_val=false;
    private $_multyVals=false;
    private $_isMulty=false;

    private $_id_model;
    private $_valueView=false;

    public $titleAttr="label";
    public $attach=null;
    public $required;
    public $id_model_ident;
    public $id_attr_model;
    public $id_group;
    public $post_id_type=1;

    public function translition(){
        return 'Атрибуты';
    }

    public function tableName()
    {
        return '{{attr}}';
    }

    public function setIdent($ident)
    {
        $this->id_model_ident=$ident;
        return $this;
    }

    public function scopes()
    {
        return array(
            'applies'=>array(
                'condition'=>'id_model_ident=:ident',
                'params'=>array(':ident'=>$this->id_model_ident),
                'join'=>'inner join {{attr_model}} attr_model on attr_model.id_attr=t.id',
                'select'=>'*,t.id as id,id_model_ident,attr_model.id as id_attr_model'
            ),
        );
    }

    public static final  function getList($id=null){
        $list=CHtml::listData(Attr::model()->findAll(),'id','label');
        return $id ? $list[$id] : $list;
    }

    public function getUrl(){
        
    }

    public function menu(){
        $criteria=new CDbCriteria;
        $criteria->join="inner join {{attr_field_type}} field_type on t.id_attr_type_field=field_type.id";
        $criteria->addCondition('id_attr_val_type=2');
        return CHtml::listData(Attr::model()->findAll($criteria),'id','label');
    }

    public function rules()
    {
        $rules=array(
            array('label,id_attr_type_field','required'),
            array('id_attr_type_field, id_attr_model, id_attr_val, id_model_ident, id_model, id_group', 'numerical', 'integerOnly'=>true),
            array('label, value, alias', 'length', 'max'=>255),
            array('required, id_group, key_type, attach','safe'),
            // The following rule is used by search().
            array('id, id_attr_type_field, label, alias', 'safe', 'on'=>'search'),
        );

        if ($this->required)
            $rules[]=array('value','required');

        return $rules;
    }
    public function getId_model(){
        return $this->_id_model;
    }
    public function setId_model($data){
        $this->_id_model=$data;
    }

    public function getWidget(){
        if ($this->_widget===false)
        {
            $this->_widget=AttrFieldType::getWidgetList($this->field_type->id_widget ? $this->field_type->id_widget : 0);
        }
        return $this->_widget;
    }

    public function getId_attr_val(){
        if ($this->_id_attr_val===false)
        {
            if ($this->id_group)
                $result=Yii::app()->db->createCommand()
                    ->select('id')
                    ->from('{{attr_val}}')
                    ->where('id_group=:group and post_id=:attr and post_type=true',array(':group'=>$this->id_group,':attr'=>$this->id))
                    ->queryRow();
            else 
                $result=Yii::app()->db->createCommand()
                    ->select('id')
                    ->from('{{attr_val}}')
                    ->where('post_id=:attr and post_type=false',array(':attr'=>$this->id_attr_model))
                    ->queryRow();
            $this->_id_attr_val=$result['id'];
        }
        return $this->_id_attr_val;
    }

    public function setId_attr_val($data){
        $this->_id_attr_val=$data;
    }

    public function getValueView(){
        if ($this->multyVals)
        {
            if ($this->isRanged)
            {
                $range=CHtml::listData($this->rangeValues,'key','value');
                foreach ($this->multyVals as $key => $multy) {
                    $vals[]=$range[$multy->value];
                }
                $this->_valueView=implode(', ', $vals);
            }else 
                $this->_valueView=implode(', ', CHtml::listData($this->multyVals,'id','value'));
        } else {
            if ($this->isRanged)
            {
                $this->_valueView=RangeValue::model()->find('t.id_range=:range and t.key=:key',array(':range'=>$this->range->id,':key'=>$this->value))->value;
            } else 
                $this->_valueView=$this->value;
        }
        return $this->_valueView;
    }

    public function getValue(){
        if ($this->_value===false)
        {
            if (!$this->id_attr_model)
                $result=Yii::app()->db->createCommand()
                    ->select('value')
                    ->from('{{attr_val}}')
                    ->where('id_group=:group and post_id=:attr and post_type=true',array(':group'=>$this->id_group,':attr'=>$this->id))
                    ->queryRow();
            else 
                $result=Yii::app()->db->createCommand()
                    ->select('value')
                    ->from('{{attr_val}}')
                    ->where('post_id=:attr and post_type=false',array(':attr'=>$this->id_attr_model))
                    ->queryRow();
            $this->_value=$result['value'];
        }
        return $this->_value;
    }

    public function setValue($data){
        $this->_value=$data;
    }

    public function getValType(){
        if ($this->_valType===false)
            $this->_valType=$this->field_type->val_type;
        return $this->_valType;
    }
    
    public function getIsMulty(){
        if (!$this->_isRanged)
            $this->_isRanged=Yii::app()->db->CreateCommand()
                ->select('value_range as multy')
                ->from('{{attr_field_type}} t')
                ->join('{{attr_val_type}} val_type','val_type.id=t.id_attr_val_type')
                ->where('t.id=:id',array(':id'=>$this->id_attr_type_field))
                ->queryRow();
        return $this->_isRanged['multy']==1;
    }

    public function getIsRanged(){
        if (!$this->_isRanged)
            $this->_isRanged=Yii::app()->db->CreateCommand()
                ->select('id_attr_val_type as ranged')
                ->from('{{attr_field_type}} t')
                ->join('{{attr_val_type}} val_type','val_type.id=t.id_attr_val_type')
                ->where('t.id=:id',array(':id'=>$this->id_attr_type_field))
                ->queryRow();
        return $this->_isRanged['ranged']==2;
    }

    public function getMultyVals(){
        if ($this->_multyVals===false){
            $criteria=new CDbCriteria;
            $criteria->addCondition('id_attr_val=:val');
            $criteria->params[':val']=$this->id_attr_val;
            $criteria->select="*,'".$this->label."' as label,false as required";
            $this->_multyVals = AttrMultyVal::model()->findAll($criteria);
        }
        return $this->_multyVals;
    }

    public function setMultyVals($data){
        $this->_multyVals=$data;
    }

    public function relations()
    {
        return array(
            'range'=>array(self::HAS_ONE,'AttrRange','id_attr'),
            'field_type'=>array(self::BELONGS_TO,'AttrFieldType','id_attr_type_field'),
            'attr_models'=>array(self::HAS_MANY,'AttrModel','id_attr'),
        );
    }

    public function setRangeValues($data)
    {
        $this->_rangeValues=$data;
    }

    public function getRangeValues(){

        if ($this->_rangeValues===false)
            if ($this->range)
                $this->_rangeValues=$this->range->rangeValues;
        return is_array($this->_rangeValues)  ? $this->_rangeValues : array();
    }

    public function afterFind(){
        parent::afterFind();
        if (!$this->id_attr_model && $this->id_model_ident)
        {
            $result=Yii::app()->db->createCommand()
                ->select('id')
                ->from('{{attr_model}}')
                ->where('id_model_ident=:ident and id_attr=:attr and attach=:attach',array(':ident'=>$this->id_model_ident,':attr'=>$this->id,':attach'=>$this->attach))
                ->queryRow();
            $this->id_attr_model=$result['id'];
        }
        return true;
    }

    public function beforeDelete(){

        parent::beforeSave();

        if ($range)
            foreach ($this->range->rangeValues as $key => $value) {
                $value->delete();
            }

        if ($this->attr_models)
        {   
            foreach ($this->attr_models as $key => $attr_m) {
                $attr_m->delete();
            }
        }

        return true;
    }
    public function beforeSave(){
        parent::beforeSave();
        return true;
    }
    public function afterSave(){
        parent::afterSave();
        $range=!$this->range ? new AttrRange : $this->range;
        if ($this->isRanged)
        {
            $range->id_attr=$this->id;
            $range->save();
            RangeValue::model()->deleteAll('id_range=:range',array(':range'=>$range->id));
            foreach ($this->rangeValues as $key => $value) {
                $rangeVal=new RangeValue;
                $rangeVal->id_range=$range->id;
                $rangeVal->key=$key;
                $rangeVal->value=$value;
                $rangeVal->save();
            }
        }
    }
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'id_attr_type_field' => 'Тип поля',
            'label' => 'Название',
            'alias' => 'Уникальное имя',
            'key_type'=>'Тип ключа',
        );
    }



    public function search()
    {
        $criteria=new CDbCriteria;
		$criteria->compare('id',$this->id);
		$criteria->compare('id_attr_type_field',$this->id_attr_type_field);
		$criteria->compare('label',$this->label,true);
		$criteria->compare('alias',$this->alias,true);
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }


}
