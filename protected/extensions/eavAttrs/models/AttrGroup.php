<?php

/**
* This is the model class for table "{{attr_group}}".
*
* The followings are the available columns in table '{{attr_group}}':
    * @property integer $id
    * @property integer $name
    * @property integer $alias
*/

class AttrGroup extends EActiveRecord
{
    private $_eavAttributes=false;
    private $_attrs=false;
    private $_eavAttrs=array('value'=>false,'required'=>array());//используется для привязки атрибутов к группе
    public $post_id_type=2;
    public $id_model_ident;
    public $id_model_group_set;
    

    public function getEavAttrs(){
        
        if ($this->_eavAttrs['value']===false)
        {
            $result=Yii::app()->db->createCommand()
              ->select('t.id,required')
              ->from('{{attr}} t')
              ->join('{{group_set_attr}} gas','gas.id_attr=t.id')
              ->where('gas.id_group=:id',array(':id'=>$this->id))
              ->queryAll();

            foreach ($result as $key => $value) {
                $this->_eavAttrs['value'][$value['id']]=$value['id'];
                if ($value['required'])
                    $this->_eavAttrs['required'][$value['id']]=$value['id'];
            }
        }
        return  $this->_eavAttrs['value']===false ? array('value'=>array(),'required'=>array()) : $this->_eavAttrs;
    }

    public function setEavAttrs($data){

        $this->_eavAttrs=$data;
    }

    public function getAttrs(){
        if ($this->_attrs===false)
        {
            $this->_attrs=$this->attr_list;
            foreach ($this->_attrs as $key => $value) {
                $value->id_group=$this->id_model_group_set;
            }
        }
        return $this->_attrs;
    }
    public function setAttrs($data){
        $this->_attrs=$data;
    }
    public static final  function getList($id=null){
        $list=CHtml::listData(AttrGroup::model()->findAll(),'id','name');
        return $id ? $list[$id] : $list;
    }

    public function translition(){
        return 'Группы атрибутов';
    }

    public function getEavAttributes(){
        
        if ($this->_eavAttributes===false)
        {
            $criteria=new CDbCriteria;
            $criteria->join="left join {{group_set_attr}} gas on gas.id_attr=t.id";
            $criteria->select="*,t.id,:id as id_group,label,required";
            $criteria->addCondition('id_group=:group');
            $criteria->params[':group']=$this->id;
            $criteria->params[':id']=$this->id_model_group_set;
            $this->_eavAttributes=Attr::model()->findAll($criteria);
        }
        return  $this->_eavAttributes ? $this->_eavAttributes : array();
    }

    public function setEavAttributes($data){
        $this->_eavAttributes=$data;
    }

    public function tableName()
    {
        return '{{attr_group}}';
    }

    public static function getAttrList($id=null){
        $attrs=CHtml::listData(Attr::model()->findAll(),'id','label');
        if ($id)
            return $attrs[$id];
        return $attrs;
    }

    public function rules()
    {
        return array(
            array('name','required'),
            array('alias','unique'),
            array('eavAttributes,eavAttrs','safe'),
            // The following rule is used by search().
            array('id, name, alias', 'safe', 'on'=>'search'),
        );
    }


    public function relations()
    {
        return array(
            'attr_list'=>array(self::MANY_MANY,'Attr','{{group_set_attr}}(id_group,id_attr)',
            ),
        );
    }

    public function afterSave(){

        $iDs=array();
        $criteria=new CDbCriteria;
        if ($this->eavAttrs['value'])
        {
            foreach ($this->eavAttrs['value'] as $key => $data) {
                $isRequired=$this->eavAttrs['required'][$data]!=null;
                $model=GroupSetAttr::model()->find('id_group=:group and id_attr=:attr',array(':group'=>$this->id,':attr'=>$data));
                
                if (!$model)
                {
                    $model=new GroupSetAttr;
                    $model->id_group=$this->id;
                }

                $model->id_attr=$data;
                $model->required=$isRequired;
                $model->save();
                $iDs[]=$model->id;
            }
            $criteria->addNotInCondition('id',$iDs);
        }

        $criteria->addCondition('id_group=:id');
        $criteria->params[":id"]=$this->id;

        $models=GroupSetAttr::model()->findAll($criteria);
        $deleted=array();

        if ($models)
        {
            foreach ($models as $key => $data) {
                $data->delete();
                
                if (!$deleted[$data->id_group])
                {
                    $result=ModelGroupSet::model()->findAll('id_group=:group',array(':group'=>$data->id_group));
                    if ($result)
                    {
                        $criteria=new CDbCriteria;
                        $criteria->addInCondition('id_group',$result);
                        AttrVal::model()->deleteAll('post_type=true and post_id=:post_id',
                            array(
                                ':post_id'=>$data->id_attr
                            )
                        );
                    }
                }
                $deleted[$data->id_group]=true;
            }
        }
    }
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'name' => 'Название группы атрибутов',
            'alias' => 'Уникальное название',
        );
    }

    public function beforeDelete(){
        parent::beforeDelete();

        foreach ($this->attr_list as $key => $value) {
            $value->delete();
        }

        return true;
    }

    public function search()
    {
        $criteria=new CDbCriteria;
		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name);
		$criteria->compare('alias',$this->alias);
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }


}
