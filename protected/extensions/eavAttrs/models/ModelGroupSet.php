<?php

/**
* This is the model class for table "{{model_group_set}}".
*
* The followings are the available columns in table '{{model_group_set}}':
    * @property integer $id
    * @property integer $id_model_ident
    * @property integer $id_group
*/
class ModelGroupSet extends EActiveRecord
{
    private $_eavAttributes=false;
    public $id_model_ident;
    public function getName(){
        return $this->group->name;
    }
    public function tableName()
    {
        return '{{model_group_set}}';
    }

    public function getEavAttributes(){

        if ($this->_eavAttributes===false)
        {
            $criteria=new CDbCriteria;
            $criteria->join="left join {{group_set_attr}} gas on gas.id_attr=t.id";
            $criteria->select="*,t.id,:id as id_group,label,required";
            $criteria->addCondition('id_group=:group');
            $criteria->params[':group']=$this->id_group;
            $criteria->params[':id']=$this->id;
            $this->_eavAttributes=Attr::model()->findAll($criteria);
        }
        return  $this->_eavAttributes ? $this->_eavAttributes : array();
    }

    public function beforeDelete(){
        parent::beforeDelete();
        $attrVals=AttrVal::model()->findAll('id_group=:group',array(':group'=>$this->id));
        foreach ($attrVals as $key => $attr) {
            $attr->delete();
        }
        return true;
    }

    public function setEavAttributes($data){
        $this->_eavAttributes=$data;
    }

    public function rules()
    {
        return array(
            array('id_model_ident, id_group, parent', 'numerical', 'integerOnly'=>true),
            array('id_model_ident, alias, id_group, attach','safe'),
            // The following rule is used by search().
            array('id, id_model_ident, id_group', 'safe', 'on'=>'search'),
        );
    }

    public function relations()
    {
        return array(
            'group'=>array(self::BELONGS_TO,'AttrGroup','id_group'),
            'ident'=>array(self::BELONGS_TO,'ModelIdent','id_model_ident'),
            'parent_model'=>array(self::BELONGS_TO,'ModelGroupSet','parent'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'id_model_ident' => 'Уникаьлный ключ модели',
            'attach'=>'Присоедененна',
            'id_group' => 'Группаатрибутов',
        );
    }

    public function search()
    {
        $criteria=new CDbCriteria;
		$criteria->compare('id',$this->id);
		$criteria->compare('id_model_ident',$this->id_model_ident);
		$criteria->compare('id_group',$this->id_group);
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

}
