<?
    class EavBehavior extends CActiveRecordBehavior
    {
        
        private $_eavAttributes=false;
        private $_eavAttributeValues=false;
        private $_eavAttrModels=false;
        private $_eavSets=false;
        private $_eavSetValues=false;
        private $_eavSetModels=false;
        private $_attachedAttrs=false;
        private $_attachedSets=false;
        private $_modelName=false;

        protected $isNew;
        protected $mirrors=array('sets'=>array(),'attrs'=>array());
        
        public $attachments=array();
        
    	public $entity_id;
        private $_id_model_ident;
        public $parentIdent=false;
        public $child_ident=true;
        public $depends;

        protected function getModelName(){
            if ($this->_modelName===false)
            {
                $this->_modelName=get_class($this->owner);
            }
            return $this->_modelName;
        }
        public function getId_model_ident()
        {
            return $this->owner->id_model_ident;
        }

        public function saveAttachedAttrsValues(){

            foreach ($_POST[$this->modelName]['AttachedAttr'] as $attr_id => $attr) {
                $attrModel=AttrModel::model()->find('attach=true and id_model_ident=:ident and id_attr=:attr',array(':attr'=>$attr_id,':ident'=>$this->id_model_ident));
                if ($attr || $attr==0)
                {   

                    $valIsArray=is_array($attr);
                    $attrVal=AttrVal::model()->find('post_id=:attr and post_type=false',array(':attr'=>$attrModel->id));
                    
                    if (!$attrVal)
                    {
                        $attrVal=new AttrVal;
                    }
                    $attrVal->attributes=array(
                        'post_id'=>$attrModel->id,
                        'post_type'=>false,
                        'value'=>$attrModel->isMulty ? '' : $attr,
                        'multy'=>$attrModel->isMulty ? true : null,
                    );
                    $attrVal->save($attrModel->isMulty);
                    if ($valIsArray && $attrVal->id)
                    {   
                        AttrMultyVal::model()->deleteAll('id_attr_val=:val',array(':val'=>$attrVal->id));
                        foreach ($attr as $multyVal_id => $multyVal) {
                            if ($multyVal)
                            {
                                $multy=new AttrMultyVal;
                                $multy->id_attr_val=$attrVal->id;
                                $multy->value=$multyVal;
                                $multy->save();
                            }
                        }
                    }
                }
            }
        }

        public function getAttachedAttrs(){

            if ($this->_attachedAttrs===false && $this->attachments['attr'])
            {   
                    $criteria=new CDbCriteria;
                    $criteria->addInCondition('t.id',$this->attachments['attr']);
                    $criteria->select="*,:ident as id_model_ident,true as attach";
                    $criteria->params[":ident"]=$this->id_model_ident;
                    $this->_attachedAttrs=Attr::model()->findAll($criteria);
                if (isset($_POST[$this->modelName]['AttachedAttr']))
                {
                    foreach ($this->_attachedAttrs as $key => $value) {
                        if ($value->isMulty)
                        {
                            $multyVals=array();
                            foreach ($_POST[$this->modelName]['AttachedAttr'][$value->id] as $key => $multyVal) {
                                $multy=new AttrMultyVal;
                                $multy->value=$multyVal;
                                $multyVals[]=$multy;
                            }
                            $value->multyVals=$multyVals;
                        }
                        else 
                        {
                            $value->value=$_POST[$this->modelName]['AttachedAttr'][$value->id];
                        }
                    }
                }
            }
            return $this->_attachedAttrs ? $this->_attachedAttrs : array();
        }

        public function getAttachedSets(){
            if ($this->_attachedSets===false && $this->attachments['group'])
            {
                if ($this->owner->isNewRecord)
                {
                    $criteria=new CDbCriteria;
                    $criteria->addInCondition('id',$this->attachments['group']);

                    $sets=AttrGroup::model()->findAll($criteria);
                    $result=new CAttributeCollection;
                    foreach ($sets as $key => $data) {
                        $attrs=array();
                        $data->id_model_ident=$this->id_model_ident;
                        foreach ($data->attrs as $key => $attr) {
                            if (!is_array($_POST[get_class($this->owner)]['AttachedSets'][$data->id][$attr->id]))
                                $attr->value=$_POST[get_class($this->owner)]['AttachedSets'][$data->id][$attr->id];
                            else {
                                $multyVals=array();
                                foreach ($_POST[get_class($this->owner)]['AttachedSets'][$data->id][$attr->id] as $key => $multy) {
                                    if ($multy)
                                    {
                                        $multyAttr=new AttrMultyVal;
                                        $multyAttr->value=$multy;
                                        $multyVals[]=$multyAttr;
                                    }
                                    
                                }
                                $attr->multyVals=$multyVals;
                            }
                            $attrs[]=$attr;
                        }
                        $data->attrs=$attrs;
                        $result->add($alias,$data);
                    }
                    $this->_attachedSets=$result;
                } else
                {
                    $findCriteria=new CDbCriteria;
                    $findCriteria->addInCondition('id',$this->attachments['group']);

                    $this->_attachedSets=AttrGroup::model()->findAll($findCriteria);
                    $result=new CAttributeCollection;
                    
                    foreach ($this->_attachedSets as $key => $value) {
                        $modelGroupSetId=Yii::app()->db->createCommand()
                            ->select('id')
                            ->from('{{model_group_set}}')
                            ->where('id_model_ident=:ident and id_group=:group and attach=true',array(':ident'=>$this->id_model_ident,':group'=>$value->id))
                            ->queryRow();
                        $modelGroupSetId=$modelGroupSetId['id'];
                        $alias=$value->alias;
                        $value->id_model_ident=$this->id_model_ident;
                        $value->id_model_group_set=$modelGroupSetId;
                        $result->add($alias,$value);
                        //$result->$alias=$value;
                    }

                    $this->_attachedSets=$result;
                }
                
            }
            return $this->_attachedSets ? $this->_attachedSets : array();
        }

        public function getEavSetModels(){
            if ($this->_eavSetModels===false)
            {
                if ($this->owner->isNewRecord)
                {
                    $attribute=$this->depends['attribute'];
                    $result=new CAttributeCollection;
                    foreach ($this->owner->$attribute as $key => $data) {
                        $ident=ModelIdent::model()->find('id_model=:model and id_entity=:entity',array(':model'=>$data,':entity'=>$this->depends['entity_id']));
                        foreach ($ident->sets as $key => $set) {
                            if (!in_array($set->id, $this->attachments['group']))
                            {
                                foreach ($set->eavAttributes as $key => $attr) {
                                    $attr->value=$_POST[get_class($this->owner)]['eavSetValues'][$set->id][$attr->id];
                                }
                                $result->add($alias,$set);
                            }
                        }
                    }
                    $this->_eavSetModels=$result;
                } else
                {
                    $findCriteria=new CDbCriteria;
                    $findCriteria->addCondition('id_model_ident=:ident and attach is null');
                    $findCriteria->params=array(':ident'=>$this->id_model_ident);
                    $this->_eavSetModels=ModelGroupSet::model()->findAll($findCriteria);
                }
                
            }
            return $this->_eavSetModels;
        }

        public function setEavSetModels($data){
            $this->_eavSetModels=$data;
        }

        public function getEavSets(){
            
            if ($this->_eavSets===false && $this->owner->id_model_ident){
                $this->_eavSets=CHtml::listData(ModelGroupSet::model()->findAll('id_model_ident=:id',array(':id'=>$this->owner->id_model_ident)),'id_group','id_group');
            }
            return $this->_eavSets;
        }

        public function setEavSets($data){
            $this->_eavSets=$data;
        }

        public function getEavAttributeValues(){

            if ($this->owner->id_model_ident && $this->_eavAttributeValues===false)
            {
                return $this->_eavAttributeValues=CHtml::listData(AttrVal::model()->findAll('id_model_ident=:ident and (post_type!=true)',array(':ident'=>$this->owner->id_model_ident)),'id_attr','value');
            }
            return array();
        }

        public function setEavAttributeValues($data){
            $this->_eavAttributeValues=$data;
        }

        public function getEavAttrModels(){
            
            if ($this->_eavAttrModels===false && $this->owner->id_model_ident)
            {
                $criteria=new CDbCriteria;
                $criteria->addCondition('t.id_model_ident=:ident and attach is null');
                $criteria->params[':ident']=$this->owner->id_model_ident;
                $criteria->join="right join {{attr}} attr on attr.id=t.id_attr";
                $criteria->select="*,t.id,attr.id as id_attr,required,t.id_model_ident";
                $this->_eavAttrModels=AttrModel::model()->findAll($criteria);
            }
            return $this->_eavAttrModels ? $this->_eavAttrModels : array();
        }

        public function getEavAttributes(){
            if ($this->_eavAttributes===false && $this->owner->id_model_ident)
            {
                $this->_eavAttributes=Yii::app()->db->createCommand()
                    ->select('attr_model.id_attr')
                    ->from('{{attr_model}} attr_model')
                    ->where('id_model_ident=:ident',array(':ident'=>$this->owner->id_model_ident))
                    ->queryAll();
                foreach ($this->_eavAttributes as $key => $data) {
                    $result[$data['id_attr']]=$data['id_attr'];
                }
                $this->_eavAttributes=$result;
            }
            return $this->_eavAttributes ? $this->_eavAttributes : array();
        }

        public function setEavAttributes($data){
            $this->_eavAttributes=$data;
        }

        public function saveAttributes(){

            $this->eavAttributes=$_POST[get_class($this->owner)]['eavAttributes'];
            $iDs=array();
            $attrCriteria=new CDbCriteria;
            $attrCriteria->addCondition('id_attr=:attr and id_model_ident=:ident');
            $attrCriteria->params=array(':ident'=>$this->owner->id_model_ident);

            if ($_POST[get_class($this->owner)]['eavAttributes'])
            {
                $childs=Yii::app()->db->createCommand()
                        ->selectDistinct('id_model_ident')
                        ->from('{{attr_model}}')
                        ->where('parent=:parent',array(':parent'=>$this->id_model_ident))
                        ->queryAll();
                $childs=array_map(function($n){return $n['id_model_ident'];}, $childs);
                
                $findCriteria=new CDbCriteria;
                $findCriteria->addCondition('parent=:parent');
                $findCriteria->params[':parent']=$this->id_model_ident;
                $findCriteria->addCondition('id_attr=:attr');

                foreach ($_POST[get_class($this->owner)]['eavAttributes'] as $key => $data) {

                    $attrCriteria->params[':attr']=$data;
                    $model=AttrModel::model()->find($attrCriteria);
                    if (!$model)
                    {
                        $model=new AttrModel;
                        $model->id_attr=$data;
                        $model->id_model_ident=$this->id_model_ident;
                        $model->save();
                    }

                    $findCriteria->params[':attr']=$data;
                    foreach ($childs as $key => $child_ident) {
                        $attrModel=AttrModel::model()->find($findCriteria);
                        if (!$attrModel);
                        {
                            $attrModel=new AttrModel;
                            $attrModel->attributes=array(
                                'id_attr'=>$data,
                                'id_model_ident'=>$child_ident,
                                'parent'=>$this->id_model_ident,
                            );
                            $attrModel->save();
                        }
                        $iDs[]=$attrModel->id;
                    }
                    $iDs[]=$model->id;
                }
            }
            //var_dump($iDs);die();
            unset($criteria);
            $criteria=new CDbCriteria;
            $criteria->addCondition('id_model_ident=:ident or parent=:ident');
            $criteria->params[':ident']=$this->owner->id_model_ident;
            $criteria->join="join {{attr_model}} attr_model on attr_model.id=post_id";
            $criteria->addCondition('post_type!=true');
            $criteria->addNotInCondition('post_id',$iDs);
            $attrVals=AttrVal::model()->findAll($criteria);

            foreach ($attrVals as $key => $value) {
                $value->delete();
            }

            unset($criteria);
            $criteria=new CDbCriteria;  
            if ($iDs)
                $criteria->addNotInCondition('id',$iDs);
            $criteria->addCondition('id_model_ident=:ident or parent=:ident');
            $criteria->addCondition('attach is null');
            $criteria->params[':ident']=$this->id_model_ident;
            AttrModel::model()->deleteAll($criteria);

            

        }

        public function saveAttrValues(){

            $iDs=array();
            $attrCriteria=new CDbCriteria;
            $attrCriteria->addCondition('post_id=:post_id and post_type=false');
            $counter=-1;
            foreach ($_POST[get_class($this->owner)]['eavAttributeValues'] as $key => $attr_val) {
                
                $counter++;
                $value=$attr_val;

                $post_id=$this->isNew ? $this->mirrors['attrs'][$counter] : $key;
                
                $valIsArray=is_array($attr_val);
                $attrCriteria->params[':post_id']=$post_id;

                $attrVal=AttrVal::model()->find($attrCriteria);
                if (!$attrVal)
                    $attrVal=new AttrVal;

                $attrVal->post_id=$post_id;

                $attrVal->value=!$valIsArray ? $attr_val : null;

                $attrVal->multy=$valIsArray;

                $attrVal->post_type=false;

                $attrVal->save();

                $iDs[]=$attrVal->post_id;
                if ($valIsArray)
                {
                    foreach ($attr_val as $key => $multyVal) {
                        
                        $multy=AttrMultyVal::model()->findByPk($key);
                        $multy=$multy ? $multy : new AttrMultyVal;

                        if (!$multy->isNewRecord && empty($multyVal))
                            $multy->delete();
                        else
                        if (!empty($multyVal))
                        {
                            $multy->id_attr_val=$attrVal->id;
                            $multy->value=$multyVal;
                            $multy->save();
                        } 
                    }
                }
            }
            if ($iDs)
            {
                $criteria=new CDbCriteria;
                $criteria->addCondition('post_id=:ident');
                $criteria->params[':ident']=$this->owner->id_model_ident;
                $criteria->addNotInCondition('post_id',$iDs);
                
                Yii::app()->db->createCommand()
                    ->join('{{attr_model}} am','am.id_model_ident=ident')
                    ->update('{{attr_val}}',array('value'=>null),$criteria->condition,$criteria->params);
            }
        }

        public function processAttributes(){

            $this->saveAttributes();

            if (isset($_POST[get_class($this->owner)]['eavAttributeValues']))
                $this->saveAttrValues();
        }

        public function saveDependSetValues(){
            foreach ($_POST[get_class($this->owner)]['eavSetValues'] as $key => $data) {
                
            }
        }

        public function saveSetValues(){
            $criteria=new CDbCriteria;
            $criteria->addCondition('id_group=:group');
            $criteria->addCondition('post_id=:attr and post_type=true');
            $counter=-1;

            foreach ($_POST[get_class($this->owner)]['eavSetValues'] as $group_id => $data) {

                $counter++;
                $group=$this->isNew ? $this->mirrors['sets'][$counter] : $group_id;
                $criteria->params[':group']=$group_id;
                $attributes=ModelGroupSet::model()->findByPk($group)->eavAttributes;
                if ($attributes)
                {
                    foreach ($attributes as $key => $attr) {
                        $criteria->params[':attr']=$attr->id;

                        $attrVal=AttrVal::model()->find($criteria);
                        $value=$_POST[get_class($this->owner)]['eavSetValues'][$group_id][$attr->id];
                        $valIsArray=is_array($value);
                        if (!$attrVal)
                            $attrVal=new AttrVal;
                        $attrVal->attributes=array(
                            'post_id'=>$attr->id,
                            'post_type'=>true,
                            'id_group'=>$group,
                            'multy'=>$valIsArray,
                            'value'=>$valIsArray ?  null : $value
                        );
                        $attrVal->save();
                        if ($valIsArray)
                        {
                            foreach ($value as $key => $multyVal) {
                                $multy=AttrMultyVal::model()->findByPk($key);
                                $multy=$multy ? $multy : new AttrMultyVal;
                                if (!$multy->isNewRecord && empty($multyVal))
                                    $multy->delete();
                                else
                                if (!empty($multyVal))
                                {
                                    $multy->id_attr_val=$attrVal->id;
                                    $multy->value=$multyVal;
                                    $multy->save();
                                } 
                            }
                        }
                    }
                }
                else {
                    Yii::app()->db->createCommand()
                        ->delete('{{attr_val}}','post_type=true and id_group=:group',array(':group'=>$group));
                }
            }
        }

        public function saveSets()
        {
            $modelName=get_class($this->owner);
            $iDs=array();
            if ($_POST[$modelName]['eavSets'])
            foreach ($_POST[$modelName]['eavSets'] as $key => $data) {

                $childs=Yii::app()->db->createCommand()
                        ->selectDistinct('id_model_ident')
                        ->from('{{model_group_set}}')
                        ->where('parent=:parent',array(':parent'=>$this->id_model_ident))
                        ->queryAll();
                $childs=array_map(function($n){return $n['id_model_ident'];}, $childs);
                
                $findCriteria=new CDbCriteria;
                $findCriteria->addCondition('parent=:parent');
                $findCriteria->params[':parent']=$this->id_model_ident;
                $findCriteria->addCondition('id_group=:group');
                $model=ModelGroupSet::model()->find('id_model_ident=:ident and id_group=:group',array(':ident'=>$this->id_model_ident,':group'=>$data));

                if (!$model)
                {
                    $model=new ModelGroupSet;
                    $model->attributes=array(
                            'id_model_ident'=>$this->id_model_ident,
                            'id_group'=>$data,
                            'attach'=>null,
                        );
                    $model->save();
                }
                $findCriteria->params[':group']=$data;

                foreach ($childs as $key => $child_ident) {
                    $groupModel=ModelGroupSet::model()->find($findCriteria);
                    if (!$groupModel);
                    {
                        $groupModel=new ModelGroupSet;
                        $groupModel->attributes=array(
                            'id_model_ident'=>$child_ident,
                            'id_group'=>$data,
                            'parent'=>$this->id_model_ident,
                            'attach'=>null,
                        );
                        $groupModel->save();
                    }
                    $iDs[]=$groupModel->id;
                }
                $iDs[]=$model->id;
            }
            $delCriteria=new CDbCriteria;
            $delCriteria->addCondition('id_model_ident=:ident or parent=:ident');
            $delCriteria->params[':ident']=$this->owner->id_model_ident;

            if ($iDs)
                $delCriteria->addNotInCondition('id',$iDs);

            $delCriteria->params[':ident']=$this->owner->id_model_ident;
            $groups=ModelGroupSet::model()->findAll($delCriteria);

            foreach ($groups as $key => $value) {
                $value->delete();
            }

        }

        public function processSets(){

            $this->saveSets();
            if (isset($_POST[get_class($this->owner)]['eavSetValues']))
            {
                $this->saveSetValues();
            }
        }

        public function saveAttachedSetValues(){
            $counter=1;
            
            foreach ($_POST[$this->modelName]['AttachedSets'] as $group_id => $group) {
                $modelGroup=ModelGroupSet::model()->find('attach=true and id_model_ident=:ident and id_group=:group',array(':group'=>$group_id,':ident'=>$this->id_model_ident));
                foreach ($group as $attr_id => $data) {
                    if ($data)
                    {
                        $valIsArray=is_array($data);

                        $attrVal=AttrVal::model()->find('id_group=:group and post_id=:attr and post_type=true',array(':group'=>$modelGroup->id,':attr'=>$attr_id));
                        if (!$attrVal)
                        {
                            $attrVal=new AttrVal;
                        }
                        $attrVal->attributes=array(
                            'post_id'=>$attr_id,
                            'id_group'=>$modelGroup->id,
                            'post_type'=>true,
                            'value'=>$valIsArray ? '' : $data,
                            'multy'=>$valIsArray ? true : null,
                        );
                        $attrVal->save();
                        if ($valIsArray)
                        {   
                            AttrMultyVal::model()->deleteAll('id_attr_val=:val',array(':val'=>$attrVal->id));
                            foreach ($data as $multyVal_id => $multyVal) {
                                
                                if ($multyVal)
                                {

                                    $multy=AttrMultyVal::model()->find('id=:id and id_attr_val=:val',array(':id'=>$multyVal_id,':val'=>$attrVal->id));
                                    if (!$multy)
                                    {
                                        $multy=new AttrMultyVal;
                                        $multy->id_attr_val=$attrVal->id;
                                    }
                                    $multy->value=$multyVal;
                                    $multy->save();
                                }
                            }
                        }
                    }
                }
            }
        }

        public function saveAttachedSets(){

            foreach ($this->attachments['group'] as $key => $data) {
                $model=ModelGroupSet::model()->find('attach=true and id_group=:id and id_model_ident=:ident',array(':id'=>$data,':ident'=>$this->id_model_ident));
                if (!$model){
                    $model=new ModelGroupSet;
                    $model->attributes=array(
                        'id_group'=>$data,
                        'id_model_ident'=>$this->id_model_ident,
                        'attach'=>true
                    );
                    $model->save();
                }
            }
            $delCriteria=new CDbCriteria;
            $delCriteria->addNotInCondition('id_group',$this->attachments['group']);
            $delCriteria->addCondition('id_model_ident=:ident and attach=true');
            $delCriteria->params[':ident']=$this->id_model_ident;
            ModelGroupSet::model()->deleteAll($delCriteria);
        }

        public function saveAttachedAttrs(){
            foreach ($this->attachments['attr'] as $key => $data) {
                $model=AttrModel::model()->find('attach=true and id_attr=:id and id_model_ident=:ident',array(':id'=>$data,':ident'=>$this->id_model_ident));
                if (!$model){
                    $model=new AttrModel;
                    $model->attributes=array(
                        'id_attr'=>$data,
                        'id_model_ident'=>$this->id_model_ident,
                        'attach'=>true
                    );
                    $model->save();
                }   
            }
        }

        public function processAttach(){

            if ($this->attachments['group']){
                $this->saveAttachedSets();
                if ($_POST[$this->modelName]['AttachedSets'])
                    $this->saveAttachedSetValues();
            }

            if ($this->attachments['attr']){
                $this->saveAttachedAttrs();
                if ($_POST[$this->modelName]['AttachedAttr'])
                    $this->saveAttachedAttrsValues();
            }

        }

        public function processDepends(){

            $attribute=$this->depends['attribute'];

            $modelIDs=$this->owner->$attribute;

            //var_dump($attribute,$modelIDs);die();
            $criteria=new CDbCriteria;  
            $criteria->addCondition('id_entity=:entity and id_model=:model');
            $criteria->params=array(':entity'=>$this->depends['entity_id'],':model'=>0);
            $iDsGroups=array();
            $iDsAttrs=array();
            
            foreach ($modelIDs as $key => $id) {

                $criteria->params[':model']=$id;
                
                $ident=ModelIdent::model()->find($criteria);
                $models=ModelGroupSet::model()->findAll('id_model_ident=:ident and (attach is null or attach=false)',array(':ident'=>$ident->id));
                if ($models)
                    foreach ($models as $key => $data) {
                        $mGroupSet=ModelGroupSet::model()->find('id_model_ident=:ident and id_group=:group and (attach is null or attach=false)',array(':group'=>$data->id_group,':ident'=>$this->id_model_ident));
                        if (!$mGroupSet)
                        {
                            $mGroupSet=new ModelGroupSet;
                            $mGroupSet->id_group=$data->id_group;
                            $mGroupSet->id_model_ident=$this->id_model_ident;
                            $mGroupSet->attach=null;
                        }
                        $mGroupSet->parent=$ident->id;
                        $mGroupSet->save();
                        $iDsGroups[]=$mGroupSet->id;
                    }
                $models=AttrModel::model()->findAll('id_model_ident=:ident',array(':ident'=>$ident->id));

                foreach ($models as $key => $data) {
                    $attrModel=AttrModel::model()->find('id_model_ident=:ident and id_attr=:attr',array(':attr'=>$data->id_attr,':ident'=>$this->id_model_ident));

                    if (!$attrModel)
                    {
                        $attrModel=new AttrModel;
                        $attrModel->id_attr=$data->id_attr;
                        $attrModel->id_model_ident=$this->id_model_ident;
                    }
                    $attrModel->parent=$ident->id;
                    $attrModel->save();
                    $iDsAttrs[]=$attrModel->id;
                }
            }
            $this->mirrors['sets']=$iDsGroups;
            $this->mirrors['attrs']=$iDsAttrs;
            
            $delCriteria=new CDbCriteria;
            $delCriteria->addCondition('id_model_ident=:ident');
            if ($iDsGroups)
                $delCriteria->addNotInCondition('id',$iDsGroups);
            $delCriteria->params[':ident']=$this->id_model_ident;
            
            $iDsGroups=Yii::app()->db->createCommand()//нужны для удаления записей из таблицы {{attr_val}}
                ->select('id')
                ->from('{{model_group_set}}')
                ->where($delCriteria->condition,$delCriteria->params)
                ->queryAll();

            $delCriteria->addCondition('attach!=true');
            Yii::app()->db->createCommand()
                ->delete('{{model_group_set}}',$delCriteria->condition,$delCriteria->params);

            $delCriteria=new CDbCriteria;
            $delCriteria->addInCondition('id_group',$iDsGroups);

            Yii::app()->db->createCommand()
                ->delete('{{attr_val}}',$delCriteria->condition,$delCriteria->params);    

            $delCriteria=new CDbCriteria;
            $delCriteria->addCondition('id_model_ident=:ident and (attach=false or attach is null)');
            if ($iDsAttrs)
            {
                $delCriteria->addNotInCondition('id',$iDsAttrs);
            }

            $delCriteria->params[':ident']=$this->id_model_ident;

            $iDsAttrs=Yii::app()->db->createCommand()//нужны для удаления записей из таблицы {{attr_val}}
                ->select('id')
                ->from('{{attr_model}}')
                ->where($delCriteria->condition,$delCriteria->params)
                ->queryAll();

            Yii::app()->db->createCommand()
                ->delete('{{attr_model}}',$delCriteria->condition,$delCriteria->params);

            $delCriteria=new CDbCriteria;
            $delCriteria->addInCondition('post_id',$iDsAttrs);
            $delCriteria->addCondition('post_type=false');

            Yii::app()->db->createCommand()
                ->delete('{{attr_val}}',$delCriteria->condition,$delCriteria->params);    
            if (isset($_POST[get_class($this->owner)]['eavSetValues']))
                $this->saveSetValues();

            if (isset($_POST[get_class($this->owner)]['eavAttributeValues']))
                $this->saveAttrValues();
        }

        public function beforeSave($event){
            parent::beforeSave($event);
            $this->isNew=$this->owner->isNewRecord;
            return true;
        }

        public function afterSave($event) {
            parent::afterSave($event);
            if (empty($this->owner->id_model_ident))
            {
                $ident=new ModelIdent;
                $ident->attributes=array(
                    'id_entity'=>$this->entity_id,
                    'id_model'=>$this->owner->id
                );
                $ident->save();
                //$this->owner->id_model_ident=$ident->id;
                $this->owner->isNewRecord=false;

                // Yii::app()->db->createCommand()->update($this->owner->tableName(),array('id_model_ident'=>$ident->id),'id=:id',array(':id'=>$this->owner->id));
                ModelIdent::model()->deleteAll('id_model=:model and id_entity=:entity and id!=:id',array(':entity'=>$this->entity_id,':model'=>$this->owner->id,':id'=>$ident->id));
            }
            

            if ($this->depends)
            {

                $this->processDepends();

            } else {
                $this->processAttributes();
                $this->processSets();
            }
            if ($this->attachments)
            {
                $this->processAttach();
            }
            return true;
        }
        
        public function beforeDelete($event){
            parent::beforeDelete($event);
            if ($this->attachments['group'])
            {
                
                $criteria=new CDbCriteria;
                $criteria->addInCondition('id_group',$this->attachments['group']);
                $criteria->addCondition('id_model_ident='.$this->id_model_ident);
                $attached=ModelGroupSet::model()->findAll($criteria);
                
                foreach ($attached as $key => $value) {
                    $value->delete();
                }

            }
            if ($this->attachments['attr'])
            {
                $criteria=new CDbCriteria;
                $criteria->addInCondition('id_attr',$this->attachments['attr']);
                $criteria->addCondition('id_model_ident='.$this->id_model_ident);
                $attached=AttrModel::model()->findAll($criteria);
                foreach ($attached as $key => $value) {
                    $value->delete();
                    AttrVal::model()->deleteAll('id_group=:group and post_type=true',array(':group'=>$value->id));
                }
            }
            
            foreach ($this->eavSetModels as $key => $value) {
                $value->delete();
                AttrVal::model()->deleteAll('id_group=:group and post_type=true',array(':group'=>$value->id));
            }

            foreach ($this->eavAttrModels as $key => $value) {
                $value->delete();
                AttrVal::model()->deleteAll('id_group=:group and post_type=true',array(':group'=>$value->id));
            }

            $ident=ModelIdent::model()->findByPk($this->id_model_ident);
            if ($ident)
                $ident->delete();
            return true;
        }

        public function getDataProviderForAttr($params=array()){
            //$criteria->
        }

        public function getFindCriteria($multy=false){
            $criteria=new CDbCriteria;
            $criteria->addCondition('attr_val.post_type=false');
            $criteria->addCondition("ident.id_entity=:id");
            $criteria->params[':id']=$this->owner->entity_id;
            $criteria->distinct=true;
            if ($multy)
            {
                $criteria->join="inner join {{model_ident}} ident on ident.id_model=t.id ";
                $criteria->join.="inner join {{attr_model}} attr_model on attr_model.id_model_ident=ident.id ";
                $criteria->join.="inner join {{attr_val}} attr_val on attr_model.id=attr_val.post_id ";
                $criteria->join.="inner join {{attr_multy_val}} multy on multy.id_attr_val=attr_val.id ";
                return $criteria;
            } else {
                $criteria->join="inner join {{model_ident}} ident on ident.id_model=t.id ";
                $criteria->join.="inner join {{attr_model}} attr_model on attr_model.id_model_ident=ident.id ";
                $criteria->join.="inner join {{attr_val}} attr_val on attr_model.id=attr_val.post_id ";
                return $criteria;
            }
        }

        public function getFindCriteriaForAttr($attr){
            
            $criteria=new CDbCriteria;
            $criteria->addCondition('attr_val.post_type=false');
            $criteria->addCondition("ident.id_entity=:id");
            $criteria->params[':id']=$this->owner->entity_id;
            $criteria->distinct=true;
            if (!is_object($attr))
            {
                $criteria->addCondition('attr_model.id_attr=:attr');
                $criteria->params[':attr']=$attr;
                $multy=Attr::model()->findByPk($attr)->isMulty;
            }
            if ($this->owner->id)
            {
                $criteria->addCondition('attr_model.id_model_ident=:ident');
                $criteria->params[':ident']=$this->id_model_ident;
            }
            if ($multy)
            {
                $criteria->join="inner join {{attr_val}} attr_val on t.id_attr_val=attr_val.id ";
                $criteria->join.="inner join {{attr_model}} attr_model on attr_model.id=attr_val.post_id ";
                $criteria->join.="inner join {{model_ident}} ident on ident.id=attr_model.id_model_ident ";
                $criteria->addCondition('t.id_attr_val=attr_val.id');
                if (is_object($attr))
                    $criteria->mergeWith($attr);
                return $criteria;
            } else {
                $criteria->join="inner join {{attr_model}} attr_model on attr_model.id=post_id ";
                $criteria->join.="inner join {{model_ident}} ident on ident.id=attr_model.id_model_ident ";
                $criteria->join.="inner join {{entity}} entity on entity.id=ident.id_entity ";
                $criteria->addCondition('entity.id='.$this->owner->entity_id);
                if (is_object($attr))
                    $criteria->mergeWith($attr);
                //$criteria->join.="inner join {{attr_val}} val on attr_model.id=val.post_id ";
                return $criteria;
            }
            
        }

        public function getModels($attrs)
        {

        }

        public function findAllAttrVals($attr_id){
            $attr=Attr::model()->findByPk($attr_id);
            if ($attr)
            {
                $criteria=new CDbCriteria;
                $criteria->addCondition('post_type=false and attr_model.id_attr=:attr');
                $criteria->addCondition("ident.id_entity=:id");
                $criteria->params[':attr']=$attr->id;
                $criteria->params[':id']=$this->owner->entity_id;
                if ($this->owner->id)
                {
                    $criteria->addCondition('attr_model.id_model_ident=:ident');
                    $criteria->params[':ident']=$this->id_model_ident;
                }
                    
                $criteria->distinct=true;

                if ($attr->isMulty)
                {
                    $criteria->join="inner join {{attr_val}} val on t.id_attr_val=val.id ";
                    $criteria->join.="inner join {{attr_model}} attr_model on attr_model.id=val.post_id ";
                    $criteria->join.="inner join {{model_ident}} ident on ident.id=attr_model.id_model_ident ";
                    $criteria->addCondition('t.id_attr_val=val.id');
                    $result=AttrMultyVal::model()->findAll($criteria);
                    return $result;
                } else {
                    $criteria->join="left join {{attr_model}} attr_model on attr_model.id=t.post_id ";
                    $criteria->join.="left join {{model_ident}} ident on ident.id=attr_model.id_model_ident ";
                    $return=AttrVal::model()->findAll($criteria);
                    return $return;
                }
            } else {
                throw new Exception("В таблице tbl_attr не был найдено записи с id {$attr_id}", 500);
            }
        }

        public function findAllGroupAttrVals($attr){
            $criteria=new CDbCriteria;
        }

    }
?>