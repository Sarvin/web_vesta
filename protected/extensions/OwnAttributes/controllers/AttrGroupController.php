<?php

class AttrGroupController extends AdminController
{
	public function ActionUpdate($id){
		
		$model=AttrGroup::model()->findByPk($id);
		if (isset($_POST['AttrGroup'])){
			$model->attributes=$_POST['AttrGroup'];
			if ($model->validate())
			{
				$model->save();
				$this->redirect(array('list'));
			}
		}
		$this->render('update',array('model'=>$model));
	}

	public function ActionCreate(){
		
		$model=new AttrGroup;
		if (isset($_POST['AttrGroup'])){
			$model->attributes=$_POST['AttrGroup'];
			if ($model->validate())
			{
				$model->save();
				$this->redirect(array('list'));
			}
		}
		$this->render('create',array('model'=>$model));
	}

}
