<?php

class AttrController extends AdminController
{
	public function actionCheckOnMultiple($id){
		$check=Yii::app()->db->createCommand()
			->select('id_attr_val_type as type')
			->from('{{attr_field_type}}')
			->where('id=:id',array(':id'=>$id))
			->queryRow();
		$response['success']=$check['type']==2;
		echo CJSON::encode($response);
	}

	public function actionCreate(){

		$model=new Attr;
		if (isset($_POST['Attr']))
		{
			if ($_POST['AttrRange'])
			{
				$values=array();
				foreach ($_POST['AttrRange'] as $key => $value) {
					
					if ($value)
						$values[$key]=$value;
				}
				$model->rangeValues=serialize($values);
			}
			$model->attributes=$_POST['Attr'];
			$valid=$model->validate();
			if ($valid)
			{
				$model->save();
				$this->redirect(array('list'));
			}
		}
		$this->render('create',array('model'=>$model));
	}

	public function actionUpdate($id){
		$model=Attr::model()->findByPk($id);
		if (isset($_POST['Attr']))
		{
			if ($_POST['AttrRange'])
			{
				$values=array();

				foreach ($_POST['AttrRange'] as $key => $value) {
					
					if ($value)
						$values[$key]=$value;
				}

				$model->rangeValues=serialize($values);
			}
		
			$model->attributes=$_POST['Attr'];
			$valid=$model->validate();
			if ($valid)
			{
				$model->save();
				$this->redirect(array('list'));
			}
		}
		$this->render('update',array('model'=>$model));
	}

}
