<?php

class AttributesWidget extends CWidget {

    public $attributes;
    public $controllerRoute;
    public $modelName;

    public function init(){

    }

    public function run(){
        foreach ($this->attributes as $key => $data) {
            $name=$this->modelName.'[eavAttributeValues]['.$data->id.']';
            $fieldView=$data->field_type->id_widget!=null && $data->attr->field_type->id_widget!=0 ? $data->attr->widget : 'string';
            $this->render('attrTypeView/'.$fieldView,array('model'=>$data,'name'=>$name, 'modelName'=>$this->modelName));
        }
    }
}