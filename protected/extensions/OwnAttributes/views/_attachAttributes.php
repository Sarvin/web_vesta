<?php
	Yii::app()->clientScript->registerScriptFile($this->getAssetsUrl('admin').'/js/addAttributes.js', CClientScript::POS_END);
	$this->widget('ext.select2.ESelect2',array(
	  'attribute'=>'eavAttributes',
	  'data'=>Attr::getList(),
	  'model'=>$model,
	  'htmlOptions'=>array(
	    'multiple'=>'multiple',
	    'class'=>'span12',
	    'id'=>'eavAttributes',
	  ),
	  'options'=>array(
	    'placeholder'=>'Выбрать',
	    'allowClear'=>false,
	    'maximumSelectionSize'=>10,
	  ),
	));
?>

<div class="EavAttributes">
	<?
		$this->widget('appext.ownattributes.AttributesWidget', array(
			'attributes' => $model->eavAttrModels,
			'modelName'=>get_class($model),
		));
	?>
</div>