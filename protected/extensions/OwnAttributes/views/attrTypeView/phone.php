<div class="control-group">
	<hr>
    <?echo CHtml::label($model->label,$name,array('required'=>$model->required));?>
    <?php
        $this->widget('CMaskedTextField', array(
            'name' => $name,
            'mask' => '+7-999-999-9999',
            'placeholder' => '*',
            'value'=>$model->value,
            'htmlOptions'=>array(
            	'value'=>$model->value,
            	'class'=>'span8'
            ),
        ));
        ?>
</div>
