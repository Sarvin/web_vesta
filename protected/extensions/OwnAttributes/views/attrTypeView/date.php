
<div class="control-group">
<hr>
<?
echo CHtml::label($model->label,'Items_eavAttributeVlues_'.$model->id,array('required'=>$model->required));
$this->widget('zii.widgets.jui.CJuiDatePicker',array(
    'name'=>$name,
    // additional javascript options for the date picker plugin
    'value'=>$model->value,
    'options'=>array(
        'showAnim'=>'fold',
    ),
    'htmlOptions'=>array(
        'class'=>'span8'
    ),
));
?>
</div>