<?php
	//Yii::app()->clientScript->registerScriptFile($this->getAssetsUrl('admin').'/js/addAttributes.js', CClientScript::POS_END);
	$this->widget('ext.select2.ESelect2',array(
	  'attribute'=>'eavSets',
	  'data'=>AttrGroup::getList(),
	  'model'=>$model,
	  'htmlOptions'=>array(
	    'multiple'=>'multiple',
	    'class'=>'span12',
	    'id'=>'eavSets',
	  ),
	  'options'=>array(
	    'placeholder'=>'Выбрать',
	    'allowClear'=>false,
	    'maximumSelectionSize'=>10,
	  ),
	));
?>
<div class="EavAttributes">
	<?
		$this->widget('appext.ownattributes.SetsWidget', array(
			'groups' => $model->eavSetModels,
			'modelName'=>get_class($model),
		));
	?>
</div>