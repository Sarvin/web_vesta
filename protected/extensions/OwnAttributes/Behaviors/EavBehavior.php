<?
    class EavBehavior extends CActiveRecordBehavior
    {
        
        private $_eavAttributes=false;
        private $_eavAttributeValues=false;
        private $_eavAttrModels=false;
        private $_eavSets=false;
        private $_eavSetValues=false;
        private $_eavSetModels=false;

    	public $entity_id=2;
        public $id_model_ident;

        public function getEavSetModels(){
            if ($this->_eavSetModels===false)
            {
                $this->_eavSetModels=ModelGroupSet::model()->findAll('id_model_ident=:id',array(':id'=>$this->owner->id_model_ident));
            }
            return $this->_eavSetModels;
        }

        public function setEavSetModels($data){
            $this->_eavSetModels=$data;
        }

        public function getEavSets(){
            
            if ($this->_eavSets===false && $this->owner->id_model_ident){
                $this->_eavSets=CHtml::listData(ModelGroupSet::model()->findAll('id_model_ident=:id',array(':id'=>$this->owner->id_model_ident)),'id_group','id_group');
            }
            return $this->_eavSets;
        }

        public function setEavSets($data){
            $this->_eavSets=$data;
        }

        public function getEavAttributeValues(){

            if ($this->owner->id_model_ident && $this->_eavAttributeValues===false)
            {
                return $this->_eavAttributeValues=CHtml::listData(AttrVal::model()->findAll('id_model_ident=:ident and (post_type!=true)',array(':ident'=>$this->owner->id_model_ident)),'id_attr','value');
            }
            return array();
        }

        public function setEavAttributeValues($data){
            $this->_eavAttributeValues=$data;
        }

        public function getEavAttrModels(){
            
            if ($this->_eavAttrModels===false && $this->owner->id_model_ident)
            {
                $criteria=new CDbCriteria;
                $criteria->addCondition('t.id_model_ident=:ident');
                $criteria->params[':ident']=$this->owner->id_model_ident;
                $criteria->join="right join {{attr}} attr on attr.id=t.id_attr";
                $criteria->select="*,t.id,attr.id as id_attr,required,t.id_model_ident";
                $this->_eavAttrModels=AttrModel::model()->findAll($criteria);
            }
            return $this->_eavAttrModels ? $this->_eavAttrModels : array();
        }

        public function getEavAttributes(){
            if ($this->_eavAttributes===false && $this->owner->id_model_ident)
            {
                $this->_eavAttributes=Yii::app()->db->createCommand()
                    ->select('attr_model.id_attr')
                    ->from('{{attr_model}} attr_model')
                    ->where('id_model_ident=:ident',array(':ident'=>$this->owner->id_model_ident))
                    ->queryAll();
                foreach ($this->_eavAttributes as $key => $data) {
                    $result[$data['id_attr']]=$data['id_attr'];
                }
                $this->_eavAttributes=$result;
            }
            return $this->_eavAttributes ? $this->_eavAttributes : array();
        }

        public function setEavAttributes($data){
            $this->_eavAttributes=$data;
        }

        public function saveAttributes(){

            if (isset($_POST[get_class($this->owner)]['eavAttributes']))
            {
                $this->eavAttributes=$_POST[get_class($this->owner)]['eavAttributes'];
                $iDs=array();

                $attrCriteria=new CDbCriteria;
                $attrCriteria->addCondition('id_attr=:attr and id_model_ident=:ident');
                $attrCriteria->params=array(':ident'=>$this->owner->id_model_ident);
                foreach ($this->eavAttributes as $key => $data) {
                    
                    $attrCriteria->params[':attr']=$data;
                    $model=AttrModel::model()->find($attrCriteria);
                    if (!$model)
                    {
                        $model=new AttrModel;
                        $model->id_attr=$data;
                        $model->id_model_ident=$this->owner->id_model_ident;
                        $model->save();
                    }
                    $iDs[]=$model->id;
                }

                $criteria=new CDbCriteria;
                if ($iDs)
                    $criteria->addNotInCondition('id',$iDs);
                $criteria->addCondition('id_model_ident=:ident');
                $criteria->params[':ident']=$this->owner->id_model_ident;

                AttrModel::model()->deleteAll($criteria);
                $criteria=new CDbCriteria;
                $criteria->addCondition('id_model_ident=:ident');
                $criteria->params[':ident']=$this->owner->id_model_ident;
                $criteria->join="join {{attr_model}} attr_model on attr_model.id=post_id";
                $criteria->addCondition('post_type!=true');
                $criteria->addNotInCondition('post_id',$iDs);

                AttrVal::model()->deleteAll($criteria);
            }
            
            if (isset($_POST[get_class($this->owner)]['eavAttributeValues']))
            {
                $iDs=array();
                $attrCriteria=new CDbCriteria;
                $attrCriteria->addCondition('post_id=:post_id');
                foreach ($this->eavAttrModels as $key => $data) {
                    $value=$_POST[get_class($this->owner)]['eavAttributeValues'][$data->id];
                    $attrCriteria->params[':post_id']=$data->id;
                    $attrVal=AttrVal::model()->find($attrCriteria);
                    if (!$attrVal)
                        $attrVal=new AttrVal;
                    $attrVal->post_id=$data->id;
                    $attrVal->value=$value;
                    $attrVal->post_type=false;
                    $attrVal->save();
                    $iDs[]=$attrVal->post_id;
                }
                if ($iDs)
                {
                    $criteria=new CDbCriteria;
                    $criteria->addCondition('post_id=:ident');
                    $criteria->params[':ident']=$this->owner->id_model_ident;
                    $criteria->addNotInCondition('post_id',$iDs);
                    Yii::app()
                        ->db
                        ->createCommand()
                        ->join('{{attr_model}} am','am.id_model_ident=ident')
                        ->update('{{attr_val}}',array('value'=>null),$criteria->condition,$criteria->params);
                }
            }
        }

        public function saveSets(){

            if (isset($_POST[get_class($this->owner)]['eavSets']))
            {
                $modelName=get_class($this->owner);
                $iDs=array();
                foreach ($_POST[$modelName]['eavSets'] as $key => $data) {
                    $model=ModelGroupSet::model()->find('id_model_ident=:ident and id_group=:group',array(':ident'=>$this->owner->id_model_ident,':group'=>$data));
                    if (!$model)
                    {
                        $model=new ModelGroupSet;
                        $model->attributes=array(
                                'id_model_ident'=>$this->owner->id_model_ident,
                                'id_group'=>$data,
                            );
                        $model->save();
                    }
                    $iDs[]=$model->id_group;
                }
                
                $delCriteria=new CDbCriteria;
                $delCriteria->addNotInCondition('t.id_group',$iDs);
                $delCriteria->addCondition('id_model_ident=:ident');
                $delCriteria->params[':ident']=$this->owner->id_model_ident;

                $result=Yii::app()->db->createCommand()
                    ->select('val.id')
                    ->from('{{attr_val}} val')
                    ->join('{{model_group_set}} t','t.id=val.id_group')
                    ->where($delCriteria->condition,$delCriteria->params)
                    ->queryAll();
                
                $delCriteria=new CDbCriteria;
                $delCriteria->addNotInCondition('id_group',$iDs);
                $delCriteria->addCondition('id_model_ident=:ident');
                $delCriteria->params[':ident']=$this->owner->id_model_ident;
                ModelGroupSet::model()->deleteAll($delCriteria);
            }

            if (isset($_POST[get_class($this->owner)]['eavSetValues']))
            {
                $criteria=new CDbCriteria;
                $criteria->addCondition('id_group=:group');
                $criteria->addCondition('post_id=:attr');

                foreach ($_POST[get_class($this->owner)]['eavSetValues'] as $group_id => $data) {
                    $criteria->params[':group']=$group_id;
                    $attributes=ModelGroupSet::model()->findByPk($group_id)->eavAttributes;
                    $attributes_id=CHtml::listData($attributes,'id','id');

                    $delCriteria=new CDbCriteria;
                    $delCriteria->addInCondition('post_id',$attributes_id);
                    $delCriteria->addCondition('id_group=:group and post_type=true');
                    $delCriteria->params[':group']=$group_id;
                    AttrVal::model()->deleteAll($delCriteria);

                    foreach ($attributes as $key => $attr) {
                        $criteria->params[':attr']=$attr->id;
                        $attrVal=AttrVal::model()->find($criteria);
                        if (!$attrVal)
                            $attrVal=new AttrVal;
                        $attrVal->attributes=array(
                            'post_id'=>$attr->id,
                            'post_type'=>true,
                            'id_group'=>$group_id,
                            'value'=>$_POST[get_class($this->owner)]['eavSetValues'][$group_id][$attr->id]
                        );
                        $attrVal->save();
                    }
                }
            }
        }

        public function beforeSave($event){
            if (!$this->owner->id_model_ident)
            {
                $ident=new ModelIdent;
                $ident->id_entity=$this->entity_id;
                $ident->id_model=$this->owner->id;
                $ident->save(false);
                $this->owner->id_model_ident=$ident->id;
            }
            $this->saveAttributes();
            return parent::beforeSave($event);
        }

        public function afterSave($event) {
            parent::afterSave($event);
            $this->saveAttributes();
            $this->saveSets();
            return true;
        }
    }
?>