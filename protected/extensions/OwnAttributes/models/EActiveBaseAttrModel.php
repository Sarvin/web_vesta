<?
	class EactiveBaseAttrModel extends EActiveRecord{

		private $_modelIdent;
		protected $entity;
		
		private $_eavAttrs=array();
		private $_eavSetAttrs=array();
		private $_eavSets=array();

		public function getIdModelIdent(){
			if (!$modelIdent)
			{
				$result=Yii::app()->db->createCommand()
					->select('id')
					->from('{{model_ident}}')
					->where('id_etity=:entity and id_model=:id',array(':entity'=>$this->entity,':id'=>$this->id))
					->queryRow();
				$this->_modelIdent=$result['id'];
			}
			return $this->_modelIdent;
		}
	}
?>