<?php

/**
* This is the model class for table "{{attr_field_type}}".
*
* The followings are the available columns in table '{{attr_field_type}}':
    * @property integer $id
    * @property string $name
    * @property string $label
    * @property integer $id_attr_val_type
*/
class AttrFieldType extends EActiveRecord
{
    public function tableName()
    {
        return '{{attr_field_type}}';
    }

    public static function getWidgetList($id=null){
        $list=array(0=>'Не задано',1=>'phone', 2=>'email', 3=>'date', 4=>'select', 5=>"multy_select", 6=>"checkbox", 7=>"radio",8=>'string');
        return $id!==null ? $list[$id] : $list;
    }

    public function rules()
    {
        return array(
            array('id_attr_val_type,value_range,id_widget', 'numerical', 'integerOnly'=>true),
            array('name', 'length', 'max'=>255),
            // The following rule is used by search().
            array('id, name, id_attr_val_type', 'safe', 'on'=>'search'),
        );
    }

    public static function getValueRangeType($id=null){
        $list=array(0=>'Single',1=>'Multy');
        return $id ? $list[$id] : $list;
    }

    public function relations()
    {
        return array(
            'val_type'=>array(self::BELONGS_TO,'AttrValType','id_attr_val_type'),
        );
    }


    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'name' => 'Название типа поля',
            'value_range'=>'Количество принимаемых значений',
            'id_attr_val_type' => 'Тип значения поля',
        );
    }
    
    public function search()
    {
        $criteria=new CDbCriteria;
		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('label',$this->label,true);
		$criteria->compare('id_attr_val_type',$this->id_attr_val_type);
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }


}
