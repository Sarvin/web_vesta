<?php

/**
* This is the model class for table "{{attr_model}}".
*
* The followings are the available columns in table '{{attr_model}}':
    * @property integer $id
    * @property integer $id_group_attr_set
    * @property integer $id_model_ident
*/
class AttrModel extends EActiveRecord
{

    private $_range;
    public $label;
    private $_value;
    public $id_attr;

    public function getValue()
    {
        if (!$this->_value)
        {
            $result=Yii::app()->db->createCommand()
            ->select('value')
            ->from('{{attr_val}} val')
            ->where('val.post_id=:id and post_type!=true',array(':id'=>$this->id))
            ->queryRow();    
            $this->_value=$result['value'];
        }
        return $this->_value;
    }

    public function tableName()
    {
        return '{{attr_model}}';
    }

    public function rules()
    {
        return array(
            array('id_attr, id_model_ident, id_attr', 'numerical', 'integerOnly'=>true),
            array('required','boolean'),
            array('label','length','max'=>255),
            array('range,id_attr,value','safe'),
            // The following rule is used by search().
            array('id, id_attr, id_model_ident', 'safe', 'on'=>'search'),
        );
    }

    public function getField_type(){
        return $this->attr->field_type;
    }

    public function getRangeValues(){
        return $this->attr->rangeValues;
    }

    public function relations()
    {
        return array(
            'attr'=>array(self::BELONGS_TO,'Attr','id_attr'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'id_group_attr_set' => 'Атрибут',
            'required'=>'Обязательный / не обязательный',
            'id_model_ident' => 'Уникальный ключ модели',
        );
    }

    public function getRange(){
        if (!$this->_range)
        {
            $this->_range=$this->attr->range;
        }
        return $this->_range;
    }

    public function setRange($data){
        $this->_range=$data;
    }

    public function search()
    {
        $criteria=new CDbCriteria;
		$criteria->compare('id',$this->id);
		$criteria->compare('id_group_attr_set',$this->id_group_attr_set);
		$criteria->compare('id_model_ident',$this->id_model_ident);
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

}
