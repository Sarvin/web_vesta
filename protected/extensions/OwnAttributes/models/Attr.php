<?php

/**
* This is the model class for table "{{attr}}".
*
* The followings are the available columns in table '{{attr}}':
    * @property integer $id
    * @property integer $id_attr_type_field
    * @property string $label
    * @property string $alias
*/
class Attr extends EActiveRecord
{

    private $_isRanged;
    private $_rangeValues=false;
    private $_valType=false;
    private $_widget=false;
    private $_value=false;
    public $required;
    public $id_model_ident;
    public $id_group;
    public $post_id_type=1;

    public function tableName()
    {
        return '{{attr}}';
    }

    public static final  function getList($id=null){
        $list=CHtml::listData(Attr::model()->findAll(),'id','label');
        return $id ? $list[$id] : $list;
    }

    public function rules()
    {
        $rules=array(
            array('label,id_attr_type_field,alias','required'),
            array('id_attr_type_field, id_model_ident, id_group', 'numerical', 'integerOnly'=>true),
            array('label, value, alias', 'length', 'max'=>255),
            array('required, id_group','safe'),
            // The following rule is used by search().
            array('id, id_attr_type_field, label, alias', 'safe', 'on'=>'search'),
        );
        if ($this->required)
            $rules[]=array('value','required');
        return $rules;
    }

    public function getWidget(){
        if ($this->_widget===false)
        {
            $this->_widget=AttrFieldType::getWidgetList($this->field_type->id_widget ? $this->field_type->id_widget : 0);
        }
        return $this->_widget;
    }

    public function getValue(){
        if ($this->_value===false)
        {
            $result=Yii::app()->db->createCommand()
                ->select('value')
                ->from('{{attr_val}}')
                ->where('id_group=:group and post_id=:attr and post_type=true',array(':group'=>$this->id_group,':attr'=>$this->id))
                ->queryRow();

            $this->_value=$result['value'];
        }
        return $this->_value;
    }

    public function setValue($data){
        $this->_value=$data;
    }

    public function getValType(){
        if ($this->_valType===false)
            $this->_valType=$this->field_type->val_type;
        return $this->_valType;
    }

    public function getIsRanged(){
        if (!$this->_isRanged)
            $this->_isRanged=Yii::app()->db->CreateCommand()
                ->select('id_attr_val_type as ranged')
                ->from('{{attr_field_type}} t')
                ->join('{{attr_val_type}} val_type','val_type.id=t.id_attr_val_type')
                ->where('t.id=:id',array(':id'=>$this->id_attr_type_field))
                ->queryRow();
        return $this->_isRanged['ranged']==2;
    }

    public function relations()
    {
        return array(
            'range'=>array(self::HAS_ONE,'AttrRange','id_attr'),
            'field_type'=>array(self::BELONGS_TO,'AttrFieldType','id_attr_type_field'),
        );
    }

    public function setRangeValues($data)
    {
        $this->_rangeValues=$data;
    }

    public function getRangeValues(){

        if (!$this->_rangeValues)
            if ($this->range)
                $this->_rangeValues=$this->range->values;
            $values=unserialize($this->_rangeValues);
        return is_array($values)  ? $values : array();
    }

    public function afterSave(){
        parent::afterSave();
        $range=!$this->range ? new AttrRange : $this->range;
        if ($this->isRanged)
        {
            $range->id_attr=$this->id;
            $range->values=serialize($this->rangeValues);
            $range->save();
        }else 
        {
            AttrRange::model()->deleteAll('id_attr=:id',array(':id'=>$this->id));
            Yii::app()->db->createCommand()
                ->update('{{attr_val}}',array('value'=>null),array('id=:id and (post_id_type=null or post_id_type=0)', ':id'=>$this->id));
        }
            
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'id_attr_type_field' => 'Тип поля',
            'label' => 'Название',
            'alias' => 'Уникальное имя',
        );
    }



    public function search()
    {
        $criteria=new CDbCriteria;
		$criteria->compare('id',$this->id);
		$criteria->compare('id_attr_type_field',$this->id_attr_type_field);
		$criteria->compare('label',$this->label,true);
		$criteria->compare('alias',$this->alias,true);
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }


}
