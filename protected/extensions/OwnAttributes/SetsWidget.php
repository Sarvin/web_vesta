<?php

class SetsWidget extends CWidget {
    public $groups;
    public $controllerRoute;
    public $modelName;
    
    public function init(){

    }

    public function run(){
        foreach ($this->groups as $key => $group) {
            echo "<hr>";
            echo "<br><p>".$group->group->name."</p>";
            foreach ($group->eavAttributes as $key => $data) {
                $name=$this->modelName.'[eavSetValues]['.$group->id.']['.$data->id.']';
                $fieldView=$data->field_type->id_widget!=null && $data->field_type->id_widget!=0 ? $data->widget : 'string';
                //var_dump($data->value);die();
                $this->render('attrTypeView/'.$fieldView,array('model'=>$data,'name'=>$name));
            }
        }
    }
}