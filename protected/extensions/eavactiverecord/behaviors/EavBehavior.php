<?
    class EavBehavior extends CActiveRecordBehavior
    {
        public function attachEavAttribute(){

        }

        public function attachEavSet(){
            
        }

        public function beforeSave($event) {
            parent::beforeSave($event);

            $owner = $this->getOwner();
            //$owner->addColumn();
            if (isset($_POST[get_class($owner)]['EavSet']))
            {
                foreach ($_POST[get_class($owner)]['EavSet'] as $attr_id => $attr_value) {
                    $owner->attachEavSet($attr_value);
                }
            }
        }

        public function afterDelete($event)
        {
            
            parent::afterDelete($event);
        }
    }
?>