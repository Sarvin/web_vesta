<?
class Search extends CModel {
	public $term;
	public $criteria;
	public function rules(){
		return array(
			array('term','safe'),
		);
	}

	public function attributeNames(){
		return array(
			'term',
		);
	}

	public function init(){
		$this->criteria=new CDbCriter;
	}

	public function getAutoCompliteData(){

		$result=Yii::app()->db->createCommand()
			->select('name as value,concat("/items/",alias) data')
			->from('{{items}}')
			->where('name like(\'%'.$this->term.'%\')')
			->queryAll();
		return $result;
	}

	public function getCriteria(){
		$result=Yii::app()->db->createCommand()
			->select('id')
			->from('{{items}}')
			->where('concat(name,wswg_body) like(\'%'.$this->term.'%\')')
			->queryAll();
		$result=array_map(function($n){return $n['id'];},$result);
		$this->criteria->addCondition('t.status=1');
		$this->criteria->addInCondition('id',$result);
		return $this->criteria;
	}
}
?>