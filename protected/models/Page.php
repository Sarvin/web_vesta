<?php

/**
* This is the model class for table "{{pages}}".
*
* The followings are the available columns in table '{{pages}}':
    * @property integer $id
    * @property string $title
    * @property string $description
    * @property string $wswg_body
    * @property string $img_preview
    * @property integer $node_id
    * @property integer $status
    * @property integer $sort
    * @property string $create_time
    * @property string $update_time
*/
class Page extends EActiveRecord
{

    public $displaySeo=true;
    public $titleAttr='title';
    public function tableName()
    {
        return '{{pages}}';
    }

    public $_seo;

    public function translition($var=0){
        $variants=array(
            'Страниц',
            'Страницы',
            'Страницами'
        );
        return $variants[$var];
    }

    public function getSeo()
    {
        if (!$this->_seo)
            $this->_seo=$this->seo_id ? Seo::model()->findByPk($this->seo_id) : new Seo;
        return $this->_seo;
    }

    public function setSeo($attrs)
    {
        $this->seo->attributes=$attrs;
    }

    public function saveSeo()
    {
        $this->_seo->save();
        $this->seo_id=$this->seo->id;
    }

    public function rules()
    {
        return array(
            array('title, alias','required'),
            array('node_id, status, seo_id, sort', 'numerical', 'integerOnly'=>true),
            array('title', 'length', 'max'=>255),
            array('alias','unique'),
            array('img_preview','file','allowEmpty'=>true),
            array('description, wswg_body, seo, create_time, img_preview, menu, seo_id, update_time', 'safe'),
            // The following rule is used by search().
            array('id, title, description, wswg_body, img_preview, node_id, status, sort, create_time, update_time', 'safe', 'on'=>'search'),
        );
    }


    public function relations()
    {
        return array(
            'seo'=>array(self::BELONGS_TO,'Seo','seo_id'),
        );
    }


    public function scopes()
    {
        return array(
            'published'=>array(
                'condition'=>'status=0',
            ),
            'menu'=>array(
                'condition'=>'menu=true',
            ),
        );
    }

    public function getMark()
    {
        $marks=array('{about}','{portfolio_carusel}');
        $result=array();
        foreach ($marks as $key => $value) {
            if (!$this->wswg_body)    
                return null;
            if (strpos($this->wswg_body, $value))
                $result[]=$value;
        }
        return $result;
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'title' => 'Заголовок страницы',
            'description' => 'Краткое описание',
            'alias' => 'Алиас | Псевдоним',
            'wswg_body' => 'Тело страницы',
            'img_preview' => 'Превью',
            'node_id' => 'Ссылка на раздел',
            'menu'=>'Выводить данную стараницу в меню сайта?',
            'status' => 'Статус',
            'sort' => 'Вес для сортировки',
            'create_time' => 'Дата создания',
            'update_time' => 'Дата последнего редактирования',
        );
    }


    public function behaviors()
    {
        return CMap::mergeArray(parent::behaviors(), array(
			'imgBehaviorPreview' => array(
                'class' => 'application.behaviors.UploadableImageBehavior',
                'attributeName' => 'img_preview',
                'versions' => array(
                    'icon' => array(
                        'centeredpreview' => array(90, 90),
                    ),
                    'small' => array(
                        'resize' => array(200, 180),
                    ),
                    'medium' => array(
                        'adaptiveResize' => array(321, 331),
                    ),
                    'mainPage' => array(
                        'adaptiveResize' => array(1903, 590),
                    ),
                ),
            ),
			// 'GalleryManager' => array(
			// 	'class' => 'application.extensions.imagesgallery.GalleryBehavior',
			// ),
			'CTimestampBehavior' => array(
				'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'create_time',
                'updateAttribute' => 'update_time',
                'setUpdateOnCreate' => true,
			),
        ));
    }

    public function search()
    {
        $criteria=new CDbCriteria;
		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('wswg_body',$this->wswg_body,true);
		$criteria->compare('img_preview',$this->img_preview,true);
		$criteria->compare('node_id',$this->node_id);
		$criteria->compare('status',$this->status);
		$criteria->compare('sort',$this->sort);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('update_time',$this->update_time,true);
        $criteria->order = 'sort';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }


}
