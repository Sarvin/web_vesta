<?php

/**
* This is the model class for table "{{favorite_item}}".
*
* The followings are the available columns in table '{{favorite_item}}':
    * @property integer $id
    * @property integer $id_favorite
    * @property integer $id_item
*/
class FavoriteItem extends EActiveRecord
{
    private $_itemData=false;

    public function tableName()
    {
        return '{{favorite_item}}';
    }

    public function rules()
    {
        return array(
            array('id_item', 'numerical', 'integerOnly'=>true),
            array('id_favorite, count, size','safe'),
            // array('params','type','type'=>'array','allowEmpty'=>true),
            // The following rule is used by search().
            array('id, id_favorite, id_item', 'safe', 'on'=>'search'),
        );
    }


    public function relations()
    {
        return array(
            'item'=>array(self::BELONGS_TO,'Items','id_item')
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'id_favorite' => 'Избранное',
            'id_item' => 'Товар',
        );
    }

    public function search()
    {
        $criteria=new CDbCriteria;
		$criteria->compare('id',$this->id);
		$criteria->compare('id_favorite',$this->id_favorite);
		$criteria->compare('id_item',$this->id_item);
        
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            )
        );

    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }


}
