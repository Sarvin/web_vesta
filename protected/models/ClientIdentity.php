<?
class ClientIdentity extends CUserIdentity
{
    private $_id;
    public $record;
    public function authenticate()
    {
        $record=Client::model()->findByAttributes(array('email'=>$this->username));
        $this->record=$record;
        if($record===null)
            $this->errorCode=self::ERROR_USERNAME_INVALID;
        else if(Yii::app()->getModule('user')->encrypting($this->password)!==$record->password)
            $this->errorCode=self::ERROR_PASSWORD_INVALID;
        else
        {
            $this->_id=$record->id;
            $this->setState('username', $record->fio);
            $this->errorCode=self::ERROR_NONE;
        }
        return !$this->errorCode;
    }
 
    public function getId()
    {
        return $this->_id;
    }
}
?>