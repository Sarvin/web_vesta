<?php

/**
* This is the model class for table "{{catalog_items}}".
*
* The followings are the available columns in table '{{catalog_items}}':
    * @property integer $id
    * @property integer $id_catalog
    * @property integer $id_item
*/
class CatalogItems extends EActiveRecord
{
    public function tableName()
    {
        return '{{catalog_items}}';
    }


    public function rules()
    {
        return array(
            array('id_catalog, id_item', 'numerical', 'integerOnly'=>true),
            // The following rule is used by search().
            array('id, id_catalog, id_item', 'safe', 'on'=>'search'),
        );
    }


    public function relations()
    {
        return array(
        );
    }


    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'id_catalog' => 'каталог',
            'id_item' => 'товар',
        );
    }



    public function search()
    {
        $criteria=new CDbCriteria;
		$criteria->compare('id',$this->id);
		$criteria->compare('id_catalog',$this->id_catalog);
		$criteria->compare('id_item',$this->id_item);
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }


}
