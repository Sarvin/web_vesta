<?php

/**
* This is the model class for table "{{menu_items}}".
*
* The followings are the available columns in table '{{menu_items}}':
    * @property integer $id
    * @property integer $id_menu
    * @property integer $id_model
    * @property integer $sort
*/
class MenuItems extends EActiveRecord
{
    public function tableName()
    {
        return '{{menu_items}}';
    }


    public function rules()
    {
        return array(
            array('id_menu, id_model, sort', 'numerical', 'integerOnly'=>true),
            // The following rule is used by search().
            array('id, id_menu, id_model, sort', 'safe', 'on'=>'search'),
        );
    }


    public function relations()
    {
        return array(
        );
    }


    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'id_menu' => 'Меню',
            'id_model' => 'Модель',
            'sort' => 'Сортировка',
        );
    }



    public function search()
    {
        $criteria=new CDbCriteria;
		$criteria->compare('id',$this->id);
		$criteria->compare('id_menu',$this->id_menu);
		$criteria->compare('id_model',$this->id_model);
		$criteria->compare('sort',$this->sort);
        $criteria->order = 'sort';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }


}
