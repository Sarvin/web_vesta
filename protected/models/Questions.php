<?php

/**
* This is the model class for table "{{questions}}".
*
* The followings are the available columns in table '{{questions}}':
    * @property integer $id
    * @property string $comment
    * @property integer $id_user
    * @property string $cookeis
    * @property integer $status
    * @property integer $sort
    * @property string $create_time
    * @property string $update_time
*/
class Questions extends EActiveRecord
{
    public function tableName()
    {
        return '{{questions}}';
    }


    public function rules()
    {
        return array(
            array('id_user, status, sort', 'numerical', 'integerOnly'=>true),
            array('cookeis', 'length', 'max'=>255),
            array('comment, create_time, update_time', 'safe'),
            // The following rule is used by search().
            array('id, comment, id_user, cookeis, status, sort, create_time, update_time', 'safe', 'on'=>'search'),
        );
    }


    public function relations()
    {
        return array(
        );
    }


    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'comment' => 'Comment',
            'id_user' => 'Id User',
            'cookeis' => 'Cookeis',
            'status' => 'Status',
            'sort' => 'Sort',
            'create_time' => 'Create Time',
            'update_time' => 'Update Time',
        );
    }


    public function behaviors()
    {
        return CMap::mergeArray(parent::behaviors(), array(
			'CTimestampBehavior' => array(
				'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'create_time',
                'updateAttribute' => 'update_time',
                'setUpdateOnCreate' => true,
			),
        ));
    }

    public function search()
    {
        $criteria=new CDbCriteria;
		$criteria->compare('id',$this->id);
		$criteria->compare('comment',$this->comment,true);
		$criteria->compare('id_user',$this->id_user);
		$criteria->compare('cookeis',$this->cookeis,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('sort',$this->sort);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('update_time',$this->update_time,true);
        $criteria->order = 'sort';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }


}
