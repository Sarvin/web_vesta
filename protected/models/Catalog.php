<?php

/**
* This is the model class for table "{{catalog}}".
*
* The followings are the available columns in table '{{catalog}}':
    * @property integer $id
    * @property string $name
    * @property string $wswg_body
    * @property string $img_preview
    * @property integer $status
    * @property integer $sort
*/
class Catalog extends EActiveRecord implements IECartPosition
{
    private $_displayed=false;

    public $displaySeo=true;

    private $_prices=false;
    
    public function getId()
    {
        return 'Catalog'.$this->id;
    }

    public function getPrice(){
        return 1000;
    }

    public function getDisplayed(){
        if ($this->_displayed===false)
        {
            $this->_displayed=CHtml::listData(self::model()->findAll('display=true'),'id','id');
        }
        return $this->_displayed;
    }

    public function setDisplayed($data){
        $this->_displayed=$data;
    }

    public function getPrices(){
        if ($this->_prices===false){
            if ($this->price_range)
                $this->_prices=explode(',', $this->price_range);
            else 
                $this->_prices=array();
            
            if (!is_array($this->_prices))
                $this->_prices=array($this->_prices);

        }
        return $this->_prices;
    }
    
    public function setPrices($data){
        $this->_prices=$data;
    }   

    public function encodePrice(){
        return '['.implode(',',$this->prices).']';
    }

    

    public function tableName()
    {
        return '{{catalog}}';
    }

    public function translition($var=0){
        $variants=array(
            'Каталог',
            'Каталога',
            'Каталогами'
        );
        return $variants[$var];
    }

    public function rules()
    {
        return array(
            array('status, sort', 'numerical', 'integerOnly'=>true),
            array('name, alias','length', 'max'=>255),
            array('wswg_body, display, seo, img_preview, price_range', 'safe'),
            // The following rule is used by search().
            array('id, name, wswg_body, img_preview, status, sort', 'safe', 'on'=>'search'),
        );
    }


    public function relations()
    {
        return array(
            'seo'=>array(self::BELONGS_TO,'Seo','seo_id'),
        );
    }

    public function beforeSave(){
        parent::beforeSave();
        return true;
    }

    public function beforeDelete(){
        parent::beforeDelete();
        Yii::app()->db->createCommand()
            ->delete('{{catalog_items}}','id_catalog=:id',array(':id'=>$this->id));
        return true;
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'name' => 'Название',
            'wswg_body' => 'Название',
            'img_preview' => 'Фото',
            'status' => 'Статус',
            'sort' => 'Вес для сортировки',
            'price_range'=>'Цены для фильтра'
        );
    }


    public function behaviors()
    {
        return CMap::mergeArray(parent::behaviors(), array(
			'imgBehaviorPreview' => array(
				'class' => 'application.behaviors.UploadableImageBehavior',
				'attributeName' => 'img_preview',
				'versions' => array(
					'icon' => array(
						'centeredpreview' => array(90, 90),
					),
					'small' => array(
						'resize' => array(200, 180),
					),
                    'main'=>array(
                        'resize'=>array(217,167)
                    ),
                    'view'=>array(
                        'resize'=>array(600,500)
                    ),
				),
			),
            'Eav' => array(
                'class' => 'appext.eavAttrs.behaviors.EavBehavior',
                'entity_id'=>$this->entity_id,
                // 'attachments'=>array(
                //     'attr'=>array(16),
                //     'group'=>array(1)
                // ),
                // 'depends'=>array(
                //     'attribute'=>'categories',
                //     'entity_id'=>Catalog::model()->entity_id,
                // )
            ),
        ));
    }

    public function search()
    {
        $criteria=new CDbCriteria;
		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('wswg_body',$this->wswg_body,true);
		$criteria->compare('img_preview',$this->img_preview,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('sort',$this->sort);
        $criteria->order = 'sort';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    public static function getMenu(){
        
        $result=Yii::app()->db->createCommand()
            ->selectDistinct('id_catalog')
            ->from('{{catalog_items}}')
            ->queryAll();
        
        $result=array_map(function($n){return $n['id_catalog'];},$result);
        
        if ($result)
        {
            $catalogs=self::model()->findAll('id in ('.implode(',',$result).') and status=1');
            return $catalogs ? $catalogs : array();
        }
        return array();
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }


}
