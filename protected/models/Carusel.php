<?php

/**
* This is the model class for table "{{carusel}}".
*
* The followings are the available columns in table '{{carusel}}':
    * @property integer $id
    * @property string $name
    * @property integer $model
    * @property integer $view
    * @property string $settings
    * @property integer $status
    * @property integer $sort
    * @property string $create_time
    * @property string $update_time
*/
class Carusel extends EActiveRecord
{

    private $_items=false;

    public function translition(){
        return 'Слайдер '.$this->name;
    }
    
    public function getItems(){
        if ($this->_items===false && $this->id_entity)
        {

            $this->_items=CHtml::listData($this->item_list,'id','id');
        }
        return $this->_items===false  ? array() : $this->_items;
    }

    public function setItems($data){
        $this->_items=$data;
    }

    public function afterFind(){
        parent::afterFind();
        if ($this->id_entity)
            $this->metaData->addRelation('item_list',array(self::MANY_MANY,$this->entity->alias,'{{carusel_items}}(id_carusel,id_item)'));
    }

    public function getEntityData(){
        if ($this->id_entity)
            return $this->entity->getInstance()->findAll();
        return array();
    }

    public function afterSave(){
        parent::afterSave();
        $criteria=new CDbCriteria;
        $criteria->addCondition('id_carusel=:id');
        $criteria->params[':id']=$this->id;

        CaruselItems::model()->deleteAll($criteria);
        if ($this->items)
        {
            foreach ($this->items as $key => $value) {
                $item=new CaruselItems;
                $item->id_item=$value;
                $item->id_carusel=$this->id;
                $item->save();
            }
        }
    }

    public function tableName()
    {
        return '{{carusel}}';
    }
    
    public function rules()
    {
        return array(
            array('name','required'),
            array('id_entity, status, sort', 'numerical', 'integerOnly'=>true),
            array('name', 'length', 'max'=>255),
            array('settings, items, create_time, update_time', 'safe'),
            // The following rule is used by search().
            array('id, name, model, settings, status, sort, create_time, update_time', 'safe', 'on'=>'search'),
        );
    }

    public function relations()
    {
        return array(
            'entity'=>array(self::BELONGS_TO,'Entity','id_entity'),
        );
    }

    public function beforeDelete(){
        parent::beforeDelete();
        CaruselItems::model()->deleteAll('id_carusel=:carusel',array(':carusel'=>$this->id));
        return true;
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'name' => 'Название',
            'id_item' => 'Модель',
            'settings' => 'Настройки',
            'status' => 'Статус',
            'sort' => 'Вес для сортировки',
            'create_time' => 'Дата создания',
            'update_time' => 'Дата последнего редактирования',
        );
    }

    public function behaviors()
    {
        return CMap::mergeArray(parent::behaviors(), array(
			
        ));
    }

    public function search()
    {
        $criteria=new CDbCriteria;
		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		//$criteria->compare('id_item',$this->id_item);
		$criteria->compare('status',$this->status);
		$criteria->compare('sort',$this->sort);
        $criteria->order = 'sort';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}