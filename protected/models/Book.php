<?php

/**
* This is the model class for table "{{book}}".
*
* The followings are the available columns in table '{{book}}':
    * @property integer $id
    * @property string $phone
    * @property string $email
    * @property string $fio
    * @property integer $delivery
    * @property string $address
    * @property string $dt_sold
    * @property integer $status
    * @property integer $sort
    * @property string $create_time
    * @property string $update_time
*/
class Book extends EActiveRecord
{

    public $id_city;
    public function tableName()
    {
        return '{{book}}';
    }

    public static function getDelivery($id=false){
        $data=array(1=>'Курьер',2=>'Самовывоз');
        return $id ? $data[$id] : $data;
    }

    public function translition($var=0){
        $variants=array(
            'Заказ',
            'Заказа',
            'Заказами'
        );
        return $variants[$var];
    }

    public static function getStatusAliases($var=null){
        $variants=array(
            'Рассмотрение',
            'Офрмление',
            'Доставлено',
            'Отказано'
        );
        return $var===null ? $variants : $variants[$var];
    }    

    public function rules()
    {
        return array(
            array('delivery, discount, id_city, cost, status, sort', 'numerical', 'integerOnly'=>true),
            array('phone, email, width, id_client, height, fio, address, dt_sold', 'length', 'max'=>255),
            array('create_time, update_time, id_shop', 'safe'),
            // The following rule is used by search().
            array('id, phone, email, fio, delivery, address, dt_sold, status, sort, create_time, update_time', 'safe', 'on'=>'search'),
        );
    }

    public function relations()
    {
        return array(
            'items'=>array(self::HAS_MANY,'BookItem','id_book'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'phone' => 'Телефон',
            'email' => 'Почта',
            'cost'=>'Стоимость',
            'width'=>'Ширина',
            'height'=>'Высота',
            'fio' => 'ФИО',
            'delivery' => 'Доставка',
            'address' => 'Адрес',
            'dt_sold' => 'Дата оплаты',
            'id_city'=>'Город',
            'id_shop'=>'Магазин',
            'status' => 'Статус',
            'sort' => 'Вес для сортировки',
            'create_time' => 'Дата создания',
            'update_time' => 'Дата последнего редактирования',
        );
    }

    public function behaviors()
    {
        return CMap::mergeArray(parent::behaviors(), array(
			'CTimestampBehavior' => array(
				'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'create_time',
                'updateAttribute' => 'update_time',
                'setUpdateOnCreate' => true,
			),
        ));
    }

    public function search()
    {
        $criteria=new CDbCriteria;
		$criteria->compare('id',$this->id);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('fio',$this->fio,true);
		$criteria->compare('delivery',$this->delivery);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('dt_sold',$this->dt_sold,true);
		$criteria->compare('status',$this->status);
        $criteria->compare('discount',$this->status);
		$criteria->compare('sort',$this->sort);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('update_time',$this->update_time,true);
        $criteria->order = 'sort';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

	public function beforeSave()
	{
		if (!empty($this->dt_sold))
			$this->dt_sold = Yii::app()->date->toMysql($this->dt_sold);
		return parent::beforeSave();
	}

	public function afterFind()
	{
		parent::afterFind();
		if ( in_array($this->scenario, array('insert', 'update')) ) { 
			$this->dt_sold = ($this->dt_sold !== '0000-00-00' && $this->dt_sold!=null ) ? date('d-m-Y', strtotime($this->dt_sold)) : '';
		}
	}
}
