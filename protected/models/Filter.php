<?
class Filter extends CModel {
	public $size;
	public $colors;
	public $images;
	public $form;
	public $catalogs;
	public $criteria;
	public $price;
	public $styles;
	public $actives=array();
	public $term=false;
	const SIZE=8;
	const COLORS=4;
	const FORM=7;
	
	public function rules(){
		return array(
			array('colors, images, styles, term, price,size,form,catalogs,criteria','safe'),
		);
	}

	public function attributeNames(){
		return array(
			'size',
			'colors',
			'catalogs',
			'criteria',
			'images',
			'styles'
		);
	}
	protected $props=array(
			'colors'=>4,
			'form'=>8,
			'images'=>5,
			'styles'=>6,
		);

	public function __construct(){
		$this->criteria=new CDbCriteria;
	}

	public function findSize(){
		$result=Attr::model()->findByPk(8)->rangeValues;
		return $result ? $result : array();
	}

	public function findForm(){
		$result=Attr::model()->findByPk(7)->rangeValues;
		return $result ? $result : array();
	}

	public function findColors(){
		$result=Attr::model()->findByPk(4)->rangeValues;
		return $result ? $result : array();
	}

	public function findImages(){
		$result=Attr::model()->findByPk(5)->rangeValues;
		return $result ? $result : array();
	}

	public function findCatalogs(){
		$result=Catalog::model()->findAll();
		return $result ? $result : array();
	}

	public function findStyles(){

		$result=Attr::model()->findByPk(6)->rangeValues;
		return $result ? $result : array();
	}

	public function isActive($id,$prop){
		if ($this->{$prop})
		foreach ($this->{$prop} as $key => $value) {
			if ($value==$id)
				return true;
		}
		return false;
	}

	public function getCriteria(){
		$this->actives=$this->getAttributes();
		$this->criteria=Items::model()->getFindCriteria(false);
		$this->criteria->distinct=true;
		$inValue=array();
		if ($this->colors)
			$this->colors=array_map(function($n){return (string)$n;},$this->colors);
		foreach ($this->props as $attr_id => $attr) {
			if ($this->{$attr_id})
			{
				$this->criteria->addCondition('attr_model.id_attr=:'.$attr_id);
				$this->criteria->params[':'.$attr_id]=$attr;
				$inValue=$inValue+$this->{$attr_id};
			}
		}
		if ($this->size)
		{
			$this->criteria->addCondition('attr_model.id_attr='.self::SIZE.' and attr_val.value='.$this->size);
			// $inValue[]=$this->size;
		}
		if (!empty($inValue))
			$this->criteria->addInCondition('attr_val.value',$inValue);
		if ($this->catalogs)
		{
			$this->criteria->join.="inner join {{catalog_items}} cat_items on cat_items.id_item=t.id";
			$this->criteria->addInCondition('id_catalog',$this->catalogs);
		}
		if ($this->term!==false && strlen($this->term)>1)
		{	
			$this->criteria->addCondition('concat(t.name,t.wswg_body) like(\'%'.$this->term.'%\')');
		}

		if ($this->price)
		{
			foreach ($this->price as $key => $data) {
				$this->criteria->addCondition('price<'.$this->price);	
			}
			
		}
		$this->criteria->addCondition('t.status=1');
		return $this->criteria;
	}
	public static function model(){
		return new Filter;
	}
}
?>