<?php

/**
* This is the model class for table "{{attr_group}}".
*
* The followings are the available columns in table '{{attr_group}}':
    * @property integer $id
    * @property integer $name
    * @property integer $alias
*/

class AttrGroup extends EActiveRecord
{
    private $_eavAttributes=array('value'=>array(),'required'=>array());
    public $post_id_type=2;

    public static final  function getList($id=null){
        $list=CHtml::listData(AttrGroup::model()->findAll(),'id','name');
        return $id ? $list[$id] : $list;
    }

    public function getEavAttributes(){
        
        if ($this->_eavAttributes['value']==null)
        {
            $result=Yii::app()->db->createCommand()
                ->select('t.id,required')
                ->from('{{attr}} t')
                ->join('{{group_set_attr}} gas','gas.id_attr=t.id')
                ->where('gas.id_group=:id',array(':id'=>$this->id))
                ->queryAll();

            foreach ($result as $key => $value) {
                $this->_eavAttributes['value'][$value['id']]=$value['id'];
                if ($value['required'])
                    $this->_eavAttributes['required'][$value['id']]=$value['id'];
            }
        }
        return  $this->_eavAttributes===false ? array('value'=>array(),'required'=>array()) : $this->_eavAttributes;
    }

    public function SetEavAttributes($data){
        $this->_eavAttributes=$data;
    }

    public function tableName()
    {
        return '{{attr_group}}';
    }

    public static function getAttrList($id=null){
        $attrs=CHtml::listData(Attr::model()->findAll(),'id','label');
        if ($id)
            return $attrs[$id];
        return $attrs;
    }

    public function rules()
    {
        return array(
            array('name','required'),
            array('alias','unique'),
            array('eavAttributes','safe'),
            // The following rule is used by search().
            array('id, name, alias', 'safe', 'on'=>'search'),
        );
    }


    public function relations()
    {
        return array(
            'attrs'=>array(self::MANY_MANY,'Attr','{{group_set_attr}}(id_attr,id_group)',
                'joinType'=>'INNER JOIN',
            ),
        );
    }

    public function afterSave(){

        $iDs=array();
        $criteria=new CDbCriteria;
        if ($this->_eavAttributes['value'])
        {
            foreach ($this->eavAttributes['value'] as $key => $data) {
                $isRequired=$this->eavAttributes['required'][$data]!=null;
                $model=GroupSetAttr::model()->find('id_group=:group and id_attr=:attr',array(':group'=>$this->id,':attr'=>$data));
                if (!$model)
                {
                    $model=new GroupSetAttr;
                    $model->id_group=$this->id;
                }
                $model->id_attr=$data;
                $model->required=$isRequired;
                $model->save();
                $iDs[]=$model->id;
            }
            $criteria->addNotInCondition('id',$iDs);
        }

        $criteria->addCondition('id_group=:id');
        $criteria->params[":id"]=$this->id;

        $models=GroupSetAttr::model()->findAll($criteria);
        $deleted=array();

        if ($models)
        {
            foreach ($models as $key => $data) {
                $data->delete();
                
                if (!$deleted[$data->id_group])
                {
                    $result=ModelGroupSet::model()->findAll('id_group=:group',array(':group'=>$data->id_group));
                    if ($result)
                    {
                        $criteria=new CDbCriteria;
                        $criteria->addInCondition('id_group',$result);
                        AttrVal::model()->deleteAll('post_type=true and post_id=:post_id',
                            array(
                                ':post_id'=>$data->id_attr
                            )
                        );
                    }
                }
                $deleted[$data->id_group]=true;
            }
        }
    }
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'name' => 'Название группы атрибутов',
            'alias' => 'Уникальное название',
        );
    }



    public function search()
    {
        $criteria=new CDbCriteria;
		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name);
		$criteria->compare('alias',$this->alias);
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }


}
