<?
	class ChangePwd extends Client{
		private $_oldPassword=false;
		public $hash;
		public $verifyPassword;
		public $newPassword;
		public $repeatNewPassword;

		public function getOldPassword(){
			return $this->_oldPassword;
		}

		public function setOldPassword($data){
			$this->_oldPassword=$data;
			$this->hash=Yii::app()->getModule('user')->encrypting($data);
		}

		public function rules()
		{
			$rules = array(
				array('oldPassword,newPassword,repeatNewPassword', 'required'),
				array('id,password', 'safe'),
				array('oldPassword', 'length', 'max'=>128, 'min' => 4,'message' => "Длина логина должна занимать от 4 до 128 символов"),
				array('oldPassword', 'checkOldPas', 'message' => 'Не правильно введен старый пароль!'),
				array('newPassword', 'compare', 'compareAttribute'=>'repeatNewPassword', 'message' => 'Пароли не совпадают!'),
			);
			return $rules;
		}

		public function checkOldPas(){
			if ($this->hash!==$this->password)
			{
				$this->addError('oldPassword','Не правильно введен старый пароль!');
			}
			return $this->errors;
		}

		public function attributeLabels()
	    {
	        return array(
	        	'fio'=>'ФИО',
	        	'email'=>'Электронная почта',
	        	'phone'=>'Телефон',
	            'verifyPassword' => 'Повторите пароль',
	            'password' => 'Пароль',
	        );
	    }

		public function beforeSave()
		{
			if (!empty($this->password))
			{
				$this->password=Yii::app()->getModule('user')->encrypting($this->password);
			}
			return true;
		}
		public function savePass(){
			return Yii::app()->db
				->createCommand()
				->update('{{client}}',array('password'=>Yii::app()->getModule('user')->encrypting($this->newPassword)),'id=:id',array(':id'=>$this->id));
		}
	}
?>