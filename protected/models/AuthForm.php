<?
class AuthForm extends CFormModel {
    public $email;
    public $password;

    public function rules(){
        return array(
            array('email','match','pattern'=>'/[0-9a-z_]+@[-0-9a-z_^\.]+\.[a-z]{2,3}/i','message'=>'Введенный адрес не является адресмо электронной почты!'),
            array('email,password','required'),
            array('password', 'authenticate'),
        );
    }

    public function attributeNames(){
        return array(
            'email',
            'password',
        );
    }

    public function authenticate()
    {
        if(!$this->hasErrors())  // we only want to authenticate when no input errors
        {
            $identity=new ClientIdentity($this->email,$this->password);
            Yii::app()->user->table="client";
            $identity->authenticate();

            switch($identity->errorCode)
            {
                case ClientIdentity::ERROR_NONE:
                    $duration=Yii::app()->controller->module->rememberMeTime;
                    Yii::app()->user->login($identity,$duration);
                    break;
                case ClientIdentity::ERROR_PASSWORD_INVALID:
                    $this->addError("password","Не верно заданы email или пароль");
                    break;
                case ClientIdentity::ERROR_USERNAME_INVALID:
                    $this->addError("password","Не верно заданы email или пароль");
                    break;
            }
            $identity->setState('username',$identity->record->fio);
        }
        return !$this->errors;
    }

}
?>