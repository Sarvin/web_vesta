<?php

/**
* This is the model class for table "{{menu}}".
*
* The followings are the available columns in table '{{menu}}':
    * @property integer $id
    * @property integer $id_entity
    * @property string $name
    * @property integer $status
    * @property integer $sort
*/
class Menu extends EActiveRecord
{
    private $_items=false;

    public function getItems(){
        if ($this->_items===false && $this->id_entity)
        {
            $this->_items=CHtml::listData($this->item_list,'id','id');
            $this->_items=$this->_items ? $this->_items : array();
        }
        return $this->_items;
    }

    public function setItems($data)
    {
        $this->_items=$data;
    }

    public function translition(){
        return 'Меню';
    }

    public function tableName()
    {
        return '{{menu}}';
    }

    public function getEntityData(){
        if ($this->id_entity)
            return $this->entity->getInstance()->findAll();
        return array();
    }

    public function afterFind(){
        parent::afterFind();
        if ($this->id_entity)
        {
            $order=$this->entity->getInstance()->hasAttribute('sort') ? 'sort' : 'id';
            $this->metaData->addRelation('item_list',array(self::MANY_MANY,$this->entity->alias,'{{menu_items}}(id_menu,id_model)','order'=>"item_list.$order asc"),array('order'=>"$order ask"));
            $this->item_list;
        }            
    }

    public function beforeDelete(){
        $criteria=new CDbCriteria;
        $criteria->addCondition('id_menu=:id');
        $criteria->params[':id']=$this->id;
        MenuItems::model()->deleteAll($criteria);
        return parent::beforeDelete();
    }

    public function afterSave(){
        parent::afterSave();
        $criteria=new CDbCriteria;
        $criteria->addCondition('id_menu=:id');
        $criteria->params[':id']=$this->id;
        MenuItems::model()->deleteAll($criteria);

        if ($this->items)
        {

            foreach ($this->items as $key => $value) {
                $item=new MenuItems;
                $item->id_model=$value;
                $item->id_menu=$this->id;
                $item->save();
            }
        }
    }

    public function rules()
    {
        return array(
            array('id_entity, status, sort', 'numerical', 'integerOnly'=>true),
            array('name,alias', 'length', 'max'=>255),
            array('items','safe'),
            // The following rule is used by search().
            array('id, id_entity, name, status, sort', 'safe', 'on'=>'search'),
        );
    }


    public function relations()
    {
        return array(
            'entity'=>array(self::BELONGS_TO,'Entity','id_entity'),
        );
    }


    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'id_entity' => 'Id Entity',
            'name' => 'Название',
            'status' => 'Статус',
            'sort' => 'Вес для сортировки',
        );
    }



    public function search()
    {
        $criteria=new CDbCriteria;
		$criteria->compare('id',$this->id);
		$criteria->compare('id_entity',$this->id_entity);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('sort',$this->sort);
        $criteria->order = 'sort';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }


}
