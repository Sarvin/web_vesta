<?php

/**
* This is the model class for table "{{group_set_attr}}".
*
* The followings are the available columns in table '{{group_set_attr}}':
    * @property integer $id
    * @property integer $id_attr_group
    * @property integer $id_attr
    * @property integer $required
*/
class GroupSetAttr extends EActiveRecord
{
    public function tableName()
    {
        return '{{group_set_attr}}';
    }


    public function rules()
    {
        return array(
            array('id_group, id_attr', 'numerical', 'integerOnly'=>true),
            // The following rule is used by search().
            array('required','boolean'),
            array('id, id_group, id_attr, required', 'safe', 'on'=>'search'),
        );
    }


    public function relations()
    {
        return array(
        );
    }


    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'id_group' => 'Комментарий',
            'id_attr' => 'Атрибут',
            'required' => 'Обязательный?',
        );
    }



    public function search()
    {
        $criteria=new CDbCriteria;
		$criteria->compare('id',$this->id);
		$criteria->compare('id_attr_group',$this->id_attr_group);
		$criteria->compare('id_attr',$this->id_attr);
		$criteria->compare('required',$this->required);
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }


}
