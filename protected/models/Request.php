<?php

/**
* This is the model class for table "{{request}}".
*
* The followings are the available columns in table '{{request}}':
    * @property integer $id
    * @property integer $id_user
    * @property integer $state
    * @property integer $post_request_id
    * @property integer $post_id
    * @property integer $status
    * @property integer $sort
    * @property string $create_time
    * @property string $update_time
*/
class Request extends EActiveRecord
{
    public function tableName()
    {
        return '{{request}}';
    }


    public function rules()
    {
        return array(
            array('id_user, state, post_request_id, post_id, status, sort', 'numerical', 'integerOnly'=>true),
            array('create_time, update_time', 'safe'),
            // The following rule is used by search().
            array('id, id_user, state, post_request_id, post_id, status, sort, create_time, update_time', 'safe', 'on'=>'search'),
        );
    }


    public function relations()
    {
        return array(
        );
    }


    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'id_user' => 'Стутус',
            'state' => 'Стутус',
            'post_request_id' => 'Звонок|Заказ',
            'post_id' => 'Ид',
            'status' => 'Статус',
            'sort' => 'Вес для сортировки',
            'create_time' => 'Дата создания',
            'update_time' => 'Дата последнего редактирования',
        );
    }


    public function behaviors()
    {
        return CMap::mergeArray(parent::behaviors(), array(
			'CTimestampBehavior' => array(
				'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'create_time',
                'updateAttribute' => 'update_time',
                'setUpdateOnCreate' => true,
			),
        ));
    }

    public function search()
    {
        $criteria=new CDbCriteria;
		$criteria->compare('id',$this->id);
		$criteria->compare('id_user',$this->id_user);
		$criteria->compare('state',$this->state);
		$criteria->compare('post_request_id',$this->post_request_id);
		$criteria->compare('post_id',$this->post_id);
		$criteria->compare('status',$this->status);
		$criteria->compare('sort',$this->sort);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('update_time',$this->update_time,true);
        $criteria->order = 'sort';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }


}
