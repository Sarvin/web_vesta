<?php

/**
* This is the model class for table "{{call}}".
*
* The followings are the available columns in table '{{call}}':
    * @property integer $id
    * @property integer $id_user
    * @property string $phone
    * @property integer $status
    * @property integer $sort
    * @property string $create_time
    * @property string $update_time
*/
class Call extends EActiveRecord
{
    public function tableName()
    {
        return '{{call}}';
    }

    public function translition($var=0){
        $variants=array(
            'Звонк',
            'Звонка',
            'Звонками'
        );
        return $variants[$var];
    }

    public function getCount(){
        $result=Yii::app()->db->createCommand()
            ->select('count(id) as count')
            ->from('{{call}}')
            ->where('status!=1')
            ->queryRow();
            return $result['count'];
    }

    public function rules()
    {
        return array(
            array('id_user, status, sort', 'numerical', 'integerOnly'=>true),
            array('phone', 'length', 'max'=>255),
            array('create_time, update_time', 'safe'),
            // The following rule is used by search().
            array('id, id_user, phone, status, sort, create_time, update_time', 'safe', 'on'=>'search'),
        );
    }


    public function relations()
    {
        return array(
        );
    }


    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'id_user' => 'Пользователь',
            'phone' => 'Телефон',
            'status' => 'Статус',
            'sort' => 'Вес для сортировки',
            'create_time' => 'Дата создания',
            'update_time' => 'Дата последнего редактирования',
        );
    }


    public function behaviors()
    {
        return CMap::mergeArray(parent::behaviors(), array(
			'CTimestampBehavior' => array(
				'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'create_time',
                'updateAttribute' => 'update_time',
                'setUpdateOnCreate' => true,
			),
        ));
    }

    public function search()
    {
        $criteria=new CDbCriteria;
		$criteria->compare('id',$this->id);
		$criteria->compare('id_user',$this->id_user);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('sort',$this->sort);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('update_time',$this->update_time,true);
        $criteria->order = 'sort desc';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }


}
