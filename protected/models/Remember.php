<?php

/**
* This is the model class for table "{{remember}}".
*
* The followings are the available columns in table '{{remember}}':
    * @property integer $id
    * @property integer $id_user
    * @property string $email
    * @property integer $cookies
*/
class Remember extends EActiveRecord
{
    public function tableName()
    {
        return '{{remember}}';
    }


    public function rules()
    {
        return array(
            array('id_user, cookies', 'numerical', 'integerOnly'=>true),
            array('email', 'length', 'max'=>255),
            // The following rule is used by search().
            array('id, id_user, email, cookies', 'safe', 'on'=>'search'),
        );
    }


    public function relations()
    {
        return array(
        );
    }


    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'id_user' => 'Пользователь',
            'email' => 'Почта',
            'cookies' => 'Куки',
        );
    }



    public function search()
    {
        $criteria=new CDbCriteria;
		$criteria->compare('id',$this->id);
		$criteria->compare('id_user',$this->id_user);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('cookies',$this->cookies);
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }


}
