<?php

/**
* This is the model class for table "{{carusel_items}}".
*
* The followings are the available columns in table '{{carusel_items}}':
    * @property integer $id
    * @property integer $id_item
    * @property integer $id_carusel
*/
class CaruselItems extends EActiveRecord
{
    public function tableName()
    {
        return '{{carusel_items}}';
    }


    public function rules()
    {
        return array(
            array('id_item, id_carusel', 'numerical', 'integerOnly'=>true),
            // The following rule is used by search().
            array('id, id_item, id_carusel', 'safe', 'on'=>'search'),
        );
    }


    public function relations()
    {
        return array(
        );
    }


    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'id_item' => 'Уникальный ключ модели',
            'id_carusel' => 'Карусель',
        );
    }



    public function search()
    {
        $criteria=new CDbCriteria;
		$criteria->compare('id',$this->id);
		$criteria->compare('id_item',$this->id_item);
		$criteria->compare('id_carusel',$this->id_carusel);
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }


}
