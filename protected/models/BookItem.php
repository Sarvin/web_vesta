<?php

/**
* This is the model class for table "{{book_item}}".
*
* The followings are the available columns in table '{{book_item}}':
    * @property integer $id
    * @property integer $id_book
    * @property integer $id_item
    * @property integer $count
    * @property integer $summ
    * @property integer $discount
*/
class BookItem extends EActiveRecord
{

    private $_size=false;

    public function getSize(){
        $h=Items::getHeightSize($this->height);
        $w=Items::getWidthSize($this->width);
        return $w .'x'. $h;
    }

    public function tableName()
    {
        return '{{book_item}}';
    }

    public function rules()
    {
        return array(
            array('id_book, width, height, id_item, count, summ, discount', 'numerical', 'integerOnly'=>true),
            // The following rule is used by search().
            array('id, id_book, size, id_item, count, summ, discount', 'safe', 'on'=>'search'),
        );
    }

    public function relations()
    {
        return array(
            'item'=>array(self::BELONGS_TO,'Items','id_item'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'id_book' => 'Заказ',
            'id_item' => 'Товар',
            'count' => 'Количество',
            'summ' => 'Общая сумма',
            'width'=>'Ширина',
            'height'=>'Высота',
            'discount' => 'Сумма скидки',
        );
    }

    public function search()
    {
        $criteria=new CDbCriteria;
		$criteria->compare('id',$this->id);
		$criteria->compare('id_book',$this->id_book);
		$criteria->compare('id_item',$this->id_item);
		$criteria->compare('count',$this->count);
		$criteria->compare('summ',$this->summ);
		$criteria->compare('discount',$this->discount);
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

}
