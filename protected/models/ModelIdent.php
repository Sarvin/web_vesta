<?php

/**
* This is the model class for table "{{model_ident}}".
*
* The followings are the available columns in table '{{model_ident}}':
    * @property integer $id
    * @property integer $id_entity
    * @property integer $id_model
*/
class ModelIdent extends EActiveRecord
{
    public function tableName()
    {
        return '{{model_ident}}';
    }


    public function rules()
    {
        return array(
            array('id_entity, id_model', 'numerical', 'integerOnly'=>true),
            // The following rule is used by search().
            array('id, id_entity, id_model', 'safe', 'on'=>'search'),
        );
    }


    public function relations()
    {
        return array(
        );
    }


    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'id_entity' => 'Сущность',
            'id_model' => 'Сущность',
        );
    }



    public function search()
    {
        $criteria=new CDbCriteria;
		$criteria->compare('id',$this->id);
		$criteria->compare('id_entity',$this->id_entity);
		$criteria->compare('id_model',$this->id_model);
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }


}
