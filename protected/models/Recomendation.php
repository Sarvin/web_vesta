<?php

/**
* This is the model class for table "{{recomendation}}".
*
* The followings are the available columns in table '{{recomendation}}':
    * @property integer $id
    * @property string $name
    * @property string $img_preview
    * @property string $wswg_body
    * @property integer $status
    * @property integer $sort
*/
class Recomendation extends EActiveRecord
{
    public function tableName()
    {
        return '{{recomendation}}';
    }


    public function rules()
    {
        return array(
            array('status, sort', 'numerical', 'integerOnly'=>true),
            array('name, img_preview, wswg_body', 'length', 'max'=>255),
            // The following rule is used by search().
            array('id, name, img_preview, wswg_body, status, sort', 'safe', 'on'=>'search'),
        );
    }


    public function relations()
    {
        return array(
        );
    }


    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'name' => 'Название',
            'img_preview' => 'Фото',
            'wswg_body' => 'Подсказка',
            'status' => 'Статус',
            'sort' => 'Вес для сортировки',
        );
    }


    public function behaviors()
    {
        return CMap::mergeArray(parent::behaviors(), array(
			'imgBehaviorPreview' => array(
				'class' => 'application.behaviors.UploadableImageBehavior',
				'attributeName' => 'img_preview',
				'versions' => array(
                    'default' => array(
                        'resize' => array(25),
                    ),
					'icon' => array(
						'centeredpreview' => array(90, 90),
					),
					'small' => array(
						'resize' => array(200, 180),
					)
				),
			),
        ));
    }

    public function search()
    {
        $criteria=new CDbCriteria;
		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('img_preview',$this->img_preview,true);
		$criteria->compare('wswg_body',$this->wswg_body,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('sort',$this->sort);
        $criteria->order = 'sort';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }


}
