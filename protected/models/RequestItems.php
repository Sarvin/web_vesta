<?php

/**
* This is the model class for table "{{request_items}}".
*
* The followings are the available columns in table '{{request_items}}':
    * @property integer $id
    * @property integer $id_request
    * @property integer $id_item
    * @property integer $status
    * @property integer $sort
    * @property string $create_time
    * @property string $update_time
*/
class RequestItems extends EActiveRecord
{
    public function tableName()
    {
        return '{{request_items}}';
    }


    public function rules()
    {
        return array(
            array('id_request, id_item, status, sort', 'numerical', 'integerOnly'=>true),
            array('create_time, update_time', 'safe'),
            // The following rule is used by search().
            array('id, id_request, id_item, status, sort, create_time, update_time', 'safe', 'on'=>'search'),
        );
    }


    public function relations()
    {
        return array(
        );
    }


    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'id_request' => 'Id Request',
            'id_item' => 'Id Item',
            'status' => 'Status',
            'sort' => 'Sort',
            'create_time' => 'Create Time',
            'update_time' => 'Update Time',
        );
    }


    public function behaviors()
    {
        return CMap::mergeArray(parent::behaviors(), array(
			'CTimestampBehavior' => array(
				'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'create_time',
                'updateAttribute' => 'update_time',
                'setUpdateOnCreate' => true,
			),
        ));
    }

    public function search()
    {
        $criteria=new CDbCriteria;
		$criteria->compare('id',$this->id);
		$criteria->compare('id_request',$this->id_request);
		$criteria->compare('id_item',$this->id_item);
		$criteria->compare('status',$this->status);
		$criteria->compare('sort',$this->sort);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('update_time',$this->update_time,true);
        $criteria->order = 'sort';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }


}
