<?php

/**
* This is the model class for table "{{items}}".
*
* The followings are the available columns in table '{{items}}':
    * @property integer $id
    * @property string $name
    * @property string $garanty
    * @property string $wswg_body
    * @property integer $width
    * @property integer $price
    * @property integer $old_price
    * @property integer $status
    * @property integer $sort
    * @property string $create_time
    * @property string $update_time
*/

class Items extends EActiveRecord implements IECartPosition
{
    const DEFAULT_SIZE='0x0';

    private $_categories=false;
    private $_state=1;
    private $_size=false;
    private $_pos=false;
    private $_catalogView=false;
    public $displaySeo=true;
    
    public $choosenWidth;
    public $choosenHeight;

    private $_populars=false;

    public function getIsFavorite(){
        $res=FavoriteItem::model()->count('id_favorite=:f and id_item=:item',array(':f'=>Yii::app()->user->id_favorite,':item'=>$this->id));
        return $res;
    }

    public function getCatalogView(){
        if ($this->_catalogView===false)
        {
            $data=CHtml::listData($this->itemCategories,'id','name');
            $this->_catalogView=implode(',', $data);
        }
        return $this->_catalogView;
    }

    public function getSize(){
        if ($this->_size===false)
        {
            if ($this->state==1)
                $this->_size=$this->choosenWidth.'x'.$this->choosenHeight;
            else 
                $this->_size=$this->favorite->size;
        }
        return $this->_size;
    }

    public function setSize($data){
        $data=explode('x',$data);
        $this->choosenWidth=$data[0];
        $this->choosenHeight=$data[1];
    }

    public function getState(){
        return $this->_state;
    }   

    public function setState($data){
        return $this->_state=$data;
    }   

    public function getPosition(){
        return $this->_pos=Yii::app()->cart->itemAt($this->getId());
        
    }

    public function getId(){
        return 'Items'.$this->id;
    }

    public function getPrice(){
        
        return $this->priceBySize;
    }
    
    public function getPriceBySize(){
        
        if ($this->state==2){

            $param=explode('x',$this->favorite->size);
            $this->choosenWidth=$param[0];
            $this->choosenHeight=$param[1];
            return !empty($this->priceRange[$this->choosenWidth][$this->choosenHeight]) ? $this->priceRange[$this->choosenWidth][$this->choosenHeight] : $this->price;
        }
        else
        {
            return $this->priceRange[$this->choosenWidth][$this->choosenHeight] ? $this->priceRange[$this->choosenWidth][$this->choosenHeight] : $this->price;
        }
    }

    public function init(){
        $this->detachBehavior('Eav');
    }

    public function getPopulars(){
        if ($this->_populars===false)
        {
            $this->_populars=CHtml::listData(self::model()->findAll('best=true'),'id','id');
        }
        return $this->_populars;
    }

    public function setPopulars($data){
        $this->_populars=$data;
    }

    public function getCategories(){
        if ($this->_categories===false)
        {
            $this->_categories=CHtml::listData($this->itemCategories,'id','id');
        }
        return $this->_categories;
    }

    public function setCategories($data){
        $this->_categories=$data;
    }

    public function tableName()
    {
        return '{{items}}';
    }

    public function getSizeList($id=false){
        $result=array();

        foreach ($this->getWidthVal() as $key_w => $w) {
            foreach ($this->getHeightVal() as $key_h => $h) {
                $result[$key_w.'x'.$key_h]=self::getWidthSize($w).'x'.self::getHeightSize($h);
            }
        }

        return $id ? $result[$id] : $result;
    }

    public function getEavAttrHeights(){
        $attrVal=$this->findAllAttrVals(4);
        return $attrVal;
    }

    public function getEavAttrWidths(){
        $attrVal=$this->findAllAttrVals(1);
        return $attrVal;
    }

    public function getEavAttrStyle(){
        $attr=Attr::model()->setIdent($this->ident->id)->applies()->findByPk(6);
        return array($attr);
    }

    public function getEavAttrImages(){
        $attr=Attr::model()->setIdent($this->ident->id)->applies()->findByPk(5);
        return array($attr);
    }

    public function getHeightVal($id=null){
        $data=array();
        if (is_array($this->height))
        {
            foreach ($this->height as $key => $value) {
                $data[$key]=$key;
            }
             if ($id)
                return $data[$id];
            return $data;
        } else
            return $this->height ? $this->height : $data;
    }

    public function getWidthVal($id=null){
        $data=array();
        if (is_array($this->width))
        {
            foreach ($this->width as $key => $value) {
                $data[$key]=$key;
            }
            if ($id)
                return $data[$id];
            return $data;
        } else 
            return $this->width ? $this->width : $data;
    }



    public function getRecomendationsVal(){
        $data=array();
        if (is_array($this->recomendations))
        {
            foreach ($this->recomendations as $key => $value) {
                $data[$key]=$value;
            }
            return $data;
        } else
            return $this->recomendations;
    }

    public function setRecomendationsVal($data){
        $this->recomendations=$data;
    }

    public function setWidthVal($data){
        $this->width=$data;
    }

    public function setHeightVal($data){
        $this->height=$data;
    }

    public function getHeightTextView(){
        $sizes=self::getHeightSize();
        $text="";
        if (is_array($this->height))
        foreach ($this->height as $key => $value) {
            $text.=$sizes[$value].', ';
        }
        return substr($text, 0, strlen($text)-2);
    }

    public function getWidthTextView(){
        $sizes=self::getWidthSize();
        $text="";
        if (is_array($this->width))
        foreach ($this->width as $key => $value) {
            $text.=$sizes[$value].', ';

        }
        return substr($text, 0, strlen($text)-2);
    }

    public function translition($var=0){
        $variants=array(
            'Товар',
            'Товара',
            'Товарами'
        );
        return $variants[$var];
    }
    public function afterFind(){
        parent::afterFind();
        if ($res=unserialize($this->height))
            $this->height=$res;
        if ($res=unserialize($this->width))
            $this->width=unserialize($this->width);
        if ($res=unserialize($this->recomendations))
            $this->recomendations=unserialize($this->recomendations);
        if ($res=unserialize($this->priceRange))
            $this->priceRange=unserialize($this->priceRange);
    }

    public static function getFavoriteCost(){
        $favorites=self::getFavorite();
        $cost=0;
        foreach ($favorites as $key => $data) {
            $cost+=$data->priceBySize*$data->favorite->count;;
        }
        return $cost;
    }

    public function getFavoriteCount(){
        $result=Yii::app()->db->createCommand()
            ->select('count(id_item) as count')
            ->from('{{favorite_item}}')
            ->where('id_favorite=:f',array(':f'=>Yii::app()->user->id_favorite))
            ->queryAll();
        $result=array_map(function($a){return $a['count'];},$result);
        return $result[0];
        return array();
    }

    public static function getFavorite(){
        $result=Yii::app()->db->createCommand()
            ->select('id_item')
            ->from('{{favorite_item}}')
            ->where('id_favorite=:f',array(':f'=>Yii::app()->user->id_favorite))
            ->queryAll();
        $result=array_map(function($a){return $a['id_item'];},$result);
        
        $criteria=new CDbCriteria;
        $criteria->addInCondition('id',$result);

        if ($result)
            return 
                $result=array_map(
                    function($a){
                        $a->state=2;
                        $param=explode('x',$a->favorite->size);
                        $a->choosenWidth=$param[0];
                        $a->choosenHeight=$param[1];
                        return $a;
                    },self::model()->findAll($criteria)
                );
        return array();
    }

    public static function getWidthSize($digit=null){
        $size=array(190,250,280);
        return $digit===null ? $size : $size[$digit];
    }

    public static function getHeightSize($digit=null){
        $size=array(190,250);
        return $digit===null ? $size : $size[$digit];
    }

    public function rules()
    {
        return array(
            array('price,name','required'),
            array('price, best, old_price, status, sort, id_gallery', 'numerical', 'integerOnly'=>true),
            array('name,alias', 'length', 'max'=>255),
            array('garanty, url, wswg_body, width, height, widthVal, seo, recomendations, recomendationsVal, heightVal, create_time, update_time, categories, priceRange,', 'safe'),
            // The following rule is used by search().
            array('id, name, garanty, wswg_body, width, price, old_price, status, sort, create_time, update_time', 'safe', 'on'=>'search'),
        );
    }


    public function relations()
    {
        return array(
            'itemCategories'=>array(self::MANY_MANY,'Catalog','{{catalog_items}}(id_item,id_catalog)'),
            'seo'=>array(self::BELONGS_TO,'Seo','seo_id'),
            'ident'=>array(self::HAS_ONE,'ModelIdent','id_model','condition'=>'id_entity=2'),
            'favorite'=>array(self::HAS_ONE,'FavoriteItem','id_item','condition'=>'id_favorite="'.Yii::app()->user->id_favorite.'"'),
        );
    }

    public function beforeSave(){
        parent::beforeSave();
        if (!empty($this->height))
        {
            $this->height=is_array($this->height) ? $this->height : array($this->height);
            $this->height=serialize($this->height);
        }
        if (!empty($this->recomendations))
        {
            $this->recomendations=is_array($this->recomendations) ? $this->recomendations : array($this->recomendations);
            $this->recomendations=serialize($this->recomendations);
        }
        if (!empty($this->width))
        {
            $this->width=is_array($this->width) ? $this->width : array($this->width);
            $this->width=serialize($this->width);
        }
        
        if (!empty($this->priceRange))
        {
            $this->priceRange=is_array($this->priceRange) ? $this->priceRange : array($this->priceRange);
            $this->priceRange=serialize($this->priceRange);
        }

        return true;
    }

    public function afterSave(){
        parent::afterSave();
        if ($this->categories)
        {
            $del=array();
            foreach ($this->categories as $key => $value) {
                if (CatalogItems::model()->find('id_item=:this and id_catalog=:catalog',array(':this'=>$this->id,':catalog'=>$value))===null)
                {
                    $catitem=new CatalogItems;
                    $catitem->attributes=array(
                        'id_item'=>$this->id,
                        'id_catalog'=>$value
                    );
                    $catitem->save();
                    $del[]=$catitem->id;
                }
            }
            if ($del)
            {
                $delCriteria=new CDbCriteria;
                $delCriteria->addNotInCondition('id',$del);
                $delCriteria->addCondition('id_item='.$this->id);
                CatalogItems::model()->deleteAll($delCriteria);
            }
        }

    }

    public function beforeDelete(){
        parent::beforeDelete();
        CatalogItems::model()->deleteAll('id_item=:item',array(':item'=>$this->id));
        return true;
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'name' => 'Название',
            'garanty' => 'Гарантия',
            'wswg_body' => 'Описание',
            'best'=>'Популярный тоар',
            'width' => 'Доступные ширины',
            'height' => 'Доступные высоты',
            'price' => 'Цены',
            'categories'=>'Категории товара',
            'recomendations'=>'Рекомендации по уходу',
            'old_price' => 'Старая цена',
            'status' => 'Статус',
            'sort' => 'Вес для сортировки',
            'create_time' => 'Дата создания',
            'update_time' => 'Дата последнего редактирования',
        );
    }


    public function behaviors()
    {
        return CMap::mergeArray(parent::behaviors(), array(
			'CTimestampBehavior' => array(
				'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'create_time',
                'updateAttribute' => 'update_time',
                'setUpdateOnCreate' => true,
			),
            'galleryBehaviorGallery' => array(
                'class' => 'appext.imagesgallery.GalleryBehavior',
                'idAttribute' => 'id_gallery',
                'versions' => array(
                    'small' => array(
                        'adaptiveResize' => array(90, 90),
                    ),
                    'medium' => array(
                        'resize' => array(600, 500),
                    ),
                    'slider' => array(
                        'adaptiveResize' => array(500, 340),
                    ),
                    'main' => array(
                        'adaptiveResize' => array(600, 500),
                    ),
                    'view' => array(
                        'adaptiveResize' => array(460, 460),
                    ),
                    'item' => array(
                        'adaptiveResize' => array(198, 219),
                    ),
                ),
                'name' => true,
                'description' => true,
            ),
            'Eav' => array(
                'class' => 'appext.eavattrs.behaviors.EavBehavior',
                'entity_id'=>$this->entity_id,
                'attachments'=>array(
                    'attr'=>array(4,5,6,7,8),
                ),
                'depends'=>array(
                    'attribute'=>'categories',
                    'entity_id'=>Catalog::model()->entity_id,
                )
            ),
        ));
    }

    public function search()
    {
        $criteria=new CDbCriteria;
		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('garanty',$this->garanty,true);
		$criteria->compare('wswg_body',$this->wswg_body,true);
		$criteria->compare('width',$this->width);
		$criteria->compare('price',$this->price);
		$criteria->compare('old_price',$this->old_price);
		$criteria->compare('status',$this->status);
		$criteria->compare('sort',$this->sort);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('update_time',$this->update_time,true);
        $criteria->order = 'sort';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }


}
