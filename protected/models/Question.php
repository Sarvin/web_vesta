<?php

/**
* This is the model class for table "{{question}}".
*
* The followings are the available columns in table '{{question}}':
    * @property integer $id
    * @property integer $id_user
    * @property integer $comment
    * @property integer $status
    * @property integer $sort
    * @property string $create_time
    * @property string $update_time
*/
class Question extends EActiveRecord
{
    public function tableName()
    {
        return '{{question}}';
    }

    public function translition($var=0){
        $variants=array(
            'Вопрос',
            'Вопроса',
            'Вопросами'
        );
        return $variants[$var];
    }

    public function rules()
    {
        return array(
            array('id_user, status, sort', 'numerical', 'integerOnly'=>true),
            array('email','match','pattern'=>'/[0-9a-z_]+@[-0-9a-z_^\.]+\.[a-z]{2,3}/i','message'=>'Введенный адрес не является адресмо электронной почты!'),
            array('create_time, comment, update_time', 'safe'),
            // The following rule is used by search().
            array('id, id_user, comment, status, sort, create_time, update_time', 'safe', 'on'=>'search'),
        );
    }

    public function getCount(){
        $result=Yii::app()->db->createCommand()
            ->select('count(id) as count')
            ->from('{{question}}')
            ->where('status!=1')
            ->queryRow();
            return $result['count'];
    }

    public function relations()
    {
        return array(

        );
    }


    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'id_user' => 'Пользователь',
            'email' => 'Почта',
            'comment' => 'Вопрос',
            'status' => 'Статус',
            'sort' => 'Вес для сортировки',
            'create_time' => 'Дата создания',
            'update_time' => 'Дата последнего редактирования',
        );
    }


    public function behaviors()
    {
        return CMap::mergeArray(parent::behaviors(), array(
			'CTimestampBehavior' => array(
				'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'create_time',
                'updateAttribute' => 'update_time',
                'setUpdateOnCreate' => true,
			),
        ));
    }

    public function search()
    {
        $criteria=new CDbCriteria;
		$criteria->compare('id',$this->id);
		$criteria->compare('id_user',$this->id_user);
		$criteria->compare('comment',$this->comment);
		$criteria->compare('status',$this->status);
		$criteria->compare('sort',$this->sort);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('update_time',$this->update_time,true);
        $criteria->order = 'sort';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }


}
