<?php

/**
* This is the model class for table "{{shop}}".
*
* The followings are the available columns in table '{{shop}}':
    * @property integer $id
    * @property string $name
    * @property string $time_table
    * @property string $address
    * @property integer $status
    * @property integer $sort
*/
class Shop extends EActiveRecord
{
    public $time_from;
    public $time_to;
    public $city;
    public $shop_address;

    public function getAddressView(){
        return $this->city.' '.$this->shop_address.' '.$this->time_from.' '.$this->time_to;
    }

    public function tableName()
    {
        return '{{shop}}';
    }

    public function translition($var=0){
        $variants=array(
            'Магазин',
            'Магазина',
            'Магазинами'
        );
        return $variants[$var];
    }

    public function rules()
    {
        return array(
            array('status, sort', 'numerical', 'integerOnly'=>true),
            array('name, time_from, time_to, city, shop_address, time_table, address', 'length', 'max'=>255),
            // The following rule is used by search().
            array('id, name, time_table, address, status, sort', 'safe', 'on'=>'search'),
        );
    }

    public function afterFind(){
        parent::afterFind();
        if ($this->address!==null)
        {
            $props=unserialize($this->address);
            foreach ($props as $key => $value) {
                $this->{$key}=$value;
            }
        }
    }

    public function beforeSave(){
        parent::beforeSave();

        $props=array('time_from','time_to','shop_address','city');
        $data=array();
        foreach ($props as $key => $value) {
            $data[$value]=$this->{$value};
        }

        $this->address=serialize($data);
        return true;
    }

    public function relations()
    {
        return array(
        );
    }


    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'name' => 'Название',
            'time_table' => 'Расписание',
            'address' => 'Адрес',
            'status' => 'Статус',
            'sort' => 'Вес для сортировки',
            'time_from'=>'Время открытия',
            'time_to'=>'Время закрытия',
            'city'=>'Город',
            'shop_address'=>'Адрес',
        );
    }



    public function search()
    {
        $criteria=new CDbCriteria;
		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('time_table',$this->time_table,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('sort',$this->sort);
        $criteria->order = 'sort';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }


}
