<?php

/**
* This is the model class for table "{{discount}}".
*
* The followings are the available columns in table '{{discount}}':
    * @property integer $id
    * @property string $hash_key
    * @property integer $status
    * @property integer $sort
    * @property string $create_time
    * @property string $update_time
*/
class Discount extends EActiveRecord
{
    public function tableName()
    {
        return '{{discount}}';
    }

    public function rules()
    {
        return array(
            array('status, sort', 'numerical', 'integerOnly'=>true),
            array('hash_key', 'length', 'max'=>255),
            array('create_time, dt_end, update_time', 'safe'),
            // The following rule is used by search().
            array('id, hash_key, status, sort, create_time, update_time', 'safe', 'on'=>'search'),
        );
    }

    public function relations()
    {
        return array(
        );
    }

    public function beforeSave(){
        if (!parent::beforeSave())
            return;
        if (!$this->hash_key)
        {
            $inc=Config::getValue('discount.time');
            $this->dt_end=date("Y-m-d", strtotime("now +{$inc} days"));
            $this->hash_key=md5('items'.$this->id.time());
        }
        return true;
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'hash_key' => 'hash',
            'status' => 'Статус',
            'sort' => 'Вес для сортировки',
            'dt_end'=>'Дата окончания скидки',
            'create_time' => 'Дата создания',
            'update_time' => 'Дата последнего редактирования',
        );
    }


    public function behaviors()
    {
        return CMap::mergeArray(parent::behaviors(), array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'create_time',
                'updateAttribute' => 'update_time',
                'setUpdateOnCreate' => true,
            ),
        ));
    }

    public function search()
    {
        $criteria=new CDbCriteria;
        $criteria->compare('id',$this->id);
        $criteria->compare('hash_key',$this->hash_key,true);
        $criteria->compare('status',$this->status);
        $criteria->compare('sort',$this->sort);
        $criteria->compare('create_time',$this->create_time,true);
        $criteria->compare('update_time',$this->update_time,true);
        $criteria->order = 'sort';
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}
